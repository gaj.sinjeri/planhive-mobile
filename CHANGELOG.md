# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.0.1+14
### Added
- Albums page with previews from each non-empty event album

### Changed
- Empty event threads are now hidden in the chats page
- Events page now has three tabs for undecided, upcoming and past events
- Chats page now sorts chats from most to least recent message
- Contact notification clicks now open contact details page, refresh pages that display contacts

## 0.0.1+9
### Changed
- Chats page view has been updated
- Create chat / group chat has been updated

### Fixed
- Fix bug when sending a contact request
- Fix bug when choosing map suggestion

## 0.0.1+8
### Added
- Search for contacts by username
- Contact details page
- Send contact requests
- Response to contact requests (accept/decline)
- Remove contacts
- Contact request management page
- App content refreshes with notifications
- Username is now supported and displayed

### Changed
- Place search page has message if location permission not granted
- Event info button is on every page now
- User tiles on event info page now go to contact info page when 'View Profile' clicked
- Account page appearance has been refreshed to new style
- Clicking on user avatar in participation details modal now opens contact info page
- Chat messages are now clustered by sender

### Fixed
- Photos page can be refreshed now
- Cancelled activities and suggestions are not shown on event tile, only confirmed

## 0.0.1+7
### Added
- Cupertino tab bar
- Country code picker for phone numbers
- Activity fields: title, description, note and location (lat, lng, title, address)
- Google maps Place Autocomplete API
- Page to search and pick a place using a text query
- Page to pick a place on the map
- Location service
- Event info page
- Edit event page
- Member view on event info page
- Admin management for events - add/remove admins
- Leave event functionality
- Cancel event functionality
- Added event has been canceled caption to event center page

### Changed
- Bottom navigation bar now changes page using a PageView instead of the navigator
- Main event page is now a full screen dialog on iOS (i.e. swipe to go back is disabled)
- Text input fields on add event page have text input actions now
- Main event page is now a full screen dialog on iOS (i.e. swipe to go back is disabled).
- Participation widget is now two horizontal tiles
- Invite-to-event button moved to participation widget
- Timeline widget redesign
- Add activity button now asks if activity / suggestion
- Increase user avatar size on event tile
- Redesign invite-to-event page
- Redesigned participant-details page
- Refresh thread view appearance
- Creating an event navigates to event page
- Changes to photos view appearance
- Use new PageScaffold for crop page

### Fixed
- Event creation executed multiple times
- Discard activity/event
- Notifications fetch missing info from server
