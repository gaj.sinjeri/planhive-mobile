import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/notification/node_notification_data.dart';
import 'package:test/test.dart';

void main() {
  test('Pattern matching', () {
    var nodeA = NodeId(id: "a", type: NodeType.event);
    var nodeB = NodeId(id: "b", type: NodeType.chatGroup);

    var dataA = NodeNotificationData([nodeA]);
    var dataAB = NodeNotificationData([nodeA, nodeB]);

    expect(dataA.matches([_makeNodeForNodeId(nodeA)]), true);
    expect(dataA.matches(["a"]), true);
    expect(dataA.matches(["a"], matchAll: true), true);
    expect(dataA.matches(["a", null]), false);
    expect(dataAB.matches(["a"]), true);
    expect(dataAB.matches(["a"], matchAll: true), false);
    expect(dataAB.matches(["a", Node]), true);
    expect(dataAB.matches(["a", "b"]), true);
    expect(dataAB.matches(["b", "a"]), false);
    expect(dataAB.matches([NodeType.event, Node]), true);
    expect(
        dataAB.matches([
          NodeType.event,
          [NodeType.event, NodeType.chatGroup]
        ]),
        true);
    expect(
        dataAB.matches([
          NodeType.event,
          [NodeType.event, NodeType.message]
        ]),
        false);
  });
}

Node _makeNodeForNodeId(NodeId nodeId) {
  return Node(
    nodeId: NodeId(
      id: nodeId.id,
      parentId: nodeId.parentId,
      type: nodeId.type,
    ),
    lastUpdateTs: 0,
    creatorId: "0",
  );
}
