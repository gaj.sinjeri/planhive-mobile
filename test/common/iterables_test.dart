import 'package:planhive/common/iterables.dart';
import 'package:test/test.dart';

void main() {
  test('Flatten iterable', () {
    expect(
      flatten([
        [
          [1],
          [2, 3]
        ],
        4,
        [
          [
            [
              [5],
            ],
            6,
          ],
          [
            [[]]
          ],
        ],
        [],
      ]),
      equals([1, 2, 3, 4, 5, 6]),
    );

    expect(
      flatten<int>([
        [
          [
            [1]
          ]
        ]
      ]) is Iterable<int>,
      true,
    );
  });
}
