import 'package:planhive/common/enums.dart';
import 'package:test/test.dart';

enum TestEnum {
  one,
  twoThree,
  fourFiveSix,
}

void main() {
  test('Snake case conversion', () {
    expect(EnumConverter.toSnakeCase("idDBId"), "id_db_id");
    expect(EnumConverter.toSnakeCase("DBId"), "db_id");
    expect(EnumConverter.toSnakeCase("DB"), "db");
    expect(EnumConverter.toSnakeCase("DBiCV"), "d_bi_cv");
    expect(EnumConverter.toSnakeCase("idIdId"), "id_id_id");
  });

  test('Factory method returns same instance', () {
    expect(
      identical(
        EnumConverter(TestEnum.values),
        EnumConverter(TestEnum.values),
      ),
      true,
    );

    expect(
      identical(
        EnumConverter(TestEnum.values, EnumCaseStyle.snakeAllCaps),
        EnumConverter(TestEnum.values, EnumCaseStyle.snakeAllCaps),
      ),
      true,
    );

    expect(
      identical(
        EnumConverter(TestEnum.values, EnumCaseStyle.snake),
        EnumConverter(TestEnum.values, EnumCaseStyle.snake),
      ),
      true,
    );
  });

  test('Factory method returns different instances for different params', () {
    expect(
      identical(
        EnumConverter(TestEnum.values, EnumCaseStyle.snake),
        EnumConverter(TestEnum.values, EnumCaseStyle.unchanged),
      ),
      false,
    );

    expect(
      identical(
        EnumConverter(TestEnum.values, EnumCaseStyle.snake),
        EnumConverter(TestEnum.values, EnumCaseStyle.snakeAllCaps),
      ),
      false,
    );

    expect(
      identical(
        EnumConverter(TestEnum.values, EnumCaseStyle.unchanged),
        EnumConverter(TestEnum.values, EnumCaseStyle.snakeAllCaps),
      ),
      false,
    );
  });

  test('Enum type correctly inferred', () {
    expect(EnumConverter(TestEnum.values) is EnumConverter<TestEnum>, true);
    expect(stringToEnum("one", TestEnum.values) is TestEnum, true);
  });

  test('Enum conversion using methods', () {
    expect(
      stringToEnum("one", TestEnum.values),
      TestEnum.one,
    );
    expect(
      stringToEnum("ONE", TestEnum.values, EnumCaseStyle.snakeAllCaps),
      TestEnum.one,
    );
    expect(
      stringToEnum("two_three", TestEnum.values, EnumCaseStyle.snake),
      TestEnum.twoThree,
    );

    expect(
      enumToString(TestEnum.one, TestEnum.values),
      "one",
    );
    expect(
      enumToString(TestEnum.one, TestEnum.values, EnumCaseStyle.snakeAllCaps),
      "ONE",
    );
    expect(
      enumToString(TestEnum.twoThree, TestEnum.values, EnumCaseStyle.snake),
      "two_three",
    );
  });

  test('Enum conversion using instance', () {
    var converter = EnumConverter(TestEnum.values, EnumCaseStyle.snakeAllCaps);

    expect(converter["ONE"], TestEnum.one);
    expect(converter["TWO_THREE"], TestEnum.twoThree);
    expect(converter["FOUR_FIVE_SIX"], TestEnum.fourFiveSix);

    expect(converter(TestEnum.one), "ONE");
    expect(converter(TestEnum.twoThree), "TWO_THREE");
    expect(converter(TestEnum.fourFiveSix), "FOUR_FIVE_SIX");

    converter = EnumConverter(TestEnum.values, EnumCaseStyle.snake);

    expect(converter["one"], TestEnum.one);
    expect(converter["two_three"], TestEnum.twoThree);
    expect(converter["four_five_six"], TestEnum.fourFiveSix);

    expect(converter(TestEnum.one), "one");
    expect(converter(TestEnum.twoThree), "two_three");
    expect(converter(TestEnum.fourFiveSix), "four_five_six");

    converter = EnumConverter(TestEnum.values, EnumCaseStyle.unchanged);

    expect(converter["one"], TestEnum.one);
    expect(converter["twoThree"], TestEnum.twoThree);
    expect(converter["fourFiveSix"], TestEnum.fourFiveSix);

    expect(converter(TestEnum.one), "one");
    expect(converter(TestEnum.twoThree), "twoThree");
    expect(converter(TestEnum.fourFiveSix), "fourFiveSix");
  });
}
