import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

enum TextContentType {
  name,
  namePrefix,
  givenName,
  middleName,
  familyName,
  nameSuffix,
  nickname,
  jobTitle,
  organizationName,
  location,
  fullStreetAddress,
  streetAddressLine1,
  streetAddressLine2,
  addressCity,
  addressState,
  addressCityAndState,
  sublocality,
  countryName,
  postalCode,
  telephoneNumber,
  emailAddress,
  url,
  creditCardNumber,
  username,
  password,
  newPassword, // iOS12+
  oneTimeCode // iOS12+
}

enum KeyboardType {
  /// Default type for the current input method.
  defaultType,

  /// Displays a keyboard which can enter ASCII characters
  asciiCapable,

  /// Numbers and assorted punctuation.
  numbersAndPunctuation,

  /// A type optimized for URL entry (shows . / .com prominently).
  url,

  /// A number pad with locale-appropriate digits (0-9, ۰-۹, ०-९, etc.). Suitable for PIN
  numberPad,

  /// A phone pad (1-9, *, 0, #, with letters under the numbers).
  phonePad,

  /// A type optimized for entering a person's name or phone number.
  namePhonePad,

  /// A type optimized for multiple email address entry (shows space @ . prominently).
  emailAddress,

  /// A number pad with a decimal point. iOS 4.1+.
  decimalPad,

  /// A type optimized for twitter text entry (easy access to @ #).
  twitter,

  /// A default keyboard type with URL-oriented addition (shows space . prominently).
  webSearch,
  // A number pad (0-9) that will always be ASCII digits. Falls back to KeyboardType.numberPad below iOS 10.
  asciiCapableNumberPad
}

enum InputAction {
  send,
  go,
  done,
  next,
  search,
}

enum AutoCapitalizationType {
  words,
  sentences,
  all,
}

class NativeTextInput extends StatefulWidget {
  const NativeTextInput({
    Key key,
    this.controller,
    this.validator,
    this.labelText,
    this.hintText,
    this.textInputAction,
    this.textCapitalization,
    this.textContentType,
    this.keyboardType = KeyboardType.defaultType,
    this.onChanged,
    this.onSubmitted,
    this.focusNode,
    this.textAlign = TextAlign.start,
    this.maxLines = 1,
    this.errorMaxLines = 1,
    this.underline = true,
  }) : super(key: key);

  /// Controls the text being edited.
  ///
  /// If null, this widget will create its own [TextEditingController].
  final TextEditingController controller;

  final Function validator;

  final String labelText;

  final String hintText;

  final TextInputAction textInputAction;

  final TextCapitalization textCapitalization;

  final TextContentType textContentType;

  final KeyboardType keyboardType;

  final Function onChanged;

  final ValueChanged<String> onSubmitted;

  final FocusNode focusNode;

  final TextAlign textAlign;

  final int errorMaxLines;

  final int maxLines;

  final bool underline;

  @override
  State<StatefulWidget> createState() => _NativeTextInputState();
}

class _NativeTextInputState extends State<NativeTextInput> {
  static const _baseHeight = 36.0;
  static const _rowHeight = 19.0;

  MethodChannel _channel;

  TextEditingController _controller;

  FocusNode _focusNode;

  double _height = _baseHeight;
  String _errorMsg;
  bool _editing = false;

  @override
  void initState() {
    super.initState();

    _focusNode = widget.focusNode ?? FocusNode();
    _controller = widget.controller ?? TextEditingController();

    _controller.addListener(() async {
      var result =
          await _channel.invokeMethod("setText", {"text": _controller.text ?? ""});
      _setHeight(result["numberOfLines"]);
    });
  }

  void initializeNumberOfLines() async {
    for (int i = 0; i < 20; i++) {
      await Future.delayed(Duration(milliseconds: 50));
      var result = await _channel.invokeMethod("getNumberOfLines");
      _setHeight(result["numberOfLines"]);
    }
  }

  @override
  void dispose() {
    if (widget.focusNode == null) _focusNode.dispose();
    if (widget.controller == null) _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (widget.labelText != null)
          Padding(
            padding: EdgeInsets.only(top: 8),
            child: Text(
              widget.labelText,
              style: theme.textTheme.bodyText2.copyWith(
                  color: _editing ? theme.primaryColor : theme.textTheme.caption.color),
            ),
          ),
        ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: _height,
            maxHeight: _height,
          ),
          child: Focus(
            focusNode: _focusNode,
            onFocusChange: (gainFocus) {
              if (gainFocus) {
                _channel.invokeMethod("focus");
              } else {
                _channel.invokeMethod("unfocus");
              }
            },
            child: UiKitView(
              viewType: "flutter_native_text_input",
              creationParamsCodec: const StandardMessageCodec(),
              creationParams: _buildCreationParams(),
              onPlatformViewCreated: _onPlatformViewCreated,
              gestureRecognizers: widget.maxLines > 1
                  ? <Factory<OneSequenceGestureRecognizer>>[
                      new Factory<OneSequenceGestureRecognizer>(
                        () => new EagerGestureRecognizer(),
                      ),
                    ].toSet()
                  : null,
            ),
          ),
        ),
        if (widget.underline)
          Divider(
            height: 8,
            thickness: 1,
            color: _editing ? theme.primaryColor : Colors.black,
          ),
        if (_errorMsg != null && _errorMsg.isNotEmpty) SizedBox(height: 4),
        if (_errorMsg != null && _errorMsg.isNotEmpty)
          Text(
            _errorMsg,
            style: theme.textTheme.caption.copyWith(color: Colors.red),
            maxLines: widget.errorMaxLines,
            overflow: TextOverflow.ellipsis,
          ),
      ],
    );
  }

  void _onPlatformViewCreated(int nativeViewId) async {
    _channel = MethodChannel("flutter_native_text_input$nativeViewId")
      ..setMethodCallHandler(_onMethodCall);

    initializeNumberOfLines();
  }

  Map<String, dynamic> _buildCreationParams() {
    return {
      "text": _controller.text ?? "",
      "placeholder": widget.hintText ?? "",
      "textInputAction": _convertInputAction(widget.textInputAction)?.toString(),
      "textAutoCapitalizationType":
          _convertAutoCapitalizationType(widget.textCapitalization)?.toString(),
      "textContentType": widget.textContentType?.toString(),
      "keyboardType": widget.keyboardType?.toString(),
      "textAlign": widget.textAlign.toString(),
      "multiline": widget.maxLines > 1 ? "true" : "false",
    };
  }

  InputAction _convertInputAction(TextInputAction textInputAction) {
    if (textInputAction == TextInputAction.send) {
      return InputAction.send;
    } else if (textInputAction == TextInputAction.go) {
      return InputAction.go;
    } else if (textInputAction == TextInputAction.done) {
      return InputAction.done;
    } else if (textInputAction == TextInputAction.next) {
      return InputAction.next;
    } else if (textInputAction == TextInputAction.search) {
      return InputAction.search;
    } else {
      return null;
    }
  }

  AutoCapitalizationType _convertAutoCapitalizationType(
      TextCapitalization textCapitalization) {
    if (textCapitalization == TextCapitalization.words) {
      return AutoCapitalizationType.words;
    } else if (textCapitalization == TextCapitalization.sentences) {
      return AutoCapitalizationType.sentences;
    } else if (textCapitalization == TextCapitalization.characters) {
      return AutoCapitalizationType.all;
    } else {
      return null;
    }
  }

  Future<void> _onMethodCall(MethodCall call) async {
    switch (call.method) {
      case "submitted":
        final String text = call.arguments["text"];
        _onSubmitted(text);
        return;

      case "inputValueChanged":
        final String text = call.arguments["text"];
        final numberOfLines = call.arguments["numberOfLines"] ?? 1;

        _onTextFieldChanged(text, numberOfLines);
        return;

      case "inputStarted":
        _inputStarted();
        return;

      case "inputFinished":
        final String text = call.arguments["text"];
        _inputFinished(text);
        return;
    }

    throw MissingPluginException(
        "UiTextField._onMethodCall: No handler for ${call.method}");
  }

  void _onSubmitted(String text) {
    if (widget.onSubmitted != null) {
      widget.onSubmitted(text);
    }
  }

  void _onTextFieldChanged(String text, int numberOfLines) {
    _controller.text = text;

    if (text != null) {
      widget.onChanged?.call();
    }

    if (widget.validator != null) {
      _errorMsg = widget.validator(text);
    }

    _setHeight(numberOfLines);
  }

  void _setHeight(int numberOfLines) {
    var height =
        _baseHeight + _rowHeight * (max(1, min(numberOfLines, widget.maxLines)) - 1);

    if (height != _height) {
      setState(() {
        _height = height;
      });

      _scrollIntoView();
    }
  }

  void _inputStarted() {
    _scrollIntoView();

    FocusScope.of(context).requestFocus(_focusNode);

    setState(() {
      _editing = true;
    });
  }

  /// Editing stopped for the specified text field.
  void _inputFinished(String text) {
    setState(() {
      _editing = false;
    });
  }

  static const Duration _caretAnimationDuration = Duration(milliseconds: 100);
  static const Curve _caretAnimationCurve = Curves.fastOutSlowIn;

  void _scrollIntoView() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      context.findRenderObject().showOnScreen(
            duration: _caretAnimationDuration,
            curve: _caretAnimationCurve,
          );
    });
  }
}
