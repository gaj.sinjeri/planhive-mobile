#import "NativeTextInput.h"
#import "NativeTextFieldDelegate.h"
#import "NativeTextViewDelegate.h"

@implementation NativeInputField {
    UITextView* _textView;
    UITextField* _textField;
    int64_t _viewId;
    FlutterMethodChannel* _channel;
    NativeTextViewDelegate* _textViewDelegate;
    NativeTextFieldDelegate* _textFieldDelegate;
}


- (instancetype)initWithFrame:(CGRect)frame
               viewIdentifier:(int64_t)viewId
                    arguments:(id _Nullable)args
              binaryMessenger:(NSObject<FlutterBinaryMessenger>*)messenger {

    if ([super init]) {
        NSString* channelName = [NSString stringWithFormat:@"flutter_native_text_input%lld", viewId];
        _channel = [FlutterMethodChannel methodChannelWithName:channelName binaryMessenger:messenger];
        
        _viewId = viewId;
        
        if([args[@"multiline"] isEqualToString: @"true"]) {
            [self setUpTextView:(frame) arguments:(args)];
        } else {
            [self setUpTextField:(frame) arguments:(args)];
        }
        
        __weak __typeof__(self) weakSelf = self;
        [_channel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
            [weakSelf onMethodCall:call result:result];
        }];
    }
    return self;
}

- (void)setUpTextField:(CGRect)frame arguments:(id _Nullable)args {
    _textField = [[UITextField alloc] initWithFrame:frame];
    _textField.text = args[@"placeholder"];
    _textField.textColor = UIColor.lightGrayColor;
    _textField.font = [UIFont systemFontOfSize:16];
    _textField.backgroundColor = UIColor.clearColor;
    _textField.keyboardType = [self keyboardTypeFromString:args[@"keyboardType"]];
    _textField.textAlignment = [self textAlignmentFromString:args[@"textAlign"]];
    _textField.autocapitalizationType = [self textAutoCapitalizationTypeFromString:args[@"textAutoCapitalizationType"]];
    _textField.returnKeyType = [self textInputActionFromString:args[@"textInputAction"]];
    
    if (![args[@"text"] isEqualToString:@""]) {
        _textField.text = args[@"text"];
        _textField.textColor = UIColor.blackColor;
    }
    
    if (@available(iOS 10.0, *)) {
        _textField.textContentType = [self textContentTypeFromString:args[@"textContentType"]];
    }
    
    [_textField addTarget:self
              action:@selector(textFieldDidChange:)
    forControlEvents:UIControlEventEditingChanged];
    
    _textFieldDelegate = [[NativeTextFieldDelegate alloc] initWithChannel:_channel arguments:args];
    _textField.delegate = _textFieldDelegate;
}

- (void)textFieldDidChange:(UITextField *)textField {
    textField.textColor = textField.text == 0 ? UIColor.lightTextColor : UIColor.blackColor;
    
    [_channel invokeMethod:@"inputValueChanged"
                 arguments:@{ @"text" : textField.text }];
}

- (void)setUpTextView:(CGRect)frame arguments:(id _Nullable)args {
    _textView = [[UITextView alloc] initWithFrame:frame];
    _textView.text = args[@"placeholder"];
    _textView.textColor = UIColor.lightGrayColor;
    _textView.font = [UIFont systemFontOfSize:16];
    _textView.backgroundColor = UIColor.clearColor;
    _textView.keyboardType = [self keyboardTypeFromString:args[@"keyboardType"]];
    _textView.textAlignment = [self textAlignmentFromString:args[@"textAlign"]];
    _textView.autocapitalizationType = [self textAutoCapitalizationTypeFromString:args[@"textAutoCapitalizationType"]];
    _textView.textContainer.lineBreakMode = NSLineBreakByCharWrapping;
    _textView.returnKeyType = [self textInputActionFromString:args[@"textInputAction"]];
    _textView.textContainer.lineFragmentPadding = 0;
    
    if (![args[@"text"] isEqualToString:@""]) {
        _textView.text = args[@"text"];
        _textView.textColor = UIColor.blackColor;
    }
    
    if (@available(iOS 10.0, *)) {
        _textView.textContentType = [self textContentTypeFromString:args[@"textContentType"]];
    }
    
    _textViewDelegate = [[NativeTextViewDelegate alloc] initWithChannel:_channel arguments:args];
    _textView.delegate = _textViewDelegate;
}

- (void)onMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([[call method] isEqualToString:@"unfocus"]) {
        [self onUnFocus:call result:result];
    } else if ([[call method] isEqualToString:@"focus"]) {
        [self onFocus:call result:result];
    } else if ([[call method] isEqualToString:@"setText"]) {
        [self onSetText:call result:result];
    } else if ([[call method] isEqualToString:@"getNumberOfLines"]) {
        [self onGetNumberOfLines:call result:result];
    } else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)onFocus:(FlutterMethodCall*)call result:(FlutterResult)result {
    _textView != nil ? [_textView becomeFirstResponder] : [_textField becomeFirstResponder];
    result(nil);
}

- (void)onUnFocus:(FlutterMethodCall*)call result:(FlutterResult)result {
    _textView != nil ? [_textView resignFirstResponder] : [_textField resignFirstResponder] ;
    result(nil);
}

- (void)onSetText:(FlutterMethodCall*)call result:(FlutterResult)result {
    if (_textView != nil) {
        _textView.text = call.arguments[@"text"];
    } else {
        _textField.text = call.arguments[@"text"];
    }
    
    result(@{ @"numberOfLines" : @([self getNumberOfLines]) });
}

- (void)onGetNumberOfLines:(FlutterMethodCall*)call result:(FlutterResult)result {
    result(@{ @"numberOfLines" : @([self getNumberOfLines]) });
}

- (int)getNumberOfLines {
    NSLayoutManager *layoutManager = [_textView layoutManager];
    unsigned numberOfLines, index, numberOfGlyphs =
    [layoutManager numberOfGlyphs];
    NSRange lineRange;
    for (numberOfLines = 0, index = 0; index < numberOfGlyphs; numberOfLines++){
        (void) [layoutManager lineFragmentRectForGlyphAtIndex:index
                                               effectiveRange:&lineRange];
        index = NSMaxRange(lineRange);
    }

    return numberOfLines;
}

- (UIView*)view {
    return _textView != nil ? _textView : _textField;
}

- (UIKeyboardType)keyboardTypeFromString:(NSString*)keyboardType {
    if (!keyboardType || [keyboardType isKindOfClass:[NSNull class]]) {
        return UIKeyboardTypeDefault;
    }
    
    if ([keyboardType isEqualToString:@"KeyboardType.asciiCapable"]) {
        return UIKeyboardTypeASCIICapable;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.numbersAndPunctuation"]) {
        return UIKeyboardTypeNumbersAndPunctuation;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.url"]) {
        return UIKeyboardTypeURL;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.numberPad"]) {
        return UIKeyboardTypeNumberPad;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.phonePad"]) {
        return UIKeyboardTypePhonePad;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.namePhonePad"]) {
        return UIKeyboardTypeNamePhonePad;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.emailAddress"]) {
        return UIKeyboardTypeEmailAddress;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.decimalPad"]) {
        return UIKeyboardTypeDecimalPad;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.twitter"]) {
        return UIKeyboardTypeTwitter;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.webSearch"]) {
        return UIKeyboardTypeWebSearch;
    }
    else if ([keyboardType isEqualToString:@"KeyboardType.asciiCapableNumberPad"]) {
        if (@available(iOS 10.0, *)) {
            return UIKeyboardTypeASCIICapableNumberPad;
        } else {
            return UIKeyboardTypeNumberPad;
        }
    }
    
    return UIKeyboardTypeDefault;
}

- (UITextContentType)textContentTypeFromString:(NSString*)contentType {
    if (!contentType || [contentType isKindOfClass:[NSNull class]]) {
        return nil;
    }
    
    if (@available(iOS 10.0, *)) {
        
        if ([contentType isEqualToString:@"TextContentType.username"]) {
            if (@available(iOS 11.0, *)) {
                return UITextContentTypeUsername;
            } else {
                return nil;
            }
        }
        
        if ([contentType isEqualToString:@"TextContentType.password"]) {
            if (@available(iOS 11.0, *)) {
                return UITextContentTypePassword;
            } else {
                return nil;
            }
        }
        
        if ([contentType isEqualToString:@"TextContentType.newPassword"]) {
            if (@available(iOS 12.0, *)) {
                return UITextContentTypeNewPassword;
            } else if (@available(iOS 11.0, *)) {
                return UITextContentTypePassword;
            } else {
                return nil;
            }
        }
        
        if ([contentType isEqualToString:@"TextContentType.oneTimeCode"]) {
            if (@available(iOS 12.0, *)) {
                return UITextContentTypeOneTimeCode;
            } else {
                return nil;
            }
        }
        
        NSDictionary *dict =
        @{
            @"TextContentType.name":                  UITextContentTypeName,
            @"TextContentType.namePrefix":            UITextContentTypeNamePrefix,
            @"TextContentType.givenName":             UITextContentTypeGivenName,
            @"TextContentType.middleName":            UITextContentTypeMiddleName,
            @"TextContentType.familyName":            UITextContentTypeFamilyName,
            @"TextContentType.nameSuffix":            UITextContentTypeNameSuffix,
            @"TextContentType.nickname":              UITextContentTypeNickname,
            @"TextContentType.jobTitle":              UITextContentTypeJobTitle,
            @"TextContentType.organizationName":      UITextContentTypeOrganizationName,
            @"TextContentType.location":              UITextContentTypeLocation,
            @"TextContentType.fullStreetAddress":     UITextContentTypeFullStreetAddress,
            @"TextContentType.streetAddressLine1":    UITextContentTypeStreetAddressLine1,
            @"TextContentType.streetAddressLine2":    UITextContentTypeStreetAddressLine2,
            @"TextContentType.city":                  UITextContentTypeAddressCity,
            @"TextContentType.addressState":          UITextContentTypeAddressState,
            @"TextContentType.addressCityAndState":   UITextContentTypeAddressCityAndState,
            @"TextContentType.sublocality":           UITextContentTypeSublocality,
            @"TextContentType.countryName":           UITextContentTypeCountryName,
            @"TextContentType.postalCode":            UITextContentTypePostalCode,
            @"TextContentType.telephoneNumber":       UITextContentTypeTelephoneNumber,
            @"TextContentType.emailAddress":          UITextContentTypeEmailAddress,
            @"TextContentType.url":                   UITextContentTypeURL,
            @"TextContentType.creditCardNumber":      UITextContentTypeCreditCardNumber
        };
        
        return dict[contentType];
    } else {
        return nil;
    }
}

- (NSTextAlignment)textAlignmentFromString:(NSString*)textAlignment {
    if (!textAlignment || [textAlignment isKindOfClass:[NSNull class]]) {
        return NSTextAlignmentNatural;
    }
    
    if ([textAlignment isEqualToString:@"TextAlign.left"]) {
        return NSTextAlignmentLeft;
    } else if ([textAlignment isEqualToString:@"TextAlign.right"]) {
        return NSTextAlignmentRight;
    } else if ([textAlignment isEqualToString:@"TextAlign.center"]) {
        return NSTextAlignmentCenter;
    } else if ([textAlignment isEqualToString:@"TextAlign.justify"]) {
        return NSTextAlignmentJustified;
    } else if ([textAlignment isEqualToString:@"TextAlign.end"]) {
        return ([self layoutDirection] == UIUserInterfaceLayoutDirectionLeftToRight)
        ? NSTextAlignmentRight
        : NSTextAlignmentLeft;
    }
    
    // TextAlign.start
    return NSTextAlignmentNatural;
}

- (UIReturnKeyType)textInputActionFromString:(NSString*)textInputAction {
    if (!textInputAction || [textInputAction isKindOfClass:[NSNull class]]) {
        return UIReturnKeyDefault;
    }
    
    if ([textInputAction isEqualToString:@"InputAction.send"]) {
        return UIReturnKeySend;
    } else if ([textInputAction isEqualToString:@"InputAction.go"]) {
        return UIReturnKeyGo;
    } else if ([textInputAction isEqualToString:@"InputAction.done"]) {
        return UIReturnKeyDone;
    } else if ([textInputAction isEqualToString:@"InputAction.next"]) {
        return UIReturnKeyNext;
    } else if ([textInputAction isEqualToString:@"InputAction.search"]) {
        return UIReturnKeySearch;
    }
    
    // TextAlign.start
    return UIReturnKeyDefault;
}

- (UITextAutocapitalizationType)textAutoCapitalizationTypeFromString:(NSString*)textAutoCapitalizationType{
    if (!textAutoCapitalizationType || [textAutoCapitalizationType isKindOfClass:[NSNull class]]) {
        return UITextAutocapitalizationTypeNone;
    }
    
    
    if ([textAutoCapitalizationType isEqualToString:@"AutoCapitalizationType.words"]) {
        return UITextAutocapitalizationTypeWords;
    } else if ([textAutoCapitalizationType isEqualToString:@"AutoCapitalizationType.sentences"]) {
        return UITextAutocapitalizationTypeSentences;
    } else if ([textAutoCapitalizationType isEqualToString:@"AutoCapitalizationType.all"]) {
        return UITextAutocapitalizationTypeAllCharacters;
    }
    
    return UITextAutocapitalizationTypeNone;
}

- (UIUserInterfaceLayoutDirection)layoutDirection {
    if (@available(iOS 9.0, *)) {
        if (_textView != nil) {
            return [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:_textView.semanticContentAttribute];
        } else {
            return [UIView userInterfaceLayoutDirectionForSemanticContentAttribute:_textField.semanticContentAttribute];
        }
    }
    
    return UIApplication.sharedApplication.userInterfaceLayoutDirection;
}

@end
