#import "NativeTextFieldDelegate.h"

@implementation NativeTextFieldDelegate {
    FlutterMethodChannel* _channel;
    id _Nullable _args;
}

- (instancetype)initWithChannel:(FlutterMethodChannel*)channel arguments:(id _Nullable)args {
    self = [super init];
    if (self) {
        _channel = channel;
        _args = args;
    }
    return self;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:_args[@"placeholder"]]) {
        textField.text = @"";
        textField.textColor = UIColor.blackColor;
    }
    [_channel invokeMethod:@"inputStarted"
                 arguments:nil];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length == 0) {
        textField.text = _args[@"placeholder"];
        textField.textColor = UIColor.lightGrayColor;
    }
    [_channel invokeMethod:@"inputFinished"
                 arguments:@{ @"text": textField.text }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [_channel invokeMethod:@"submitted" arguments:@{ @"text" : textField.text}];
    return NO;
}

@end
