import 'package:injectable/injectable.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user_permission.dart';
import 'package:planhive/storage/common/converters.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/queries.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:sqflite/sqflite.dart';

@lazySingleton
class PermissionStore {
  final Database _db;

  PermissionStore(Sqlite sqlite) : _db = sqlite.db;

  Future<void> addPermissions(Iterable<Permission> permissions) async {
    final batch = _db.batch();
    for (var permission in permissions) {
      if (permission.deleted == true) {
        batch.rawDelete(
            "delete from $permissionsTable "
            "where $permissionUserIdKey = ? "
            "and $permissionGroupIdKey = ?",
            [permission.userId, permission.groupId]);
      } else {
        batch.rawInsert(
            "insert or replace into $permissionsTable"
            "($permissionUserIdKey, $permissionGroupIdKey,  $permissionFlagsKey, $permissionCreationTsKey, $permissionUpdateTsKey) "
            "values(?, ?, ?, ?, ?)",
            [
              permission.userId,
              permission.groupId,
              permission.flags,
              permission.creationTs,
              permission.lastUpdateTs,
            ]);
      }
    }
    await batch.commit(noResult: true);
  }

  Future<List<UserPermission>> getContactPermissions(String groupId) async {
    final rows = await _db.rawQuery(
      "select * from $userTable join $permissionsTable "
      "on $userIdKey = $permissionUserIdKey "
      "where $userTable.$rowId != $localUserRowIdQuery and $permissionGroupIdKey = ?",
      [groupId],
    );
    return List.unmodifiable(
      rows.map(
        (row) => UserPermission(
          makeUser(row),
          makePermission(row),
        ),
      ),
    );
  }

  Future<UserPermission> getUserPermission(String groupId) async {
    final rows = await _db.rawQuery(
      "select * from $userTable join $permissionsTable "
      "on $userIdKey = $permissionUserIdKey "
      "where $userTable.$rowId = $localUserRowIdQuery and $permissionGroupIdKey = ?",
      [groupId],
    );
    if (rows.length == 0) {
      return null;
    }
    final row = rows[0];
    return UserPermission(
      makeUser(row),
      makePermission(row),
    );
  }

  Future<int> getLastUpdateTs(String groupId) async {
    const maxTsKey = "maxTs";
    var rows = await _db.rawQuery(
      "select max($permissionUpdateTsKey) as $maxTsKey from $permissionsTable "
      "where $permissionGroupIdKey = ?",
      [groupId],
    );
    return rows[0][maxTsKey] ?? 0;
  }

  Future<void> deletePermission(String userId, String permissionId) async {
    await _db.rawDelete(
        "delete from $permissionsTable "
        "where $permissionUserIdKey = ? "
        "and $permissionGroupIdKey = ?",
        [userId, permissionId]);
  }

  Future<void> deletePermissions(Iterable<String> permissionGroupIds) async {
    var batch = _db.batch();
    for (var permissionGroupId in permissionGroupIds) {
      batch.rawDelete("delete from $permissionsTable where $permissionGroupIdKey = ?",
          [permissionGroupId]);
    }
    await batch.commit(noResult: true);
  }

  Future<bool> exists(String groupId) async {
    var rows = await _db.rawQuery(
      "select $rowId from $permissionsTable where $permissionGroupIdKey = ? limit 1",
      [groupId],
    );

    return rows.isNotEmpty;
  }

  Future<List<String>> getMissing(Iterable<String> groupIds) async {
    var batch = _db.batch();

    var all = Set.of(groupIds);

    for (var groupId in all) {
      batch.rawQuery(
        "select $permissionGroupIdKey from $permissionsTable where $permissionGroupIdKey = ? limit 1",
        [groupId],
      );
    }

    var results = await batch.commit();
    all.removeAll(
      results
          .cast<List>()
          .where((e) => e.isNotEmpty)
          .map((e) => e.first[permissionGroupIdKey]),
    );
    return List.unmodifiable(all);
  }
}
