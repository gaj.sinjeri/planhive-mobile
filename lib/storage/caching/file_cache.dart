import 'dart:developer';
import 'dart:io';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class CacheEntry {
  final Uint8List content;
  final String tag;

  CacheEntry(this.content, this.tag);
}

@preResolve
@singleton
class FileCacheStore {
  final Database _db;
  final Directory _cacheDir;

  static const _maxCacheBytes = 200 * 1024 * 1024;
  static const _rowIdKey = "rowid";

  static const _filesTable = "cache";
  static const _nameKey = "name";
  static const _tagKey = "tag";
  static const _sizeKey = "size";
  static const _lastAccessTsKey = "lastAccessTs";

  static const _metaTable = "cacheMeta";
  static const _totalBytesKey = "totalBytes";

  FileCacheStore(this._db, this._cacheDir);

  @factoryMethod
  static Future<FileCacheStore> factory() async {
    var tempDir = await getTemporaryDirectory();
    var cachePath = tempDir.path + "file_cache" + Platform.pathSeparator;
    var cacheDir = Directory.fromUri(Uri.directory(cachePath));
    await cacheDir.create();

    var db = await openDatabase(
      "file_cache.db",
      version: 1,
      singleInstance: true,
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
    );

    return FileCacheStore(
      db,
      cacheDir,
    );
  }

  static Future<void> _onCreate(Database db, int version) async {
    final batch = db.batch();
    batch.execute(
      "create table $_filesTable("
      "$_nameKey text,"
      "$_tagKey text,"
      "$_sizeKey int,"
      "$_lastAccessTsKey int)",
    );
    batch.execute("create unique index cache_id on $_filesTable($_nameKey)");
    batch.execute("create index cache_ts on $_filesTable($_lastAccessTsKey)");

    batch.execute("create table $_metaTable($_totalBytesKey int)");
    batch.rawInsert("insert into $_metaTable($_totalBytesKey) values(0)");
    await batch.commit(noResult: true);
  }

  static Future<void> _onUpgrade(
    Database db,
    int oldVersion,
    int newVersion,
  ) async {}

  Future<CacheEntry> get(String name) async {
    try {
      var rows = await _db.rawQuery(
        "select rowid, $_tagKey from $_filesTable "
        "where $_nameKey = '$name'",
      );

      assert(rows.length <= 1);

      if (rows.length == 0) {
        assert(
          !await _getFile(name).exists(),
          "File entry not in DB, but file exists on disk",
        );
        return null;
      }

      int rowId = rows[0][_rowIdKey];
      String tag = rows[0][_tagKey];

      // write the current timestamp in the file's row
      // the operation is not awaited intentionally to reduce lag
      final now = DateTime.now().millisecondsSinceEpoch;
      final tsUpdate = _db.execute(
        "update $_filesTable set $_lastAccessTsKey = $now "
        "where $_rowIdKey = $rowId",
      );
      tsUpdate.catchError((e) => log(e.toString()));

      var content = await _getFile(name).readAsBytes();

      return CacheEntry(content, tag);
    } on FileSystemException {
      if (!await _getFile(name).exists()) {
        log("File entry '$name' found in database, but file does not exist");
      }
      return null;
    }
  }

  Future<void> put(String name, String tag, Uint8List content) async {
    try {
      await _put(name, tag, content);
    } catch (e, s) {
      log(
        "Error putting file '$name' with tag '$tag' in the cache",
        error: e,
        stackTrace: s,
      );
    }
  }

  Future<void> _put(String name, String tag, Uint8List content) async {
    log("The app requested to add file '$name' with tag '$tag' to the cache");

    await _db.transaction((txn) async {
      final now = DateTime.now().millisecondsSinceEpoch;

      var rows = await txn.rawQuery(
        "select $_rowIdKey from $_filesTable where $_nameKey = '$name'",
      );

      assert(rows.length <= 1);

      if (rows.length == 1) {
        // file already exists in the DB
        // let's overwrite it and update tag and timestamp in the DB
        await _getFile(name).writeAsBytes(content);

        var updatedRows = await txn.rawUpdate(
          "update $_filesTable "
          "set $_lastAccessTsKey = $now, $_tagKey = '$tag' "
          "where $_rowIdKey = '${rows[0][_rowIdKey]}'",
        );
        assert(updatedRows == 1);

        log("File '$name' was updated with new tag '$tag'");
      } else {
        // file does not yet exist
        // let's make sure there is enough space in the cache
        // then insert it in the DB and write it on storage
        await _ensureSpace(content.length, txn);

        txn.execute(
          "insert into $_filesTable"
          "($_nameKey, $_tagKey, $_sizeKey, $_lastAccessTsKey) "
          "values('$name', '$tag', ${content.length}, $now)",
        );

        await _getFile(name).writeAsBytes(content);

        log("File '$name' with tag '$tag' was added to the cache");
      }
    });
  }

  Future<void> _ensureSpace(int writeSize, Transaction txn) async {
    var meta = await txn.rawQuery("select * from $_metaTable");
    assert(meta.length == 1);

    int currentCacheSize = meta[0][_totalBytesKey];
    int cacheSize = currentCacheSize + writeSize;
    int bytesToRemove = math.max(0, cacheSize - _maxCacheBytes);

    log(
      "Cache has size $currentCacheSize. "
      "Trying to write $writeSize bytes, "
      "$bytesToRemove bytes need be removed",
    );

    while (bytesToRemove > 0) {
      var row = await txn.rawQuery(
        "select $_rowIdKey, * from $_filesTable "
        "order by $_lastAccessTsKey asc limit 1",
      );

      assert(row.length == 1);

      String name = row[0][_nameKey];
      int size = row[0][_sizeKey];
      int rowId = row[0][_rowIdKey];

      var file = _getFile(name);
      if (await file.exists()) {
        await file.delete();
      }

      await txn.execute(
        "delete from $_filesTable where $_rowIdKey = $rowId",
      );

      bytesToRemove -= size;
      cacheSize -= size;

      log(
        "Deleted file '$name' with size $size. "
        "$bytesToRemove bytes left to remove.",
      );
    }

    txn.execute("update $_metaTable set $_totalBytesKey = $cacheSize");
  }

  File _getFile(String fileName) {
    return File.fromUri(Uri.file(_cacheDir.path + fileName));
  }

  void delete(String name) async {
    try {
      await _delete(name);
    } catch (e, s) {
      log(
        "Error deleting file '$name' from the cache",
        error: e,
        stackTrace: s,
      );
    }
  }

  Future<void> _delete(String name) async {
    var file = _getFile(name);
    if (await file.exists()) {
      await file.delete();
    }

    await _db.rawDelete("delete from $_filesTable where $_nameKey = ?", [name]);
  }

  Future<void> deleteAllFiles() async {
    final batch = _db.batch();

    batch.execute(
      "delete from $_filesTable",
    );
    batch.rawUpdate("update $_metaTable set $_totalBytesKey = 0");

    await batch.commit(noResult: true);

    await _cacheDir.delete(recursive: true);
    await _cacheDir.create();
  }
}
