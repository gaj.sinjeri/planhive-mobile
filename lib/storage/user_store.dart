import 'package:injectable/injectable.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/contact_user.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/storage/common/converters.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/queries.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class UserStore {
  final Database _db;

  UserStore(Sqlite sqlite) : _db = sqlite.db;

  Future<User> get localUser async {
    var result = await _db
        .rawQuery("select * from $userTable where $userIdKey = $localUserIdQuery");
    return result.isEmpty ? null : makeUser(result.first);
  }

  Future<User> updateLocalUser({
    String username,
    String name,
    String email,
    int lastUpdateTs,
    int lastAvatarUpdateTs,
  }) async {
    var batch = _db.batch();

    var values = new Map<String, dynamic>();

    if (username != null) {
      values[userUsernameKey] = username;
    }
    if (name != null) {
      values[userNameKey] = name;
    }
    if (email != null) {
      values[userEmailKey] = email;
    }
    if (lastUpdateTs != null) {
      values[userLastUpdateTsKey] = lastUpdateTs;
    }
    if (lastAvatarUpdateTs != null) {
      values[userLastAvatarUpdateTsKey] = lastAvatarUpdateTs;
    }

    batch.update(userTable, values, where: "$userIdKey = $localUserIdQuery");
    batch.rawQuery("select * from $userTable where $userIdKey = $localUserIdQuery");

    var result = await batch.commit();

    return makeUser(result[1][0]);
  }

  Future<void> addUsers(Iterable<User> users) async {
    var batch = _db.batch();
    for (var user in users) {
      _insertUserQuery(batch, user);
    }
    await batch.commit(noResult: true);
  }

  void _insertUserQuery(Batch batch, User user) {
    batch.rawInsert(
      "insert or replace into $userTable"
      "($userIdKey, $userUsernameKey, $userNameKey, $userEmailKey, $userLastUpdateTsKey, $userLastAvatarUpdateTsKey) "
      "values(?, ?, ?, ?, ?, ?)",
      [
        user.id,
        user.username,
        user.name,
        user.email,
        user.lastUpdateTs,
        user.lastAvatarUpdateTs,
      ],
    );
  }

  Future<List<User>> getConnectedUsers() async {
    var rows = await _db.rawQuery(
        "select * "
        "from $userTable "
        "join $contactTable "
        "on $userIdKey = $contactIdKey "
        "where $contactStateKey = ?",
        [ContactState.connected.index]);

    return List.unmodifiable(rows.map(makeUser));
  }

  Future<List<ContactUser>> getAllContactUsers() async {
    var rows = await _db.rawQuery("select * "
        "from $userTable "
        "join $contactTable "
        "on $userIdKey = $contactIdKey");

    return List.unmodifiable(
      rows.map(
        (row) => ContactUser(
          contact: makeContact(row),
          user: makeUser(row),
        ),
      ),
    );
  }

  Future<List<User>> getUsers(Iterable<String> userIds) async {
    if (userIds.isEmpty) {
      return const [];
    }

    var ids = userIds.toList();

    var rows = await _db.rawQuery(
      "select * from $userTable "
      "where $userIdKey "
      "in (${('?' * (ids.length)).split('').join(', ')})",
      ids.toList(),
    );

    return List.unmodifiable(rows.map(makeUser));
  }
}
