import 'dart:math';

import 'package:injectable/injectable.dart';
import 'package:planhive/common/iterables.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_node.dart';
import 'package:planhive/data/user_reaction.dart';
import 'package:planhive/storage/common/converters.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/queries.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:planhive/storage/update_management_store.dart';
import 'package:sqflite/sqflite.dart';

@lazySingleton
class NodeStore {
  final Database _db;
  final UpdateManagementStore _updateManagementStore;

  NodeStore(Sqlite sqlite, this._updateManagementStore) : _db = sqlite.db;

  Future<void> addNodes(Iterable<Node> nodes) async {
    var batch = _db.batch();
    for (var node in nodes) {
      if (node.deleted == true) {
        batch.rawDelete("delete from $nodeTable where $nodeIdKey = ?", [node.id]);
      } else {
        batch.rawInsert(
          "with _existing(_unread, _ts) as ("
          "select $nodeUnreadKey, $nodeLastUpdateTsKey from $nodeTable where $nodeIdKey = ?"
          ") insert or replace into $nodeTable("
          "$nodeIdKey, $nodeParentIdKey, $nodePermissionGroupKey, $nodeCreatorIdKey, $nodeTypeKey, "
          "$nodeCreationTsKey, $nodeLastUpdateTsKey, $nodeDataKey, $nodeUnreadKey, $nodeLocalOnlyKey"
          ") values(?, ?, ?, ?, ?, ?, ?, ?, "
          // set the unread flag if the node doesn't exist or has a smaller timestamp
          "case when not exists(select * from _existing where _ts >= ?) then ? else (select _unread from _existing) end"
          ", ?)",
          [
            node.id,
            node.id,
            node.parentId,
            node.permissionGroupId,
            node.creatorId,
            node.type.index,
            node.creationTs,
            node.lastUpdateTs,
            node.jsonData,
            node.lastUpdateTs,
            node.lastUpdateTs > _updateManagementStore.updateStartTs,
            node.localOnly,
          ],
        );
      }
    }
    await batch.commit(noResult: true);
  }

  /// Returns a map from a [ParticipantStatus] to a list of [User]s.
  /// A null key is used for users without a set [ParticipantStatus].
  Future<Map<ParticipantStatus, List<User>>> getParticipants(
    Node<ParticipationData> node,
  ) async {
    var rows = await _db.rawQuery(
      "select * from "
      // use a LEFT OUTER JOIN to keep all values from the permission table
      "$permissionsTable left outer join $nodeTable on $nodeCreatorIdKey = $permissionUserIdKey and $nodeTypeKey = ? and $nodeParentIdKey = ? "
      "join $userTable on $userIdKey = $permissionUserIdKey "
      "where $permissionGroupIdKey = ? order by $userNameKey",
      [NodeType.participant.index, node.id, node.permissionGroupId],
    );

    var ret = Map.fromEntries(
      ParticipantStatus.values.map((v) => MapEntry(v, <User>[])),
    );

    // users that haven't set their participation are grouped under key 'null'
    ret[null] = [];

    for (var row in rows) {
      var json = row[nodeDataKey];
      var data = json != null ? NodeData.parse<ParticipantData>(json) : null;
      ret[data?.status].add(makeUser(row));
    }

    return ret;
  }

  Future<ParticipantStatus> getUserParticipation(Node<ParticipationData> node) async {
    var rows = await _db.rawQuery(
      "select $nodeDataKey from $nodeTable "
      "where $nodeCreatorIdKey = $localUserIdQuery and $nodeTypeKey = ? and $nodeParentIdKey = ?",
      [NodeType.participant.index, node.id],
    );

    if (rows.length == 0) {
      return null;
    }

    return NodeData.parse<ParticipantData>(rows[0][nodeDataKey]).status;
  }

  Future<List<Node<T>>> getChildrenOfType<T extends NodeData>(
    String nodeId, [
    int limit = -1,
  ]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable "
      "where $nodeParentIdKey = ? and $nodeTypeKey = ? "
      "order by $nodeLastUpdateTsKey desc limit ?",
      [nodeId, NodeData.getNodeType<T>().index, limit],
    );
    return List.unmodifiable(rows.map((row) => makeNode<T>(row)));
  }

  Future<Node<T>> getFirstChildOfType<T extends NodeData>(String nodeId) async {
    var rows = await getChildrenOfType<T>(nodeId, 1);
    return rows.isEmpty ? null : rows.first;
  }

  Future<List<UserNode<T>>> getChildrenOfTypeWithUser<T extends NodeData>(
    Node node, [
    int limit = -1,
  ]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable join $userTable on $userIdKey = $nodeCreatorIdKey "
      "where $nodeParentIdKey = ? and $nodeTypeKey = ? "
      "order by $nodeLastUpdateTsKey desc limit ?",
      [node.id, NodeData.getNodeType<T>().index, limit],
    );
    return List.unmodifiable(rows.map(
      (row) => UserNode<T>(
        makeUser(row),
        makeNode<T>(row),
      ),
    ));
  }

  Future<List<Node>> getChildren(Node node, [int limit = -1]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable where $nodeParentIdKey = ? "
      "order by $nodeLastUpdateTsKey desc limit ?",
      [node.id, limit],
    );
    return List.unmodifiable(rows.map(makeNode));
  }

  Future<int> getMaxChildTs(String nodeId) async {
    const maxTs = "maxTs";
    var rows = await _db.rawQuery(
      "select max($nodeLastUpdateTsKey) as $maxTs from $nodeTable "
      "where $nodeParentIdKey = ?",
      [nodeId],
    );
    return rows[0][maxTs] ?? 0;
  }

  Future<int> getMaxTs() async {
    const maxTs = "maxTs";
    var batch = _db.batch();

    for (var type in NodeType.values) {
      batch.rawQuery(
        "select max($nodeLastUpdateTsKey) as $maxTs from $nodeTable "
        "where $nodeTypeKey = ?",
        [type.index],
      );
    }
    var rows = await batch.commit();
    return rows.map<int>((row) => row[0][maxTs] ?? 0).reduce(max);
  }

  Future<List<UserReaction>> getReactions(Node<ActivityData> node) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable join $userTable on $nodeCreatorIdKey = $userIdKey "
      "where $nodeParentIdKey = ? and $nodeIdKey like ? and $nodeTypeKey = ?",
      [node.parentId, "${node.id}%", NodeType.activityReaction.index],
    );

    return List.unmodifiable(rows.map(
      (row) => UserReaction(
          makeUser(row), NodeData.parse<ActivityReactionData>(row[nodeDataKey]).reaction),
    ));
  }

  Future<Node> getParent(String nodeId) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable where $nodeIdKey = "
      "(select $nodeParentIdKey from $nodeTable where $nodeIdKey = ?)",
      [nodeId],
    );
    return rows.length == 0 ? null : makeNode(rows.first);
  }

  Future<Node<T>> getNode<T extends NodeData>(String nodeId) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable where $nodeIdKey = ?",
      [nodeId],
    );
    return rows.length == 0 ? null : makeNode<T>(rows.first);
  }

  Future<List<Node<T>>> getNodes<T extends NodeData>([int limit = -1]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable where $nodeTypeKey = ? "
      "order by $nodeLastUpdateTsKey desc limit ?",
      [NodeData.getNodeType<T>().index, limit],
    );
    return List.unmodifiable(rows.map((row) => makeNode<T>(row)));
  }

  Future<List<Node<T>>> getNonEmptyNodes<T extends NodeData>([int limit = -1]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable as t where "
      "$nodeTypeKey = ? and $nodeLastUpdateTsKey != $nodeCreationTsKey and "
      "exists(select * from $nodeTable where $nodeIdKey = t.$nodeParentIdKey) "
      "order by $nodeLastUpdateTsKey desc limit ?",
      [NodeData.getNodeType<T>().index, limit],
    );
    return List.unmodifiable(rows.map((row) => makeNode<T>(row)));
  }

  /// Returns the nodes sorted by last update timestamp, except for the ones whose
  /// parent's type is in [parentTypes], which are only returned if they have children.
  Future<List<Node<T>>> getNonEmptyForParentTypes<T extends NodeData>(
    List<NodeType> parentTypes, [
    int limit = -1,
  ]) async {
    var rows = await _db.rawQuery(
      "select * from $nodeTable as t where t.$nodeTypeKey = ? and "
      "("
      "not exists ("
      "select * from $nodeTable where "
      "$nodeIdKey = t.$nodeParentIdKey and "
      "$nodeTypeKey in (${parentTypes.map((_) => "?").join(", ")})"
      ") or "
      "t.$nodeLastUpdateTsKey != t.$nodeCreationTsKey "
      ") and "
      "exists(select * from $nodeTable where $nodeIdKey = t.$nodeParentIdKey) "
      "order by t.$nodeLastUpdateTsKey desc limit ?",
      flatten([
        NodeData.getNodeType<T>().index,
        parentTypes.map((e) => e.index),
        limit,
      ]).toList(),
    );
    return List.unmodifiable(rows.map((row) => makeNode<T>(row)));
  }

  Future<List<NodeId>> getMissing(List<NodeId> nodes) async {
    var batch = _db.batch();

    for (var node in nodes) {
      batch.rawQuery(
        "select $rowId from $nodeTable where $nodeIdKey = ?",
        [node.id],
      );
    }

    var missing = [];
    int counter = 0;

    for (var rows in await batch.commit()) {
      if (rows.isEmpty) {
        missing.add(nodes[counter]);
      }
      counter++;
    }

    return List.unmodifiable(missing);
  }

  Future<void> deleteNode(String nodeId) async {
    await _db.rawDelete(
        "delete from $nodeTable "
        "where $nodeIdKey = ?",
        [nodeId]);
  }

  Future<void> deleteNodes(Iterable<Node> nodes) async {
    var batch = _db.batch();
    for (var node in nodes) {
      batch.rawDelete(
          "delete from $nodeTable "
          "where $nodeIdKey = ?",
          [node.id]);
    }
    await batch.commit(noResult: true);
  }

  Future<void> deleteChildNodes(String parentNodeId) async {
    await _db.rawDelete(
        "delete from $nodeTable "
        "where $nodeParentIdKey = ?",
        [parentNodeId]);
  }

  Future<void> deleteNodesForPermissionGroupIds(
      Iterable<String> permissionGroupIds) async {
    if (permissionGroupIds.isNotEmpty) {
      var batch = _db.batch();
      for (var permissionGroupId in permissionGroupIds) {
        batch.rawDelete(
            "delete from $nodeTable "
            "where $nodePermissionGroupKey = ?",
            [permissionGroupId]);
      }
      await batch.commit(noResult: true);
    }
  }

  Future<int> countUnread(Iterable<NodeType> nodeTypes) async {
    const countKey = "unreadCount";

    assert(nodeTypes != null && nodeTypes.isNotEmpty);

    var rows = await _db.rawQuery(
      "select count(*) as $countKey from $nodeTable where "
      "$nodeUnreadKey = 1 and "
      "$nodeTypeKey in (${nodeTypes.map((_) => "?").join(", ")})",
      nodeTypes.map((e) => e.index).toList(),
    );

    return rows.first[countKey];
  }

  Future<Set<String>> getUnreadRoots(Iterable<NodeType> nodeTypes) async {
    assert(nodeTypes != null && nodeTypes.isNotEmpty);

    var rows = await _db.rawQuery(
      "with recursive n(_id, _parent) as ("
      "select $nodeIdKey, $nodeParentIdKey from $nodeTable where "
      "$nodeUnreadKey = 1 and "
      "$nodeTypeKey in (${nodeTypes.map((_) => "?").join(", ")}) "
      "union all "
      "select $nodeIdKey, $nodeParentIdKey from $nodeTable join n on $nodeIdKey = n._parent"
      ") select n._id from n where n._parent is null",
      nodeTypes.map((e) => e.index).toList(),
    );

    return Set<String>.from(rows.map((e) => e["_id"]));
  }

  Future<int> countUnreadChildren(String parentId, Iterable<NodeType> nodeTypes) async {
    const countKey = "unreadCount";

    assert(nodeTypes != null && nodeTypes.isNotEmpty);

    var rows = await _db.rawQuery(
      "select count(*) as $countKey from $nodeTable where "
      "$nodeUnreadKey = 1 and "
      "$_isChildOfExpression and "
      "$nodeTypeKey in (${nodeTypes.map((_) => "?").join(", ")})",
      [parentId]..addAll(nodeTypes.map((e) => e.index)),
    );

    return rows.first[countKey];
  }

  Future<void> markReadWithChildren(String parentId) async {
    await _db.rawUpdate(
      "update $nodeTable set $nodeUnreadKey = 0 where $nodeIdKey = ? or $_isChildOfExpression",
      [parentId, parentId],
    );
  }

  Future<void> markRead(Iterable<String> nodeIds) async {
    var batch = _db.batch();

    for (var nodeId in nodeIds) {
      batch.rawUpdate(
        "update $nodeTable set $nodeUnreadKey = 0 where $nodeIdKey = ?",
        [nodeId],
      );
    }

    await batch.commit(noResult: true);
  }

  static const _isChildOfExpression = "exists(with recursive parent(p) as("
      "select $nodeParentIdKey "
      "union all "
      "select t.$nodeParentIdKey from parent JOIN $nodeTable as t on parent.p = t.$nodeIdKey"
      ") select * from parent where parent.p = ?)";
}
