import 'package:injectable/injectable.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/storage/common/converters.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:sqflite/sqflite.dart';

@singleton
class ContactStore {
  final Database _db;

  ContactStore(Sqlite sqlite) : _db = sqlite.db;

  Future<void> addContacts(Iterable<Contact> contacts) async {
    var batch = _db.batch();
    for (var contact in contacts) {
      batch.rawInsert(
        "insert or replace into $contactTable"
        "($contactIdKey, $contactStateKey) "
        "values(?, ?)",
        [
          contact.contactId,
          contact.contactState.index,
        ],
      );
    }
    await batch.commit(noResult: true);
  }

  Future<Contact> getContact(String contactId) async {
    var rows = await _db.rawQuery(
      "select * "
      "from $contactTable "
      "where $contactIdKey = ?",
      [contactId],
    );

    return rows.isNotEmpty ? makeContact(rows.first) : null;
  }
}
