import 'package:injectable/injectable.dart';
import 'package:planhive/api/data/verification_result.dart';
import 'package:planhive/common/event.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:sqflite/sqflite.dart';

@singleton
@preResolve
class AccountStore {
  static const _logger = Logger(AccountStore);

  final Sqlite _sqlite;
  var _values = <String, dynamic>{};

  final _accountStateChanged = EventSource<AccountState>();

  Event<AccountState> get onAccountStateChanged => _accountStateChanged.event;

  AccountStore._(this._sqlite) {
    _sqlite.addResetListener((txn) async {
      _logger.debug("Resetting $AccountStore");
      _loadData(txn);
    });

    _sqlite.addResetListener(_loadData);
  }

  @factoryMethod
  static Future<AccountStore> create(Sqlite sqlite) async {
    var instance = AccountStore._(sqlite);
    await sqlite.db.transaction(instance._loadData);
    return instance;
  }

  Future<void> _loadData(DatabaseExecutor db) async {
    var rows = await db.rawQuery("select * from $accountTable");

    if (rows.isNotEmpty) {
      _values = Map.from(rows[0]);
    } else {
      await db.rawInsert(
        "insert into $accountTable($accountStateKey) values(?)",
        [AccountState.Initial.index],
      );
      _values = {accountStateKey: AccountState.Initial.index};
    }
  }

  Future<void> _set(Map<String, dynamic> values) async {
    await _sqlite.db.update(accountTable, values, where: "$rowId = 1");

    var accountStateChanged =
        values.containsKey(accountStateKey) && values[accountStateKey] != accountState;

    _values.addAll(values);

    if (accountStateChanged) {
      await _accountStateChanged(accountState);
    }
  }

  AccountState get accountState {
    return AccountState.values[_values[accountStateKey]];
  }

  String get email => _values[accountEmailKey];

  Future<void> setEmail(String value) {
    assert(value != null);
    assert([
      AccountState.Initial,
      AccountState.PendingEmailVerification,
    ].contains(accountState));

    return _set({
      accountEmailKey: value,
      accountStateKey: AccountState.PendingEmailVerification.index,
    });
  }

  String get sessionId => _values[accountSessionIdKey];

  String get userId => _values[accountUserIdKey];

  String get mac => _values[accountMacKey];

  int get macCreationTs => _values[accountMacCreationTsKey];

  String get notificationToken => _values[accountNotificationToken];

  String get inviteCode => _values[accountInviteCodeKey];

  Future<void> setCodeVerificationData(VerificationResult result) {
    assert(result.mac != null);
    assert(result.macCreationTs != null);
    assert(result.userId != null);
    assert([
      AccountState.Initial,
      AccountState.PendingEmailVerification,
    ].contains(accountState));

    return _set({
      accountMacKey: result.mac,
      accountMacCreationTsKey: result.macCreationTs,
      accountUserIdKey: result.userId,
      accountStateKey: AccountState.PendingAccountCreation.index,
    });
  }

  Future<void> setUserSession(String userId, String sessionId) {
    assert([
      AccountState.Initial,
      AccountState.PendingEmailVerification,
      AccountState.PendingAccountCreation,
    ].contains(accountState));

    return _set({
      accountUserIdKey: userId,
      accountSessionIdKey: sessionId,
      accountStateKey: AccountState.Complete.index,
      // clear data only needed during registration
      accountEmailKey: null,
      accountMacKey: null,
      accountMacCreationTsKey: null,
    });
  }

  Future<void> resetRegistrationSession() {
    return _set({
      accountStateKey: AccountState.Initial.index,
      accountMacKey: null,
      accountMacCreationTsKey: null,
    });
  }

  Future<void> setNotificationToken(String token) {
    return _set({
      accountNotificationToken: token,
    });
  }

  Future<void> setInviteCode(String invite) {
    return _set({
      accountInviteCodeKey: invite,
    });
  }
}
