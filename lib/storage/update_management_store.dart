import 'package:injectable/injectable.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/update_timestamps.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:sqflite/sqflite.dart';

@singleton
@preResolve
class UpdateManagementStore {
  static const _appUpdateManagementId = "app";
  static const _logger = Logger(UpdateManagementStore);

  final Database _db;

  int updateStartTs;

  UpdateManagementStore(Sqlite sqlite) : _db = sqlite.db {
    sqlite.addResetListener((txn) async {
      _logger.debug("Resetting $UpdateManagementStore");
      _loadData(txn);
    });
  }

  @factoryMethod
  static Future<UpdateManagementStore> factory(Sqlite sqlite) async {
    var instance = UpdateManagementStore(sqlite);
    await sqlite.db.transaction(instance._loadData);
    return instance;
  }

  Future<void> _loadData(DatabaseExecutor txn) async {
    var rows = await txn.rawQuery(
      "select $updateManagementLatestTsKey from $updateManagementTable "
      "where $updateManagementIdKey = ?",
      [_appUpdateManagementId],
    );

    if (rows.isEmpty) {
      var now = DateTime.now().millisecondsSinceEpoch;
      await txn.rawInsert(
        "insert into $updateManagementTable"
        "($updateManagementIdKey, $updateManagementOldestTsKey, $updateManagementLatestTsKey) "
        "values(?, ?, ?)",
        [_appUpdateManagementId, now, now],
      );
      updateStartTs = now;
    } else {
      updateStartTs = rows.first[updateManagementLatestTsKey];
    }
  }

  Future<UpdateTimestamps> getTimestamps(String updateManagementId) async {
    var result = await _db.rawQuery(
      "select * "
      "from $updateManagementTable "
      "where $updateManagementIdKey = ?",
      [updateManagementId],
    );

    return result.isNotEmpty
        ? UpdateTimestamps(
            result.first[updateManagementOldestTsKey],
            result.first[updateManagementLatestTsKey],
          )
        : const UpdateTimestamps(null, null);
  }

  Future<void> setTimestamps(String updateManagementId, UpdateTimestamps ts) async {
    await _db.transaction((txn) async {
      var updatedCount = await txn.rawUpdate(
        "update $updateManagementTable set "
        "$updateManagementOldestTsKey = coalesce("
        "min(?, $updateManagementOldestTsKey), ?, $updateManagementOldestTsKey"
        "), "
        "$updateManagementLatestTsKey = coalesce("
        "max(?, $updateManagementLatestTsKey), ?, $updateManagementLatestTsKey"
        ") "
        "where $updateManagementIdKey = ?",
        [ts.oldest, ts.oldest, ts.latest, ts.latest, updateManagementId],
      );

      if (updatedCount == 1) {
        return;
      }

      await txn.rawInsert(
        "insert into $updateManagementTable("
        "$updateManagementIdKey, $updateManagementOldestTsKey, $updateManagementLatestTsKey"
        ") values (?, ?, ?)",
        [updateManagementId, ts.oldest, ts.latest],
      );
    });
  }

  Future<void> deleteTimestamps(List<String> updateManagementIds) async {
    var batch = _db.batch();

    for (var id in updateManagementIds) {
      batch.rawDelete(
        "delete from $updateManagementTable where $updateManagementIdKey = ?",
        [id],
      );
    }

    await batch.commit(noResult: true);
  }

  Future<int> getLatestTs(String updateManagementId) async {
    var result = await _db.rawQuery(
        "select * "
        "from $updateManagementTable "
        "where $updateManagementIdKey = ?",
        [updateManagementId]);

    return result.isNotEmpty ? result.first[updateManagementLatestTsKey] : null;
  }

  Future<void> updateLatestTs(String updateManagementId, int latestTs) async {
    await _db.rawInsert(
        "insert or replace into $updateManagementTable("
        "$updateManagementIdKey, $updateManagementLatestTsKey"
        ") values (?, ?)",
        [updateManagementId, latestTs]);
  }
}
