import 'package:planhive/storage/common/identifiers.dart';

/// Can be used as a nested query to get the [rowId] of the logged in user
const localUserRowIdQuery = "(select $rowId from $userTable "
    "where $userIdKey = (select $accountUserIdKey from $accountTable))";

const localUserIdQuery = "(select $accountUserIdKey from $accountTable)";
