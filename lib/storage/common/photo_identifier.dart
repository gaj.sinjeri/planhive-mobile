String toThumbnailId(String id) {
  return "t-$id";
}

String toMicroThumbnailId(String id) {
  return "m-$id";
}
