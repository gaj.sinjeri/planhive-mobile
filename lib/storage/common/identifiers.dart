const rowId = "rowid";

//-------------------------------------------

const accountTable = "account";

const accountStateKey = "accountState";
const accountEmailKey = "accountEmail";
const accountSessionIdKey = "accountSessionId";
const accountUserIdKey = "accountUserId";
const accountMacCreationTsKey = "accountMacCreationTs";
const accountUserNameKey = "accountUserName";
const accountMacKey = "accountMac";
const accountNotificationToken = "accountNotificationToken";
const accountInviteCodeKey = "accountInviteCode";

//-------------------------------------------

const nodeTable = "node";

const nodeIdKey = "nodeId";
const nodeParentIdKey = "nodeParentId";
const nodePermissionGroupKey = "nodePermissionGroup";
const nodeCreatorIdKey = "nodeCreatorId";
const nodeTypeKey = "nodeType";
const nodeCreationTsKey = "nodeCreationTs";
const nodeLastUpdateTsKey = "nodeLastUpdateTs";
const nodeUnreadKey = "nodeUnread";
const nodeDataKey = "nodeData";
const nodeLocalOnlyKey = "localOnly";

//-------------------------------------------

const userTable = "user";

const userIdKey = "userId";
const userUsernameKey = "userUsername";
const userNameKey = "userName";

const userEmailKey = "userEmail";
const userLastUpdateTsKey = "userLastUpdateTsKey";
const userLastAvatarUpdateTsKey = "userLastAvatarUpdateTsKey";

//-------------------------------------------

const contactTable = "contact";

const contactIdKey = "contactId";
const contactStateKey = "contactState";
const contactLastUpdateTsKey = "lastUpdateTs";

//-------------------------------------------

const permissionsTable = "permission";

const permissionUserIdKey = "permissionUserId";
const permissionGroupIdKey = "permissionGroup";
const permissionFlagsKey = "permissionFlags";
const permissionCreationTsKey = "permissionCreationTs";
const permissionUpdateTsKey = "permissionLastUpdateTs";

//-------------------------------------------

const updateManagementTable = "updateManagement";

const updateManagementIdKey = "updateManagementId";
const updateManagementOldestTsKey = "updateManagementOldestTs";
const updateManagementLatestTsKey = "updateManagementLastExecutedTs";
