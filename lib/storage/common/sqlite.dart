import 'dart:developer';

import 'package:injectable/injectable.dart';
import 'package:planhive/storage/common/identifiers.dart';
import 'package:sqflite/sqflite.dart';

typedef ResetListener = Future<void> Function(DatabaseExecutor);

@singleton
@preResolve
class Sqlite {
  final Database db;

  var _resetListeners = <ResetListener>[];

  Sqlite._(this.db);

  @factoryMethod
  static Future<Sqlite> openDB() async {
    var db = await openDatabase(
      "app.db",
      version: 3,
      singleInstance: true,
      onCreate: _create,
      onUpgrade: _upgrade,
    );

    // log all the sql statements used to create tables and indices
    db.rawQuery("select * from SQLite_master").then((info) {
      var queries = info.map((e) => e['sql']).join("\n");
      log("Opened Sqlite DB with the following structure:\n$queries");
    });

    return Sqlite._(db);
  }

  void addResetListener(ResetListener listener) {
    _resetListeners.add(listener);
  }

  void removeResetListener(ResetListener listener) {
    _resetListeners.remove(listener);
  }

  static Future<void> _create(DatabaseExecutor db, int version) async {
    final batch = db.batch();

    batch.execute(
      "create table $userTable("
      "$userIdKey text,"
      "$userUsernameKey text,"
      "$userNameKey text,"
      "$userEmailKey text,"
      "$userLastUpdateTsKey int default 0,"
      "$userLastAvatarUpdateTsKey int default 0)",
    );
    batch.execute("create index user_name on $userTable($userNameKey)");
    batch.execute("create unique index user_id on $userTable($userIdKey)");

    batch.execute(
      "create table $contactTable("
      "$contactIdKey text,"
      "$contactStateKey int)",
    );
    batch.execute("create unique index contact_id on $contactTable($contactIdKey)");
    batch.execute("create index contact_state on $contactTable($contactStateKey)");

    batch.execute(
      "create table $nodeTable("
      "$nodeIdKey text,"
      "$nodeParentIdKey text,"
      "$nodePermissionGroupKey text,"
      "$nodeCreatorIdKey text,"
      "$nodeTypeKey int,"
      "$nodeCreationTsKey int,"
      "$nodeLastUpdateTsKey int,"
      "$nodeDataKey text,"
      "$nodeUnreadKey int default 0,"
      "$nodeLocalOnlyKey int default 0)",
    );
    batch.execute(
      "create unique index node_id on $nodeTable($nodeIdKey)",
    );
    batch.execute(
      "create index node_parent on $nodeTable($nodeParentIdKey, $nodeLastUpdateTsKey)",
    );
    batch.execute(
      "create index node_typed_ts on $nodeTable($nodeTypeKey, $nodeLastUpdateTsKey)",
    );
    batch.execute(
      "create index node_unread on $nodeTable($nodeUnreadKey)",
    );

    batch.execute(
      "create table $accountTable("
      "$accountStateKey int,"
      "$accountEmailKey text,"
      "$accountSessionIdKey text,"
      "$accountUserIdKey text,"
      "$accountUserNameKey text,"
      "$accountMacKey text,"
      "$accountMacCreationTsKey int,"
      "$accountNotificationToken text,"
      "$accountInviteCodeKey text)",
    );

    batch.execute(
      "create table $permissionsTable("
      "$permissionUserIdKey text, "
      "$permissionGroupIdKey text, "
      "$permissionFlagsKey int, "
      "$permissionCreationTsKey int, "
      "$permissionUpdateTsKey int)",
    );
    batch.execute(
      "create unique index permission_qualifier on "
      "$permissionsTable($permissionGroupIdKey, $permissionUserIdKey)",
    );
    batch.execute(
      "create index permission_ts on "
      "$permissionsTable($permissionGroupIdKey, $permissionUpdateTsKey)",
    );

    // PM: This table was originally created with the "without rowId" directive, which is
    // not supported on older Android versions. I fixed this by removing that directive
    // and adding a separate unique index on the primary key. Older devices will continue
    // to have the old table schema and all should work because rowId is not used in any
    // query.
    batch.execute(
      "create table $updateManagementTable("
      "$updateManagementIdKey text, "
      "$updateManagementOldestTsKey int, "
      "$updateManagementLatestTsKey int)",
    );
    batch.execute(
      "create unique index update_management_id on "
      "$updateManagementTable($updateManagementIdKey)",
    );

    await batch.commit(noResult: true);
  }

  static Future<void> _upgrade(
    Database db,
    int oldVersion,
    int newVersion,
  ) async {
    log('Upgrading DB from v$oldVersion to v$newVersion');

    var batch = db.batch();

    if (oldVersion <= 1) {
      batch.execute(
        "alter table $nodeTable add $nodeLocalOnlyKey int default 0",
      );
    }

    if (oldVersion <= 2) {
      batch.execute(
        "alter table $accountTable add $accountInviteCodeKey text",
      );
    }

    await batch.commit(noResult: true);
  }

  Future<void> deleteAllData() async {
    await db.transaction((txn) async {
      var tables = await txn.rawQuery(
        "select name from sqlite_master where type = 'table'",
      );

      var batch = txn.batch();

      for (var row in tables) {
        batch.rawDelete("delete from ${row["name"]}");
      }

      await batch.commit(noResult: true);

      for (var listener in _resetListeners) {
        await listener(txn);
      }
    });
  }
}
