import 'package:planhive/data/contact.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/storage/common/identifiers.dart';

Contact makeContact(Map<String, dynamic> row) {
  return Contact(
    contactId: row[contactIdKey],
    contactState: ContactState.values[row[contactStateKey]],
  );
}

User makeUser(Map<String, dynamic> row) {
  return User(
    id: row[userIdKey],
    username: row[userUsernameKey],
    name: row[userNameKey],
    email: row[userEmailKey],
    lastUpdateTs: row[userLastUpdateTsKey],
    lastAvatarUpdateTs: row[userLastAvatarUpdateTsKey],
  );
}

Permission makePermission(Map<String, dynamic> row) {
  return Permission(
    userId: row[permissionUserIdKey],
    groupId: row[permissionGroupIdKey],
    flags: row[permissionFlagsKey],
    creationTs: row[permissionCreationTsKey],
    lastUpdateTs: row[permissionUpdateTsKey],
  );
}

Node<T> makeNode<T extends NodeData>(Map<String, dynamic> row) {
  return Node<T>(
    nodeId: NodeId(
      id: row[nodeIdKey],
      parentId: row[nodeParentIdKey],
      type: NodeType.values[row[nodeTypeKey]],
    ),
    permissionGroupId: row[nodePermissionGroupKey],
    creatorId: row[nodeCreatorIdKey],
    creationTs: row[nodeCreationTsKey],
    lastUpdateTs: row[nodeLastUpdateTsKey],
    jsonData: row[nodeDataKey],
    unread: row[nodeUnreadKey] == 1,
    localOnly: row[nodeLocalOnlyKey] == 1,
  );
}
