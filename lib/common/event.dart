import "dart:async";

typedef EventListener<T> = FutureOr<void> Function(T);

class Event<T> {
  final _listeners = <EventListener<T>>[];

  void addListener(EventListener<T> listener) {
    _listeners.add(listener);
  }

  void removeListener(EventListener<T> listener) {
    _listeners.remove(listener);
  }
}

class EventSource<T> {
  final event = Event<T>();

  Future<void> call(T arg) {
    return Future.forEach<EventListener<T>>(
      event._listeners,
      (listener) => listener?.call(arg),
    );
  }
}
