typedef AsyncDelegate<T> = Future<T> Function();

final _debounceCache = <dynamic, Future>{};

/// Makes sure [delegate] is only executed if not already in progress for the same
/// [debounceId]. [debounceId] will default to [delegate] if null.
/// If the [Future] returned by the last invocation of [delegate] has not completed yet,
/// the same [Future] is returned instead of a new one.
Future<T> debounce<T>(AsyncDelegate<T> delegate, {dynamic debounceId}) {
  debounceId ??= delegate;

  return _debounceCache.putIfAbsent(debounceId, () {
    return delegate()..whenComplete(() => _debounceCache.remove(debounceId));
  });
}
