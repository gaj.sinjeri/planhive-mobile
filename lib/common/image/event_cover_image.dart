import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/event_cover_image_provider.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class EventCoverImage extends StatelessWidget {
  static const placeholder = const AssetImage('images/cover_placeholder.jpg');

  final Node<EventData> event;

  EventCoverImage(this.event);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 2,
      child: event.data.hasCoverImage
          ? FadeInImage(
              fit: BoxFit.cover,
              fadeOutDuration: const Duration(milliseconds: 1),
              fadeOutCurve: Curves.linear,
              fadeInDuration: const Duration(milliseconds: 1),
              fadeInCurve: Curves.linear,
              placeholder: CachedImageProvider(
                toThumbnailId(event.id),
                event.data.lastCoverUpdateTs.toString(),
                EventCoverImageThumbnailFetcher(event.id),
              ),
              image: CachedImageProvider(
                event.id,
                event.data.lastCoverUpdateTs.toString(),
                EventCoverImageFetcher(event.id),
              ))
          : Image(
              image: placeholder,
            ),
    );
  }
}
