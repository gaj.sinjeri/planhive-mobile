import 'dart:typed_data';

import 'package:get_it/get_it.dart';
import 'package:planhive/api/chat_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';

class GroupChatCoverImageProvider extends CachedImageProvider {
  GroupChatCoverImageProvider(
    Node<GroupChatData> groupChatNode, {
    void Function() imageLoaded,
  }) : super(
          groupChatNode.id,
          groupChatNode.data.coverTs.toString(),
          GroupChatCoverImageFetcher(groupChatNode.id),
          imageLoaded: imageLoaded,
        );
}

class GroupChatCoverImageFetcher implements ImageFetcher {
  final String groupChatId;

  GroupChatCoverImageFetcher(this.groupChatId);

  @override
  Future<Uint8List> fetch() {
    return GetIt.I<ChatApi>().getGroupChatCoverImage(groupChatId);
  }
}

class GroupChatCoverImageThumbnailFetcher implements ImageFetcher {
  final String groupChatId;

  GroupChatCoverImageThumbnailFetcher(this.groupChatId);

  @override
  Future<Uint8List> fetch() {
    return GetIt.I<ChatApi>().getGroupChatCoverImageThumbnail(groupChatId);
  }
}
