import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/painting.dart';

class RawImageProvider extends ImageProvider<RawImageProvider> {
  final _RawImageStreamCompleter _completer;

  Image get image => _completer._image;

  RawImageProvider(Image image)
      : assert(image != null),
        _completer = _RawImageStreamCompleter(image);

  void dispose() => _completer.dispose();

  @override
  Future<RawImageProvider> obtainKey(ImageConfiguration configuration) {
    return SynchronousFuture(this);
  }

  @override
  ImageStreamCompleter load(RawImageProvider key, DecoderCallback decode) {
    throw UnimplementedError();
  }

  @override
  void resolveStreamForKey(
    ImageConfiguration configuration,
    ImageStream stream,
    RawImageProvider key,
    handleError,
  ) {
    stream.setCompleter(_completer);
  }
}

class _RawImageStreamCompleter extends ImageStreamCompleter {
  final Image _image;

  _RawImageStreamCompleter(Image image) : _image = image {
    setImage(ImageInfo(image: image));
  }

  void dispose() {
    if (hasListeners) {
      addOnLastListenerRemovedCallback(() => _image.dispose());
    } else {
      _image.dispose();
    }
  }
}
