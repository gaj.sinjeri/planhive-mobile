import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/photo_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/common/image/photo_thumbnail.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class PhotoOriginal extends StatelessWidget {
  final String groupNodeId;
  final Node<PhotoData> photo;

  PhotoOriginal(this.groupNodeId, this.photo);

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      fadeOutDuration: const Duration(milliseconds: 1),
      fadeOutCurve: Curves.linear,
      fadeInDuration: const Duration(milliseconds: 1),
      fadeInCurve: Curves.linear,
      fit: BoxFit.contain,
      placeholder: CachedImageProvider(
        toThumbnailId(photo.id),
        photo.lastUpdateTs.toString(),
        PhotoThumbnailFetcher(
          groupNodeId,
          photo,
        ),
      ),
      image: CachedImageProvider(
        photo.id,
        photo.lastUpdateTs.toString(),
        _PhotoOriginalFetcher(
          groupNodeId,
          photo,
        ),
      ),
    );
  }
}

class _PhotoOriginalFetcher implements ImageFetcher {
  final _photoApi = GetIt.I<PhotoApi>();

  final String _groupNodeId;
  final Node<PhotoData> _photo;

  _PhotoOriginalFetcher(this._groupNodeId, this._photo);

  @override
  Future<Uint8List> fetch() {
    return _photoApi.getPhoto(_groupNodeId, _photo.parentId, _photo.id);
  }
}
