import 'dart:math';
import 'dart:ui' as ui;

import 'package:crop/crop.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:flutter/scheduler.dart';
import 'package:planhive/common/image/image_utils.dart';
import 'package:planhive/common/image/raw_image_provider.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';

class ImageCropPage extends StatefulWidget {
  final Future<Image> imageFuture;
  final Size resolution;

  ImageCropPage(this.imageFuture, this.resolution);

  @override
  State<StatefulWidget> createState() {
    return _ImageCropState();
  }
}

class _ImageCropState extends State<ImageCropPage>
    with ViewControllerMixin, SingleTickerProviderStateMixin {
  CropController _cropController;

  Size _cropSize;
  ui.Image _image;

  double _minScale;
  double _normalizedWidth;
  double _normalizedHeight;
  int _longestSideResolution;

  Ticker _ticker;

  @override
  void initState() {
    _cropController = CropController(
      aspectRatio: widget.resolution.aspectRatio,
    );

    widget.imageFuture.then((image) {
      resolveProvider(image.image).then((image) {
        if (!mounted) {
          return;
        }

        setState(() {
          var targetSize = widget.resolution;

          var heightMatchScale =
              targetSize.width / (targetSize.height / image.height * image.width);
          var widthMatchScale =
              targetSize.height / (targetSize.width / image.width * image.height);

          _minScale = max(heightMatchScale, widthMatchScale);

          var longest = max(image.width, image.height);

          _longestSideResolution = longest;

          _normalizedWidth = min(1, 1 / heightMatchScale);
          _normalizedHeight = min(1, 1 / widthMatchScale);

          _cropController.scale = _minScale;

          _ticker = createTicker((_) => _constrainPanScale())..start();

          _image = image;
        });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _cropController.dispose();
    _ticker?.dispose();

    super.dispose();
  }

  void _constrainPanScale() {
    var scale = max(_minScale, _cropController.scale);

    if (scale != _cropController.scale) {
      _cropController.scale = scale;
    }

    if (_cropSize != null) {
      var offset = _cropController.offset;

      var maxDx = max(0.0, (_normalizedWidth * scale * 0.5 - 0.5) * _cropSize.width);
      var maxDy = max(0.0, (_normalizedHeight * scale * 0.5 - 0.5) * _cropSize.height);

      if (_cropController.rotation.round() % 180 != 0) {
        var tmp = maxDx;
        maxDx = maxDy;
        maxDy = tmp;
      }

      var clampedOffset = Offset(
        _absMin(offset.dx, maxDx),
        _absMin(offset.dy, maxDy),
      );

      if (clampedOffset != _cropController.offset) {
        _cropController.offset = clampedOffset;
      }
    }
  }

  static double _absMin(double value, double absMin) {
    assert(absMin >= 0);
    if (value > 0) {
      return min(value, absMin);
    } else {
      return max(value, -absMin);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: () => navigation.pop(),
      ),
      title: Text("Scale and Crop"),
      trailing: NavBarButton(
        text: "Done",
        materialIcon: Icons.done,
        onPressed: _crop,
      ),
      safeAreaEnabled: true,
      body: LoadingIndicator(
        loading: loading || _image == null,
        child: Column(
          children: <Widget>[
            Expanded(
              child: Crop(
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                child: LayoutBuilder(
                  builder: (context, constraints) {
                    _cropSize = constraints.biggest;

                    return _image == null
                        ? SizedBox()
                        : Image(
                            image: RawImageProvider(_image),
                            fit: BoxFit.contain,
                          );
                  },
                ),
                controller: _cropController,
              ),
            ),
            TextButton(
              onPressed: () => _cropController.rotation += 90,
              text: "Rotate",
            ),
          ],
        ),
      ),
    );
  }

  void _crop() async {
    if (_cropSize == null || _longestSideResolution == null) {
      return;
    }

    runAsync(() async {
      var desiredResolution = widget.resolution.longestSide;
      var maxResolution = _longestSideResolution / _cropController.scale;
      var pixelRatio = min(maxResolution, desiredResolution) / _cropSize.longestSide;

      var image = await _cropController.crop(pixelRatio: pixelRatio);

      logger.debug(
        "Cropped image: "
        "desired resolution = ${widget.resolution}, "
        "actual resolution = ${Size(image.width.toDouble(), image.height.toDouble())}",
      );

      NavigationService.instance.pop(image);
    }, exclusive: true);
  }
}
