import 'dart:async';
import 'dart:ui';

import 'package:flutter/painting.dart';

Future<Image> copyImage(Image image) async {
  var byteData = await image.toByteData(format: ImageByteFormat.rawRgba);
  final completer = Completer<Image>();
  decodeImageFromPixels(
    byteData.buffer.asUint8List(),
    image.width,
    image.height,
    PixelFormat.rgba8888,
    (result) => completer.complete(result),
  );
  return completer.future;
}

Future<Image> resolveProvider(ImageProvider provider) {
  var stream = provider.resolve(ImageConfiguration.empty);
  final completer = Completer<Image>.sync();
  stream.addListener(
    ImageStreamListener((info, _) async {
      completer.complete(info.image);
    }, onError: (e, s) {
      completer.completeError(e, s);
    }),
  );
  return completer.future;
}
