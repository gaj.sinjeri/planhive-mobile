import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image/image.dart' as img;
import 'package:planhive/common/image/image_crop_page.dart';
import 'package:planhive/common/image/image_utils.dart';
import 'package:planhive/common/image/raw_image_provider.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/core/photos/multi_photo_picker.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';

enum _ImageAction { change, remove, crop }

// TODO(#71): Crop resolution should be an upper boundary
/// Utility for picking and cropping an image
class ImagePickAndCrop {
  final BuildContext _context;
  final Size _cropResolution;

  ImagePickAndCrop(this._context, this._cropResolution);

  void dispose() {
    _setCroppedImage(null);
  }

  Image _image;

  RawImageProvider _provider;

  ImageProvider get imageProvider => _provider;

  Future<void> setImage(ui.Image image) async {
    _setCroppedImage(await copyImage(image));
  }

  Future<bool> pickImage(BuildContext context) async {
    var pick = _ImageAction.change;

    if (_provider != null) {
      // if the image is already set, we show a dialog asking whether the user
      // wants to choose a new one, re-crop, or just remove the current one.
      var modalData = [
        ModalActionData<_ImageAction>(
          text: "Change",
          icon: Icons.edit,
          value: _ImageAction.change,
        ),
        if (_image != null)
          ModalActionData<_ImageAction>(
            text: "Crop",
            icon: Icons.crop,
            value: _ImageAction.crop,
          ),
        ModalActionData<_ImageAction>(
          text: "Remove",
          icon: Icons.delete,
          value: _ImageAction.remove,
        ),
      ];

      pick = await showModalSheet<_ImageAction>(
        context: _context,
        title: "Change photo",
        actions: modalData,
      );
    }

    switch (pick) {
      case _ImageAction.change:
        var pickedImage = await GetIt.I<MultiPhotoPicker>().getPhotos(context, 1);
        if (pickedImage.isNotEmpty) {
          var imageFuture = () async {
            var data = await pickedImage[0].getByteData();
            return Image.memory(data.buffer.asUint8List());
          }();
          var cropped = await _crop(imageFuture);
          if (cropped != null) {
            _image = await imageFuture;
            _setCroppedImage(cropped);
          }
          break;
        }
        return false;
      case _ImageAction.crop:
        var cropped = await _crop(Future.value(_image));
        if (cropped != null) {
          _setCroppedImage(cropped);
        }
        break;
      case _ImageAction.remove:
        _image = null;
        _setCroppedImage(null);
        break;
      default:
        // dialog dismissed, no change
        return false;
    }

    return true;
  }

  Future<img.Image> toImage() async {
    if (_provider == null) {
      return null;
    }

    var data = await _provider.image.toByteData(
      format: ui.ImageByteFormat.rawRgba,
    );

    return img.Image.fromBytes(
      _provider.image.width,
      _provider.image.height,
      data.buffer.asUint32List(),
    );
  }

  Future<ui.Image> _crop(Future<Image> image) {
    return cropImage(
      context: _context,
      image: image,
      resolution: _cropResolution,
    );
  }

  void _setCroppedImage(ui.Image newImage) {
    if (_provider?.image == newImage) {
      return;
    }
    _provider?.dispose();
    _provider = newImage == null ? null : RawImageProvider(newImage);
  }
}

/// Utility method for cropping an image
Future<ui.Image> cropImage({
  @required BuildContext context,
  @required Future<Image> image,
  @required Size resolution,
}) {
  return NavigationService.instance.push<ui.Image>(ImageCropPage(image, resolution));
}
