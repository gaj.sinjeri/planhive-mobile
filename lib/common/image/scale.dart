import 'dart:math';

import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:planhive/common/constants/image_settings.dart';

Future<List<int>> scaleImage(Asset original, int maxResolution) async {
  var scaleFactor =
      min(1, maxResolution / max(original.originalHeight, original.originalWidth));
  int targetWidth = (original.originalWidth * scaleFactor).floor();
  int targetHeight = (original.originalHeight * scaleFactor).floor();

  var originalByteData = await original.getThumbByteData(
    targetWidth,
    targetHeight,
    quality: imageQuality,
  );
  return originalByteData.buffer.asUint8List();
}
