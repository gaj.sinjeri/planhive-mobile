import 'package:flutter/material.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/event_cover_image_provider.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

const _placeholder = const AssetImage('images/cover_placeholder_thumbnail.jpg');

ImageProvider<dynamic> getEventCoverImageThumbnail(Node<EventData> event) {
  return event.data.hasCoverImage
      ? CachedImageProvider(
          toThumbnailId(event.id),
          event.data.lastCoverUpdateTs.toString(),
          EventCoverImageThumbnailFetcher(event.id),
        )
      : _placeholder;
}
