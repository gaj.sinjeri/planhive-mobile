import 'dart:typed_data';

import 'package:get_it/get_it.dart';
import 'package:planhive/api/event_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';

class EventCoverImageProvider extends CachedImageProvider {
  EventCoverImageProvider(
    Node<EventData> eventNode, {
    void Function() imageLoaded,
  }) : super(
          eventNode.id,
          eventNode.data.lastCoverUpdateTs.toString(),
          EventCoverImageFetcher(eventNode.id),
          imageLoaded: imageLoaded,
        );
}

class EventCoverImageFetcher implements ImageFetcher {
  final String _eventId;

  EventCoverImageFetcher(this._eventId);

  @override
  Future<Uint8List> fetch() {
    return GetIt.I<EventApi>().getEventCoverImage(_eventId);
  }
}

class EventCoverImageThumbnailFetcher implements ImageFetcher {
  final String _eventId;

  EventCoverImageThumbnailFetcher(this._eventId);

  @override
  Future<Uint8List> fetch() {
    return GetIt.I<EventApi>().getEventCoverImageThumbnail(_eventId);
  }
}
