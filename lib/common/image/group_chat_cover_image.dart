import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/group_chat_cover_image_provider.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class GroupChatCoverImage extends StatelessWidget {
  static const placeholder = const AssetImage('images/cover_placeholder.jpg');

  final Node<GroupChatData> groupChatNode;

  GroupChatCoverImage(this.groupChatNode);

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 2,
      child: groupChatNode.data.hasCoverImage
          ? FadeInImage(
              fit: BoxFit.cover,
              fadeOutDuration: const Duration(milliseconds: 1),
              fadeOutCurve: Curves.linear,
              fadeInDuration: const Duration(milliseconds: 1),
              fadeInCurve: Curves.linear,
              placeholder: CachedImageProvider(
                toThumbnailId(groupChatNode.id),
                groupChatNode.data.coverTs.toString(),
                GroupChatCoverImageThumbnailFetcher(groupChatNode.id),
              ),
              image: CachedImageProvider(
                groupChatNode.id,
                groupChatNode.data.coverTs.toString(),
                GroupChatCoverImageFetcher(groupChatNode.id),
              ))
          : Image(
              image: placeholder,
            ),
    );
  }
}
