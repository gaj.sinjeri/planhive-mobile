import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/thread_api.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/common/image/message_photo.dart';
import 'package:planhive/data/node/message_data.dart';
import 'package:planhive/data/node/node.dart';

class MessagePhotoThumbnail extends StatelessWidget {
  static const placeholder = const AssetImage('images/white_image.jpg');

  final String threadNodeId;
  final Node<MessageData> messageNode;

  MessagePhotoThumbnail(this.threadNodeId, this.messageNode);

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      placeholder: placeholder,
      fit: BoxFit.cover,
      image: getMessagePhotoThumbnailProvider(threadNodeId, messageNode),
    );
  }
}

class MessagePhotoThumbnailFetcher implements ImageFetcher {
  final _threadApi = GetIt.I<ThreadApi>();

  final String threadNodeId;
  final Node<MessageData> messageNode;

  MessagePhotoThumbnailFetcher(this.threadNodeId, this.messageNode);

  @override
  Future<Uint8List> fetch() {
    return _threadApi.getImageThumbnail(threadNodeId, messageNode.id);
  }
}
