import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/user_avatar_key.dart';
import 'package:planhive/common/image/user_avatar_provider.dart';
import 'package:planhive/data/user.dart';

enum AvatarSize {
  fullSize,
  thumbnail,
  micro,
}

class UserAvatar extends StatefulWidget {
  final User user;
  final AvatarSize avatarSize;
  final double placeholderSize;

  UserAvatar({
    @required this.user,
    @required this.avatarSize,
    this.placeholderSize,
  }) : super(
          key: UserAvatarKey(
            userId: user.id,
            lastAvatarUpdateTs: user.lastAvatarUpdateTs,
          ),
        );

  @override
  State<StatefulWidget> createState() {
    return _UserAvatarState(user, avatarSize, placeholderSize);
  }
}

class _UserAvatarState extends State<UserAvatar> {
  final User user;
  final AvatarSize avatarSize;
  final double placeholderSize;

  bool _showPlaceholder = true;
  CachedImageProvider avatar;

  _UserAvatarState(this.user, this.avatarSize, this.placeholderSize) {
    avatar = user.hasAvatar
        ? UserAvatarProvider(
            user,
            avatarSize,
            imageLoaded: () => setState(() => _showPlaceholder = false),
          )
        : null;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: CircleAvatar(
        backgroundColor: Colors.grey[400],
        child: _showPlaceholder
            ? Center(
                child: Icon(
                Icons.person,
                color: Colors.grey[200],
                size: placeholderSize,
              ))
            : null,
        backgroundImage: avatar,
      ),
    );
  }
}
