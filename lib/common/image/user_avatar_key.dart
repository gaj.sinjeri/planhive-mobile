import 'package:flutter/material.dart';

class UserAvatarKey extends LocalKey {
  final String userId;
  final int lastAvatarUpdateTs;

  UserAvatarKey({
    @required this.userId,
    @required this.lastAvatarUpdateTs,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAvatarKey &&
          lastAvatarUpdateTs == other.lastAvatarUpdateTs &&
          userId == other.userId;

  @override
  int get hashCode => userId.hashCode ^ lastAvatarUpdateTs.hashCode;
}
