import 'dart:typed_data';

abstract class ImageFetcher {
  Future<Uint8List> fetch();
}
