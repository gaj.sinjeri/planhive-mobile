import 'package:flutter/material.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

import 'group_chat_cover_image_provider.dart';

const _placeholder = const AssetImage('images/cover_placeholder_thumbnail.jpg');

ImageProvider<dynamic> getGroupChatCoverImageThumbnail(
    Node<GroupChatData> groupChatNode) {
  return groupChatNode.data.hasCoverImage
      ? CachedImageProvider(
          toThumbnailId(groupChatNode.id),
          groupChatNode.data.coverTs.toString(),
          GroupChatCoverImageThumbnailFetcher(groupChatNode.id),
        )
      : _placeholder;
}
