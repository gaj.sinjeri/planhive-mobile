import 'dart:typed_data';

import 'package:get_it/get_it.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class UserAvatarProvider extends CachedImageProvider {
  UserAvatarProvider(
    User user,
    AvatarSize avatarSize, {
    void Function() imageLoaded,
  }) : super(
          _getFileId(user, avatarSize),
          user.lastAvatarUpdateTs.toString(),
          _AvatarPhotoFetcher(user.id, avatarSize),
          imageLoaded: imageLoaded,
        );

  static String _getFileId(User user, AvatarSize avatarSize) {
    switch (avatarSize) {
      case AvatarSize.fullSize:
        return user.id;
      case AvatarSize.thumbnail:
        return toThumbnailId(user.id);
      case AvatarSize.micro:
        return toMicroThumbnailId(user.id);
      default:
        throw Exception("Unhandled avatar size");
    }
  }
}

class _AvatarPhotoFetcher implements ImageFetcher {
  final String userId;
  final AvatarSize avatarSize;

  _AvatarPhotoFetcher(this.userId, this.avatarSize);

  @override
  Future<Uint8List> fetch() {
    switch (avatarSize) {
      case AvatarSize.fullSize:
        return GetIt.I<UserApi>().getAvatar(userId);
      case AvatarSize.thumbnail:
        return GetIt.I<UserApi>().getAvatar(userId);
      case AvatarSize.micro:
        return GetIt.I<UserApi>().getAvatarMicro(userId);
      default:
        throw Exception("Unhandled avatar size");
    }
  }
}
