import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/thread_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/common/image/message_photo_thumbnail.dart';
import 'package:planhive/data/node/message_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class MessagePhoto extends StatelessWidget {
  final String threadNodeId;
  final Node<MessageData> messageNode;

  MessagePhoto(this.threadNodeId, this.messageNode);

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      fadeOutDuration: const Duration(milliseconds: 1),
      fadeOutCurve: Curves.linear,
      fadeInDuration: const Duration(milliseconds: 1),
      fadeInCurve: Curves.linear,
      fit: BoxFit.contain,
      placeholder: getMessagePhotoThumbnailProvider(threadNodeId, messageNode),
      image: getMessagePhotoProvider(threadNodeId, messageNode),
    );
  }
}

ImageProvider getMessagePhotoProvider(
    String threadNodeId, Node<MessageData> messageNode) {
  return CachedImageProvider(
    messageNode.id,
    messageNode.lastUpdateTs.toString(),
    _MessagePhotoFetcher(
      threadNodeId,
      messageNode,
    ),
  );
}

ImageProvider getMessagePhotoThumbnailProvider(
    String threadNodeId, Node<MessageData> messageNode) {
  return CachedImageProvider(
    toThumbnailId(messageNode.id),
    messageNode.lastUpdateTs.toString(),
    MessagePhotoThumbnailFetcher(
      threadNodeId,
      messageNode,
    ),
  );
}

class _MessagePhotoFetcher implements ImageFetcher {
  final _threadApi = GetIt.I<ThreadApi>();

  final String threadNodeId;
  final Node<MessageData> messageNode;

  _MessagePhotoFetcher(this.threadNodeId, this.messageNode);

  @override
  Future<Uint8List> fetch() {
    return _threadApi.getImage(threadNodeId, messageNode.id);
  }
}
