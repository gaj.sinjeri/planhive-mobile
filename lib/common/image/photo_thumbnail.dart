import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/photo_api.dart';
import 'package:planhive/common/image/cached_image_provider.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

class PhotoThumbnail extends StatelessWidget {
  static const placeholder = const AssetImage('images/white_image.jpg');

  final String groupNodeId;
  final Node<PhotoData> photo;

  PhotoThumbnail(this.groupNodeId, this.photo);

  @override
  Widget build(BuildContext context) {
    return FadeInImage(
      placeholder: placeholder,
      fit: BoxFit.cover,
      image: CachedImageProvider(
        toThumbnailId(photo.id),
        photo.lastUpdateTs.toString(),
        PhotoThumbnailFetcher(
          groupNodeId,
          photo,
        ),
      ),
    );
  }
}

class PhotoThumbnailFetcher implements ImageFetcher {
  final _photoApi = GetIt.I<PhotoApi>();

  final String _groupNodeId;
  final Node<PhotoData> _photo;

  PhotoThumbnailFetcher(this._groupNodeId, this._photo);

  @override
  Future<Uint8List> fetch() {
    return _photoApi.getThumbnail(_groupNodeId, _photo.parentId, _photo.id);
  }
}
