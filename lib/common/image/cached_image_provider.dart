import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/common/image/image_fetcher.dart';
import 'package:planhive/storage/caching/file_cache.dart';

class CachedImageProvider extends ImageProvider<CachedImageProvider> {
  final String name;
  final String tag;
  final ImageFetcher _imageFetcher;
  final void Function() _imageLoaded;

  ImageStream _cachedImageStream;

  CachedImageProvider(
    this.name,
    this.tag,
    this._imageFetcher, {
    void Function() imageLoaded,
  })  : assert(name != null),
        assert(tag != null),
        assert(_imageFetcher != null),
        _imageLoaded = imageLoaded;

  @override
  Future<CachedImageProvider> obtainKey(ImageConfiguration configuration) {
    return SynchronousFuture(this);
  }

  @override
  ImageStream createStream(ImageConfiguration configuration) {
    if (_cachedImageStream == null) {
      _cachedImageStream = ImageStream();
      ImageStreamListener listener;
      listener = ImageStreamListener((_, synchronousCall) {
        _cachedImageStream.removeListener(listener);
        if (synchronousCall) {
          // if the callback is called immediately, it's safer to wait until
          // the end of the frame to invoke the callback, because flutter does
          // not allow certain operations while the widget tree in being built
          Future.microtask(() => _imageLoaded?.call());
        } else {
          _imageLoaded?.call();
        }
      });
      _cachedImageStream.addListener(listener);
    }
    return _cachedImageStream;
  }

  @override
  ImageStreamCompleter load(CachedImageProvider key, decode) {
    final completer = _ImageStreamCompleter();
    _loadAsync(completer);
    return completer;
  }

  void _loadAsync(_ImageStreamCompleter completer) async {
    try {
      final cache = GetIt.I<FileCacheStore>();
      var cached = await cache.get(name);

      if (cached != null) {
        completer.setImageBytes(cached.content);
      }

      if (cached == null || cached.tag != tag) {
        var bytes = await _imageFetcher.fetch();
        completer.setImageBytes(bytes);
        cache.put(name, tag, bytes);
      }
    } on ApiNotFoundException {
      return;
    }
  }

  @override
  bool operator ==(Object other) =>
      other is CachedImageProvider && name == other.name && tag == other.tag;

  @override
  int get hashCode => name.hashCode ^ tag.hashCode;

  @override
  String toString() => "$runtimeType{name: $name, tag: $tag}";
}

class _ImageStreamCompleter extends ImageStreamCompleter {
  void setImageBytes(Uint8List bytes) async {
    var codec = await instantiateImageCodec(bytes);

    assert(codec.frameCount == 1, "Animated images are not supported");

    var frame = await codec.getNextFrame();
    setImage(ImageInfo(image: frame.image));
  }
}
