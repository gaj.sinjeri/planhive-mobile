import 'package:flutter/foundation.dart';
import 'package:planhive/common/enums.dart';

enum _Level {
  trace,
  debug,
  info,
  warn,
  error,
}

final _levelEnumConverter = EnumConverter(_Level.values);

class Logger {
  final Object tag;
  static const _maxLogLength = 1000;
  static final _lineEndingPattern = RegExp(r"\r?\n", multiLine: true);

  const Logger(this.tag) : assert(tag != null);

  void _log(_Level level, String message) {
    if (kReleaseMode && level.index < _Level.info.index) {
      return;
    }
    var levelString = _levelEnumConverter(level);

    var lines = "[$levelString][$tag] $message".split(_lineEndingPattern);

    var buffer = StringBuffer();

    for (var line in lines) {
      if (buffer.length + line.length >= _maxLogLength) {
        print(buffer);
        buffer.clear();
      }

      while (line.length > _maxLogLength) {
        print(line.substring(0, _maxLogLength));
        line = line.substring(_maxLogLength);
      }

      buffer.writeln(line);
    }

    if (buffer.isNotEmpty) {
      print(buffer);
    }
  }

  /// Fine-grained debug message, typically capturing the flow through the application.
  void trace(dynamic message) {
    _log(_Level.trace, "$message");
  }

  /// General debugging event.
  void debug(dynamic message) {
    _log(_Level.debug, "$message");
  }

  /// Event for informational purposes.
  void info(dynamic message) {
    _log(_Level.info, "$message");
  }

  /// Event that might possibly lead to an error.
  void warn(dynamic message) {
    _log(_Level.warn, "$message");
  }

  /// Error in the application, possibly recoverable.
  void error(dynamic message, [dynamic exception, StackTrace stackTrace]) {
    String msg = "$message";
    if (exception != null) {
      msg += "\n$exception";
    }
    if (stackTrace != null) {
      msg += "\n$stackTrace";
    }
    _log(_Level.error, msg);
  }
}
