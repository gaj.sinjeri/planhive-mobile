import 'package:flutter/widgets.dart';

class PageViewPage extends StatelessWidget {
  final int pageIndex;
  final PageController _pageController;
  final Widget _child;

  PageViewPage(
    this.pageIndex, {
    @required PageController pageController,
    @required Widget child,
    Key key,
  })  : _pageController = pageController,
        _child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return _child;
  }

  static PageViewPage of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<PageViewPage>();
  }

  bool get isVisible => _pageController.page == pageIndex;
}
