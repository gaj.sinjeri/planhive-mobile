import 'dart:async';

/// Used to limit the rate at which a function can fire.
class Debouncer {
  bool _disposed = false;
  Timer _timer;

  final Duration duration;

  Debouncer({this.duration = const Duration(milliseconds: 100)})
      : assert(duration != null);

  /// As long as this method continues to be invoked, [callback] will not be triggered.
  /// It will be called only after the debounce function stops being called for
  /// [duration].
  void debounce(void Function() callback, [Duration duration]) {
    if (_disposed) {
      throw StateError("$Debouncer used after disposed");
    }

    _timer?.cancel();
    _timer = Timer(duration ?? this.duration, callback);
  }

  /// Cancels any pending callback and prevents any future calls to [debounce]
  void dispose() {
    _timer?.cancel();
    _disposed = true;
  }
}
