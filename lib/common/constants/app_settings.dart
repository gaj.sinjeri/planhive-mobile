const appName = "PlanHive";

const String contactUsURL = "mailto:contact@planhive.app";
const String privacyPolicyURL = "https://www.planhive.app/privacy-policy";
const String termsAndConditionsURL = "https://www.planhive.app/terms-and-conditions";
