const int groupChatCoverPhotoWidth = 2048;
const int groupChatCoverPhotoHeight = 1024;

const int groupChatCoverPhotoThumbnailResolution = 256;

// --------------------------------------------------------------------------------------

const int eventCoverPhotoWidth = 2048;
const int eventCoverPhotoHeight = 1024;

const int eventCoverPhotoThumbnailWidth = 640;
const int eventCoverPhotoThumbnailHeight = 320;

// --------------------------------------------------------------------------------------

const int messageImageMaxResolution = 2048;
const int messageImageThumbnailResolution = 1024;

// --------------------------------------------------------------------------------------

const int photoMaxResolution = 2048;
const int photoThumbnailResolution = 512;

// --------------------------------------------------------------------------------------

const int userAvatarMaxResolution = 2048;
const int userAvatarThumbnailResolution = 2048;
const int userAvatarMicroResolution = 2048;

// --------------------------------------------------------------------------------------

const int imageQuality = 75;
