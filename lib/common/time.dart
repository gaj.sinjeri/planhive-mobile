import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

bool yearMatches(DateTime dt1, DateTime dt2) {
  return dt1.year == dt2.year;
}

bool dateMatches(DateTime dt1, DateTime dt2) {
  return dt1.year == dt2.year && dt1.month == dt2.month && dt1.day == dt2.day;
}

bool weekMatches(DateTime dt1, DateTime dt2) {
  return dateMatches(
    dt1.subtract(Duration(days: dt1.weekday)),
    dt2.subtract(Duration(days: dt2.weekday)),
  );
}

bool minutesMatch(DateTime dt1, DateTime dt2) {
  return dateMatches(dt1, dt2) && dt1.hour == dt2.hour && dt1.minute == dt2.minute;
}

bool use24hFormat(BuildContext context) {
  var localizations = MaterialLocalizations.of(context);

  return !(localizations.timeOfDayFormat() == TimeOfDayFormat.h_colon_mm_space_a ||
      localizations.timeOfDayFormat() == TimeOfDayFormat.a_space_h_colon_mm);
}
