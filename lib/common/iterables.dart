Iterable<T> flatten<T>(e) => e is Iterable ? e.expand<T>((e) => flatten<T>(e)) : <T>[e];
