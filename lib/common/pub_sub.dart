import 'package:flutter/foundation.dart';
import 'package:planhive/common/logger.dart';

mixin PubSubMixin<T, V> {
  static const _logger = Logger("PubSub");

  final _topicSubs = <T, List<void Function(V)>>{};

  void subscribe(T topic, void Function(V) subCallback) {
    _topicSubs.putIfAbsent(topic, () => []).add(subCallback);
  }

  bool unsubscribe(T topic, void Function(V) subCallback) {
    return _topicSubs[topic]?.remove(subCallback) ?? false;
  }

  @protected
  void publish(T topic, V value) {
    _topicSubs[topic]?.forEach((cb) {
      try {
        cb(value);
      } catch (e, s) {
        _logger.error("Exception thrown by subscriber callback", e, s);
      }
    });
  }
}
