import 'package:flutter/foundation.dart';

enum EnumCaseStyle {
  unchanged,
  snake,
  snakeAllCaps,
}

T allCapsToEnum<T>(String value, List<T> values) {
  return EnumConverter<T>(values, EnumCaseStyle.snakeAllCaps)[value];
}

String enumToAllCaps<T>(T value, List<T> values) {
  return EnumConverter<T>(values, EnumCaseStyle.snakeAllCaps)(value);
}

T stringToEnum<T>(
  String value,
  List<T> values, [
  EnumCaseStyle caseStyle = EnumCaseStyle.unchanged,
]) {
  return EnumConverter<T>(values, caseStyle)[value];
}

String enumToString<T>(
  T value,
  List<T> values, [
  EnumCaseStyle caseStyle = EnumCaseStyle.unchanged,
]) {
  return EnumConverter<T>(values, caseStyle)(value);
}

class _CacheKey {
  Type enumType;
  EnumCaseStyle style;

  _CacheKey(this.enumType, this.style);

  @override
  bool operator ==(Object other) =>
      other is _CacheKey && enumType == other.enumType && style == other.style;

  @override
  int get hashCode => enumType.hashCode ^ style.hashCode;
}

final _cache = <_CacheKey, EnumConverter>{};

final _tmpCacheKey = _CacheKey(null, null);

class EnumConverter<T> {
  final List<String> values;

  final Map<String, T> _enums;
  final Map<T, String> _strings;

  factory EnumConverter(
    List<T> values, [
    EnumCaseStyle caseStyle = EnumCaseStyle.unchanged,
  ]) {
    // We modify this global object rather that creating a new one every time.
    // This saves the time required to allocate a new object.
    // This is safe without any synchronization, because of Dart's threading model.
    _tmpCacheKey
      ..style = caseStyle
      ..enumType = T;

    var converter = _cache[_tmpCacheKey];

    if (converter == null) {
      final strings = _stringify(values, caseStyle);
      converter = EnumConverter<T>._fromLists(values, strings);

      // It's important that a new _CacheKey is created when adding a new
      // entry, because keys should not change.
      _cache[_CacheKey(T, caseStyle)] = converter;
    }

    return converter;
  }

  EnumConverter._fromLists(List<T> enums, List<String> strings)
      : values = strings,
        _enums = Map.fromIterables(strings, enums),
        _strings = Map.fromIterables(enums, strings);

  T operator [](String stringValue) => _enums[stringValue];

  String call(T enumValue) => _strings[enumValue];

  static List<String> _stringify<T>(List<T> values, EnumCaseStyle caseStyle) {
    return List.unmodifiable(values.map((value) {
      var str = _enumToString(value);
      if (caseStyle == EnumCaseStyle.snake) {
        str = _toSnakeCase(str);
      } else if (caseStyle == EnumCaseStyle.snakeAllCaps) {
        str = _toSnakeCase(str).toUpperCase();
      }
      return str;
    }));
  }

  static String _enumToString<T>(T value) {
    var str = value.toString();
    // toString() for enums returns something like "EnumType.enumValue"
    // but we're only interested in the substring after the dot
    return str.substring(str.indexOf(".") + 1);
  }

  static final _capRegExp = RegExp(r"([A-Z]+?)([A-Z])?([a-z]|$)");

  static String _toSnakeCase(String str) {
    return str.replaceAllMapped(
      _capRegExp,
      (match) {
        var caps = match.group(1).toLowerCase();
        var lastCap = match.group(2)?.toLowerCase();
        var end = match.group(3);

        String leftUnderscore = match.start == 0 ? "" : "_";
        String rightUnderscore = end.length == 0 ? "" : "_";

        if (lastCap == null) {
          return "$leftUnderscore" "$caps" "$end";
        } else {
          return "$leftUnderscore" "$caps" "$rightUnderscore" "$lastCap" "$end";
        }
      },
    );
  }

  @visibleForTesting
  static String toSnakeCase(str) => _toSnakeCase(str);
}
