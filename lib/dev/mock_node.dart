import 'package:planhive/data/node/node.dart';

class MockNode<T extends NodeData> {
  final String id;
  final String parentId;
  final String permissionGroupId;
  final String creatorId;
  final NodeType type;
  final int creationTs;
  final int lastUpdateTs;
  final T data;

  MockNode({
    this.id,
    this.parentId,
    this.lastUpdateTs,
    this.creatorId,
    this.creationTs,
    this.permissionGroupId,
    this.data,
  }) : type = NodeData.getNodeType<T>();
}
