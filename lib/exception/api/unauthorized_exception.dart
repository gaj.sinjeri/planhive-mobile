class UnauthorizedException implements Exception {
  final String message;

  UnauthorizedException(this.message);

  String toString() => 'UnauthorizedException: $message';
}
