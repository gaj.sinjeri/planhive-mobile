class CodeRequestLimitException implements Exception {
  final DateTime earliestTime;

  CodeRequestLimitException(this.earliestTime);
}
