class UsernameException implements Exception {
  final String message;

  UsernameException(this.message);

  String toString() => 'UsernameException: $message';
}
