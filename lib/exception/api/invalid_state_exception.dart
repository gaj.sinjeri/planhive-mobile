class InvalidStateException implements Exception {
  final String message;

  InvalidStateException(this.message);

  String toString() => 'InvalidStateException: $message';
}
