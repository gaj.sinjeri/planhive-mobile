import 'package:flutter/material.dart';

Map<int, Color> _color = {
  50: Color.fromRGBO(255, 127, 0, .1),
  100: Color.fromRGBO(255, 127, 0, .2),
  200: Color.fromRGBO(255, 127, 0, .3),
  300: Color.fromRGBO(255, 127, 0, .4),
  400: Color.fromRGBO(255, 127, 0, .5),
  500: Color.fromRGBO(255, 127, 0, .6),
  600: Color.fromRGBO(255, 127, 0, .7),
  700: Color.fromRGBO(255, 127, 0, .8),
  800: Color.fromRGBO(255, 127, 0, .9),
  900: Color.fromRGBO(255, 127, 0, 1),
};
MaterialColor _customColor = MaterialColor(0xFFFF7F00, _color);

final baseTheme = ThemeData(
  brightness: Brightness.light,
  primarySwatch: _customColor,
  cardColor: Colors.white,
  dividerColor: Colors.grey,
  scaffoldBackgroundColor: Colors.grey[100],
  canvasColor: Colors.grey[100],
  bottomAppBarColor: Colors.white,
  backgroundColor: Colors.white,
  appBarTheme: AppBarTheme(
    color: Colors.white,
    elevation: 1,
    iconTheme: IconThemeData(color: Colors.black),
  ),
  primaryTextTheme: TextTheme(
    headline6: TextStyle(
      color: Colors.black,
    ),
  ),
);
