import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/debouncer.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/contact_user.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/core/contacts/contact_requests_page.dart';
import 'package:planhive/views/core/contacts/user_grid.dart';
import 'package:planhive/views/cross_platform/item_list.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';

class ContactsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContactsPageState();
  }
}

class _ContactsPageState extends State<ContactsPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _userService = GetIt.I<UserService>();
  final _debouncer = Debouncer();

  TextEditingController _searchTextController;
  FocusNode _searchTextFocusNode;

  List<ContactUser> _allContacts = const [];
  List<User> _connectedContacts = const [];
  var _numberOfContactInvites = 0;

  var _searchView = false;
  var _searching = false;
  var _searchComplete = false;
  List<User> _searchResults = const [];

  @override
  void initState() {
    _searchTextFocusNode = FocusNode();
    _searchTextFocusNode.addListener(() {
      if (_searchTextFocusNode.hasFocus == true) {
        setState(() {
          _searchView = true;
        });
      }
    });

    _searchTextController = TextEditingController();

    super.initState();
  }

  @override
  Future<void> loadState() async {
    _allContacts = await _userService.getAllContactUsers();

    var connectedContacts = _allContacts
        .where(
            (contactUser) => contactUser.contact.contactState == ContactState.connected)
        .map((contactUser) => contactUser.user)
        .toList();
    connectedContacts
        .sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));

    _connectedContacts = List.unmodifiable(connectedContacts);

    _numberOfContactInvites = _allContacts
        .where((contactUser) => contactUser.contact.contactState == ContactState.invited)
        .length;
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([User])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    _searchTextFocusNode.dispose();
    _debouncer.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Contacts"),
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: <Widget>[
            _buildSearchBar(),
            if (_searchView && _searchResults.isEmpty)
              Padding(
                padding: EdgeInsets.all(16),
                child: Text(
                  _searching
                      ? "Searching..."
                      : _searchComplete &&
                              _userService.isUsernameAutocompleteQueryValid(
                                  _searchTextController.text)
                          ? "No users found."
                          : "Please enter at least three letters to begin searching.",
                  style: theme.textTheme.caption,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            if (_searchView) _buildContactSuggestionsList(),
            if (!_searchView)
              Expanded(
                child: Column(
                  children: <Widget>[
                    _buildManageContactsTile(),
                    Expanded(
                      child: RefreshIndicator(
                        onRefresh: () => refresh(true),
                        child: UserGrid(
                          users: _connectedContacts,
                          placeholderText: "There are no contacts to show.",
                          fullScreenDialog: true,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildSearchBar() {
    return Container(
      color: theme.cardColor,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(width: 1, color: Colors.grey[200]),
              ),
              margin: EdgeInsets.only(
                left: 16,
                top: 8,
                bottom: 8,
                right: _searchView ? 0 : 16,
              ),
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: TextFormField(
                controller: _searchTextController,
                focusNode: _searchTextFocusNode,
                hintText: "Search by Username",
                textCapitalization: TextCapitalization.sentences,
                textInputAction: TextInputAction.search,
                onFieldSubmitted: (string) => {_search(true)},
                underline: false,
                isDense: true,
                onChanged: _search,
              ),
            ),
          ),
          if (_searchView)
            TextButton(
              text: "Cancel",
              onPressed: _cancelSearch,
            ),
        ],
      ),
    );
  }

  Widget _buildContactSuggestionsList() {
    return Expanded(
      child: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 8),
          itemCount: _searchResults.length,
          itemBuilder: (context, index) {
            var user = _searchResults[index];
            return ListTile(
              leading: Container(
                padding: EdgeInsets.all(8),
                child: UserAvatar(
                  user: user,
                  avatarSize: AvatarSize.thumbnail,
                  placeholderSize: 32,
                ),
              ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.name,
                    style: theme.textTheme.subtitle1,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                  Text(
                    "@${user.username}",
                    style: theme.textTheme.caption,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ],
              ),
              trailing: _connectedContacts.contains(user)
                  ? Text(
                      "Connected",
                      style: theme.textTheme.caption,
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    )
                  : null,
              onTap: () {
                _navigateToContactPage(user);
              },
            );
          }),
    );
  }

  Widget _buildManageContactsTile() {
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: ItemList(
        itemListData: [
          ItemListData(
            text: "View Contact Requests ($_numberOfContactInvites)",
            icon: PlatformIcon(
              Icons.person,
              CupertinoIcons.person_solid,
              color: theme.primaryColor,
            ),
            onTap: _navigateToContactRequestsPage,
            showForwardArrow: false,
          )
        ],
      ),
    );
  }

  void _search([bool force = false]) {
    if (_userService.isUsernameAutocompleteQueryValid(_searchTextController.text)) {
      setState(() {
        _searching = true;
      });
    } else if (_searching == true) {
      setState(() {
        _searching = false;
      });
    }

    _debouncer.debounce(() {
      setStateAsync(() async {
        _searchResults = await _userService
            .getUsernameAutocompleteSuggestion(_searchTextController.text);
        _searching = false;
        _searchComplete = true;
      });
    }, Duration(seconds: force ? 0 : 1));

    if (force) _searchTextFocusNode.unfocus();
  }

  void _cancelSearch() {
    _searchTextFocusNode.unfocus();
    _searchTextController.text = "";

    setState(() {
      _searchView = false;
      _searching = false;
      _searchResults = const [];
      _searchComplete = false;
    });
  }

  void _navigateToContactRequestsPage() async {
    await navigation.push(ContactRequestsPage());
  }

  void _navigateToContactPage(User user) async {
    await navigation.push(ContactInfoPage(user));

    _cancelSearch();
  }
}
