import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide TextFormField;
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/common/views/scroll_view_placeholder.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class UserGrid extends StatelessWidget {
  final List<User> users;
  final String placeholderText;
  final bool fullScreenDialog;

  UserGrid({
    @required this.users,
    @required this.placeholderText,
    this.fullScreenDialog = false,
  });

  @override
  Widget build(BuildContext context) {
    return ScrollViewPlaceholder(
      hasContent: users.isNotEmpty,
      placeholder: Text(placeholderText),
      content: GridView.builder(
        padding: const EdgeInsets.all(16),
        itemCount: users.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisSpacing: 16,
          mainAxisSpacing: 16,
          childAspectRatio: 0.7,
          crossAxisCount: 3,
        ),
        itemBuilder: (context, index) {
          final user = users[index];
          return TouchTarget(
            onTap: () => _onUserTileTapped(context, user),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 16,
                ),
                Expanded(
                  child: UserAvatar(
                    user: user,
                    avatarSize: AvatarSize.thumbnail,
                    placeholderSize: 32,
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  // the '\n' at the end is a hack to take
                  // a min of two lines.
                  user.name + '\n',
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 17),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void _onUserTileTapped(BuildContext context, User user) async {
    await NavigationService.instance.push<bool>(
      ContactInfoPage(user),
      modal: fullScreenDialog,
    );
  }
}
