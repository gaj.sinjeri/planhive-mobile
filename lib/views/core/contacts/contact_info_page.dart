import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/account/edit_profile_page.dart';
import 'package:planhive/views/core/account/user_card.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/tile_button.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class ContactInfoPage extends StatefulWidget {
  final User user;

  ContactInfoPage(this.user);

  @override
  State<StatefulWidget> createState() {
    return _ContactInfoPageState();
  }
}

class _ContactInfoPageState extends State<ContactInfoPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _accountService = GetIt.I<AccountService>();
  final _chatService = GetIt.I<ChatService>();
  final _contactService = GetIt.I<ContactService>();
  final _navigationService = GetIt.I<NavigationService>();
  final _userService = GetIt.I<UserService>();

  Contact _contact;
  User _user;

  var _isLocalUser = false;

  var _hasDeclinedRequest = false;

  @override
  Future<bool> sync() async {
    return (await _contactService.refreshContacts()).isNotEmpty;
  }

  @override
  Future<void> loadState() async {
    _isLocalUser = _accountService.userId == widget.user.id;

    _user = await _userService.getUserById(widget.user.id);
    _contact = await _contactService.getContact(widget.user.id);

    _hasDeclinedRequest = false;
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([widget.user])) {
      refresh();

      return SynchronousFuture(clicked);
    }

    return SynchronousFuture(false);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Profile"),
      body: LoadingIndicator(
        loading: loading,
        child: RefreshIndicator(
          onRefresh: () => refresh(true),
          child: ListView(
            children: <Widget>[
              if (_user != null)
                UserCard(
                  _user,
                  editCallback: _isLocalUser ? _editProfile : null,
                ),
              if (_contact?.contactState == ContactState.connected)
                _buildConnectedWidget(),
              if (_contact?.contactState == ContactState.requested)
                _buildRequestedWidget(),
              if (_contact?.contactState == ContactState.invited) _buildInvitedWidget(),
              if (!_isLocalUser && _isContactUnknown()) _buildNewContactWidget(),
              if (_hasDeclinedRequest)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Align(
                    child: Text(
                      "You have declined ${_user?.name}'s request to connect.",
                      style: ThemeUtility.caption1(theme.textTheme),
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildConnectedWidget() {
    return Column(
      children: <Widget>[
        TileButton(
          text: "Send Message",
          onTap: _sendMessage,
        ),
        SizedBox(
          height: 16,
        ),
        TileButton(
          text: "Remove Contact",
          onTap: _removeContact,
        ),
      ],
    );
  }

  void _sendMessage() {
    runAsync(() async {
      var chat = await _chatService.getDirectChat(_user.id);
      await _navigationService.navigateToNode(chat);
    }, exclusive: true);
  }

  void _removeContact() async {
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Remove Contact",
      content: "Are you sure you want to remove ${_user.name} from your contacts?",
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );

    if (decision == true) {
      runAsync(() async {
        await _contactService.deleteContact(_user.id);
        await refresh();
      }, exclusive: true);
    }
  }

  Widget _buildRequestedWidget() {
    return Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Text(
              "Contact Request Sent",
              style: theme.textTheme.subtitle1,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
          // TODO: We use row to fill up the horizontal space.
          //  Something more appropriate should be used.
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextButton(
                text: "Cancel Request",
                onPressed: _cancelRequest,
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  void _cancelRequest() async {
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Cancel Request",
      content: "Are you sure you want to cancel the contact request?",
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );

    if (decision == true) {
      runAsync(() async {
        await _contactService.cancelContactRequest(_user.id);
        await refresh();
      }, exclusive: true);
    }
  }

  Widget _buildInvitedWidget() {
    return Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Text(
              "${_user.name} wants to connect",
              style: theme.textTheme.subtitle1,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              TextButton(
                text: "Accept",
                onPressed: () => _respondToConnectRequest(true),
              ),
              TextButton(
                text: "Decline",
                onPressed: () => _respondToConnectRequest(false),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }

  void _respondToConnectRequest(bool accepted) async {
    runAsync(() async {
      await _contactService.respondToContactRequest(_user.id, accepted);
      await refresh();

      setState(() {
        _hasDeclinedRequest = accepted ? false : true;
      });
    }, exclusive: true);
  }

  Widget _buildNewContactWidget() {
    return TouchTarget(
      onTap: _sendConnectRequest,
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        child: Column(
          children: <Widget>[
            // TODO: We use row to fill up the horizontal space.
            //  Something more appropriate should be used.
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextButton(
                    text: "Send Request to Connect",
                    onPressed: _sendConnectRequest,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _sendConnectRequest() async {
    runAsync(() async {
      await _contactService.createContactRequest(_user.id);
      await refresh();
    }, exclusive: true);
  }

  void _editProfile() async {
    await NavigationService.instance.push<bool>(EditProfilePage(_user));
  }

  bool _isContactUnknown() {
    return _contact == null ||
        _contact.contactState == ContactState.rejected ||
        _contact.contactState == ContactState.deleted;
  }
}
