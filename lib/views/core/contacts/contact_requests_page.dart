import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/contacts/user_grid.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class ContactRequestsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ContactRequestsPageState();
  }
}

class _ContactRequestsPageState extends State<ContactRequestsPage>
    with
        ViewControllerMixin,
        RefreshControllerMixin,
        NotificationHandlerMixin,
        SingleTickerProviderStateMixin {
  final _userService = GetIt.I<UserService>();
  final _contactService = GetIt.I<ContactService>();

  TabController _tabController;

  List<User> _contactRequestsReceived = const [];
  List<User> _contactRequestsSent = const [];

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: 0,
      vsync: this,
      length: 2,
    );

    super.initState();
  }

  @override
  Future<bool> sync() async {
    return (await _contactService.refreshContacts()).isNotEmpty;
  }

  @override
  Future<void> loadState() async {
    var _allContacts = await _userService.getAllContactUsers();

    _contactRequestsReceived = List.unmodifiable(
      _allContacts
          .where((e) => e.contact.contactState == ContactState.invited)
          .map((e) => e.user),
    );
    _contactRequestsSent = List.unmodifiable(
      _allContacts
          .where((e) => e.contact.contactState == ContactState.requested)
          .map((e) => e.user),
    );
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([User])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Contact Requests"),
      body: LoadingIndicator(
        loading: loading,
        child: RefreshIndicator(
          onRefresh: () => refresh(true),
          child: Column(
            children: <Widget>[
              Container(
                color: theme.backgroundColor,
                height: 40,
                child: TabBar(
                  controller: _tabController,
                  labelColor: Theme.of(context).primaryColor,
                  tabs: [
                    Tab(text: "Received (${_contactRequestsReceived.length})"),
                    Tab(text: "Sent (${_contactRequestsSent.length})"),
                  ],
                ),
              ),
              Divider(
                height: 0,
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    RefreshIndicator(
                      onRefresh: () => refresh(true),
                      child: UserGrid(
                        users: _contactRequestsReceived,
                        placeholderText: "There are no pending contact requests.",
                      ),
                    ),
                    RefreshIndicator(
                      onRefresh: () => refresh(true),
                      child: UserGrid(
                        users: _contactRequestsSent,
                        placeholderText: "There are no sent contact requests.",
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
