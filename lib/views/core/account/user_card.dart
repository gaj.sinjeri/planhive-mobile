import 'package:flutter/material.dart' hide TextButton;
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_button.dart';

class UserCard extends StatelessWidget {
  final User user;
  final Function editCallback;

  UserCard(this.user, {this.editCallback});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 3,
                    right: MediaQuery.of(context).size.width / 3,
                    top: 16,
                    bottom: 16,
                  ),
                  child: user != null
                      ? UserAvatar(
                          user: user,
                          avatarSize: AvatarSize.fullSize,
                          placeholderSize: (MediaQuery.of(context).size.width / 3) * 0.7,
                        )
                      : null,
                ),
                SelectableText(
                  user.name,
                  style: theme.textTheme.headline5,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                if (user.username != null)
                  Padding(
                    padding: EdgeInsets.only(top: 4),
                    child: SelectableText(
                      "@${user.username}",
                      style: theme.textTheme.subtitle1
                          .copyWith(color: theme.textTheme.caption.color),
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
            Visibility(
              visible: editCallback != null,
              child: Positioned(
                bottom: platformLook == PlatformLook.cupertino ? 3 : 1,
                right: 0,
                child: TextButton(
                  text: "Edit",
                  onPressed: editCallback,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
