import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/app_settings.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/platform/platform_permission_service.dart';
import 'package:planhive/views/common/more_icons.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/account/edit_profile_page.dart';
import 'package:planhive/views/core/account/user_card.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/item_list.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/tile.dart';
import 'package:url_launcher/url_launcher.dart';

class AccountPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AccountPageState();
  }
}

class _AccountPageState extends State<AccountPage>
    with ViewControllerMixin, RefreshControllerMixin {
  final _accountService = GetIt.I<AccountService>();
  final _platformPermissionService = GetIt.I<PlatformPermissionService>();

  User _user;

  bool _hasNotificationsPermission = true;

  @override
  Future<void> loadState() async {
    _user = await _accountService.user;
    _hasNotificationsPermission =
        await _platformPermissionService.hasNotificationsPermission();
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Account"),
      body: _user != null
          ? ListView(
              padding: EdgeInsets.only(bottom: 16),
              physics: AlwaysScrollableScrollPhysics(),
              scrollDirection: Axis.vertical,
              children: <Widget>[
                UserCard(
                  _user,
                  editCallback: _editProfile,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: Tile(
                    content: Row(
                      children: <Widget>[
                        Text(
                          "Email  ",
                          style: theme.textTheme.subtitle1,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                        SelectableText(
                          _user.email,
                          style: ThemeUtility.caption1(theme.textTheme),
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ItemList(
                  itemListData: [
                    ItemListData(
                      text: "Notification Settings",
                      icon: PlatformIcon(
                        Icons.notifications,
                        CupertinoIcons.bell_solid,
                        color: theme.primaryColor,
                      ),
                      onTap: () => AppSettings.openNotificationSettings(),
                    ),
                  ],
                ),
                if (_hasNotificationsPermission == false)
                  Padding(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                    child: Text(
                      "Warning: Push notifications are disabled. "
                      "You must enable them in your device's settings "
                      "in order to receive updates.",
                      style: theme.textTheme.caption.copyWith(color: Colors.red),
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                SizedBox(height: 16),
                _getInfoCard(),
                SizedBox(height: 16),
                _getLogOutCard(),
              ],
            )
          : Container(
              alignment: Alignment.topLeft,
              color: Theme.of(context).scaffoldBackgroundColor,
              child: LinearProgressIndicator(),
            ),
    );
  }

  Widget _getInfoCard() {
    return ItemList(
      itemListData: [
        ItemListData(
          text: "Contact us",
          icon: PlatformIcon(
            Icons.email,
            CupertinoIcons.mail_solid,
            color: theme.primaryColor,
          ),
          onTap: () {
            _launchURL(contactUsURL);
          },
        ),
        ItemListData(
          text: "Privacy Policy",
          icon: PlatformIcon(
            Icons.lock,
            CupertinoIcons.padlock,
            color: theme.primaryColor,
          ),
          onTap: () {
            _launchURL(privacyPolicyURL);
          },
        ),
        ItemListData(
          text: "Terms & Conditions",
          icon: PlatformIcon(
            Icons.info,
            MoreIcons.cupertino_info_filled,
            color: theme.primaryColor,
          ),
          onTap: () {
            _launchURL(termsAndConditionsURL);
          },
        )
      ],
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget _getLogOutCard() {
    return ItemList(
      itemListData: [
        ItemListData(
          text: "Log Out",
          icon: PlatformIcon(
            Icons.power_settings_new,
            MoreIcons.cupertino_sleep_filled,
            color: theme.primaryColor,
          ),
          onTap: _logout,
        )
      ],
    );
  }

  Future<void> _logout() async {
    var alertData = [
      AlertActionData<bool>(text: "No", value: false),
      AlertActionData<bool>(text: "Yes", value: true),
    ];
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Log out",
      content: "Are you sure you want to log out?",
      actions: alertData,
    );

    if (decision == true) {
      runAsync(() async {
        await _accountService.logout();
      }, exclusive: true);
    }
  }

  void _editProfile() async {
    var profileUpdated = await NavigationService.instance.push<bool>(
      EditProfilePage(_user),
    );

    if (profileUpdated) {
      setStateAsync(() async {
        _user = await _accountService.user;
      });
    }
  }
}
