import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/image_utils.dart';
import 'package:planhive/common/image/pick_and_crop.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/common/image/user_avatar_provider.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/format/force_lowercase_text_input_formatter.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';

class EditProfilePage extends StatefulWidget {
  final User user;

  EditProfilePage(this.user);

  @override
  State<StatefulWidget> createState() {
    return _EditProfilePageState();
  }
}

class _EditProfilePageState extends State<EditProfilePage> with ViewControllerMixin {
  final _userService = GetIt.I<UserService>();

  final _formKey = GlobalKey<FormState>();

  TextEditingController _displayNameInputController;
  TextEditingController _usernameInputController;

  ImagePickAndCrop _imagePickAndCrop;
  ImageProvider _image;

  bool _imageChanged = false;

  @override
  void initState() {
    super.initState();

    _displayNameInputController = TextEditingController(text: widget.user.name);
    _usernameInputController = TextEditingController(text: widget.user.username);

    _imagePickAndCrop = ImagePickAndCrop(
      context,
      Size(userAvatarMaxResolution.toDouble(), userAvatarMaxResolution.toDouble()),
    );

    runAsync(_loadExistingImage);
  }

  Future<void> _loadExistingImage() async {
    if (widget.user.hasAvatar) {
      await _imagePickAndCrop.setImage(
        await resolveProvider(
          UserAvatarProvider(widget.user, AvatarSize.fullSize),
        ),
      );

      setState(() {
        _image = _imagePickAndCrop.imageProvider;
      });
    }
  }

  @override
  void dispose() {
    _imagePickAndCrop.dispose();
    _displayNameInputController.dispose();
    _usernameInputController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: _discardChanges,
      ),
      title: Text("Update Profile"),
      trailing: NavBarButton(
        text: "Save",
        materialIcon: Icons.done,
        onPressed: _submitForm,
      ),
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 32),
            physics: AlwaysScrollableScrollPhysics(),
            children: <Widget>[
              Center(
                child: SizedBox(
                  height: MediaQuery.of(context).size.width / 2.5,
                  width: MediaQuery.of(context).size.width / 2.5,
                  child: GestureDetector(
                    onTap: _onEditAvatar,
                    child: CircleAvatar(
                      child: _image == null
                          ? Icon(
                              Icons.add_photo_alternate,
                              color: theme.primaryColor,
                            )
                          : null,
                      backgroundImage: _image,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: TextButton(
                  text: "Edit",
                  onPressed: _onEditAvatar,
                ),
              ),
              TextFormField(
                controller: _displayNameInputController,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.next,
                textCapitalization: TextCapitalization.words,
                labelText: "Display Name",
                validator: _userService.validateDisplayName,
                onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
                useIOSNativeView: false,
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                controller: _usernameInputController,
                textInputAction: TextInputAction.done,
                textCapitalization: TextCapitalization.none,
                keyboardType: TextInputType.text,
                labelText: "Username",
                validator: _userService.validateUsername,
                cacheValidationResult: true,
                inputFormatters: [
                  const ForceLowercaseTextInputFormatter(),
                  WhitelistingTextInputFormatter(User.usernameCharactersRegex),
                ],
                useIOSNativeView: false,
              ),
              SizedBox(height: 8),
              Text(
                "You can use letters, numbers, periods (.) and underscores (_).",
                style: theme.textTheme.caption,
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onEditAvatar() async {
    if (await _imagePickAndCrop.pickImage(context)) {
      _imageChanged = true;
    }

    setState(() {
      _image = _imagePickAndCrop.imageProvider;
    });
  }

  void _discardChanges() async {
    if (_changesMade()) {
      var alertData = [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ];
      var decision = await showAlertDialog<bool>(
        context: context,
        title: "Discard Changes?",
        actions: alertData,
      );
      if (decision == true) {
        NavigationService.instance.pop(false);
      }
    } else {
      NavigationService.instance.pop(false);
    }
  }

  bool _changesMade() {
    if (_displayNameInputController.text != widget.user.name) return true;
    if (_usernameInputController.text != widget.user.username) return true;
    if (_image == null && widget.user.lastAvatarUpdateTs == 0) return false;
    if (_imageChanged) return true;
    return false;
  }

  void _submitForm() async {
    if (!_changesMade()) {
      NavigationService.instance.pop(false);
      return;
    }

    var displayName = _displayNameInputController.text.trim();
    var username = _usernameInputController.text.trim();

    if (!User.displayNameRegExp.hasMatch(displayName)) {
      await showAlertDialog<bool>(
        title: "Invalid Display Name",
        content: "Please make sure you enter a valid display name.",
        actions: [AlertActionData(text: "OK", value: true)],
      );

      return;
    }

    if (!User.usernameRegExp.hasMatch(username)) {
      await showAlertDialog<bool>(
        title: "Invalid Username",
        content: "Please make sure you enter a valid username.",
        actions: [AlertActionData(text: "OK", value: true)],
      );
      return;
    }

    runAsync(() async {
      await _userService.updateLocalUser(
        _usernameInputController.text != widget.user.username
            ? _usernameInputController.text
            : null,
        _displayNameInputController.text != widget.user.name
            ? _displayNameInputController.text
            : null,
        _imageChanged,
        await _imagePickAndCrop.toImage(),
      );

      NavigationService.instance.pop(true);
    }, exclusive: true);
  }
}
