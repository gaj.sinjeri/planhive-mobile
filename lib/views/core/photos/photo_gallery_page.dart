import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/photo.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/photo_service.dart';
import 'package:planhive/services/platform/platform_permission_service.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:vector_math/vector_math_64.dart' hide Colors;

class PhotoGalleryPage extends StatefulWidget {
  final List<User> users;
  final String groupNodeId;
  final List<Node<PhotoData>> photoNodes;
  final int selectedIndex;

  const PhotoGalleryPage({
    this.users,
    @required this.groupNodeId,
    @required this.photoNodes,
    this.selectedIndex = 0,
  })  : assert(selectedIndex != null),
        assert(groupNodeId != null),
        assert(photoNodes != null && photoNodes.length > 0);

  @override
  State<StatefulWidget> createState() {
    return _PhotoGalleryState();
  }
}

class _PhotoGalleryState extends State<PhotoGalleryPage>
    with TickerProviderStateMixin, ViewControllerMixin {
  final _photoService = GetIt.I<PhotoService>();
  final _accountService = GetIt.I<AccountService>();

  int _currentIndex;
  PageController _pageController;

  var _userMap = new Map<String, User>();

  bool _showOptions = false;

  TransformationController _transformationController;

  GlobalKey _viewportKey = GlobalKey();

  bool _isZoomedIn = false;
  bool _infiniteMargin = false;
  bool _isZoomInteraction = false;

  double get _scale => _transformationController.value.getMaxScaleOnAxis();

  @override
  void initState() {
    if (widget.users != null) {
      _userMap = Map.fromIterables(widget.users.map((user) => user.id), widget.users);
    }

    _currentIndex = widget.selectedIndex;
    _pageController = PageController(initialPage: widget.selectedIndex);

    _transformationController = TransformationController();

    super.initState();
  }

  @override
  void dispose() {
    _transformationController.dispose();

    super.dispose();
  }

  void _updateIsZoomedIn() {
    var isZoomedIn = _scale > 1.001;

    if (isZoomedIn != _isZoomedIn) {
      setState(() {
        _isZoomedIn = isZoomedIn;
      });
    }
  }

  void _setInfiniteMargin(bool infiniteMargin) {
    if (_infiniteMargin != infiniteMargin) {
      setState(() {
        _infiniteMargin = infiniteMargin;
      });
    }
  }

  void _onInteractionStart(ScaleStartDetails details) {
    _updateIsZoomedIn();
    _setInfiniteMargin(!_isZoomedIn);
    _isZoomInteraction |= _isZoomedIn;
  }

  void _onInteractionUpdate(ScaleUpdateDetails details) {
    _updateIsZoomedIn();
    _setInfiniteMargin(!_isZoomedIn);
    _isZoomInteraction |= _isZoomedIn;
  }

  void _onInteractionEnd(ScaleEndDetails details) {
    _updateIsZoomedIn();

    if (!_isZoomInteraction) {
      if (details.velocity.pixelsPerSecond.distance > 1000) {
        navigation.pop();
      } else {
        _transformationController.value = _transformationController.value.clone()
          ..setTranslation(Vector3.zero());
        _setInfiniteMargin(false);
      }
    }

    _isZoomInteraction = false;
  }

  bool get _canDelete =>
      widget.photoNodes[_currentIndex].creatorId == _accountService.userId;

  void _save() async {
    runAsync(() async {
      var photoId = widget.photoNodes[_currentIndex].id;
      var image = await GetIt.I<FileCacheStore>().get(photoId);

      if (!await GetIt.I<PlatformPermissionService>().canAccessStorage(true)) {
        return;
      }

      await ImageGallerySaver.saveImage(
        image.content,
        name: photoId,
        quality: imageQuality,
      );

      await showAlertDialog(
        title: "Photo Saved",
        actions: [AlertActionData(text: "OK")],
      );
    }, exclusive: true);
  }

  void _toggleUIOverlay() {
    setState(() {
      _showOptions = !_showOptions;
    });
  }

  void _toggleZoom() {
    if (_isZoomedIn) {
      _transformationController.value = Matrix4.identity();
    } else {
      var viewportSize = _viewportKey.currentContext.size * 0.5;

      _transformationController.value = Matrix4.compose(
        Vector3(-viewportSize.width, -viewportSize.height, 0),
        Quaternion.identity(),
        Vector3.all(2),
      );
    }

    setState(() {
      _isZoomedIn = !_isZoomedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: theme.canvasColor,
      child: SafeArea(
        child: ColoredBox(
          color: Colors.black,
          child: GestureDetector(
            onTap: _toggleUIOverlay,
            onDoubleTap: _toggleZoom,
            child: Stack(
              children: <Widget>[
                InteractiveViewer(
                  key: _viewportKey,
                  transformationController: _transformationController,
                  onInteractionStart: _onInteractionStart,
                  onInteractionUpdate: _onInteractionUpdate,
                  onInteractionEnd: _onInteractionEnd,
                  minScale: 1,
                  boundaryMargin: _infiniteMargin
                      ? const EdgeInsets.all(double.infinity)
                      : EdgeInsets.zero,
                  child: PageView.builder(
                    controller: _pageController,
                    onPageChanged: _onPageChanged,
                    itemCount: widget.photoNodes.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Hero(
                        tag: widget.photoNodes[index].id,
                        child: PhotoOriginal(
                          widget.groupNodeId,
                          widget.photoNodes[index],
                        ),
                      );
                    },
                    physics: _isZoomedIn
                        ? const NeverScrollableScrollPhysics()
                        : const BouncingScrollPhysics(),
                  ),
                ),
                Visibility(
                  visible: _showOptions,
                  child: Stack(
                    children: [
                      Positioned(
                        top: 16,
                        left: 16,
                        child: _CircleButton(
                          onPressed: () => navigation.pop(),
                          child: const Icon(Icons.close),
                        ),
                      ),
                      if (_canDelete)
                        Positioned(
                          top: 16,
                          right: 16,
                          child: _CircleButton(
                            onPressed: _showDeleteDialog,
                            child: const Icon(Icons.delete),
                          ),
                        ),
                      Positioned(
                        bottom: 16,
                        left: 16,
                        child: _CircleButton(
                          onPressed: _save,
                          child: const Icon(Icons.file_download),
                        ),
                      ),
                      if (_getPhotoCreator() != null)
                        Positioned(
                          bottom: 16,
                          right: 16,
                          child: Card(
                            margin: EdgeInsets.zero,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                kMinInteractiveDimension,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(4),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  const SizedBox(width: 16),
                                  Text(
                                    _isUser() ? "Me" : _getPhotoCreator().name,
                                    style: theme.textTheme.subtitle1,
                                    strutStyle: StrutStyle(
                                      forceStrutHeight: true,
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  SizedBox(
                                    height: 40,
                                    child: UserAvatar(
                                      user: _getPhotoCreator(),
                                      avatarSize: AvatarSize.micro,
                                      placeholderSize: 28,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showDeleteDialog() async {
    var alertData = [
      AlertActionData<bool>(text: "No", value: false),
      AlertActionData<bool>(text: "Yes", value: true),
    ];
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Delete Photo",
      content: "Are you sure you want to delete this photo?",
      actions: alertData,
    );

    if (decision == true) {
      _deletePhoto();
    }
  }

  void _deletePhoto() async {
    // TODO(#55): Add loading indicator
    await runAsync(() async {
      await _photoService.deletePhoto(
        widget.groupNodeId,
        widget.photoNodes[_currentIndex].parentId,
        widget.photoNodes[_currentIndex].id,
      );
      navigation.pop(true);
    }, exclusive: true);
  }

  void _onPageChanged(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  User _getPhotoCreator() {
    return _userMap[widget.photoNodes[_currentIndex].creatorId];
  }

  bool _isUser() {
    return _getPhotoCreator()?.id == _accountService.userId;
  }
}

class _CircleButton extends StatelessWidget {
  final Function() onPressed;
  final Widget child;

  _CircleButton({this.onPressed, this.child, Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      shape: const CircleBorder(),
      clipBehavior: Clip.hardEdge,
      child: IconButton(
        onPressed: onPressed,
        icon: child,
      ),
    );
  }
}
