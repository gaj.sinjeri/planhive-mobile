import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide IconButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/photo_thumbnail.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/data/node/photos_data.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/badge/top_right_badge.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/photos/photo_gallery_page.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/tile.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class PhotosPreview extends StatefulWidget {
  final Node<PhotosData> node;

  PhotosPreview(this.node) : super(key: ValueKey(node.id));

  @override
  State<StatefulWidget> createState() {
    return _PhotosPreviewState();
  }
}

class _PhotosPreviewState extends State<PhotosPreview>
    with ViewControllerMixin, RefreshControllerMixin {
  static const double _imagePreviewSize = 100;

  final _nodeService = GetIt.I<NodeService>();
  final _userService = GetIt.I<UserService>();
  final _navigationService = GetIt.I<NavigationService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();

  List<Node<PhotoData>> _photos = const [];
  Node<EventData> _event;

  int _badgeCounter = 0;

  @override
  Future<bool> sync() => _nodeService.fetchChildren(widget.node);

  @override
  Future<void> loadState() async {
    await Future.wait([
      _loadPhotos(),
      _loadEvent(),
      _loadBadgeCounter(),
    ]);
  }

  Future<void> _loadPhotos() async {
    _photos = List.unmodifiable(
      await _nodeService.getCachedChildNodes<PhotoData>(widget.node),
    );
  }

  Future<void> _loadEvent() async {
    _event = await _nodeService.getNode<EventData>(widget.node.parentId);
  }

  Future<void> _loadBadgeCounter() async {
    _badgeCounter = await _updateCounterService.countChildren(widget.node.id, [
      NodeType.photo,
    ]);
  }

  void _openAlbum() {
    _navigationService.navigateToNode(widget.node);
  }

  void _openFullScreen(int index) async {
    var users = await _userService.getUsersById(_photos.map((e) => e.creatorId));

    if (mounted) {
      _navigationService.push(PhotoGalleryPage(
        users: users,
        groupNodeId: widget.node.parentId,
        photoNodes: _photos,
        selectedIndex: index,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return TopRightBadge(
      _badgeCounter,
      top: 25,
      right: 36,
      child: Tile(
        margin: const EdgeInsets.symmetric(vertical: 8),
        padding: const EdgeInsets.all(0),
        content: Column(
          children: <Widget>[
            TouchTarget(
              onTap: _openAlbum,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      _event?.data?.name ?? "",
                      style: theme.textTheme.subtitle1,
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                    PlatformIcon(
                      Icons.navigate_next,
                      CupertinoIcons.forward,
                      color: theme.primaryColor,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: _imagePreviewSize,
              color: Colors.black12,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: _photos.length,
                physics: const AlwaysScrollableScrollPhysics(
                  parent: const BouncingScrollPhysics(),
                ),
                itemBuilder: (context, index) {
                  return TouchTarget(
                    onTap: () => _openFullScreen(index),
                    child: SizedBox(
                      height: _imagePreviewSize,
                      width: _imagePreviewSize,
                      child: Hero(
                        tag: _photos[index].id,
                        transitionOnUserGestures: true,
                        child: PhotoThumbnail(
                          widget.node.parentId,
                          _photos[index],
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) => const SizedBox(width: 1),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
