import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:planhive/services/platform/platform_permission_service.dart';

@lazySingleton
class MultiPhotoPicker {
  final _platformPermissionService = GetIt.I<PlatformPermissionService>();

  Future<List<Asset>> getPhotos(BuildContext context, int maxImages) async {
    var theme = Theme.of(context);

    if (await _platformPermissionService.canAccessPhotos(true) != true) {
      return const [];
    }

    List<Asset> photos;
    try {
      photos = await MultiImagePicker.pickImages(
        maxImages: maxImages,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          allViewTitle: "Image Selection",
          actionBarColor: _toHex(theme.primaryColor),
          actionBarTitleColor: "#FFFFFF",
          lightStatusBar: false,
          statusBarColor: _toHex(theme.primaryColor),
          startInAllView: true,
          selectCircleStrokeColor: _toHex(theme.primaryColor),
          selectionLimitReachedText:
              "You can select maximum $maxImages ${maxImages > 1 ? "images" : "image"}.",
        ),
      );
    } on NoImagesSelectedException {
      return const [];
    }

    return photos;
  }

  static String _toHex(Color color) {
    return "#${color.value.toRadixString(16)}";
  }
}
