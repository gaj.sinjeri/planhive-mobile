import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photos_data.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/views/scroll_view_placeholder.dart';
import 'package:planhive/views/core/photos/photos_preview.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class PhotosPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PhotosPageState();
  }
}

class _PhotosPageState extends State<PhotosPage> with RefreshControllerMixin {
  final _nodeService = GetIt.I<NodeService>();

  List<Node<PhotosData>> _albums = const [];

  @override
  Future<void> loadState() async {
    _albums = List.unmodifiable(
      await _nodeService.getNonEmptyNodes<PhotosData>(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Albums"),
      body: RefreshIndicator(
        onRefresh: () => refresh(true),
        child: ScrollViewPlaceholder(
          hasContent: _albums.isNotEmpty,
          content: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 8),
            itemCount: _albums.length,
            itemBuilder: (context, index) {
              return PhotosPreview(_albums[index]);
            },
          ),
          placeholder: Text("Photos from your events will appear here."),
        ),
      ),
    );
  }
}
