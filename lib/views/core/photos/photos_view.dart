import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/photo_thumbnail.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/data/node/photos_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/photo_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/common/views/scroll_view_placeholder.dart';
import 'package:planhive/views/common/views/stream_progress_modal.dart';
import 'package:planhive/views/core/photos/multi_photo_picker.dart';
import 'package:planhive/views/core/photos/photo_gallery_page.dart';
import 'package:planhive/views/cross_platform/text_button.dart';

class PhotosView extends StatefulWidget {
  final String groupNodeId;

  const PhotosView(this.groupNodeId, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PhotosViewState();
  }
}

class _PhotosViewState extends State<PhotosView>
    with
        AutomaticKeepAliveClientMixin,
        ViewControllerMixin,
        RefreshControllerMixin,
        NotificationHandlerMixin {
  final _nodeStore = GetIt.I<NodeStore>();
  final _nodeService = GetIt.I<NodeService>();
  final _photoService = GetIt.I<PhotoService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();
  final _multiPhotoPicker = GetIt.I<MultiPhotoPicker>();

  Node<PhotosData> _photosNode;
  var _photoNodes = const <Node<PhotoData>>[];
  var _users = const <User>[];
  double _uploadProgress = 0;

  @override
  Future<void> initAsync() async {
    _photosNode = await _nodeStore.getFirstChildOfType<PhotosData>(widget.groupNodeId);
  }

  @override
  Future<bool> sync() {
    return _nodeService.fetchChildren(_photosNode);
  }

  @override
  Future<void> loadState() async {
    var userNodes = await _nodeStore.getChildrenOfTypeWithUser<PhotoData>(_photosNode);

    _photoNodes = List.unmodifiable(userNodes.map((e) => e.node));
    _users = List.unmodifiable(userNodes.map((e) => e.user));

    _updateCounterService.clearWithChildren(_photosNode.id);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return RefreshIndicator(
      onRefresh: () => refresh(true),
      child: ScrollViewPlaceholder(
        hasContent: _photoNodes.isNotEmpty,
        placeholder: _buildPlaceholder(),
        content: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              backgroundColor: theme.canvasColor,
              title: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  TextButton(
                    text: "Add photos",
                    onPressed: _addPhotos,
                  ),
                  LinearProgressIndicator(
                    value: _uploadProgress,
                    backgroundColor: Colors.transparent,
                  ),
                ],
              ),
              excludeHeaderSemantics: true,
              automaticallyImplyLeading: false,
              centerTitle: true,
              floating: true,
              primary: false,
              elevation: 0,
            ),
            SliverPadding(
              padding: EdgeInsets.only(top: 2, bottom: 32),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 2,
                  mainAxisSpacing: 2,
                  crossAxisCount: 3,
                ),
                delegate: SliverChildBuilderDelegate(
                  (context, index) {
                    return GestureDetector(
                      onTap: () => _onTap(index),
                      child: Hero(
                        tag: _photoNodes[index].id,
                        child: PhotoThumbnail(
                          _photosNode.parentId,
                          _photoNodes[index],
                        ),
                        transitionOnUserGestures: true,
                      ),
                    );
                  },
                  childCount: _photoNodes.length,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlaceholder() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        TextButton(
          text: "Add photos",
          onPressed: _addPhotos,
        ),
        Text(
          "There are no photos from this event.",
          style: ThemeUtility.caption1(theme.textTheme),
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      ],
    );
  }

  void _addPhotos() async {
    var photos = await _multiPhotoPicker.getPhotos(context, 30);

    await showStreamProgressModal(
      title: "Uploading",
      operationCount: photos.length,
      operationDescription: "Uploading photo",
      stream: _photoService.addPhotos(_photosNode, photos),
    );
  }

  void _onTap(int index) {
    GetIt.I<NavigationService>().push(PhotoGalleryPage(
      users: _users,
      groupNodeId: _photosNode.parentId,
      photoNodes: _photoNodes,
      selectedIndex: index,
    ));
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (_photosNode != null && notification.matches([Node, _photosNode])) {
      refresh();
    }

    return SynchronousFuture(false);
  }
}
