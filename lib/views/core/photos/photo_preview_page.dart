import 'package:flutter/material.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class PhotoPreviewPage extends StatefulWidget {
  final ImageProvider imageProvider;
  final ImageProvider placeholderProvider;
  final String heroTag;

  const PhotoPreviewPage(this.imageProvider, this.placeholderProvider, this.heroTag);

  @override
  State<StatefulWidget> createState() {
    return _PhotoPreviewPageState();
  }
}

class _PhotoPreviewPageState extends State<PhotoPreviewPage>
    with TickerProviderStateMixin, ViewControllerMixin {
  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Container(),
      body: Dismissible(
        direction: DismissDirection.vertical,
        key: UniqueKey(),
        onDismissed: (_) => NavigationService.instance.pop(),
        child: Center(
          child: Hero(
            tag: widget.heroTag,
            child: FadeInImage(
              fadeOutDuration: const Duration(milliseconds: 1),
              fadeOutCurve: Curves.linear,
              fadeInDuration: const Duration(milliseconds: 1),
              fadeInCurve: Curves.linear,
              fit: BoxFit.contain,
              placeholder: widget.placeholderProvider,
              image: widget.imageProvider,
            ),
          ),
        ),
      ),
    );
  }
}
