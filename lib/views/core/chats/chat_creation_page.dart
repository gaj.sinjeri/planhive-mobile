import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide Switch;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/user_tile.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/chats/group_chat_creation_page.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/round_checkbox.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/switch.dart';

class ChatCreationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChatCreationPageState();
  }
}

class _ChatCreationPageState extends State<ChatCreationPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _userService = GetIt.I<UserService>();
  final _contactService = GetIt.I<ContactService>();
  final _chatService = GetIt.I<ChatService>();
  final _navigationService = GetIt.I<NavigationService>();

  List<User> _contacts = const [];
  Set<User> _selection = {};
  bool _isGroupSelection = false;

  @override
  Future<bool> sync() async {
    return (await _contactService.fetchUpdates()).isNotEmpty;
  }

  @override
  Future<void> loadState() async {
    _contacts = List.unmodifiable(await _userService.getConnectedUsers());
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([User])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  void _toggleGroupSelection() {
    setState(() {
      _isGroupSelection = !_isGroupSelection;
    });
  }

  void _onContactSelected(User contact) {
    if (_isGroupSelection) {
      setState(() {
        if (!_selection.add(contact)) {
          _selection.remove(contact);
        }
      });
    } else {
      runAsync(() => _createDirectChat(contact), exclusive: true);
    }
  }

  Future<void> _createDirectChat(User contact) async {
    var chat = await _chatService.getDirectChat(contact.id);
    await _navigationService.navigateToNode(chat);
  }

  void _createGroupChat() {
    navigation.push(GroupChatCreationPage(users: _selection), modal: false);
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return PageScaffold(
      title: const Text("Create chat"),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: () => navigation.pop(),
      ),
      trailing: _isGroupSelection && _selection.length > 0
          ? NavBarButton(
              text: "Next",
              onPressed: _createGroupChat,
              materialIcon: Icons.done,
            )
          : null,
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  SizedBox(
                    width: 40,
                    height: 40,
                    child: PlatformIcon(
                      Icons.group,
                      CupertinoIcons.group_solid,
                      color: theme.primaryColor,
                    ),
                  ),
                  SizedBox(width: 16),
                  Expanded(
                    child: Text(
                      "Create group",
                      style: theme.textTheme.subtitle1,
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                  SizedBox(width: 16),
                  Switch(
                    value: _isGroupSelection,
                    onChanged: (_) => _toggleGroupSelection(),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: const EdgeInsets.symmetric(vertical: 16),
                children: _buildUserTiles(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildUserTiles() {
    List<Widget> userTiles = [];

    for (int i = 0; i < _contacts.length; i++) {
      var contact = _contacts[i];

      userTiles.add(
        UserTile(
          user: contact,
          onTap: () => _onContactSelected(contact),
          trailing:
              _isGroupSelection ? RoundCheckBox(_selection.contains(contact)) : null,
          withTopDivider: false,
        ),
      );
    }

    return userTiles;
  }
}
