import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/user_tile.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/round_checkbox.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class GroupChatInvitePeoplePage extends StatefulWidget {
  final Node<GroupChatData> groupChatNode;
  final List<User> members;

  GroupChatInvitePeoplePage(this.groupChatNode, this.members);

  @override
  State<StatefulWidget> createState() {
    return _GroupChatInvitePeoplePageState();
  }
}

class _GroupChatInvitePeoplePageState extends State<GroupChatInvitePeoplePage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _contactService = GetIt.I<ContactService>();
  final _userService = GetIt.I<UserService>();
  final _chatService = GetIt.I<ChatService>();

  List<User> _contacts = [];
  final _selection = Set<User>();

  @override
  Future<bool> sync() async {
    return (await _contactService.fetchUpdates()).isNotEmpty;
  }

  @override
  Future<void> loadState() async {
    var connectedUsers = await _userService.getConnectedUsers();
    _contacts = List<User>.unmodifiable(
      connectedUsers.where(
        (connectedUser) => !widget.members.contains(connectedUser),
      ),
    );
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([User])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> userTiles = [];

    for (int i = 0; i < _contacts.length; i++) {
      var contact = _contacts[i];

      userTiles.add(
        UserTile(
          user: contact,
          onTap: () => _toggleSelected(contact),
          trailing: RoundCheckBox(_selection.contains(contact)),
          withTopDivider: i == 0 ? true : false,
        ),
      );
    }

    return PageScaffold(
      title: Text("Invite People"),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: () => navigation.pop(false),
      ),
      trailing: _selection.length != 0
          ? NavBarButton(
              text: "Invite",
              materialIcon: Icons.done,
              onPressed: _sendInvites,
            )
          : null,
      body: LoadingIndicator(
        loading: loading,
        child: Card(
          elevation: 0,
          margin: EdgeInsets.all(0),
          child: Padding(
            padding: EdgeInsets.only(top: 16),
            child: userTiles.isNotEmpty
                ? ListView(
                    padding: EdgeInsets.only(bottom: 32),
                    children: userTiles,
                  )
                : Center(
                    child: Text(
                      "There is no one else to invite at the moment.",
                      style: ThemeUtility.caption1(theme.textTheme),
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
          ),
        ),
      ),
    );
  }

  void _toggleSelected(User user) {
    if (_selection.contains(user)) {
      _selection.remove(user);
    } else {
      _selection.add(user);
    }

    setState(() {});
  }

  void _sendInvites() {
    runAsync(() async {
      await _chatService.inviteToGroupChat(widget.groupChatNode, _selection);
      navigation.pop(true);
    }, exclusive: true);
  }
}
