import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/common/views/scroll_view_placeholder.dart';
import 'package:planhive/views/core/chats/chat_creation_page.dart';
import 'package:planhive/views/core/chats/thread_preview_tile.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class ChatsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChatsPageState();
  }
}

class _ChatsPageState extends State<ChatsPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _nodeService = GetIt.I<NodeService>();

  List<Node<ThreadData>> _threads = const [];

  @override
  Future<void> loadState() async {
    _threads = List.unmodifiable(await _nodeService.getNonEmptyForParentTypes<ThreadData>(
      [NodeType.event, NodeType.chatDirect],
    ));
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([
      [NodeType.chatGroup, NodeType.chatDirect, NodeType.event]
    ])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  void createChat() {
    GetIt.I<NavigationService>().push(ChatCreationPage(), modal: true);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Chats"),
      trailing: NavBarButton(
        materialIcon: Icons.add,
        cupertinoIcon: CupertinoIcons.add,
        onPressed: createChat,
      ),
      body: RefreshIndicator(
        onRefresh: () => refresh(true),
        child: ScrollViewPlaceholder(
          hasContent: _threads.isNotEmpty,
          content: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 4),
            itemCount: _threads.length,
            itemBuilder: (context, index) {
              return ThreadPreviewTile(_threads[index]);
            },
          ),
          placeholder: Text(
            "Start a new chat by clicking on the + button.",
          ),
        ),
      ),
    );
  }
}
