import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField, IconButton, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/group_chat_cover_image.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_permission.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/user_tile.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/chats/group_chat_creation_page.dart';
import 'package:planhive/views/core/chats/group_chat_invite_people_page.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/tile_button.dart';

class GroupChatInfoPage extends StatefulWidget {
  final Node<GroupChatData> groupChatNode;

  GroupChatInfoPage(this.groupChatNode);

  @override
  State<StatefulWidget> createState() {
    return _GroupChatInfoPageState();
  }
}

class _GroupChatInfoPageState extends State<GroupChatInfoPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _chatService = GetIt.I<ChatService>();
  final _nodeService = GetIt.I<NodeService>();
  final _permissionService = GetIt.I<PermissionService>();

  Node<GroupChatData> _groupChatNode;

  var _isCreator = false;
  var _isAdmin = false;

  User _creator;
  List<UserPermission> _members = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Future<bool> sync() {
    return _permissionService.fetchUpdates();
  }

  @override
  Future<void> loadState() async {
    _groupChatNode = widget.groupChatNode;

    var userPermission =
        await _permissionService.getUserPermission(_groupChatNode.permissionGroupId);
    var contactPermissions =
        await _permissionService.getContactPermissions(_groupChatNode.permissionGroupId);

    _isCreator = userPermission.user.id == _groupChatNode.creatorId;
    _isAdmin = userPermission.permission.can(Permissions.write);

    _creator = _groupChatNode.creatorId == userPermission.user.id
        ? userPermission.user
        : contactPermissions
            .where((e) => e.user.id == _groupChatNode.creatorId)
            .first
            .user;

    _members = _constructMembersList(userPermission, contactPermissions);
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([_groupChatNode])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  List<UserPermission> _constructMembersList(
      UserPermission localPermission, Iterable<UserPermission> userPermissions) {
    var admins = userPermissions
        .where((e) =>
            e.permission.can(Permissions.write) && e.user.id != localPermission.user.id)
        .toList(growable: false);
    admins.sort((a, b) => a.user.name.toLowerCase().compareTo(b.user.name.toLowerCase()));

    var regulars = userPermissions
        .where((e) => !admins.contains(e) && e.user.id != localPermission.user.id)
        .toList(growable: false);
    regulars
        .sort((a, b) => a.user.name.toLowerCase().compareTo(b.user.name.toLowerCase()));

    var members = [localPermission];
    members.addAll(admins);
    members.addAll(regulars);

    return members;
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Group Chat Info"),
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          children: <Widget>[
            _getGroupChatDataWidget(),
            SizedBox(height: 16),
            _getMembersWidget(),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }

  Widget _getGroupChatDataWidget() {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      color: theme.cardColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: _groupChatNode.id,
            child: GroupChatCoverImage(_groupChatNode),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 16),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          "Name",
                          textAlign: TextAlign.left,
                          style: theme.textTheme.subtitle1,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, top: 5, right: 16),
                        child: Text(
                          _groupChatNode.data.name,
                          textAlign: TextAlign.left,
                          style: ThemeUtility.caption1(theme.textTheme),
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  if (_isAdmin)
                    TextButton(
                      text: "Edit",
                      onPressed: _onEditTapped,
                    ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _getMembersWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _buildMembersHeader(),
        if (_members.length != 0) _buildMembersList(),
        SizedBox(height: 16),
        _buildButtons(),
      ],
    );
  }

  Widget _buildMembersHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 16, bottom: 8),
            child: Text(
              "Members",
              style: theme.textTheme.subtitle1,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMembersList() {
    List<Widget> members = [];
    for (int i = 0; i < _members.length; i++) {
      var userPermission = _members[i];

      members.add(
        UserTile(
          user: userPermission.user,
          onTap: () => _onUserTileTapped(userPermission),
          trailing: Text(
            userPermission.user.id == _creator.id
                ? "Organizer"
                : (userPermission.permission.can(Permissions.write) ? "Admin" : ""),
            style: theme.textTheme.caption,
            strutStyle: StrutStyle(
              forceStrutHeight: true,
            ),
          ),
          withBottomDivider: i != _members.length - 1,
        ),
      );
    }

    return Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      child: Column(
        children: members,
      ),
    );
  }

  Widget _buildButtons() {
    return Column(
      children: <Widget>[
        TileButton(
          text: "Clear Group Chat",
          onTap: _onClearChatTapped,
        ),
        SizedBox(height: 16),
        if (_isAdmin)
          Padding(
            padding: EdgeInsets.only(bottom: 16),
            child: TileButton(
              text: "Invite People",
              onTap: _onInvitePeopleTapped,
            ),
          ),
        TileButton(
          text: "Leave Group",
          onTap: _onLeaveGroupChatTapped,
        ),
        if (_isCreator)
          Column(
            children: <Widget>[
              Container(
                color: theme.cardColor,
                child: Divider(
                  indent: 16,
                  height: 2,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 0),
                child: TileButton(
                  text: "Delete Group",
                  onTap: _onDeleteGroupChatTapped,
                ),
              ),
            ],
          ),
      ],
    );
  }

  Future<void> _refreshGroupChatNode() async {
    var groupChatNode = await _nodeService.getNode(_groupChatNode.id);
    setState(() {
      _groupChatNode = groupChatNode.as<GroupChatData>();
    });
  }

  void _onEditTapped() async {
    var updatedGroupChatNode = await navigation.push<Node<GroupChatData>>(
      GroupChatCreationPage(
        groupChatNode: _groupChatNode,
      ),
    );

    if (updatedGroupChatNode != null) {
      await _refreshGroupChatNode();
    }
  }

  void _onUserTileTapped(UserPermission userPermission) async {
    // Return if it's the local user.
    if (userPermission.user == _members[0].user) return;

    List<ModalActionData<Function>> actions = [
      ModalActionData<Function>(
        text: "View Profile",
        icon: Icons.person,
        value: () {
          navigation.push(ContactInfoPage(userPermission.user), modal: false);
        },
      ),
    ];

    if (_isCreator && !userPermission.permission.can(Permissions.write)) {
      actions.add(
        ModalActionData<Function>(
          text: "Make Admin",
          icon: Icons.add,
          value: () {
            _setMemberAdminStatus(userPermission.user, true);
          },
        ),
      );
    } else if (_isCreator && userPermission.permission.can(Permissions.write)) {
      actions.add(
        ModalActionData<Function>(
          text: "Remove Admin",
          icon: Icons.remove,
          value: () {
            _setMemberAdminStatus(userPermission.user, false);
          },
        ),
      );
    }

    var isTargetAdmin = userPermission.permission.can(Permissions.write) ||
        userPermission.permission.can(Permissions.delete);
    if (_isCreator || (_isAdmin && !isTargetAdmin)) {
      actions.add(
        ModalActionData<Function>(
          text: "Remove From Group",
          icon: Icons.remove,
          value: () {
            _removeFromGroupChat(userPermission.user);
          },
        ),
      );
    }

    var result = await showModalSheet<Function>(
      context: context,
      actions: actions,
    );
    result?.call();
  }

  void _setMemberAdminStatus(User user, bool status) {
    runAsync(() async {
      await _chatService.setGroupChatMemberAdminStatus(
          _groupChatNode.id, user.id, status);
      await refresh(true);
    }, exclusive: true);
  }

  void _onClearChatTapped() async {
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Clear Chat?",
      content: "Clearing the group chat will only remove the messages for you.",
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );

    if (decision == true) {
      runAsync(() async {
        // For now, we are assuming there is only one thread per group chat
        var threadNode =
            (await _nodeService.getCachedChildNodes<ThreadData>(widget.groupChatNode))
                .first;
        _chatService.clearThreadForUser(threadNode);
      }, exclusive: true);
    }
  }

  void _removeFromGroupChat(User user) async {
    var confirmed = await showAlertDialog(
      title: "Remove From Group",
      content: "Are you sure you want to remove ${user.name} from this group?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _chatService.removeFromGroupChat(_groupChatNode, user.id);
        await refresh();
      }, exclusive: true);
    }
  }

  void _onInvitePeopleTapped() async {
    await NavigationService.instance.push<bool>(
      GroupChatInvitePeoplePage(
        _groupChatNode,
        List.unmodifiable(_members.map((e) => e.user)),
      ),
      modal: false,
    );
  }

  void _onLeaveGroupChatTapped() async {
    var confirmed = await showAlertDialog(
      title: "Leave Group",
      content: "Are you sure you want to leave this group?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _chatService.removeFromGroupChat(_groupChatNode);

        GetIt.I<NavigationService>().popUntilStart();
      }, exclusive: true);
    }
  }

  void _onDeleteGroupChatTapped() async {
    var confirmed = await showAlertDialog(
      title: "Delete Group",
      content: "Are you sure you want to delete this group?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _chatService.deleteGroupChat(_groupChatNode);

        GetIt.I<NavigationService>().popUntilStart();
      }, exclusive: true);
    }
  }
}
