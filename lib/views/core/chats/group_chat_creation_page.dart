import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/group_chat_cover_image_provider.dart';
import 'package:planhive/common/image/image_utils.dart';
import 'package:planhive/common/image/pick_and_crop.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class GroupChatCreationPage extends StatefulWidget {
  final Node<GroupChatData> groupChatNode;
  final Set<User> users;

  GroupChatCreationPage({this.users, this.groupChatNode, Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _GroupChatCreationPageState();
  }
}

class _GroupChatCreationPageState extends State<GroupChatCreationPage>
    with ViewControllerMixin {
  final _chatService = GetIt.I<ChatService>();

  final _formKey = GlobalKey<FormState>();
  var _nameInput = TextEditingController();

  ImagePickAndCrop _imagePickAndCrop;
  ImageProvider _image;

  bool _imageChanged = false;

  List<User> _users = const [];

  @override
  void initState() {
    super.initState();

    _imagePickAndCrop = ImagePickAndCrop(
        context,
        Size(
          groupChatCoverPhotoWidth.toDouble(),
          groupChatCoverPhotoHeight.toDouble(),
        ));

    _nameInput = TextEditingController();

    if (widget.users != null) {
      var users = List.from(widget.users);
      users.sort((a, b) => a.name.compareTo(b.name));
      _users = List.unmodifiable(users);
    }

    runAsync(_loadExistingData, errorDialog: false);
  }

  @override
  void dispose() {
    _imagePickAndCrop.dispose();
    _nameInput.dispose();

    super.dispose();
  }

  Future<void> _loadExistingData() async {
    if (widget.groupChatNode != null) {
      _nameInput.text = widget.groupChatNode.data.name;

      if (widget.groupChatNode.data.hasCoverImage) {
        await _imagePickAndCrop.setImage(await resolveProvider(
          GroupChatCoverImageProvider(widget.groupChatNode),
        ));
      }

      if (widget.groupChatNode.data.coverTs != 0) {
        setState(() {
          _image = _imagePickAndCrop.imageProvider;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return PageScaffold(
      leading: NavBarButton(
        onPressed: _onDiscard,
        materialIcon: Icons.close,
        text: "Cancel",
      ),
      title: Text(widget.groupChatNode == null ? "Create Group Chat" : "Edit Group Chat"),
      trailing: NavBarButton(
        text: "Done",
        materialIcon: Icons.done,
        onPressed: _submitForm,
      ),
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          children: <Widget>[
            TouchTarget(
              onTap: _onEditCoverImage,
              child: AspectRatio(
                aspectRatio: 2,
                child: Container(
                  color: theme.primaryColor.withOpacity(0.3),
                  child: _image != null
                      ? Image(
                          image: _image,
                        )
                      : Icon(
                          Icons.add_photo_alternate,
                          color: theme.primaryColor,
                          size: 40,
                        ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: TextButton(
                text: _imagePickAndCrop.imageProvider == null
                    ? "Add a Cover Photo - Optional"
                    : "Change Cover Photo",
                onPressed: _onEditCoverImage,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      controller: _nameInput,
                      textInputAction: TextInputAction.done,
                      textCapitalization: TextCapitalization.words,
                      keyboardType: TextInputType.text,
                      labelText: "Title",
                      validator: _nameValidator,
                      onFieldSubmitted: (_) => FocusScope.of(context).unfocus(),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 32),
            if (_users.isNotEmpty)
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  "Members",
                  style: theme.textTheme.subtitle1,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            if (_users.isNotEmpty) SizedBox(height: 8),
            _getUsersList(),
          ],
        ),
      ),
    );
  }

  Widget _getUsersList() {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16, bottom: 32),
      child: Column(
        children: _users
            .map((user) => Padding(
                  padding: EdgeInsets.symmetric(vertical: 8),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 40,
                        height: 40,
                        child: UserAvatar(
                          user: user,
                          avatarSize: AvatarSize.thumbnail,
                          placeholderSize: 28,
                        ),
                      ),
                      SizedBox(width: 16),
                      Expanded(
                        child: Text(
                          user.name,
                          style: theme.textTheme.bodyText2,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
            .toList(),
      ),
    );
  }

  String _nameValidator(String input) {
    return GroupChatData.nameRegExp.hasMatch(input)
        ? null
        : "Please enter a name between 1 and 25 characters";
  }

  void _onEditCoverImage() async {
    if (await _imagePickAndCrop.pickImage(context) == true) {
      _imageChanged = true;
    }

    setState(() {
      _image = _imagePickAndCrop.imageProvider;
    });
  }

  void _onDiscard() async {
    if (_changesMade()) {
      var alertData = [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ];
      var decision = await showAlertDialog<bool>(
        context: context,
        title: "Discard Changes?",
        actions: alertData,
      );
      if (decision == true) {
        navigation.pop();
      }
    } else {
      navigation.pop();
    }
  }

  bool _changesMade() {
    if (widget.groupChatNode != null) {
      if (widget.groupChatNode.data.name != _nameInput.text) return true;
      if (_image == null && widget.groupChatNode.data.coverTs == 0) return false;
      if (_imageChanged) return true;
      return false;
    }

    return _image != null || _nameInput.text.isNotEmpty;
  }

  void _submitForm() async {
    if (!_changesMade()) {
      navigation.pop();
      return;
    }

    if (_nameValidator(_nameInput.text) != null) {
      await showAlertDialog<bool>(
        title: "Event Title",
        content: "Please enter a valid title.",
        actions: [AlertActionData(text: "OK", value: true)],
      );
      return;
    }

    if (widget.groupChatNode == null) {
      runAsync(() async {
        var coverImage = await _imagePickAndCrop.toImage();
        var groupChatNode = await _chatService.createGroupChat(
          _users.map((user) => user.id),
          _nameInput.text,
          coverImage,
        );

        GetIt.I<NavigationService>().navigateToNode(groupChatNode);
      }, exclusive: true);
    } else {
      runAsync(() async {
        var coverImage = await _imagePickAndCrop.toImage();
        var groupChatNode = await _chatService.updateGroupChatData(
          widget.groupChatNode,
          _nameInput.text,
          coverImage,
          _imageChanged,
        );

        navigation.pop(groupChatNode);
      }, exclusive: true);
    }
  }
}
