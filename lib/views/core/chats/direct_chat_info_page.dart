import 'dart:async';

import 'package:flutter/material.dart' hide TextFormField, IconButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/account/user_card.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/tile_button.dart';

class DirectChatInfoPage extends StatefulWidget {
  final Node<DirectChatData> directChatNode;
  final Node<ThreadData> threadNode;

  DirectChatInfoPage(this.directChatNode, this.threadNode);

  @override
  State<StatefulWidget> createState() {
    return _DirectChatInfoPageState();
  }
}

class _DirectChatInfoPageState extends State<DirectChatInfoPage>
    with ViewControllerMixin, RefreshControllerMixin {
  final _chatService = GetIt.I<ChatService>();
  final _permissionService = GetIt.I<PermissionService>();

  User _contact;

  @override
  Future<bool> sync() {
    return _permissionService.fetchUpdates();
  }

  @override
  Future<void> loadState() async {
    _contact = await _chatService.getDirectChatUser(widget.directChatNode.id);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Chat Info"),
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          children: <Widget>[
            if (_contact != null)
              UserCard(
                _contact,
              ),
            TileButton(
              text: "Clear Chat",
              onTap: _onClearChatTapped,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _onClearChatTapped() async {
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Clear Chat?",
      content: "Clearing this chat will only remove the messages for you.",
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );

    if (decision == true) {
      runAsync(() async {
        // For now, we are assuming there is only one thread per direct chat
        _chatService.clearThreadForUser(widget.threadNode);
      }, exclusive: true);
    }
  }
}
