import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide IconButton, TextFormField;
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:planhive/common/image/message_photo.dart';
import 'package:planhive/common/image/message_photo_thumbnail.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/common/time.dart';
import 'package:planhive/data/node/message_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/common/view_visibility.dart';
import 'package:planhive/views/common/views/stream_progress_modal.dart';
import 'package:planhive/views/core/photos/multi_photo_picker.dart';
import 'package:planhive/views/core/photos/photo_preview_page.dart';
import 'package:planhive/views/cross_platform/icon_button.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';
import 'package:uuid/uuid.dart';

class ThreadView extends StatefulWidget {
  final Node parentNode;
  final Node<ThreadData> threadNode;
  final bool showUserNames;

  ThreadView(this.parentNode, this.threadNode, this.showUserNames, {Key key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ThreadVewState();
  }
}

class _ThreadVewState extends State<ThreadView>
    with
        AutomaticKeepAliveClientMixin,
        ViewControllerMixin,
        NotificationHandlerMixin,
        RefreshControllerMixin {
  final _accountService = GetIt.I<AccountService>();
  final _chatService = GetIt.I<ChatService>();
  final _multiPhotoPicker = GetIt.I<MultiPhotoPicker>();
  final _nodeService = GetIt.I<NodeService>();
  final _nodeStore = GetIt.I<NodeStore>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();
  final _uuid = Uuid();

  var _messages = const <Node<MessageData>>[];
  var _users = const <User>[];

  TextEditingController _newMessageText;
  FocusNode _newMessageTextFocusNode;
  ScrollController _scrollController;

  var _isTextEmpty = true;
  var _sendingInProgressIds = Set();

  @override
  void initState() {
    super.initState();

    _newMessageText = TextEditingController();
    _newMessageTextFocusNode = FocusNode();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _newMessageText.dispose();
    _newMessageTextFocusNode.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Column(
      children: <Widget>[
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 16),
            controller: _scrollController,
            physics: AlwaysScrollableScrollPhysics(),
            reverse: true,
            itemCount: _messages.length,
            itemBuilder: (context, index) {
              var msg = _messages[index];
              var usr = _users[index];

              // the offset between the edges of the left and right aligned bubbles
              const bubbleOffset = 24.0;
              // the padding between the bubble and the edge of the screen
              const edgePadding = 16.0;
              const avatarSize = 24.0;
              const avatarTotalWidth = avatarSize + 8.0;

              var avatar = Container(
                width: avatarTotalWidth,
                height: avatarSize,
                margin: EdgeInsets.only(top: 4),
                // padding to align with name label
                alignment: Alignment.centerLeft,
                child: UserAvatar(
                  user: usr,
                  avatarSize: AvatarSize.micro,
                  placeholderSize: 21,
                ),
              );

              var isUser = usr.id == _accountService.userId;
              var sequencedMessage =
                  index + 1 < _users.length && _users[index + 1] == _users[index];

              // left padding to apply to account for missing avatar when sequenced
              var sequencedPadding = sequencedMessage ? avatarTotalWidth : 0.0;

              var msgDate = DateTime.fromMillisecondsSinceEpoch(msg.creationTs);

              bool showName = widget.showUserNames && !isUser;
              if (index != _messages.length - 1) {
                var prevMsgDate =
                    DateTime.fromMillisecondsSinceEpoch(_messages[index + 1].creationTs);
                if (dateMatches(msgDate, prevMsgDate)) {
                  showName = showName && !sequencedMessage;
                }
              }

              String dateBefore;
              String dateAfter;
              bool showTime = true;

              if (msg.localOnly == true) {
                showTime = false;
              }

              if (index != 0) {
                if (index == _messages.length - 1) {
                  dateBefore = _formatDate(msgDate);
                }

                var nextMsg = _messages[index - 1];
                var nextMsgDate = DateTime.fromMillisecondsSinceEpoch(nextMsg.creationTs);

                if (!dateMatches(msgDate, nextMsgDate))
                  dateAfter = _formatDate(nextMsgDate);

                if (_messages[index - 1].localOnly != true &&
                    minutesMatch(msgDate, nextMsgDate) &&
                    msg.creatorId == nextMsg.creatorId) showTime = false;
              }

              return Column(
                crossAxisAlignment:
                    isUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                children: [
                  if (dateBefore != null)
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 12),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          dateBefore,
                          style: theme.textTheme.bodyText1,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                    ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: isUser
                          ? bubbleOffset + avatarTotalWidth + edgePadding
                          : edgePadding + sequencedPadding,
                      right: isUser ? edgePadding : edgePadding + bubbleOffset,
                      top: sequencedMessage ? 2 : 12,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment:
                          isUser ? MainAxisAlignment.end : MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (!isUser && !sequencedMessage) avatar,
                        Flexible(
                          child: _buildMessageBubble(
                            msg,
                            usr,
                            isUser,
                            sequencedMessage,
                            showName,
                            showTime,
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (dateAfter != null)
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 12),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          dateAfter,
                          style: theme.textTheme.bodyText1,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                    ),
                ],
              );
            },
          ),
        ),
        Container(
          color: theme.cardColor,
          // This bottom padding was added as a workaround because the selection handles
          // are partially clipped under the keyboard.
          padding: Platform.isAndroid ? const EdgeInsets.only(bottom: 12) : null,
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Divider(height: 0),
              Row(
                children: <Widget>[
                  IconButton(
                    materialIcon: Icons.add,
                    cupertinoIcon: CupertinoIcons.add,
                    onPressed: _onMediaTapped,
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        border: Border.all(width: 1, color: Colors.grey[200]),
                      ),
                      margin: EdgeInsets.symmetric(vertical: 8),
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: TextFormField(
                        focusNode: _newMessageTextFocusNode,
                        controller: _newMessageText,
                        hintText: "Message",
                        textCapitalization: TextCapitalization.sentences,
                        textInputAction: TextInputAction.send,
                        maxLines: 5,
                        minLines: 1,
                        validator: (s) => s.length > 512 ? "Message too long" : null,
                        onFieldSubmitted: (string) => _sendMessage(),
                        underline: false,
                        isDense: true,
                        onChanged: _onTextInputChanged,
                      ),
                    ),
                  ),
                  IconButton(
                    materialIcon: Icons.send,
                    cupertinoIcon: CupertinoIcons.up_arrow,
                    onPressed: _sendMessage,
                    color: _isTextEmpty ? Colors.grey : null,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  String _formatDate(DateTime date) {
    var now = DateTime.now();
    return dateMatches(date, now)
        ? "Today"
        : weekMatches(date, now)
            ? DateFormat('EEEE').format(date)
            : yearMatches(date, now)
                ? localizations.formatShortMonthDay(date)
                : localizations.formatShortDate(date);
  }

  Widget _buildMessageBubble(
    Node<MessageData> msg,
    User usr,
    bool isUser,
    bool sequencedMessage,
    bool showName,
    bool showTime,
  ) {
    return Column(
      crossAxisAlignment: isUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: isUser ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            Flexible(
              child: Card(
                elevation: 0,
                color: isUser
                    ? (msg.localOnly != true
                        ? theme.primaryColor
                        : theme.primaryColor.withAlpha(128))
                    : theme.cardColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                margin: const EdgeInsets.all(0),
                child: Padding(
                  padding: msg.data.imagePresent != true
                      ? EdgeInsets.symmetric(horizontal: 12, vertical: 8)
                      : EdgeInsets.all(0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      if (showName)
                        Padding(
                          padding: EdgeInsets.only(bottom: 4),
                          child: Text(
                            "${usr.name}",
                            style: theme.textTheme.bodyText2
                                .copyWith(color: theme.primaryColor),
                            strutStyle: StrutStyle(
                              forceStrutHeight: true,
                            ),
                          ),
                        ),
                      if (msg.data.text != null)
                        SelectableText(
                          msg.data.text,
                          style: theme.textTheme.bodyText2.copyWith(
                            fontSize: 16,
                            color: isUser ? Colors.white : Colors.black,
                          ),
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      if (msg.data.imagePresent == true)
                        TouchTarget(
                          onTap: () => _onPhotoTapped(msg),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(15.0),
                            child: Container(
                              width: 200,
                              height: 200,
                              child: Hero(
                                tag: msg.id,
                                child: MessagePhotoThumbnail(widget.threadNode.id, msg),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ),
            if (!_sendingInProgressIds.contains(msg.id) && msg.localOnly == true)
              IconButton(
                  materialIcon: Icons.error,
                  cupertinoIcon: Icons.error,
                  onPressed: () => _onErrorButtonPressed(msg),
                  color: Colors.red),
          ],
        ),
        if (msg.localOnly == true)
          Padding(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Text(
              _sendingInProgressIds.contains(msg.id) ? "Sending..." : "Sending failed",
              style: theme.textTheme.caption,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
        if (showTime)
          Padding(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Text(
              localizations.formatTimeOfDay(TimeOfDay.fromDateTime(
                  DateTime.fromMillisecondsSinceEpoch(msg.creationTs))),
              style: theme.textTheme.caption,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
      ],
    );
  }

  @override
  Future<bool> sync() {
    return _nodeService.fetchChildren(widget.threadNode);
  }

  @override
  Future<void> loadState() async {
    var userNodes = await _nodeStore.getChildrenOfTypeWithUser<MessageData>(
      widget.threadNode,
    );

    _messages = List.unmodifiable(userNodes.map((e) => e.node));
    _users = List.unmodifiable(userNodes.map((e) => e.user));

    _updateCounterService.clearWithChildren(widget.threadNode.id);
  }

  void _onMediaTapped() async {
    var photos = await _multiPhotoPicker.getPhotos(context, 5);

    await showStreamProgressModal(
      title: "Sending",
      operationCount: photos.length,
      operationDescription: "Sending photo",
      stream: _chatService.sendPhotos(widget.parentNode, widget.threadNode, photos),
    );
  }

  void _sendMessage() async {
    var localNodeId = _uuid.v4();

    var text = _newMessageText.text.trim();
    if (text.isEmpty) {
      return;
    }

    var now = DateTime.now();
    var messageData = MessageData(text: text, imagePresent: false);
    var localNode = Node(
      nodeId: NodeId(
        id: localNodeId,
        parentId: widget.threadNode.id,
        type: NodeType.message,
      ),
      permissionGroupId: widget.parentNode.permissionGroupId,
      creatorId: _accountService.userId,
      creationTs: now.millisecondsSinceEpoch,
      lastUpdateTs: now.millisecondsSinceEpoch,
      jsonData: jsonEncode(messageData.toMap()),
      localOnly: true,
    );

    // This is better than setting the text to an empty string, because it applies
    // the capitalization of the first character immediately.
    _newMessageText.value = const TextEditingValue(
      selection: const TextSelection.collapsed(offset: 0),
    );
    _isTextEmpty = true;

    _sendingInProgressIds.add(localNodeId);

    () async {
      await _nodeService.addNodes([localNode]);
      await refresh();
    }();

    _sendLocalMessage(text, localNodeId);
  }

  void _sendLocalMessage(String text, String localOnlyNodeId) async {
    try {
      await _chatService.sendMessage(
        parentNode: widget.parentNode,
        thread: widget.threadNode,
        textContent: text,
      );
      await _nodeService.deleteNode(localOnlyNodeId);

      refresh();
    } finally {
      _sendingInProgressIds.remove(localOnlyNodeId);
    }
  }

  void _onErrorButtonPressed(Node<MessageData> messageNode) {
    var modalData = [
      ModalActionData<Function>(
        text: "Resend",
        icon: Icons.replay,
        value: () => _sendLocalMessage(messageNode.data.text, messageNode.id),
      ),
      ModalActionData<Function>(
        text: "Delete",
        icon: Icons.delete,
        value: () async {
          await _nodeService.deleteNode(messageNode.id);
          await refresh();
        },
      ),
    ];

    showModalSheet<Function>(
      context: context,
      actions: modalData,
    ).then((r) => r.call());
  }

  void _onTextInputChanged() {
    if (_isTextEmpty != _newMessageText.text.isEmpty) {
      setState(() {
        _isTextEmpty = _newMessageText.text.isEmpty;
      });
    }
  }

  void _onPhotoTapped(Node<MessageData> messageNode) async {
    await navigation.push(
      PhotoPreviewPage(
        getMessagePhotoProvider(widget.threadNode.id, messageNode),
        getMessagePhotoThumbnailProvider(widget.threadNode.id, messageNode),
        messageNode.id,
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) async {
    if (!notification.matches([Node, widget.threadNode])) {
      return false;
    }

    await refresh();

    if (clicked) {
      await _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 200),
        curve: Curves.ease,
      );
      return true;
    } else {
      return isCurrentPage && ViewVisibility.of(context) && _scrollController.offset == 0;
    }
  }
}
