import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/chats/group_chat_info_page.dart';
import 'package:planhive/views/core/chats/thread_view.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class GroupChatPage extends StatefulWidget {
  final Node<GroupChatData> chat;
  final Node<ThreadData> targetThread;

  GroupChatPage(this.chat, [this.targetThread]);

  @override
  State<StatefulWidget> createState() {
    return _GroupChatPageState();
  }
}

class _GroupChatPageState extends State<GroupChatPage> with ViewControllerMixin {
  final _nodeStore = GetIt.I<NodeStore>();

  Node<ThreadData> _threadNode;

  @override
  void initState() {
    super.initState();

    runAsync(loadLocal);
  }

  Future<void> loadLocal() async {
    var thread = widget.targetThread ??
        await _nodeStore.getFirstChildOfType<ThreadData>(widget.chat.id);

    setState(() {
      _threadNode = thread;
    });
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text(widget.chat.data.name),
      trailing: NavBarButton(
        onPressed: _onGroupChatInfoTapped,
        materialIcon: Icons.tune,
        cupertinoIcon: CupertinoIcons.info,
      ),
      safeAreaEnabled: true,
      safeAreaColor: theme.cardColor,
      body: _threadNode == null
          ? Container()
          : ThreadView(widget.chat, _threadNode, true, key: ValueKey(_threadNode.id)),
    );
  }

  void _onGroupChatInfoTapped() async {
    await NavigationService.instance.push(
      GroupChatInfoPage(
        widget.chat,
      ),
      modal: false,
    );
  }
}
