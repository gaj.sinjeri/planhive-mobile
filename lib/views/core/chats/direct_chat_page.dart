import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/chats/thread_view.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

import 'direct_chat_info_page.dart';

class DirectChatPage extends StatefulWidget {
  final Node<DirectChatData> chat;
  final Node<ThreadData> targetThread;

  DirectChatPage(this.chat, [this.targetThread]);

  @override
  State<StatefulWidget> createState() {
    return _DirectChatPageState();
  }
}

class _DirectChatPageState extends State<DirectChatPage> with ViewControllerMixin {
  final _nodeStore = GetIt.I<NodeStore>();
  final _chatService = GetIt.I<ChatService>();

  Node<ThreadData> _threadNode;
  String _userName = "";

  @override
  void initState() {
    super.initState();

    runAsync(loadLocal);
  }

  Future<void> loadLocal() async {
    var thread = widget.targetThread ??
        await _nodeStore.getFirstChildOfType<ThreadData>(widget.chat.id);
    var name = await _chatService.getChatName(widget.chat);

    setState(() {
      _threadNode = thread;
      _userName = name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text(_userName),
      trailing: NavBarButton(
        onPressed: _onDirectChatInfoTapped,
        materialIcon: Icons.tune,
        cupertinoIcon: CupertinoIcons.info,
      ),
      safeAreaEnabled: true,
      safeAreaColor: theme.cardColor,
      body: _threadNode == null
          ? Container()
          : ThreadView(widget.chat, _threadNode, false, key: ValueKey(_threadNode.id)),
    );
  }

  void _onDirectChatInfoTapped() async {
    await NavigationService.instance.push(
      DirectChatInfoPage(
        widget.chat,
        widget.targetThread,
      ),
      modal: false,
    );
  }
}
