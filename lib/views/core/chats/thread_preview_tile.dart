import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:planhive/common/image/event_cover_image_thumbnail.dart';
import 'package:planhive/common/image/group_chat_cover_image_thumbnail.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/common/time.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/message_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/badge/top_right_badge.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/tile.dart';

class ThreadPreviewTile extends StatefulWidget {
  final Node<ThreadData> thread;

  ThreadPreviewTile(this.thread) : super(key: ValueKey(thread.id));

  @override
  State<StatefulWidget> createState() {
    return ThreadPreviewTileState();
  }
}

class ThreadPreviewTileState extends State<ThreadPreviewTile>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _accountService = GetIt.I<AccountService>();
  final _chatService = GetIt.I<ChatService>();
  final _nodeService = GetIt.I<NodeService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();
  final _nodeStore = GetIt.I<NodeStore>();

  Node _groupNode;
  User _contact;

  String _groupName = "";
  String _groupType = "";

  String _lastMessageSender;
  String _lastMessageTime;
  String _messagePreview;

  int _badgeCounter = 0;

  @override
  Future<bool> sync() {
    return _nodeService.fetchChildren(widget.thread);
  }

  @override
  Future<void> loadState() async {
    await Future.wait([
      _getChatDetails(),
      _getMessagePreview(),
      _getBadgeCounter(),
    ]);
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([Node, widget.thread])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  Future<void> _getBadgeCounter() async {
    _badgeCounter = await _updateCounterService.countChildren(widget.thread.id, [
      NodeType.message,
    ]);
  }

  Future<void> _getChatDetails() async {
    _groupNode = await _nodeService.getNode(widget.thread.parentId);

    if(_groupNode == null) {
      return;
    }

    switch (_groupNode.type) {
      case NodeType.chatDirect:
        _groupName = await _chatService.getChatName(_groupNode.as<DirectChatData>());
        _groupType = "";
        _contact = await _chatService.getDirectChatUser(_groupNode.id);
        break;
      case NodeType.chatGroup:
        _groupName = _groupNode.as<GroupChatData>().data.name;
        _groupType = "Group";
        break;
      case NodeType.event:
        _groupName = _groupNode.as<EventData>().data.name;
        _groupType = "Event";
        break;
      default:
        throw UnsupportedError(
            "Unsupported parent: $_groupNode for thread ${widget.thread}");
    }
  }

  Future<void> _getMessagePreview() async {
    var messages = await _nodeStore.getChildrenOfTypeWithUser<MessageData>(
      widget.thread,
      1,
    );

    if (messages.length > 0) {
      var msg = messages.first;
      if (msg.user.id == _accountService.userId) {
        _lastMessageSender = "You";
      } else {
        _lastMessageSender = msg.user.name;
      }

      var now = DateTime.now();
      var lastUpdate = DateTime.fromMillisecondsSinceEpoch(msg.node.lastUpdateTs);
      _lastMessageTime = dateMatches(lastUpdate, now)
          ? localizations.formatTimeOfDay(TimeOfDay.fromDateTime(lastUpdate))
          : weekMatches(lastUpdate, now)
              ? DateFormat('EEEE').format(lastUpdate)
              : yearMatches(lastUpdate, now)
                  ? localizations.formatShortMonthDay(lastUpdate)
                  : localizations.formatShortDate(lastUpdate);

      var msgData = msg.node.data;
      _messagePreview = msgData.imagePresent ? "Image" : msgData.text;
    } else {
      _lastMessageSender = null;
      _messagePreview = null;
    }
  }

  void _openThread() {
    GetIt.I<NavigationService>().navigateToNode(widget.thread);
  }

  @override
  Widget build(BuildContext context) {
    return TopRightBadge(
      _badgeCounter,
      top: 12,
      right: 16,
      child: Tile(
        margin: const EdgeInsets.symmetric(vertical: 4),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        leading: _getGroupPhotoWidget(),
        onTap: _openThread,
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: [
                Expanded(
                  child: Text(
                    _groupName,
                    style: theme.textTheme.subtitle1,
                    overflow: TextOverflow.ellipsis,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
                if (_badgeCounter == 0)
                  Text(
                    _groupType,
                    style: theme.textTheme.caption.copyWith(color: theme.primaryColor),
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
              ],
            ),
            SizedBox(height: 4),
            Padding(
              padding: EdgeInsets.only(bottom: 6),
              child: RichText(
                text: TextSpan(
                  text: _lastMessageSender != null ? "$_lastMessageSender: " : "",
                  style: ThemeUtility.caption1(theme.textTheme)
                      .copyWith(color: theme.primaryColor),
                  children: <TextSpan>[
                    TextSpan(
                      text: _messagePreview ?? "There are no messages yet.",
                      style: ThemeUtility.caption1(theme.textTheme),
                    ),
                  ],
                ),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Text(
                _lastMessageTime ?? "",
                style: theme.textTheme.caption.copyWith(color: theme.primaryColor),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _getGroupPhotoWidget() {
    if (_groupNode == null) return SizedBox(height: 64, width: 64);

    switch (_groupNode.type) {
      case NodeType.chatDirect:
        return SizedBox(
          height: 64,
          width: 64,
          child: _contact != null
              ? UserAvatar(
                  user: _contact,
                  avatarSize: AvatarSize.thumbnail,
                  placeholderSize: 32,
                )
              : Container(),
        );
      case NodeType.chatGroup:
        return SizedBox(
          height: 64,
          width: 64,
          child: ClipOval(
            child: Image(
              image: getGroupChatCoverImageThumbnail(_groupNode.as<GroupChatData>()),
              fit: BoxFit.cover,
            ),
          ),
        );
      case NodeType.event:
        return SizedBox(
          height: 64,
          width: 64,
          child: ClipOval(
            child: Image(
              image: getEventCoverImageThumbnail(_groupNode.as<EventData>()),
              fit: BoxFit.cover,
            ),
          ),
        );
      default:
        throw UnsupportedError(
            "Unsupported parent: $_groupNode for thread ${widget.thread}");
    }
  }
}
