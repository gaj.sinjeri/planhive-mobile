import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/event_cover_image_thumbnail.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/event/event_preview_data.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/node/timeline_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/badge/top_right_badge.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/event_main_page.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class EventPreview extends StatefulWidget {
  final EventPreviewData previewData;

  EventPreview(this.previewData) : super(key: ValueKey(previewData.event.id));

  @override
  State<StatefulWidget> createState() {
    return _EventPreviewState();
  }
}

class _EventPreviewState extends State<EventPreview>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _accountService = GetIt.I<AccountService>();
  final _userService = GetIt.I<UserService>();
  final _nodeService = GetIt.I<NodeService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();
  final _nodeStore = GetIt.I<NodeStore>();

  Node<EventData> _event;

  User _creator;
  DateTime _startTime;
  String _activityTitle;
  bool _isActivityCanceled = false;
  int _participantsCount;
  int _unreadCounter = 0;

  @override
  void initState() {
    _event = widget.previewData.event;

    if (widget.previewData.activity != null) {
      _setStateFromMainActivity(widget.previewData.activity.data,
          widget.previewData.activity.data.state == ActivityState.canceled);
    }

    super.initState();
  }

  Future<bool> sync() async {
    var res = [
      await _nodeService.fetchChildren(_event),
    ];

    res.add(await _syncParticipation());

    return res.contains(true);
  }

  Future<bool> _syncParticipation() async {
    return await _nodeService.fetchChildren(
      await _nodeStore.getFirstChildOfType<ParticipationData>(_event.id),
    );
  }

  @override
  Future<void> loadState() async {
    var eventNode = await _nodeService.getNode(_event.id);

    if (eventNode == null) {
      // event was probably deleted
      return;
    }

    _event = eventNode.as<EventData>();

    await Future.wait([
      _getUser(),
      _getEventHighlights(),
      _getUnreadCounter(),
    ]);
  }

  Future<void> _getUser() async {
    _creator = await _userService.getUserById(_event.creatorId);
  }

  Future<void> _getEventHighlights() async {
    var children = await _nodeStore.getChildren(_event);
    var timeline = children
        .firstWhere(
          (e) => e.type == NodeType.timeline,
          orElse: () => null,
        )
        ?.as<TimelineData>();
    var participation = children
        .firstWhere(
          (e) => e.type == NodeType.participation,
          orElse: () => null,
        )
        ?.as<ParticipationData>();

    await Future.wait([
      if (timeline != null) _getActivityInfo(timeline),
      if (participation != null) _getParticipationInfo(participation),
    ]);
  }

  Future<void> _getUnreadCounter() async {
    _unreadCounter = await _updateCounterService.countChildren(_event.id, [
      NodeType.participant,
      NodeType.activity,
    ]);
  }

  Future<void> _getActivityInfo(Node<TimelineData> timeline) async {
    var activities = await _nodeStore.getChildrenOfType<ActivityData>(timeline.id);

    var approvedActivities =
        activities.where((e) => e.data.state == ActivityState.approved);
    var canceledActivities =
        activities.where((e) => e.data.state == ActivityState.canceled);

    ActivityData activity;
    bool isActivityCanceled = false;

    if (approvedActivities.isNotEmpty) {
      activity =
          approvedActivities.reduce((a, b) => a.data.compareTo(b.data) < 0 ? a : b).data;
    } else {
      if (canceledActivities.isEmpty) {
        _startTime = null;
        _activityTitle = null;
        return;
      } else {
        activity = canceledActivities
            .reduce((a, b) => a.data.compareTo(b.data) < 0 ? a : b)
            .data;
        isActivityCanceled = true;
      }
    }

    _setStateFromMainActivity(activity, isActivityCanceled);
  }

  void _setStateFromMainActivity(ActivityData activity, bool isActivityCanceled) {
    _startTime = DateTime.fromMillisecondsSinceEpoch(activity.time.startTs);
    _activityTitle = activity.title;
    _isActivityCanceled = isActivityCanceled;
  }

  Future<void> _getParticipationInfo(Node<ParticipationData> participation) async {
    var participants = await _nodeStore.getParticipants(participation);

    var participantsCount = 0;
    participants.forEach((_, value) {
      participantsCount += value.length;
    });

    _participantsCount = participantsCount;
  }

  void _openEvent() async {
    await navigation.push(EventPage(_event));

    refresh(false);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return TopRightBadge(
      _unreadCounter + (_event.unread ? 1 : 0),
      top: 16,
      right: 19.5,
      empty: true,
      child: Container(
        height: 104,
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Card(
          elevation: 0,
          margin: EdgeInsets.only(top: 0),
          child: TouchTarget(
            onTap: _openEvent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: _event.id,
                  child: Container(
                    width: 120,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: getEventCoverImageThumbnail(_event),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        _event.data.name,
                        style: textTheme.subtitle1,
                        strutStyle: StrutStyle(
                          forceStrutHeight: true,
                        ),
                      ),
                      _buildActivityDataRow(),
                      Spacer(),
                      _buildPeopleRow(textTheme),
                    ],
                  ),
                ),
                SizedBox(width: 16),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildActivityDataRow() {
    if (_event.data.eventState == EventState.canceled)
      return Padding(
        padding: EdgeInsets.only(top: 12),
        child: Text(
          "Event Canceled",
          style: theme.textTheme.caption.copyWith(color: Colors.red),
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      );

    return Padding(
      padding: EdgeInsets.only(top: 12),
      child: _activityTitle != null || _startTime != null
          ? Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                if (_startTime != null && !_isActivityCanceled)
                  Padding(
                    padding: EdgeInsets.only(right: 8),
                    child: Text(
                      localizations.formatTimeOfDay(
                        TimeOfDay.fromDateTime(_startTime),
                      ),
                      style: theme.textTheme.caption,
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                if (_activityTitle != null)
                  RichText(
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                      text: _activityTitle,
                      style: theme.textTheme.caption,
                      children: <TextSpan>[
                        if (_isActivityCanceled)
                          TextSpan(
                            text: " (Canceled)",
                            style: theme.textTheme.caption.copyWith(color: Colors.red),
                          ),
                      ],
                    ),
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
              ],
            )
          : Text(
              "There is nothing planned yet",
              style: theme.textTheme.caption,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
    );
  }

  Widget _buildPeopleRow(TextTheme textTheme) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8),
      child: Row(
        children: <Widget>[
          if (_participantsCount != null)
            _iconTextLine(
              "$_participantsCount",
            ),
          Spacer(),
          if (_creator != null)
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  _creator.id == _accountService.userId ? "Me" : _creator.name,
                  style: textTheme.caption,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                SizedBox(width: 8),
                SizedBox(
                  width: 28,
                  height: 28,
                  child: UserAvatar(
                    user: _creator,
                    avatarSize: AvatarSize.micro,
                    placeholderSize: 24,
                  ),
                ),
              ],
            ),
        ],
      ),
    );
  }

  Widget _iconTextLine(String text) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        PlatformIcon(
          Icons.people,
          CupertinoIcons.group_solid,
          color: theme.primaryColor,
        ),
        SizedBox(width: 16),
        Text(
          text,
          style: theme.textTheme.caption,
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      ],
    );
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([
      _event,
      [NodeType.participation, NodeType.timeline]
    ])) {
      refresh();
    }

    return SynchronousFuture(false);
  }
}
