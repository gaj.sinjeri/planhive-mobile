import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/time.dart';
import 'package:planhive/data/event/event_preview_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/views/common/badge/top_right_badge.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/common/views/scroll_view_placeholder.dart';
import 'package:planhive/views/core/event/create_event_page.dart';
import 'package:planhive/views/core/event/event_preview.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class EventsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EventsPageState();
  }
}

class _EventsPageState extends State<EventsPage>
    with
        ViewControllerMixin,
        RefreshControllerMixin,
        NotificationHandlerMixin,
        TickerProviderStateMixin {
  final _eventService = GetIt.I<EventService>();
  final _updateCounterChanges = GetIt.I<UpdateCounterService>();

  List<EventPreviewData> _eventsPlanning = const [];
  List<List<EventPreviewData>> _eventsUpcoming = const [];
  List<List<EventPreviewData>> _eventsPast = const [];

  int _planningCounter = 0;
  int _upcomingCounter = 0;
  int _pastCounter = 0;

  bool get _hasAnyEvent =>
      _eventsPlanning.isNotEmpty || _eventsUpcoming.isNotEmpty || _eventsPast.isNotEmpty;

  TabController _tabController;

  Widget get _placeholder => _hasAnyEvent
      ? const Text("There is nothing to show in this tab.")
      : const Text("Tap the + button to create your first event.");

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

    _tabController.dispose();
  }

  void _startEventCreation() {
    navigation.push(CreateEventPage());
  }

  @override
  Future<void> loadState() async {
    var events = await _eventService.getMainActivities();

    var eventsPlanning = List.of(
      events.where((e) => e.section == EventPreviewSection.undecided),
    );
    eventsPlanning.sort(EventPreviewData.lastChangeComparator);
    _eventsPlanning = List.unmodifiable(eventsPlanning);

    var eventsUpcoming = List.of(
      events.where((e) => e.section == EventPreviewSection.upcoming),
    );
    eventsUpcoming.sort(EventPreviewData.activityTimeComparator);
    _eventsUpcoming = List.unmodifiable(_generateEventList(eventsUpcoming));

    var eventsPast = List.of(
      events.where((e) => e.section == EventPreviewSection.past),
    );
    eventsPast.sort(EventPreviewData.activityTimeComparator);
    _eventsPast = List.unmodifiable(_generateEventList(eventsPast));

    // compute update counters for tabs
    {
      var counters = await _updateCounterChanges.getUnreadRoots(
        [NodeType.activity, NodeType.participant, NodeType.event],
      );

      _planningCounter = eventsPlanning.fold(
        0,
        (prev, e) => counters.contains(e.event.id) ? prev + 1 : prev,
      );

      _upcomingCounter = eventsUpcoming.fold(
        0,
        (prev, e) => counters.contains(e.event.id) ? prev + 1 : prev,
      );

      _pastCounter = eventsPast.fold(
        0,
        (prev, e) => counters.contains(e.event.id) ? prev + 1 : prev,
      );
    }
  }

  List<List<EventPreviewData>> _generateEventList(List<EventPreviewData> events) {
    List<List<EventPreviewData>> list = [];

    var dt = DateTime.fromMillisecondsSinceEpoch(0);
    var index = -1;
    for (var event in events) {
      var activityStartTime =
          DateTime.fromMillisecondsSinceEpoch(event.activity.data.time.startTs);

      if (!dateMatches(dt, activityStartTime)) {
        dt = DateTime(
            activityStartTime.year, activityStartTime.month, activityStartTime.day);

        index++;
        list.insert(index, []);
      }

      list[index].add(event);
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Events"),
      trailing: NavBarButton(
        onPressed: _startEventCreation,
        materialIcon: Icons.add,
        cupertinoIcon: CupertinoIcons.add,
      ),
      body: Column(
        children: <Widget>[
          Container(
            color: theme.backgroundColor,
            height: 40,
            child: TabBar(
              controller: _tabController,
              labelColor: Theme.of(context).primaryColor,
              tabs: [
                _TabBarBadge(
                  _planningCounter + _upcomingCounter,
                  child: Tab(text: "Upcoming"),
                ),
                _TabBarBadge(
                  _pastCounter,
                  child: Tab(text: "Past"),
                ),
              ],
            ),
          ),
          Divider(
            height: 0,
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                _EventList(
                  onRefresh: () => refresh(true),
                  undecidedEvents: _eventsPlanning,
                  upcomingEvents: _eventsUpcoming,
                  placeholder: _placeholder,
                ),
                _PastEventsList(
                  onRefresh: () => refresh(true),
                  events: _eventsPast,
                  placeholder: _placeholder,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([NodeType.event])) {
      refresh();
    }

    return SynchronousFuture(false);
  }
}

class _EventList extends StatelessWidget {
  final List<EventPreviewData> undecidedEvents;
  final List<List<EventPreviewData>> upcomingEvents;
  final void Function() onRefresh;
  final Widget placeholder;

  _EventList({
    this.undecidedEvents,
    this.upcomingEvents,
    this.onRefresh,
    this.placeholder,
  });

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [];

    if (undecidedEvents.isNotEmpty)
      widgets.add(_buildSectionHeader(context, "Still Planning"));
    widgets.addAll(undecidedEvents.map((e) => EventPreview(e)));

    if (upcomingEvents.isNotEmpty) widgets.add(_buildSectionHeader(context, "Upcoming"));
    widgets.addAll(upcomingEvents.map((e) => _buildDayView(context, e)));

    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ScrollViewPlaceholder(
        hasContent: undecidedEvents.isNotEmpty || upcomingEvents.isNotEmpty,
        content: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 8),
          itemBuilder: (context, index) {
            return widgets[index];
          },
          itemCount: widgets.length,
        ),
        placeholder: placeholder,
      ),
    );
  }
}

Widget _buildSectionHeader(BuildContext context, String title) {
  var theme = Theme.of(context);

  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 16, left: 16),
        child: Text(
          title,
          style: theme.textTheme.headline6.copyWith(color: theme.primaryColor),
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      ),
      Center(
        child: Divider(
          indent: 16,
          height: 16,
        ),
      ),
    ],
  );
}

class _PastEventsList extends StatelessWidget {
  final List<List<EventPreviewData>> events;
  final void Function() onRefresh;
  final Widget placeholder;

  _PastEventsList({this.events, this.onRefresh, this.placeholder});

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ScrollViewPlaceholder(
        hasContent: events.length != 0,
        content: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 8),
          itemBuilder: (context, index) {
            return _buildDayView(context, events[index]);
          },
          itemCount: events.length,
        ),
        placeholder: placeholder,
      ),
    );
  }
}

Widget _buildDayView(BuildContext context, List<EventPreviewData> events) {
  List<Widget> widgets = [
    Container(
      padding: EdgeInsets.only(left: 16),
      margin: EdgeInsets.only(top: 16, bottom: 8),
      child: Text(
        events.first.activity.data.time.getStartDateText(context),
        style: Theme.of(context)
            .textTheme
            .bodyText2
            .copyWith(color: Theme.of(context).primaryColor),
        textAlign: TextAlign.left,
        strutStyle: StrutStyle(
          forceStrutHeight: true,
        ),
      ),
    )
  ];
  widgets.addAll(events.map((event) => EventPreview(event)));

  return Column(
    mainAxisSize: MainAxisSize.min,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: widgets,
  );
}

class _TabBarBadge extends TopRightBadge {
  _TabBarBadge(int counter, {@required Widget child, Key key})
      : super(counter, top: 3, right: -23, child: child, key: key);
}
