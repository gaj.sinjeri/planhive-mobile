import 'package:clipboard/clipboard.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide Switch, IconButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/links/invite_link_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/user_tile.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/icon_button.dart';
import 'package:planhive/views/cross_platform/round_checkbox.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/switch.dart';

class InviteToEventPage extends StatefulWidget {
  final Node<EventData> eventNode;
  final List<User> participants;

  InviteToEventPage(this.eventNode, this.participants);

  @override
  State<StatefulWidget> createState() {
    return _InviteToEventPageState();
  }
}

class _InviteToEventPageState extends State<InviteToEventPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _contactService = GetIt.I<ContactService>();
  final _eventService = GetIt.I<EventService>();
  final _inviteLinkService = GetIt.I<InviteLinkService>();
  final _userService = GetIt.I<UserService>();

  Node<EventData> _eventNode;

  List<User> _contacts = [];
  final _selection = Set<User>();

  String _inviteLink;
  bool _inviteLinkEnabled = false;
  bool _showCopiedToClipboardMessage = false;

  @override
  void initState() {
    _eventNode = widget.eventNode;
    _inviteLinkEnabled = _eventNode.data.inviteCode != null;

    super.initState();
  }

  @override
  Future<bool> sync() async {
    return (await _contactService.fetchUpdates()).isNotEmpty;
  }

  @override
  Future<void> loadState() async {
    var connectedUsers = await _userService.getConnectedUsers();
    _contacts = List<User>.unmodifiable(
      connectedUsers.where(
        (connectedUser) => !widget.participants.contains(connectedUser),
      ),
    );

    _inviteLink = _eventNode.data.inviteCode != null
        ? (await _inviteLinkService.shareInviteLink(_eventNode)).toString()
        : null;
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([User])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> userTiles = [];

    for (int i = 0; i < _contacts.length; i++) {
      var contact = _contacts[i];

      userTiles.add(
        UserTile(
          user: contact,
          onTap: () => _toggleSelected(contact),
          trailing: RoundCheckBox(_selection.contains(contact)),
          withTopDivider: i == 0 ? true : false,
        ),
      );
    }

    return PageScaffold(
      title: Text("Invite People"),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: () => navigation.pop(false),
      ),
      trailing: _selection.length != 0
          ? NavBarButton(
              text: "Invite",
              materialIcon: Icons.done,
              onPressed: _sendInvites,
            )
          : null,
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: 16,
                right: 16,
                top: 16,
                bottom: _inviteLink != null ? 16 : 0,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      "Enable Invite Link",
                      style: theme.textTheme.subtitle1,
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                  Switch(
                    value: _inviteLinkEnabled,
                    onChanged: _onInviteLinkSwitchToggled,
                  ),
                ],
              ),
            ),
            if (_inviteLink != null)
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  "Share this link to invite people directly to the event, "
                  "even if they are not on PlanHive yet.",
                  style: theme.textTheme.caption,
                  textAlign: TextAlign.center,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            if (_inviteLink != null)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SelectableText(
                    _inviteLink,
                    style: theme.textTheme.caption,
                    textAlign: TextAlign.center,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                  IconButton(
                    materialIcon: Icons.content_copy,
                    cupertinoIcon: Icons.content_copy,
                    onPressed: _onCopyToClipboardTapped,
                  ),
                ],
              ),
            if (_showCopiedToClipboardMessage)
              Text(
                "Copied to clipboard",
                style: theme.textTheme.caption.copyWith(color: Colors.green),
                textAlign: TextAlign.center,
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            SizedBox(height: 8),
            Divider(height: 0),
            Expanded(
              child: userTiles.isNotEmpty
                  ? ListView(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      children: userTiles,
                    )
                  : Center(
                      child: Text(
                        "There is no one else to invite at the moment.",
                        style: ThemeUtility.caption1(theme.textTheme),
                        strutStyle: StrutStyle(
                          forceStrutHeight: true,
                        ),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }

  void _onInviteLinkSwitchToggled(bool selected) {
    runAsync(() async {
      var eventNode = await _eventService.updateEventData(
        _eventNode,
        inviteLinkEnabled: selected,
      );
      var inviteLink = eventNode.data.inviteCode != null
          ? (await _inviteLinkService.shareInviteLink(eventNode)).toString()
          : null;

      setState(() {
        _eventNode = eventNode;
        _inviteLinkEnabled = selected;
        _inviteLink = inviteLink;
      });
    }, exclusive: true);
  }

  void _onCopyToClipboardTapped() {
    FlutterClipboard.copy(_inviteLink).then((_) {
      setState(() {
        _showCopiedToClipboardMessage = true;
      });
      Future.delayed(Duration(seconds: 2)).then((_) {
        setState(() {
          _showCopiedToClipboardMessage = false;
        });
      });
    });
  }

  void _toggleSelected(User user) {
    if (_selection.contains(user)) {
      _selection.remove(user);
    } else {
      _selection.add(user);
    }

    setState(() {});
  }

  void _sendInvites() {
    runAsync(() async {
      await _eventService.invite(widget.eventNode, _selection);
      navigation.pop(true);
    }, exclusive: true);
  }
}
