import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide TextFormField, Switch, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/event_cover_image_provider.dart';
import 'package:planhive/common/image/image_utils.dart';
import 'package:planhive/common/image/pick_and_crop.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/data/node/activity/time_range.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/center/timeline/activity_location_widget.dart';
import 'package:planhive/views/core/event/center/timeline/activity_time_widget.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/switch.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class CreateEventPage extends StatefulWidget {
  final Node<EventData> eventNode;

  CreateEventPage({
    this.eventNode,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CreateEventPageState();
  }
}

class _CreateEventPageState extends State<CreateEventPage> with ViewControllerMixin {
  final _eventService = GetIt.I<EventService>();

  final _formKey = GlobalKey<FormState>();

  var _nameInput = TextEditingController();
  var _descriptionInput = TextEditingController();

  FocusNode _nameInputFocusNode;
  FocusNode _descriptionInputFocusNode;

  ImagePickAndCrop _imagePickAndCrop;
  ImageProvider _image;

  bool _withMainActivity = false;
  bool _imageChanged = false;

  DateTime _startDate;
  DateTime _endDate;
  bool _hasEndDate = false;
  bool _timeChanged = false;
  Location _location;

  DateTime now = DateTime.now();

  @override
  void initState() {
    super.initState();

    _imagePickAndCrop = ImagePickAndCrop(
      context,
      Size(eventCoverPhotoWidth.toDouble(), eventCoverPhotoHeight.toDouble()),
    );

    _nameInputFocusNode = FocusNode();
    _descriptionInputFocusNode = FocusNode();

    _startDate = DateTime.now();
    _endDate = DateTime.now();

    runAsync(_loadExistingData, errorDialog: false);
  }

  @override
  void dispose() {
    super.dispose();

    _imagePickAndCrop.dispose();
    _nameInput.dispose();
    _descriptionInput.dispose();
    _nameInputFocusNode.dispose();
    _descriptionInputFocusNode.dispose();
  }

  Future<void> _loadExistingData() async {
    if (widget.eventNode != null) {
      _nameInput.text = widget.eventNode.data.name;
      _descriptionInput.text = widget.eventNode.data.description ?? "";

      if (widget.eventNode.data.hasCoverImage) {
        await _imagePickAndCrop.setImage(await resolveProvider(
          EventCoverImageProvider(widget.eventNode),
        ));
      }

      if (widget.eventNode.data.lastCoverUpdateTs != 0) {
        setState(() {
          _image = _imagePickAndCrop.imageProvider;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      leading: NavBarButton(
        onPressed: _onDiscard,
        materialIcon: Icons.close,
        text: "Cancel",
      ),
      title: Text(widget.eventNode == null ? "Create Event" : "Edit Event"),
      trailing: NavBarButton(
        text: "Done",
        materialIcon: Icons.done,
        onPressed: _submitForm,
      ),
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          padding: EdgeInsets.only(bottom: 16),
          children: <Widget>[
            TouchTarget(
              onTap: _onEditCoverImage,
              child: AspectRatio(
                aspectRatio: 2,
                child: Container(
                  color: theme.primaryColor.withOpacity(0.3),
                  child: _image != null
                      ? Image(
                          image: _image,
                        )
                      : Icon(
                          Icons.add_photo_alternate,
                          color: theme.primaryColor,
                          size: 40,
                        ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: TextButton(
                text: _imagePickAndCrop.imageProvider == null
                    ? "Add a Cover Photo - Optional"
                    : "Change Cover Photo",
                onPressed: _onEditCoverImage,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    TextFormField(
                      controller: _nameInput,
                      focusNode: _nameInputFocusNode,
                      textInputAction: TextInputAction.next,
                      textCapitalization: TextCapitalization.words,
                      keyboardType: TextInputType.text,
                      labelText: "Title",
                      validator: _nameValidator,
                      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    TextFormField(
                      controller: _descriptionInput,
                      focusNode: _descriptionInputFocusNode,
                      textInputAction: TextInputAction.done,
                      textCapitalization: TextCapitalization.sentences,
                      keyboardType: TextInputType.text,
                      labelText: "Description (Optional)",
                      validator: _descriptionValidator,
                      minLines: 1,
                      maxLines: 5,
                      onFieldSubmitted: (_) => FocusScope.of(context).unfocus(),
                    ),
                    if (widget.eventNode == null) SizedBox(height: 16),
                    if (widget.eventNode == null)
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "Add Time and Location",
                              style: theme.textTheme.subtitle1,
                              strutStyle: StrutStyle(
                                forceStrutHeight: true,
                              ),
                            ),
                          ),
                          Switch(
                            value: _withMainActivity,
                            onChanged: (selected) {
                              setState(() {
                                _withMainActivity = selected;
                              });
                            },
                          ),
                        ],
                      ),
                    if (_withMainActivity) SizedBox(height: 8),
                    if (_withMainActivity)
                      ActivityTimeWidget(
                        startDate: DateTime(now.year, now.month, now.day, now.hour + 1),
                        endDate: DateTime(now.year, now.month, now.day, now.hour + 1)
                            .add(Duration(hours: 1)),
                        hasEndDate: false,
                        onChanged:
                            (DateTime startDate, DateTime endDate, bool hasEndDate) {
                          setState(() {
                            _startDate = startDate;
                            _endDate = endDate;
                            _hasEndDate = hasEndDate;
                            _timeChanged = true;
                          });
                        },
                      ),
                    if (_withMainActivity) Divider(height: 16),
                    if (_withMainActivity)
                      ActivityLocationWidget(
                        onChanged: (Location location) {
                          setState(() {
                            _location = location;
                          });
                        },
                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _nameValidator(String input) {
    return EventData.nameRegExp.hasMatch(input)
        ? null
        : "Please enter a name between 1 and 25 characters";
  }

  String _descriptionValidator(String input) {
    return EventData.descriptionRegExp.hasMatch(input)
        ? null
        : "Please enter a description up to 512 characters.";
  }

  void _onEditCoverImage() async {
    if (await _imagePickAndCrop.pickImage(context) == true) {
      _imageChanged = true;
    }

    setState(() {
      _image = _imagePickAndCrop.imageProvider;
    });
  }

  void _onDiscard() async {
    if (_changesMade()) {
      var alertData = [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ];
      var decision = await showAlertDialog<bool>(
        context: context,
        title: "Discard Changes?",
        actions: alertData,
      );
      if (decision == true) {
        navigation.pop();
      }
    } else {
      navigation.pop();
    }
  }

  bool _changesMade() {
    if (widget.eventNode != null) {
      if (widget.eventNode.data.name != _nameInput.text) return true;
      if ((widget.eventNode.data.description ?? "") != _descriptionInput.text)
        return true;
      if (_image == null && widget.eventNode.data.lastCoverUpdateTs == 0) return false;
      if (_imageChanged) return true;
      if (_withMainActivity) {
        if (_timeChanged) return true;
        if (_location != null) return true;
      }
      return false;
    }

    return _image != null ||
        _nameInput.text.isNotEmpty ||
        _descriptionInput.text.isNotEmpty;
  }

  void _submitForm() async {
    if (!_changesMade()) {
      navigation.pop();
      return;
    }

    if (_nameValidator(_nameInput.text) != null) {
      await showAlertDialog(
        title: "Event Title",
        content: "Please enter a valid title.",
        actions: [AlertActionData(text: "OK")],
      );
    } else if (_descriptionValidator(_descriptionInput.text) != null) {
      await showAlertDialog(
        title: "Event Description",
        content: "Please enter a valid description.",
        actions: [AlertActionData(text: "OK")],
      );
    } else if (_withMainActivity && _startDate.isAfter(_endDate)) {
      await showAlertDialog(
        context: context,
        title: "Invalid Time",
        content: "The activity must end after it begins.",
        actions: [AlertActionData(text: "Ok", value: true)],
      );
      return;
    } else {
      if (widget.eventNode == null) {
        runAsync(() async {
          var coverImage = await _imagePickAndCrop.toImage();
          var eventNode = await _eventService.createEvent(
            _nameInput.text,
            _descriptionInput.text,
            coverImage,
            _withMainActivity
                ? TimeRange.fromDateTime(_startDate, _hasEndDate ? _endDate : null)
                : null,
            _location,
          );
          navigation.navigateToNode(eventNode);
        }, exclusive: true);
      } else {
        runAsync(() async {
          await _eventService.updateEventData(
            widget.eventNode,
            name: _nameInput.text,
            description: _descriptionInput.text,
            image: _imageChanged ? await _imagePickAndCrop.toImage() : null,
            imageChanged: _imageChanged,
          );

          navigation.pop();
        }, exclusive: true);
      }
    }
  }
}
