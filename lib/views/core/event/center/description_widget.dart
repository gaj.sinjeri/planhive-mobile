import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class DescriptionWidget extends StatefulWidget {
  final String description;

  const DescriptionWidget(this.description);

  @override
  State<StatefulWidget> createState() {
    return _DescriptionWidgetState();
  }
}

class _DescriptionWidgetState extends State<DescriptionWidget> with ViewControllerMixin {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16),
          child: TouchTarget(
            onTap: _toggleExpanded,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  "Description",
                  textAlign: TextAlign.left,
                  style: theme.textTheme.subtitle1,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(
                  widget.description,
                  textAlign: TextAlign.left,
                  style: ThemeUtility.caption1(theme.textTheme),
                  overflow: TextOverflow.fade,
                  maxLines: _expanded ? 10 : 2,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void _toggleExpanded() {
    setState(() {
      _expanded = !_expanded;
    });
  }
}
