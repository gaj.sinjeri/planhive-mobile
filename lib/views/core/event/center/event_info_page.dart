import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField, IconButton, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/event_cover_image.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_permission.dart';
import 'package:planhive/services/chat_service.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/user_tile.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/core/event/create_event_page.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/tile_button.dart';

class EventInfoPage extends StatefulWidget {
  final Node<EventData> eventNode;

  EventInfoPage(this.eventNode);

  @override
  State<StatefulWidget> createState() {
    return _EventInfoPageState();
  }
}

class _EventInfoPageState extends State<EventInfoPage>
    with ViewControllerMixin, RefreshControllerMixin, NotificationHandlerMixin {
  final _chatService = GetIt.I<ChatService>();
  final _eventService = GetIt.I<EventService>();
  final _nodeService = GetIt.I<NodeService>();
  final _permissionService = GetIt.I<PermissionService>();

  Node<EventData> _eventNode;

  var _isOrganizer = false;
  var _isAdmin = false;

  User _organizer;
  List<UserPermission> _members = [];

  @override
  void initState() {
    _eventNode = widget.eventNode;

    super.initState();
  }

  @override
  Future<bool> sync() {
    return _permissionService.fetchUpdates();
  }

  @override
  Future<void> loadState() async {
    _eventNode = await _nodeService.getNode(_eventNode.id);

    var localPermission =
        await _permissionService.getUserPermission(_eventNode.permissionGroupId);
    var userPermissions =
        await _permissionService.getContactPermissions(_eventNode.permissionGroupId);

    _isOrganizer = localPermission.user.id == _eventNode.creatorId;
    _isAdmin = localPermission.permission.can(Permissions.write);

    _organizer = userPermissions
        .firstWhere(
          (e) => e.user.id == _eventNode.creatorId,
          orElse: () => null,
        )
        ?.user;

    _members = _constructMembersList(localPermission, userPermissions);
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([_eventNode])) {
      refresh();
    }

    return SynchronousFuture(false);
  }

  List<UserPermission> _constructMembersList(
      UserPermission localPermission, Iterable<UserPermission> userPermissions) {
    var admins = userPermissions
        .where((e) =>
            e.permission.can(Permissions.write) && e.user.id != localPermission.user.id)
        .toList(growable: false);
    admins.sort((a, b) => a.user.name.toLowerCase().compareTo(b.user.name.toLowerCase()));

    var regulars = userPermissions
        .where((e) => !admins.contains(e) && e.user.id != localPermission.user.id)
        .toList(growable: false);
    regulars
        .sort((a, b) => a.user.name.toLowerCase().compareTo(b.user.name.toLowerCase()));

    var members = [localPermission];
    members.addAll(admins);
    members.addAll(regulars);

    return members;
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Event Info"),
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          children: <Widget>[
            _getEventDataWidget(),
            if (_eventNode.data.eventState == EventState.canceled) SizedBox(height: 16),
            if (_eventNode.data.eventState == EventState.canceled)
              Center(
                child: Text(
                  "Event has been canceled",
                  style: theme.textTheme.subtitle1.copyWith(color: Colors.red),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            SizedBox(height: 16),
            _getMembersWidget(),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }

  Widget _getEventDataWidget() {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      color: theme.cardColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Hero(
            tag: _eventNode.id,
            child: EventCoverImage(_eventNode),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 16),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          "Name",
                          textAlign: TextAlign.left,
                          style: theme.textTheme.subtitle1,
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 16, top: 5, right: 16),
                        child: Text(
                          _eventNode.data.name,
                          textAlign: TextAlign.left,
                          style: ThemeUtility.caption1(theme.textTheme),
                          strutStyle: StrutStyle(
                            forceStrutHeight: true,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Spacer(),
                  if (_isAdmin)
                    TextButton(
                      text: "Edit",
                      onPressed: _onEditTapped,
                    ),
                ],
              ),
              if (_eventNode.data.description != null)
                Padding(
                  padding: EdgeInsets.only(left: 16, top: 16, right: 16),
                  child: Text(
                    "Description",
                    textAlign: TextAlign.left,
                    style: theme.textTheme.subtitle1,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
              if (_eventNode.data.description != null)
                Padding(
                  padding: EdgeInsets.only(left: 16, top: 5, right: 16),
                  child: SelectableText(
                    _eventNode.data.description,
                    textAlign: TextAlign.left,
                    style: ThemeUtility.caption1(theme.textTheme),
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _getMembersWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        _buildMembersHeader(),
        if (_members.length != 0) _buildMembersList(),
        SizedBox(height: 16),
        _buildButtons(),
      ],
    );
  }

  Widget _buildMembersHeader() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: 16, bottom: 8),
            child: Text(
              "Members",
              style: theme.textTheme.subtitle1,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildMembersList() {
    List<Widget> members = [];
    for (int i = 0; i < _members.length; i++) {
      var userPermission = _members[i];

      members.add(
        UserTile(
          user: userPermission.user,
          onTap: () => _onUserTileTapped(userPermission),
          trailing: Text(
            userPermission.user == _organizer
                ? "Organizer"
                : (userPermission.permission.can(Permissions.write) ? "Admin" : ""),
            style: theme.textTheme.caption,
            strutStyle: StrutStyle(
              forceStrutHeight: true,
            ),
          ),
          withBottomDivider: i != _members.length - 1,
        ),
      );
    }

    return Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      child: Column(
        children: members,
      ),
    );
  }

  Widget _buildButtons() {
    return Column(
      children: <Widget>[
        TileButton(
          text: "Clear Event Chat",
          onTap: _onClearChatTapped,
        ),
        if (_isAdmin && _eventNode.data.eventState != EventState.canceled)
          Padding(
            padding: EdgeInsets.only(top: 16),
            child: TileButton(
              text: "Cancel Event",
              onTap: _onCancelEventTapped,
            ),
          ),
        Padding(
          padding: EdgeInsets.only(top: 16),
          child: TileButton(
            text: "Leave Event",
            onTap: _onLeaveEventTapped,
          ),
        ),
        if (_isOrganizer)
          Column(
            children: <Widget>[
              Container(
                color: theme.cardColor,
                child: Divider(
                  indent: 16,
                  height: 2,
                ),
              ),
              TileButton(
                text: "Delete Event",
                onTap: _onDeleteEventTapped,
              ),
            ],
          ),
      ],
    );
  }

  void _onEditTapped() {
    navigation.push(CreateEventPage(eventNode: _eventNode));
  }

  void _onUserTileTapped(UserPermission userPermission) async {
    // Return if it's the local user.
    if (userPermission.user == _members[0].user) return;

    List<ModalActionData<Function>> actions = [
      ModalActionData<Function>(
        text: "View Profile",
        icon: Icons.person,
        value: () {
          navigation.push<bool>(ContactInfoPage(userPermission.user), modal: false);
        },
      ),
    ];

    if (_isOrganizer && !userPermission.permission.can(Permissions.write)) {
      actions.add(
        ModalActionData<Function>(
          text: "Make Admin",
          icon: Icons.add,
          value: () {
            _setMemberAdminStatus(userPermission.user, true);
          },
        ),
      );
    } else if (_isOrganizer && userPermission.permission.can(Permissions.write)) {
      actions.add(
        ModalActionData<Function>(
          text: "Remove Admin",
          icon: Icons.remove,
          value: () {
            _setMemberAdminStatus(userPermission.user, false);
          },
        ),
      );
    }

    var isTargetAdmin = userPermission.permission.can(Permissions.write) ||
        userPermission.permission.can(Permissions.delete);
    if (_isOrganizer || (_isAdmin && !isTargetAdmin)) {
      actions.add(
        ModalActionData<Function>(
          text: "Remove From Event",
          icon: Icons.remove,
          value: () {
            _removeFromEvent(userPermission.user);
          },
        ),
      );
    }

    var result = await showModalSheet<Function>(
      context: context,
      actions: actions,
    );
    result?.call();
  }

  void _setMemberAdminStatus(User user, bool status) {
    runAsync(() async {
      await _eventService.setMemberAdminStatus(_eventNode.id, user.id, status);
      await refresh();
    }, exclusive: true);
  }

  void _removeFromEvent(User user) async {
    var confirmed = await showAlertDialog(
      title: "Remove From Event",
      content: "Are you sure you want to remove ${user.name} from this event?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _eventService.removeFromEvent(_eventNode, user.id);
        await refresh();
      }, exclusive: true);
    }
  }

  void _onClearChatTapped() async {
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Clear Chat?",
      content: "Clearing the event chat will only remove the messages for you.",
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );

    if (decision == true) {
      runAsync(() async {
        // For now, we are assuming there is only one thread per event chat
        var threadNode =
            (await _nodeService.getCachedChildNodes<ThreadData>(widget.eventNode)).first;
        _chatService.clearThreadForUser(threadNode);
      }, exclusive: true);
    }
  }

  void _onCancelEventTapped() async {
    var confirmed = await showAlertDialog(
      title: "Cancel Event",
      content: "Are you sure you want to cancel this event?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _eventService.updateEventState(_eventNode, EventState.canceled);
        refresh();
      }, exclusive: true);
    }
  }

  void _onLeaveEventTapped() async {
    var confirmed = await showAlertDialog(
      title: "Leave Event",
      content: "Are you sure you want to leave this event?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _eventService.removeFromEvent(_eventNode);

        navigation.popUntilStart();
      }, exclusive: true);
    }
  }

  void _onDeleteEventTapped() async {
    var confirmed = await showAlertDialog(
      title: "Delete Event",
      content: "Are you sure you want to delete this event?",
      actions: [
        AlertActionData(text: "No", value: false),
        AlertActionData(text: "Yes", value: true),
      ],
    );

    if (confirmed == true) {
      runAsync(() async {
        await _eventService.deleteEvent(_eventNode);

        GetIt.I<NavigationService>().popUntilStart();
      }, exclusive: true);
    }
  }
}
