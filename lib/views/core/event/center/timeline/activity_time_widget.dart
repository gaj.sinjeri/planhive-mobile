import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:planhive/common/time.dart';
import 'package:planhive/views/common/more_icons.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/picker/date_picker.dart';
import 'package:planhive/views/cross_platform/picker/time_picker.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class ActivityTimeWidget extends StatefulWidget {
  final DateTime startDate;
  final DateTime endDate;
  final bool hasEndDate;
  final Function(DateTime, DateTime, bool) onChanged;

  const ActivityTimeWidget({
    @required this.startDate,
    @required this.endDate,
    @required this.hasEndDate,
    @required this.onChanged,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ActivityTimeWidgetState();
  }
}

class _ActivityTimeWidgetState extends State<ActivityTimeWidget>
    with ViewControllerMixin {
  DateTime _startDate;
  DateTime _endDate;
  bool _hasEndDate;

  @override
  void initState() {
    _startDate = widget.startDate;
    _endDate = widget.endDate;
    _hasEndDate = widget.hasEndDate;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _buildDateTimePickerRow(
          title: "Begins",
          datetime: _startDate,
          onDateTapped: _pickStartDate,
          onTimeTapped: _pickStartTime,
        ),
        SizedBox(height: 8),
        _buildDateTimePickerRow(
          title: "Ends",
          datetime: _endDate,
          onDateTapped: _pickEndDate,
          onTimeTapped: _pickEndTime,
          withToggle: true,
          showDetails: _hasEndDate,
        ),
        if (_hasEndDate) SizedBox(height: 8),
      ],
    );
  }

  Widget _buildDateTimePickerRow({
    @required String title,
    @required DateTime datetime,
    @required onDateTapped,
    @required onTimeTapped,
    bool withToggle = false,
    bool showDetails = true,
  }) {
    return Column(
      children: <Widget>[
        if (!showDetails)
          TextButton(
            onPressed: _onToggleEndDateView,
            text: "Add End Date",
          ),
        if (showDetails)
          SizedBox(
            height: 32,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  title,
                  style: theme.textTheme.subtitle1,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                if (withToggle)
                  TextButton(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    onPressed: _onToggleEndDateView,
                    text: "Remove",
                  ),
              ],
            ),
          ),
        if (showDetails)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildDateTimeButton(
                text: localizations.formatMediumDate(datetime) +
                    (yearMatches(datetime, DateTime.now()) == false
                        ? ", " + localizations.formatYear(datetime)
                        : ""),
                onTap: onDateTapped,
                textAlign: TextAlign.left,
                icon: PlatformIcon(
                  Icons.calendar_today,
                  MoreIcons.cupertino_calendar,
                  color: theme.primaryColor,
                ),
              ),
              _buildDateTimeButton(
                text: localizations.formatTimeOfDay(TimeOfDay.fromDateTime(datetime)),
                onTap: onTimeTapped,
                textAlign: TextAlign.right,
                icon: PlatformIcon(
                  Icons.access_time,
                  CupertinoIcons.time,
                  color: theme.primaryColor,
                ),
              ),
            ],
          ),
      ],
    );
  }

  Widget _buildDateTimeButton({
    String text,
    Icon icon,
    void Function() onTap,
    TextAlign textAlign,
  }) {
    return TouchTarget(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: platformLook == PlatformLook.material
                  ? EdgeInsets.zero
                  : EdgeInsets.only(bottom: 2.5),
              child: icon,
            ),
            SizedBox(
              width: 8,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                // this is to ensure start/end times are vertically aligned
                minWidth: use24hFormat(context) ? 38 : 62,
              ),
              child: Text(
                text,
                style: ThemeUtility.caption1(theme.textTheme),
                textAlign: textAlign,
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onToggleEndDateView() {
    setState(() {
      _hasEndDate = !_hasEndDate;
    });

    widget.onChanged.call(_startDate, _endDate, _hasEndDate);
  }

  void _pickStartDate() async {
    var newDate = await _pickDate(initialDate: _startDate);

    if (newDate != null) {
      DateTime startDate;

      if (newDate.isBefore(DateTime.now())) {
        var now = DateTime.now();
        if (now.hour == 23 && now.minute >= 50) {
          startDate = new DateTime(now.year, now.month, now.day, now.hour, 59);
        } else {
          startDate =
              new DateTime(now.year, now.month, now.day, now.hour, now.minute ~/ 10 * 10)
                  .add(Duration(minutes: 10));
        }
      } else {
        startDate = DateTime(
            newDate.year, newDate.month, newDate.day, _startDate.hour, _startDate.minute);
      }

      setState(() {
        _startDate = startDate;
        if (!_endDate.isAfter(startDate)) {
          _endDate = startDate.add(Duration(minutes: 1));
        }
      });

      widget.onChanged.call(_startDate, _endDate, _hasEndDate);
    }
  }

  void _pickEndDate() async {
    var newDate = await _pickDate(initialDate: _endDate, firstDate: _startDate);

    if (newDate != null) {
      var endDate = DateTime(
          newDate.year, newDate.month, newDate.day, _startDate.hour, _startDate.minute);

      // TODO(#102): There is an edge case here where when editing an activity, the start
      // date-time could be before the current time. If that is the case, then the
      // end-time should be set similarly to how the start time is set in _pickStartDate.

      setState(() {
        _endDate =
            endDate.isAfter(_startDate) ? endDate : _startDate.add(Duration(minutes: 1));
      });

      widget.onChanged.call(_startDate, _endDate, _hasEndDate);
    }
  }

  Future<DateTime> _pickDate({DateTime initialDate, DateTime firstDate}) async {
    var now = DateTime.now();

    return await pickDate(
      context: context,
      initialDate: (initialDate != null && initialDate.isAfter(now)) ? initialDate : now,
      firstDate: (firstDate != null && firstDate.isAfter(now)) ? firstDate : now,
      lastDate: now.add(Duration(days: 1825)), // 5 years
    );
  }

  void _pickStartTime() async {
    var newTime = await pickTime(context: context, initialDate: _startDate);

    if (newTime != null) {
      var now = DateTime.now();
      var newStartDate = DateTime(_startDate.year, _startDate.month, _startDate.day,
          newTime.hour, newTime.minute);

      if (newStartDate.isBefore(now)) {
        await showAlertDialog(
          context: context,
          title: "Invalid Time",
          content: "Cannot choose a past time for an activity.",
          actions: [AlertActionData(text: "Ok", value: true)],
        );
      } else {
        setState(() {
          _startDate = newStartDate;
          if (!_endDate.isAfter(newStartDate)) {
            _endDate = newStartDate.add(Duration(minutes: 1));
          }
        });

        widget.onChanged.call(_startDate, _endDate, _hasEndDate);
      }
    }
  }

  void _pickEndTime() async {
    var newTime = await pickTime(context: context, initialDate: _endDate);

    if (newTime != null) {
      var newEndDate = DateTime(
          _endDate.year, _endDate.month, _endDate.day, newTime.hour, newTime.minute);

      if (!_startDate.isBefore(newEndDate)) {
        await showAlertDialog(
          context: context,
          title: "Invalid Time",
          content: "The activity must end after it begins.",
          actions: [AlertActionData(text: "Ok", value: true)],
        );
      } else {
        setState(() {
          _endDate = newEndDate;
        });

        widget.onChanged.call(_startDate, _endDate, _hasEndDate);
      }
    }
  }
}
