import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide IconButton;
import 'package:get_it/get_it.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:planhive/common/time.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/timeline_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_reaction.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/more_icons.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/center/timeline/add_activity_page.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/filled_button.dart';
import 'package:planhive/views/cross_platform/icon_button.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:platform_maps_flutter/platform_maps_flutter.dart' as maps;

class TimelineWidget extends StatefulWidget {
  final Node<TimelineData> timelineNode;

  const TimelineWidget(this.timelineNode);

  @override
  State<StatefulWidget> createState() {
    return _TimelineWidgetState();
  }
}

class _TimelineWidgetState extends State<TimelineWidget>
    with
        ViewControllerMixin,
        RefreshControllerMixin,
        NotificationHandlerMixin,
        TickerProviderStateMixin {
  final _nodeStore = GetIt.I<NodeStore>();

  final _accountService = GetIt.I<AccountService>();
  final _eventService = GetIt.I<EventService>();
  final _nodeService = GetIt.I<NodeService>();
  final _userService = GetIt.I<UserService>();
  final _permissionService = GetIt.I<PermissionService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();

  bool _isAdmin = false;
  List<List<Node<ActivityData>>> _activityNodes = [];
  Map<Node<ActivityData>, List<UserReaction>> _userReactions = const {};
  Map<String, User> _activityCreators = {};

  Map<String, AnimationController> _activityTileControllers = {};
  var _expandedTileActivityIds = Set<String>();
  var _firstExpand = false;

  var _googleMapsAvailable = false;

  @override
  void dispose() {
    super.dispose();

    _activityTileControllers.forEach((_, controller) {
      controller.dispose();
    });
  }

  @override
  Future<bool> sync() async {
    var changes = await Future.wait([
      _nodeService.fetchChildren(widget.timelineNode),
      _permissionService.fetchUpdates(),
    ]);

    return changes.contains(true);
  }

  @override
  Future<void> loadState() async {
    await Future.wait([
      _loadPermission(),
      _loadTimeline(),
    ]);

    _googleMapsAvailable =
        Platform.isAndroid || await MapLauncher.isMapAvailable(MapType.google);

    _updateCounterService.clearWithChildren(widget.timelineNode.id);
  }

  Future<void> _loadPermission() async {
    _isAdmin = (await _permissionService
            .getUserPermission(widget.timelineNode.permissionGroupId))
        .permission
        .can(Permissions.write);
  }

  Future<void> _loadTimeline() async {
    var children = await _nodeStore.getChildren(widget.timelineNode);
    var activityNodes = children
        .where((node) => node.type == NodeType.activity)
        .map((node) => node.as<ActivityData>())
        .toList(growable: false);
    activityNodes.sort((a, b) => a.data.compareTo(b.data));

    await Future.wait([
      _loadUserReactions(activityNodes),
      _loadActivityCreators(activityNodes),
    ]);

    _activityNodes = _generateActivityNodesList(activityNodes);

    activityNodes.forEach((activityNode) {
      _activityTileControllers.putIfAbsent(
          activityNode.id,
          () => AnimationController(
                duration: Duration(milliseconds: 400),
                vsync: this,
              ));
    });

    if (_firstExpand == false && _activityNodes.isNotEmpty) {
      _toggleActivityTile(_activityNodes[0][0].id);
      _firstExpand = true;
    }
  }

  Future<void> _loadUserReactions(List<Node<ActivityData>> activityNodes) async {
    var userReactions = {};
    await Future.forEach(activityNodes, (activity) async {
      userReactions[activity] = await _nodeStore.getReactions(activity);
    });
    _userReactions = Map.unmodifiable(userReactions);
  }

  Future<void> _loadActivityCreators(List<Node<ActivityData>> activityNodes) async {
    var activityCreatorIds = activityNodes.map((e) => e.creatorId).toSet().toList();
    var activityCreators = await _userService.getUsersById(activityCreatorIds);
    _activityCreators = Map.fromIterable(
      activityCreators,
      key: (e) => e.id,
      value: (e) => e,
    );
  }

  List<List<Node<ActivityData>>> _generateActivityNodesList(
      List<Node<ActivityData>> activityNodes) {
    List<List<Node<ActivityData>>> list = [];

    var dt = DateTime.fromMillisecondsSinceEpoch(0);
    var index = -1;
    for (var activity in activityNodes) {
      var activityStartTime =
          DateTime.fromMillisecondsSinceEpoch(activity.data.time.startTs);

      if (!dateMatches(dt, activityStartTime)) {
        dt = DateTime(
            activityStartTime.year, activityStartTime.month, activityStartTime.day);

        index++;
        list.insert(index, []);
      }

      list[index].add(activity);
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      _buildTimelineHeader(),
    ];
    if (_activityNodes.length != 0) {
      widgets.addAll(_activityNodes.map(_buildDayView));
    } else {
      widgets.add(_buildTimelinePlaceholder());
    }

    return Container(
      margin: EdgeInsets.only(top: 16),
      padding: EdgeInsets.only(bottom: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: widgets,
      ),
    );
  }

  Widget _buildTimelineHeader() {
    return Padding(
      padding: EdgeInsets.only(left: 16),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Text(
              "Activities",
              style: theme.textTheme.headline6,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
          Container(
            child: IconButton(
              materialIcon: Icons.add,
              cupertinoIcon: CupertinoIcons.add,
              onPressed: _addActivity,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildTimelinePlaceholder() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 16),
          child: Text(
            "There are no activities yet for this event."
            "\n Tap the + button to add a new activity.",
            style: ThemeUtility.caption1(theme.textTheme),
            strutStyle: StrutStyle(
              forceStrutHeight: true,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 16, top: 16),
          child: Row(
            children: <Widget>[
              PlatformIcon(
                Icons.arrow_back,
                MoreIcons.cupertino_arrow_back,
                color: Colors.grey,
              ),
              SizedBox(
                width: 8,
              ),
              Text(
                "Swipe right to see the event chat",
                style: ThemeUtility.caption1(theme.textTheme),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 16, top: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(
                "Swipe left to see the event photos",
                style: ThemeUtility.caption1(theme.textTheme),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
              SizedBox(
                width: 8,
              ),
              PlatformIcon(
                Icons.arrow_forward,
                MoreIcons.cupertino_arrow_forward,
                color: Colors.grey,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildDayView(List<Node<ActivityData>> activityNodes) {
    var widgets = [
      Container(
        padding: EdgeInsets.only(left: 16),
        margin: EdgeInsets.only(top: 8, bottom: 8),
        child: Text(
          activityNodes[0].data.time.getStartDateText(context),
          style: ThemeUtility.caption1(theme.textTheme),
          textAlign: TextAlign.left,
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      ),
      SizedBox(
        height: 8,
      ),
    ];
    widgets.addAll(activityNodes.map(_buildActivityWidget));

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    );
  }

  Widget _buildActivityWidget(Node<ActivityData> activityNode) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        if (activityNode.data.isSuggestion)
          Padding(
            padding: EdgeInsets.only(left: 16, bottom: 8, right: 16),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    "Suggested${_getSuggesterName(activityNode.creatorId)}",
                    style: theme.textTheme.bodyText2.copyWith(color: theme.primaryColor),
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
                Icon(
                  Icons.thumb_up,
                  size: theme.textTheme.bodyText2.fontSize,
                  color: theme.primaryColor,
                ),
                SizedBox(width: 8),
                Text(
                  _countReactions(activityNode, ActivityReactionType.vote).toString(),
                  style: theme.textTheme.bodyText2.copyWith(
                    color: theme.primaryColor,
                  ),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                SizedBox(width: 16),
                Icon(
                  Icons.thumb_down,
                  size: theme.textTheme.bodyText2.fontSize,
                  color: theme.primaryColor,
                ),
                SizedBox(width: 8),
                Text(
                  _countReactions(activityNode, ActivityReactionType.reject).toString(),
                  style: theme.textTheme.bodyText2.copyWith(
                    color: theme.primaryColor,
                  ),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ],
            ),
          ),
        _buildActivityCard(activityNode),
        SizedBox(height: 16),
      ],
    );
  }

  Widget _buildActivityCard(Node<ActivityData> activityNode) {
    return GestureDetector(
      onTap: () => _toggleActivityTile(activityNode.id),
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.only(left: 16),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(right: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 16),
                              RichText(
                                text: TextSpan(
                                  text: activityNode.data.title,
                                  style: theme.textTheme.subtitle1,
                                  children: <TextSpan>[
                                    if (activityNode.data.isCanceled)
                                      TextSpan(
                                        text: " (Canceled)",
                                        style: theme.textTheme.bodyText2
                                            .copyWith(color: Colors.red),
                                      ),
                                  ],
                                ),
                                strutStyle: StrutStyle(
                                  forceStrutHeight: true,
                                ),
                              ),
                              _buildDescriptionWidget(activityNode),
                              SizedBox(height: 16),
                              Row(
                                children: <Widget>[
                                  PlatformIcon(
                                    Icons.access_time,
                                    CupertinoIcons.time,
                                    size: ThemeUtility.caption1(theme.textTheme).fontSize,
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    "${activityNode.data.time.getStartTimeText(context)}",
                                    style: ThemeUtility.caption1(theme.textTheme),
                                    strutStyle: StrutStyle(
                                      forceStrutHeight: true,
                                    ),
                                  ),
                                  if (activityNode.data.time.endTs != null)
                                    Text(
                                      " - ${activityNode.data.time.getEndTimeText(context)}",
                                      style: ThemeUtility.caption1(theme.textTheme),
                                      strutStyle: StrutStyle(
                                        forceStrutHeight: true,
                                      ),
                                    ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (_infoButtonVisible(activityNode))
                        IconButton(
                          materialIcon: Icons.more_horiz,
                          cupertinoIcon: CupertinoIcons.ellipsis,
                          onPressed: () => _onActivityInfoButtonPressed(activityNode),
                        ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Row(
                    children: <Widget>[
                      PlatformIcon(
                        Icons.location_on,
                        CupertinoIcons.location,
                        size: ThemeUtility.caption1(theme.textTheme).fontSize,
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(
                              right: _hasOptionalData(activityNode) &&
                                      !_expandedTileActivityIds.contains(activityNode.id)
                                  ? 56
                                  : 16),
                          child: SelectableText(
                            activityNode.data.location == null
                                ? "-"
                                : activityNode.data.location.title ?? "Dropped Pin",
                            style: ThemeUtility.caption1(theme.textTheme),
                            strutStyle: StrutStyle(
                              forceStrutHeight: true,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  _buildLocationDetailsWidget(activityNode),
                  _buildNotesWidget(activityNode),
                  _buildLikeButtons(activityNode),
                  if (!(activityNode.data.isSuggestion && !_isCreator(activityNode)))
                    SizedBox(height: 16),
                ],
              ),
              if (_hasOptionalData(activityNode))
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: _expandedTileActivityIds.contains(activityNode.id)
                      ? IconButton(
                          materialIcon: Icons.keyboard_arrow_up,
                          cupertinoIcon: Icons.keyboard_arrow_up,
                          onPressed: () => _toggleActivityTile(activityNode.id),
                        )
                      : IconButton(
                          materialIcon: Icons.keyboard_arrow_down,
                          cupertinoIcon: Icons.keyboard_arrow_down,
                          onPressed: () => _toggleActivityTile(activityNode.id),
                        ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  bool _hasOptionalData(Node<ActivityData> activityNode) {
    return activityNode.data.description != null ||
        activityNode.data.note != null ||
        activityNode.data.location != null;
  }

  Widget _buildLikeButtons(Node<ActivityData> activityNode) {
    if (_isCreator(activityNode) || !activityNode.data.isSuggestion) {
      return Container();
    }

    var _hasReactedVote = _getUserReaction(activityNode) == ActivityReactionType.vote;
    var _hasReactedReject = _getUserReaction(activityNode) == ActivityReactionType.reject;

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FilledButton(
          content: Icon(
            Icons.thumb_up,
            color: _hasReactedVote ? Colors.white : theme.primaryColor,
          ),
          onPressed: () => _updateReaction(activityNode, ActivityReactionType.vote),
          backgroundColor: _hasReactedVote ? theme.primaryColor : Colors.white,
        ),
        SizedBox(width: 64),
        FilledButton(
          content: Icon(
            Icons.thumb_down,
            color: _hasReactedReject ? Colors.white : theme.primaryColor,
          ),
          onPressed: () => _updateReaction(activityNode, ActivityReactionType.reject),
          backgroundColor: _hasReactedReject ? theme.primaryColor : Colors.white,
        ),
      ],
    );
  }

  Widget _buildDescriptionWidget(Node<ActivityData> activityNode) {
    if (activityNode.data.description == null) {
      return Container();
    }

    return SizeTransition(
      child: Padding(
        padding: EdgeInsets.only(top: 4),
        child: SelectableText(
          activityNode.data.description,
          style: ThemeUtility.caption1(theme.textTheme),
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
      ),
      sizeFactor: CurvedAnimation(
        curve: Curves.fastOutSlowIn,
        parent: _activityTileControllers[activityNode.id],
      ),
    );
  }

  Widget _buildLocationDetailsWidget(Node<ActivityData> activityNode) {
    if (activityNode.data.location == null) {
      return Container();
    }

    return SizeTransition(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (activityNode.data.location.address != null)
            Padding(
              padding: EdgeInsets.only(top: 4, right: 16),
              child: Row(
                children: <Widget>[
                  // This is a hack to get the text to align
                  PlatformIcon(
                    Icons.access_time,
                    CupertinoIcons.time,
                    size: ThemeUtility.caption1(theme.textTheme).fontSize,
                    color: Colors.white,
                  ),
                  SizedBox(width: 8),
                  // TODO(#113): Investigate alternative to Expanded
                  Expanded(
                    child: SelectableText(
                      activityNode.data.location.address,
                      style: ThemeUtility.caption1(theme.textTheme),
                      strutStyle: StrutStyle(
                        forceStrutHeight: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          if (_expandedTileActivityIds.contains(activityNode.id))
            Padding(
              padding: EdgeInsets.only(top: 16, right: 16),
              child: SizedBox(
                height: 160,
                child: maps.PlatformMap(
                  onTap: (location) => _onMapTapped(activityNode),
                  initialCameraPosition: maps.CameraPosition(
                    target: maps.LatLng(
                      activityNode.data.location.coordinates.lat,
                      activityNode.data.location.coordinates.lng,
                    ),
                    zoom: 16,
                  ),
                  markers: [
                    maps.Marker(
                      position: maps.LatLng(
                        activityNode.data.location.coordinates.lat,
                        activityNode.data.location.coordinates.lng,
                      ),
                      markerId: maps.MarkerId("event_location"),
                    ),
                  ].toSet(),
                  scrollGesturesEnabled: false,
                  rotateGesturesEnabled: false,
                  zoomGesturesEnabled: false,
                  tiltGestureEnabled: false,
                ),
              ),
            ),
          if (activityNode.data.note == null &&
              !(activityNode.data.isSuggestion && !_isCreator(activityNode)))
            SizedBox(height: 32),
        ],
      ),
      sizeFactor: CurvedAnimation(
        curve: Curves.fastOutSlowIn,
        parent: _activityTileControllers[activityNode.id],
      ),
    );
  }

  void _onMapTapped(Node<ActivityData> activityNode) async {
    if (Platform.isIOS && _googleMapsAvailable) {
      _showMapsModal(activityNode);
    } else {
      _showMapsAlertDialog(activityNode);
    }
  }

  void _showMapsModal(Node<ActivityData> activityNode) async {
    var modalData = [
      ModalActionData<MapType>(
        text: "Apple Maps",
        value: MapType.apple,
      ),
      ModalActionData<MapType>(
        text: "Google Maps",
        value: MapType.google,
      ),
    ];

    var decision = await showModalSheet<MapType>(
      title: "Open With",
      actions: modalData,
    );
    if (decision != null) _launchMapsApp(activityNode, decision);
  }

  void _showMapsAlertDialog(Node<ActivityData> activityNode) async {
    var title = Platform.isIOS ? "Open with Apple Maps?" : "Open with Google Maps?";

    var alertData = [
      AlertActionData<MapType>(text: "No", value: null),
      AlertActionData<MapType>(
          text: "Yes", value: Platform.isIOS ? MapType.apple : MapType.google),
    ];

    var decision = await showAlertDialog<MapType>(
      context: context,
      title: title,
      actions: alertData,
    );

    if (decision != null) _launchMapsApp(activityNode, decision);
  }

  void _launchMapsApp(Node<ActivityData> activityNode, MapType mapType) async {
    await MapLauncher.showMarker(
      mapType: mapType,
      coords: Coords(activityNode.data.location.coordinates.lat,
          activityNode.data.location.coordinates.lng),
      title: activityNode.data.location.title ?? "Dropped Pin",
    );
  }

  Widget _buildNotesWidget(Node<ActivityData> activityNode) {
    if (activityNode.data.note == null) {
      return Container();
    }

    return SizeTransition(
      child: Padding(
        padding: EdgeInsets.only(top: 16, right: 48),
        child: SelectableText.rich(
          TextSpan(
            text: 'Notes: ',
            style: ThemeUtility.caption1(theme.textTheme)
                .copyWith(fontWeight: FontWeight.bold),
            children: <TextSpan>[
              TextSpan(
                text: activityNode.data.note,
                style: ThemeUtility.caption1(theme.textTheme),
              ),
            ],
          ),
        ),
      ),
      sizeFactor: CurvedAnimation(
        curve: Curves.fastOutSlowIn,
        parent: _activityTileControllers[activityNode.id],
      ),
    );
  }

  void _addActivity() async {
    ActivityState selection;
    if (_isAdmin) {
      selection = await showModalSheet<ActivityState>(
        context: context,
        actions: [
          ModalActionData<ActivityState>(
            text: "Add Activity",
            value: ActivityState.approved,
          ),
          ModalActionData<ActivityState>(
            text: "Suggest Activity",
            value: ActivityState.suggestion,
          ),
        ],
      );
    } else {
      selection = ActivityState.suggestion;
    }

    if (selection != null) {
      var added = await navigation.push(
        AddActivityPage(
          timelineNode: widget.timelineNode,
          targetActivityState: selection,
        ),
      );

      if (added == true) {
        await refresh();
      }
    }
  }

  String _getSuggesterName(String userId) {
    if (_activityCreators[userId] != null) {
      var name = userId == _accountService.userId ? "Me" : _activityCreators[userId].name;
      return " by $name";
    } else {
      return "";
    }
  }

  bool _isCreator(Node<ActivityData> node) => node.creatorId == _accountService.userId;

  bool _infoButtonVisible(Node<ActivityData> activityNode) {
    return _canApprove(activityNode) ||
        _canEdit(activityNode) ||
        _canDelete(activityNode);
  }

  void _onActivityInfoButtonPressed(Node<ActivityData> activityNode) async {
    var selection = await showModalSheet<Function>(
      context: context,
      title: activityNode.data.title,
      actions: _generateModalActionData(activityNode),
    );

    if (selection != null) await selection.call();
  }

  List<ModalActionData<Function>> _generateModalActionData(
      Node<ActivityData> activityNode) {
    return [
      if (_canApprove(activityNode))
        ModalActionData<Function>(
          text: "Approve",
          icon: Icons.check,
          value: () => _approveActivity(activityNode),
        ),
      if (_canEdit(activityNode))
        ModalActionData<Function>(
          text: "Edit",
          icon: Icons.edit,
          value: () => _editActivity(activityNode),
        ),
      if (_canCancel(activityNode))
        ModalActionData<Function>(
          text: "Cancel",
          icon: Icons.cancel,
          value: () => _cancelActivity(activityNode),
        ),
      if (_canDelete(activityNode))
        ModalActionData<Function>(
          text: "Delete",
          icon: Icons.delete,
          value: () => _deleteActivity(activityNode),
        ),
    ];
  }

  bool _canApprove(Node<ActivityData> activityNode) {
    return _isAdmin && activityNode.data.state == ActivityState.suggestion;
  }

  void _approveActivity(Node<ActivityData> activityNode) {
    runAsync(() async {
      await _eventService.updateActivityState(
        widget.timelineNode,
        activityNode.id,
        ActivityState.approved,
      );
      await refresh();
    }, exclusive: true);
  }

  bool _canEdit(Node<ActivityData> activityNode) {
    var isApproved = activityNode.data.state == ActivityState.approved;
    var isSuggestion = activityNode.data.state == ActivityState.suggestion;
    return (isSuggestion && _isCreator(activityNode)) || (isApproved && _isAdmin);
  }

  void _editActivity(Node<ActivityData> activityNode) async {
    var completed = await navigation.push(
      AddActivityPage(
        timelineNode: widget.timelineNode,
        activityNode: activityNode,
      ),
      modal: false,
    );

    if (completed == true) {
      await refresh();
    }
  }

  bool _canCancel(Node<ActivityData> activityNode) {
    var isApproved = activityNode.data.state == ActivityState.approved;
    return _isAdmin && isApproved;
  }

  void _cancelActivity(Node<ActivityData> activityNode) async {
    var alertData = [
      AlertActionData<bool>(text: "No", value: false),
      AlertActionData<bool>(text: "Yes", value: true),
    ];
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Cancel Activity",
      content: "Are you sure you want to cancel the activity?",
      actions: alertData,
    );

    if (decision == true) {
      runAsync(() async {
        await _eventService.updateActivityState(
          widget.timelineNode,
          activityNode.id,
          ActivityState.canceled,
        );
        await refresh();
      }, exclusive: true);
    }
  }

  bool _canDelete(Node<ActivityData> activityNode) {
    var isSuggestion = activityNode.data.state == ActivityState.suggestion;
    return _isAdmin || (isSuggestion && _isCreator(activityNode));
  }

  void _deleteActivity(Node<ActivityData> activityNode) async {
    var alertData = [
      AlertActionData<bool>(text: "No", value: false),
      AlertActionData<bool>(text: "Yes", value: true),
    ];
    var decision = await showAlertDialog<bool>(
      context: context,
      title: "Delete Activity",
      content: "Are you sure you want to delete the activity?",
      actions: alertData,
    );

    if (decision == true) {
      runAsync(() async {
        await _eventService.deleteActivity(widget.timelineNode, activityNode);
        await refresh();
      }, exclusive: true);
    }
  }

  int _countReactions(Node<ActivityData> activity, ActivityReactionType type) {
    int count = 0;
    for (var userReaction in _userReactions[activity]) {
      if (userReaction.reaction == type) {
        count++;
      }
    }
    return count;
  }

  ActivityReactionType _getUserReaction(Node<ActivityData> activity) {
    for (var userReaction in _userReactions[activity]) {
      if (userReaction.user.id == _accountService.userId) {
        return userReaction.reaction;
      }
    }
    return null;
  }

  void _updateReaction(Node<ActivityData> activityNode, ActivityReactionType reaction) {
    if (_isCreator(activityNode)) {
      showAlertDialog(
        title: "Oops",
        content: "You can't react to your own suggestion.",
        actions: [AlertActionData(value: true, text: "Ok")],
      );
    } else {
      runAsync(() async {
        await _eventService.updateActivityReaction(
          widget.timelineNode,
          activityNode,
          _getUserReaction(activityNode) == reaction ? null : reaction,
        );
        await refresh();
      }, exclusive: true);
    }
  }

  void _toggleActivityTile(String activityNodeId) async {
    if (!_expandedTileActivityIds.contains(activityNodeId)) {
      _expandedTileActivityIds.add(activityNodeId);
      setState(() {});

      await _activityTileControllers[activityNodeId].forward();
    } else if (_expandedTileActivityIds.contains(activityNodeId)) {
      await _activityTileControllers[activityNodeId].reverse();

      _expandedTileActivityIds.remove(activityNodeId);
      setState(() {});
    }
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([Node, widget.timelineNode])) {
      refresh();
    }

    return SynchronousFuture(false);
  }
}
