import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/data/node/activity/time_range.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/timeline_data.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/center/timeline/activity_time_widget.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';

import 'activity_location_widget.dart';

class AddActivityPage extends StatefulWidget {
  final Node<TimelineData> timelineNode;
  final ActivityState targetActivityState;
  final Node<ActivityData> activityNode;

  const AddActivityPage({
    @required this.timelineNode,
    this.targetActivityState = ActivityState.suggestion,
    this.activityNode,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AddActivityPageState();
  }
}

class _AddActivityPageState extends State<AddActivityPage> with ViewControllerMixin {
  static const _suggestionHint = "You are creating a suggestion for an activity."
      "\nEvent members will be able to vote for your activity."
      "\nThe organizer and admins can approve it.";
  static const _suggestionOrganizerHint = "You are creating a suggestion for an activity."
      "\nEvent members will be able to vote for your activity."
      "\nYou will be able to confirm it later.";

  final _eventService = GetIt.I<EventService>();
  final _permissionService = GetIt.I<PermissionService>();

  TextEditingController _nameInput;
  TextEditingController _descInput;
  TextEditingController _noteInput;

  bool _isOrganizer = false;

  DateTime _startDateInitial = DateTime.now();
  DateTime _endDateInitial = DateTime.now();
  bool _hasEndDateInitial = false;

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now();
  bool _hasEndDate = false;

  ActivityState _activityState;
  Location _location;
  Location _locationInitial;

  bool _changesMade = false;

  @override
  void initState() {
    _nameInput = TextEditingController();
    _descInput = TextEditingController();
    _noteInput = TextEditingController();

    if (widget.activityNode != null) {
      var data = widget.activityNode.data;

      _nameInput.text = data.title ?? "";
      _descInput.text = data.description ?? "";
      _noteInput.text = data.note ?? "";

      _startDate = DateTime.fromMillisecondsSinceEpoch(data.time.startTs);
      _hasEndDate = data.time.endTs != null;
      _endDate = _hasEndDate
          ? DateTime.fromMillisecondsSinceEpoch(data.time.endTs)
          : _startDate.add(Duration(hours: 1));

      _activityState = data.state;
      _location = data.location;
    } else {
      var now = DateTime.now();
      _startDate = DateTime(now.year, now.month, now.day, now.hour + 1);
      _endDate = _startDate.add(Duration(hours: 1));

      _activityState = widget.targetActivityState;

      runAsync(_loadPermissions);
    }

    _startDateInitial = _startDate;
    _endDateInitial = _endDate;
    _hasEndDateInitial = _hasEndDate;
    _locationInitial = _location;

    super.initState();
  }

  void dispose() {
    _nameInput.dispose();
    _descInput.dispose();
    _noteInput.dispose();

    super.dispose();
  }

  Future<void> _loadPermissions() async {
    var permission =
        await _permissionService.getUserPermission(widget.timelineNode.permissionGroupId);

    setState(() {
      _isOrganizer = permission.permission.can(Permissions.write);
    });
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text(
        widget.activityNode == null
            ? (_activityState == ActivityState.approved
                ? "Create Activity"
                : "Suggest Activity")
            : (_activityState == ActivityState.approved
                ? "Edit Activity"
                : "Edit Suggestion"),
      ),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.close,
        onPressed: _discard,
      ),
      trailing: NavBarButton(
        text: widget.activityNode == null ? "Done" : "Save",
        materialIcon: Icons.done,
        onPressed: _done,
      ),
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          physics: ClampingScrollPhysics(),
          padding: EdgeInsets.all(16),
          children: <Widget>[
            TextFormField(
              controller: _nameInput,
              textCapitalization: TextCapitalization.words,
              keyboardType: TextInputType.text,
              labelText: "Name",
              textInputAction: TextInputAction.next,
              validator: _nameValidator,
              onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            ),
            TextFormField(
              controller: _descInput,
              textCapitalization: TextCapitalization.sentences,
              keyboardType: TextInputType.text,
              labelText: "Description (Optional)",
              minLines: 1,
              maxLines: 5,
              textInputAction: TextInputAction.next,
              validator: _descriptionValidator,
              onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            ),
            TextFormField(
              controller: _noteInput,
              textCapitalization: TextCapitalization.sentences,
              keyboardType: TextInputType.text,
              labelText: "Notes (Optional)",
              minLines: 1,
              maxLines: 5,
              validator: _notesValidator,
              textInputAction: TextInputAction.done,
              onFieldSubmitted: (_) => FocusScope.of(context).unfocus(),
            ),
            SizedBox(height: 16),
            ActivityTimeWidget(
              startDate: _startDateInitial,
              endDate: _endDateInitial,
              hasEndDate: _hasEndDateInitial,
              onChanged: (DateTime startDate, DateTime endDate, bool hasEndDate) {
                setState(() {
                  _startDate = startDate;
                  _endDate = endDate;
                  _hasEndDate = hasEndDate;
                  _changesMade = true;
                });
              },
            ),
            Divider(height: 16),
            ActivityLocationWidget(
              location: _locationInitial,
              onChanged: (Location location) {
                setState(() {
                  _location = location;
                  _changesMade = true;
                });
              },
            ),
            if (widget.activityNode == null &&
                widget.targetActivityState == ActivityState.suggestion)
              Divider(height: 16),
            if (widget.activityNode == null &&
                widget.targetActivityState == ActivityState.suggestion)
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Text(
                  _isOrganizer ? _suggestionOrganizerHint : _suggestionHint,
                  textAlign: TextAlign.center,
                  style: ThemeUtility.caption1(theme.textTheme),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  String _nameValidator(String input) {
    return ActivityData.nameRegExp.hasMatch(input)
        ? null
        : "Please enter a name between 1 and 25 characters";
  }

  String _descriptionValidator(String input) {
    return ActivityData.descriptionRegExp.hasMatch(input)
        ? null
        : "Please enter a description up to 512 characters.";
  }

  String _notesValidator(String input) {
    return ActivityData.notesRegExp.hasMatch(input)
        ? null
        : "Maximum length is 128 characters.";
  }

  ActivityData _constructActivityData() {
    return ActivityData(
      state: _activityState,
      title: _nameInput.text,
      description: _descInput.text.isNotEmpty ? _descInput.text : null,
      note: _noteInput.text.isNotEmpty ? _noteInput.text : null,
      time: TimeRange.fromDateTime(_startDate, _hasEndDate ? _endDate : null),
      location: _location,
    );
  }

  void _discard() async {
    if (widget.activityNode == null) {
      if (_changesMade || _nameInput.text.isNotEmpty) {
        if (await _confirmDiscard("Discard Activity?")) {
          NavigationService.instance.pop(false);
        }
      } else {
        NavigationService.instance.pop(false);
      }
    } else {
      if (_constructActivityData() != widget.activityNode.data) {
        if (await _confirmDiscard("Discard Changes?")) {
          NavigationService.instance.pop(false);
        }
      } else {
        NavigationService.instance.pop(false);
      }
    }
  }

  Future<bool> _confirmDiscard(String title) async {
    return await showAlertDialog<bool>(
      context: context,
      title: title,
      actions: [
        AlertActionData<bool>(text: "No", value: false),
        AlertActionData<bool>(text: "Yes", value: true),
      ],
    );
  }

  void _done() async {
    if (_nameValidator(_nameInput.text) != null) {
      await showAlertDialog(
        context: context,
        title: "Activity Name",
        content: "Please enter a valid activity name.",
        actions: [AlertActionData(text: "Ok", value: true)],
      );
      return;
    } else if (_descriptionValidator(_descInput.text) != null) {
      await showAlertDialog(
        context: context,
        title: "Activity Description",
        content: "Please enter a valid activity description.",
        actions: [AlertActionData(text: "Ok", value: true)],
      );
      return;
    } else if (_notesValidator(_noteInput.text) != null) {
      await showAlertDialog(
        context: context,
        title: "Activity Notes",
        content: "Please enter valid activity notes.",
        actions: [AlertActionData(text: "Ok", value: true)],
      );
      return;
    } else if (_startDate.isAfter(_endDate)) {
      await showAlertDialog(
        context: context,
        title: "Invalid Time",
        content: "The activity must end after it begins.",
        actions: [AlertActionData(text: "Ok", value: true)],
      );
      return;
    }

    runAsync(() async {
      var data = _constructActivityData();

      if (widget.activityNode == null) {
        await _eventService.addActivity(widget.timelineNode, data);
      } else {
        await _eventService.updateActivityData(
            widget.timelineNode, widget.activityNode, data);
      }

      navigation.pop(true);
    }, exclusive: true);
  }
}
