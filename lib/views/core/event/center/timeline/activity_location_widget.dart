import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:planhive/api/third_party/google_maps.dart';
import 'package:planhive/data/lat_lng.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/maps/map_location_page.dart';
import 'package:planhive/views/maps/place_search_page.dart';
import 'package:platform_maps_flutter/platform_maps_flutter.dart' as maps;

class ActivityLocationWidget extends StatefulWidget {
  final Location location;
  final Function(Location) onChanged;

  const ActivityLocationWidget({
    this.location,
    this.onChanged,
    Key key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ActivityLocationWidgetState();
  }
}

class _ActivityLocationWidgetState extends State<ActivityLocationWidget>
    with ViewControllerMixin {
  Location _location;
  maps.PlatformMapController _mapController;

  @override
  void initState() {
    _location = widget.location;

    super.initState();
  }

  void _removeLocation() {
    setState(() {
      _location = null;
    });

    widget.onChanged(_location);
  }

  void _addOrChangeLocation() async {
    var option = await showModalSheet(
      actions: [
        ModalActionData(
          icon: Icons.search,
          text: "Search by Name or Address",
          value: 0,
        ),
        ModalActionData(
          icon: Icons.location_searching,
          text: "Find Location on Map",
          value: 1,
        ),
      ],
    );

    switch (option) {
      case 0:
        var placeDetails =
            await navigation.push<PlaceDetails>(PlaceSearchPage(), modal: false);

        if (placeDetails != null) {
          setState(() {
            _location = Location(
              title: placeDetails.name.isNotEmpty ? placeDetails.name : null,
              address: placeDetails.address.isNotEmpty ? placeDetails.address : null,
              coordinates: LatLng(placeDetails.lat, placeDetails.lng),
            );
            _updateMapLocation();
          });

          widget.onChanged(_location);
        }
        break;
      case 1:
        var latLng = await navigation.push<LatLng>(
          MapLocationPage(_location?.coordinates),
          modal: false,
        );

        if (latLng != null) {
          setState(() {
            _location = Location(coordinates: latLng);
            _updateMapLocation();
          });

          widget.onChanged(_location);
        }
        break;
    }
  }

  void _onMapCreated(maps.PlatformMapController controller) {
    _mapController = controller;
    _updateMapLocation();
  }

  void _updateMapLocation() {
    if (_location != null && _mapController != null) {
      _mapController.moveCamera(maps.CameraUpdate.newLatLngZoom(
        maps.LatLng(_location.coordinates.lat, _location.coordinates.lng),
        16,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            TextButton(
              text: _location == null ? "Add Location" : "Change Location",
              onPressed: _addOrChangeLocation,
            ),
            if (_location != null)
              TextButton(
                text: "Remove Location",
                onPressed: _removeLocation,
              ),
          ],
        ),
        if (_location?.title != null)
          ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              _location.title,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
            subtitle: Text(
              _location.address,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
        if (_location != null)
          SizedBox(
            height: 160,
            child: maps.PlatformMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: maps.CameraPosition(
                target: maps.LatLng(
                  _location.coordinates.lat,
                  _location.coordinates.lng,
                ),
                zoom: 16,
              ),
              markers: [
                maps.Marker(
                  position: maps.LatLng(
                    _location.coordinates.lat,
                    _location.coordinates.lng,
                  ),
                  markerId: maps.MarkerId("event_location"),
                ),
              ].toSet(),
              scrollGesturesEnabled: false,
              rotateGesturesEnabled: false,
              zoomGesturesEnabled: false,
              tiltGestureEnabled: false,
            ),
          ),
      ],
    );
  }
}
