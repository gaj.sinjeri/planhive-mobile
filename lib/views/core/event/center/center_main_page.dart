import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/event_cover_image.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/node/timeline_data.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/center/description_widget.dart';
import 'package:planhive/views/core/event/center/participation/participation_widget.dart';
import 'package:planhive/views/core/event/center/timeline/timeline_widget.dart';

class CenterPage extends StatefulWidget {
  final Node<EventData> event;

  CenterPage(this.event);

  @override
  State<StatefulWidget> createState() {
    return _CenterPageState();
  }
}

class _CenterPageState extends State<CenterPage>
    with ViewControllerMixin, RefreshControllerMixin {
  final _nodeService = GetIt.I<NodeService>();
  final _nodeStore = GetIt.I<NodeStore>();

  Node<ParticipationData> _participationNode;
  Node<TimelineData> _timelineNode;

  @override
  Future<bool> sync() {
    return _nodeService.fetchChildren(widget.event);
  }

  @override
  Future<void> loadState() async {
    var children = await _nodeStore.getChildren(widget.event);

    for (var child in children) {
      if (child.type == NodeType.participation) {
        _participationNode = child.as<ParticipationData>();
      } else if (child.type == NodeType.timeline) {
        _timelineNode = child.as<TimelineData>();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var event = widget.event;

    return LoadingIndicator(
      loading: loading,
      child: RefreshIndicator(
        onRefresh: () => refresh(true),
        child: ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          children: [
            Container(
              color: theme.cardColor,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Hero(
                    tag: event.id,
                    child: EventCoverImage(event),
                  ),
                  if (event.data.description != null)
                    DescriptionWidget(event.data.description),
                ],
              ),
            ),
            SizedBox(height: 16),
            if (_participationNode != null)
              ParticipationWidget(
                widget.event,
                _participationNode,
              ),
            if (widget.event.data.eventState == EventState.canceled) SizedBox(height: 16),
            if (widget.event.data.eventState == EventState.canceled)
              Center(
                child: Text(
                  "Event has been canceled",
                  style: theme.textTheme.subtitle1.copyWith(color: Colors.red),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            if (_timelineNode != null)
              TimelineWidget(
                _timelineNode,
              ),
          ],
        ),
      ),
    );
  }
}
