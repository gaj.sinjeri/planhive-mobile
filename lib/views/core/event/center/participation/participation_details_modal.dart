import 'package:flutter/material.dart' hide IconButton, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class ParticipationDetailsWidget extends StatefulWidget {
  final Map<ParticipantStatus, List<User>> participants;
  final bool showInviteButton;

  const ParticipationDetailsWidget({
    @required this.participants,
    @required this.showInviteButton,
  }) : assert(showInviteButton != null);

  @override
  State<StatefulWidget> createState() {
    return _ParticipationDetailsWidgetState();
  }
}

class _ParticipationDetailsWidgetState extends State<ParticipationDetailsWidget>
    with ViewControllerMixin, TickerProviderStateMixin {
  static const _divider = const Divider(height: 0, indent: 16, endIndent: 16);

  Map<int, ParticipantStatus> _tileIndexToParticipantStatus = {
    0: ParticipantStatus.accepted,
    1: ParticipantStatus.maybe,
    2: ParticipantStatus.declined,
    3: null
  };

  List<AnimationController> _tileControllers = [];
  int _expandedTileIndex;

  List<User> get accepted {
    return widget.participants[ParticipantStatus.accepted];
  }

  List<User> get maybe {
    return widget.participants[ParticipantStatus.maybe];
  }

  List<User> get declined {
    return widget.participants[ParticipantStatus.declined];
  }

  List<User> get pending {
    return widget.participants[null];
  }

  String _localUserId = "";

  @override
  void initState() {
    _tileControllers = [
      _getNewAnimationController(),
      _getNewAnimationController(),
      _getNewAnimationController(),
      _getNewAnimationController(),
    ];

    if (accepted.isNotEmpty) _toggleActivityTile(0);

    _localUserId = GetIt.I<AccountService>().userId;

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

    for (var controller in _tileControllers) {
      controller.dispose();
    }
  }

  AnimationController _getNewAnimationController() {
    return AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: theme.cardColor,
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildUserList(context, 0, "Confirmed (${accepted.length})", accepted),
            _divider,
            _buildUserList(context, 1, "Maybe (${maybe.length})", maybe),
            _divider,
            _buildUserList(context, 2, "Declined (${declined.length})", declined),
            _divider,
            _buildUserList(context, 3, "Pending (${pending.length})", pending),
            _divider,
            if (widget.showInviteButton)
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: TextButton(
                  text: "Invite People",
                  onPressed: () {
                    NavigationService.instance.pop(true);
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _buildUserList(
      BuildContext context, int tileIndex, String title, List<User> users) {
    var theme = Theme.of(context);

    return Column(
      children: <Widget>[
        TouchTarget(
          onTap: _expandableTile(tileIndex) ? () => _toggleActivityTile(tileIndex) : null,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    title,
                    style: theme.textTheme.subtitle1,
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
                if (_expandableTile(tileIndex))
                  _expandedTileIndex != tileIndex
                      ? Icon(Icons.keyboard_arrow_up, color: theme.primaryColor)
                      : Icon(Icons.keyboard_arrow_down, color: theme.primaryColor),
              ],
            ),
          ),
        ),
        SizeTransition(
          child: Container(
            height: 100,
            child: ListView.separated(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              scrollDirection: Axis.horizontal,
              physics: const AlwaysScrollableScrollPhysics(
                parent: const BouncingScrollPhysics(),
              ),
              itemBuilder: (context, index) {
                return _buildUserAvatar(users[index]);
              },
              separatorBuilder: (context, index) => const SizedBox(width: 16),
              itemCount: users.length,
            ),
          ),
          sizeFactor: CurvedAnimation(
            curve: Curves.easeOut,
            reverseCurve: Curves.easeIn,
            parent: _tileControllers[tileIndex],
          ),
        ),
      ],
    );
  }

  Widget _buildUserAvatar(User user) {
    return TouchTarget(
      onTap: () => _onUserAvatarTapped(user),
      child: Container(
        width: 56,
        child: Column(
          children: <Widget>[
            Container(
              child: UserAvatar(
                user: user,
                avatarSize: AvatarSize.thumbnail,
                placeholderSize: 32,
              ),
            ),
            const SizedBox(height: 4),
            Text(
              user.id == _localUserId ? "Me" : user.name,
              style: ThemeUtility.caption1(theme.textTheme),
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleActivityTile(int tileIndex) {
    if (_expandedTileIndex != tileIndex) {
      if (_expandedTileIndex != null) {
        _tileControllers[_expandedTileIndex].reverse();
      }
      _tileControllers[tileIndex].forward();
      _expandedTileIndex = tileIndex;
    } else {
      _tileControllers[tileIndex].reverse();
      _expandedTileIndex = null;
    }

    setState(() {});
  }

  bool _expandableTile(int tileIndex) {
    return widget.participants[_tileIndexToParticipantStatus[tileIndex]].length != 0;
  }

  void _onUserAvatarTapped(User user) {
    navigation.push<bool>(ContactInfoPage(user), modal: false);
  }
}
