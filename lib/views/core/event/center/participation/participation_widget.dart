import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide IconButton, TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/event_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/core/event/center/participation/participation_details_modal.dart';
import 'package:planhive/views/core/event/invite_to_event_page.dart';
import 'package:planhive/views/cross_platform/icon_button.dart';
import 'package:planhive/views/cross_platform/modal_sheet.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_formatter.dart';

class ParticipationWidget extends StatefulWidget {
  final Node<EventData> eventNode;
  final Node<ParticipationData> participationNode;

  const ParticipationWidget(this.eventNode, this.participationNode);

  @override
  State<StatefulWidget> createState() {
    return _ParticipationWidgetState();
  }
}

class _ParticipationWidgetState extends State<ParticipationWidget>
    with
        AutomaticKeepAliveClientMixin,
        ViewControllerMixin,
        RefreshControllerMixin,
        NotificationHandlerMixin {
  final _nodeStore = GetIt.I<NodeStore>();
  final _nodeService = GetIt.I<NodeService>();
  final _eventService = GetIt.I<EventService>();
  final _permissionService = GetIt.I<PermissionService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();

  var _isOrganizer = false;
  var _isAdmin = false;

  bool _dataLoaded = false;

  Map<ParticipantStatus, List<User>> _participants = const {
    ParticipantStatus.accepted: [],
    ParticipantStatus.maybe: [],
    ParticipantStatus.declined: [],
    // null is the 'pending' status
    null: [],
  };
  var _participantsList = const <User>[];
  ParticipantStatus _userParticipation;

  @override
  Future<bool> sync() async {
    var changes = await Future.wait([
      _nodeService.fetchChildren(widget.participationNode),
      _permissionService.fetchUpdates(),
    ]);

    return changes.contains(true);
  }

  @override
  Future<void> loadState() async {
    await Future.wait([
      loadParticipants(),
      loadUserParticipation(),
      loadPermission(),
    ]);

    _dataLoaded = true;
    _updateCounterService.clearWithChildren(widget.participationNode.id);
  }

  Future<void> loadParticipants() async {
    _participants = await _nodeStore.getParticipants(widget.participationNode);
    _participantsList = List.unmodifiable(_participants.values.expand((e) => e));
  }

  Future<void> loadUserParticipation() async {
    _userParticipation = await _nodeStore.getUserParticipation(widget.participationNode);
  }

  Future<void> loadPermission() async {
    var localPermission = await _permissionService
        .getUserPermission(widget.participationNode.permissionGroupId);

    _isOrganizer = localPermission.user.id == widget.participationNode.creatorId;
    _isAdmin = localPermission.permission.can(Permissions.write);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return Column(
      children: <Widget>[
        _buildParticipantsInfo(),
        if (!_isOrganizer) SizedBox(height: 8),
        if (!_isOrganizer) _buildParticipationResponse(),
      ],
    );
  }

  Widget _buildParticipantsInfo() {
    return GestureDetector(
      onTap: _onParticipationDetailsPressed,
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        child: Padding(
          padding: EdgeInsets.only(left: 16),
          child: Row(
            children: <Widget>[
              Text(
                "Participants",
                style: theme.textTheme.bodyText2,
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
              SizedBox(width: 8),
              Text(
                "${_participantsList.length}",
                style: ThemeUtility.caption1(theme.textTheme),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
              SizedBox(width: 16),
              Text(
                "Confirmed",
                style: theme.textTheme.bodyText2,
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
              SizedBox(width: 8),
              Expanded(
                child: Text(
                  "${_participants[ParticipantStatus.accepted].length}",
                  style: ThemeUtility.caption1(theme.textTheme),
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
              IconButton(
                onPressed: _onParticipationDetailsPressed,
                materialIcon: Icons.info_outline,
                cupertinoIcon: CupertinoIcons.info,
              ),
              if (_isAdmin)
                IconButton(
                  onPressed: _invitePeopleToEvent,
                  materialIcon: Icons.group_add,
                  cupertinoIcon: CupertinoIcons.person_add,
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildParticipationResponse() {
    if (!_dataLoaded) {
      return Container();
    }

    if (_userParticipation == null) {
      return GestureDetector(
        onTap: _openParticipationSelectionDialog,
        child: Card(
          elevation: 0,
          margin: EdgeInsets.all(0),
          child: Padding(
            padding: EdgeInsets.only(left: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "You have not responded yet.",
                  style: theme.textTheme.bodyText2,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                TextButton(
                  onPressed: _openParticipationSelectionDialog,
                  text: "RSVP",
                  formatText: false,
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return GestureDetector(
        onTap: _openParticipationSelectionDialog,
        child: Card(
          elevation: 0,
          margin: EdgeInsets.all(0),
          child: Padding(
            padding: EdgeInsets.only(left: 16),
            child: Row(
              children: <Widget>[
                Text(
                  "Your Response",
                  style: theme.textTheme.bodyText2,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
                SizedBox(width: 8),
                Expanded(
                  child: Text(
                    _userParticipation == ParticipantStatus.accepted
                        ? "Confirmed"
                        : toCapitalizedWords(
                            enumToAllCaps(_userParticipation, ParticipantStatus.values)),
                    style: ThemeUtility.caption1(theme.textTheme),
                    strutStyle: StrutStyle(
                      forceStrutHeight: true,
                    ),
                  ),
                ),
                IconButton(
                  onPressed: _openParticipationSelectionDialog,
                  materialIcon: Icons.more_horiz,
                  cupertinoIcon: CupertinoIcons.ellipsis,
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  void _invitePeopleToEvent() {
    GetIt.I<NavigationService>().push(
      InviteToEventPage(widget.eventNode, _participantsList),
    );
  }

  void _openParticipationSelectionDialog() async {
    var result = await showModalSheet<ParticipantStatus>(
      context: context,
      title: "Your response",
      actions: [
        ModalActionData<ParticipantStatus>(
          text: "Confirm",
          icon: Icons.check,
          value: ParticipantStatus.accepted,
        ),
        ModalActionData<ParticipantStatus>(
          text: "Maybe",
          icon: Icons.help_outline,
          value: ParticipantStatus.maybe,
        ),
        ModalActionData<ParticipantStatus>(
          text: "Decline",
          icon: Icons.cancel,
          value: ParticipantStatus.declined,
        ),
      ],
    );
    if (result != null) _changeStatus(result);
  }

  void _changeStatus(ParticipantStatus status) {
    runAsync(
      () async {
        await _eventService.changeParticipation(
          widget.participationNode,
          _userParticipation,
          status,
        );
        await refresh();
      },
      exclusive: true,
    );
  }

  void _onParticipationDetailsPressed() async {
    var inviteToEvent = await showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (ctx) {
        return ParticipationDetailsWidget(
          participants: _participants,
          showInviteButton: _isAdmin,
        );
      },
    );

    if (inviteToEvent == true) _invitePeopleToEvent();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([Node, widget.participationNode])) {
      refresh();
    }

    return SynchronousFuture(false);
  }
}
