import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/notification/node_notification_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/common/tab_analytics.dart';
import 'package:planhive/views/common/view_visibility.dart';
import 'package:planhive/views/core/event/center/center_main_page.dart';
import 'package:planhive/views/core/event/center/event_info_page.dart';
import 'package:planhive/views/core/event/threads/thread_widget.dart';
import 'package:planhive/views/core/photos/photos_view.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class EventPage extends StatefulWidget implements NavigationPage {
  final Node<EventData> event;
  final Node targetNode;

  EventPage(this.event, [this.targetNode]);

  @override
  State<StatefulWidget> createState() {
    return EventPageState();
  }

  @override
  bool get isWrapperPage => true;
}

class EventPageState extends State<EventPage>
    with
        RefreshControllerMixin,
        NotificationHandlerMixin,
        SingleTickerProviderStateMixin {
  final _nodeService = GetIt.I<NodeService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();

  Node<EventData> _event;

  TabController _tabController;

  @override
  void initState() {
    _event = widget.event;

    _tabController = TabController(
      initialIndex: _getPageFromNodeType(widget.targetNode?.type),
      vsync: this,
      length: 3,
    );
    _tabController.addListener(() {
      FocusScope.of(context).unfocus();
    });

    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  @override
  Future<void> loadState() async {
    _event = await _nodeService.getNode(_event.id);

    _updateCounterService.clear([_event.id]);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text(
        _event.data.name,
        overflow: TextOverflow.ellipsis,
      ),
      trailing: NavBarButton(
        onPressed: _onEventInfoTapped,
        materialIcon: Icons.tune,
        cupertinoIcon: CupertinoIcons.info,
      ),
      leading: NavBarButton(
        text: "Close",
        materialIcon: Icons.close,
        onPressed: () => NavigationService.instance.pop(),
      ),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Column(
        children: <Widget>[
          Container(
            color: Theme.of(context).backgroundColor,
            height: 40,
            child: TabAnalytics(
              controller: _tabController,
              tabPrefix: "EventPage",
              tabNames: ["Chat", "Event", "Album"],
              child: TabBar(
                controller: _tabController,
                labelColor: Theme.of(context).primaryColor,
                tabs: [
                  Tab(text: "Chat"),
                  Tab(text: "Event"),
                  Tab(text: "Album"),
                ],
              ),
            ),
          ),
          const Divider(
            height: 0,
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                ViewVisibility(
                  visibility: () => _tabController.index == 0,
                  child: ThreadWidgetPage(_event),
                ),
                CenterPage(_event),
                PhotosView(_event.id),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _onEventInfoTapped() async {
    await NavigationService.instance.push(
      EventInfoPage(
        _event,
      ),
      modal: false,
    );
  }

  void _showNodeType(NodeType nodeType) async {
    var pageIndex = _getPageFromNodeType(nodeType);

    _tabController.animateTo(
      pageIndex,
      duration: const Duration(milliseconds: 250),
      curve: Curves.ease,
    );
  }

  int _getPageFromNodeType(NodeType type) {
    switch (type) {
      case NodeType.thread:
        return 0;
      case NodeType.photos:
        return 2;
      default:
        return 1;
    }
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    if (notification.matches([_event, Node])) {
      if (clicked) {
        _showNodeType((notification as NodeNotificationData).nodes[1].type);
        return SynchronousFuture(true);
      }
    }

    return SynchronousFuture(false);
  }
}
