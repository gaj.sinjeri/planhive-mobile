import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/views/core/chats/thread_view.dart';

class ThreadWidgetPage extends StatefulWidget {
  final Node<EventData> event;

  ThreadWidgetPage(this.event);

  @override
  State<StatefulWidget> createState() {
    return _ThreadWidgetState();
  }
}

class _ThreadWidgetState extends State<ThreadWidgetPage> {
  final _nodeStore = GetIt.I<NodeStore>();

  Node<ThreadData> _thread;

  @override
  void initState() {
    super.initState();
    _findThread();
  }

  @override
  Widget build(BuildContext context) {
    if (_thread == null) {
      return Container(color: Theme.of(context).canvasColor);
    } else {
      return Container(
        color: Theme.of(context).cardColor,
        child: SafeArea(
          child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: ThreadView(
              widget.event,
              _thread,
              true,
              key: ValueKey(_thread.id),
            ),
          ),
        ),
      );
    }
  }

  void _findThread() async {
    var thread = await _nodeStore.getFirstChildOfType<ThreadData>(widget.event.id);
    setState(() {
      _thread = thread;
    });
  }
}
