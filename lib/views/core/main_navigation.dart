import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/notification/update_counter_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/badge/top_right_badge.dart';
import 'package:planhive/views/common/more_icons.dart';
import 'package:planhive/views/common/refresh_controller.dart';
import 'package:planhive/views/core/account/account_main_page.dart';
import 'package:planhive/views/core/chats/chats_main_page.dart';
import 'package:planhive/views/core/contacts/contacts_main_page.dart';
import 'package:planhive/views/core/event/events_page.dart';
import 'package:planhive/views/core/photos/photos_main_page.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/scaffold/tab_scaffold.dart';

class MainNavigation extends StatefulWidget implements NavigationPage {
  @override
  State<StatefulWidget> createState() {
    return _MainNavigationState();
  }

  @override
  bool get isWrapperPage => true;
}

class _MainNavigationState extends State<MainNavigation>
    with RefreshControllerMixin, NotificationHandlerMixin {
  final _nodeService = GetIt.I<NodeService>();
  final _permissionService = GetIt.I<PermissionService>();
  final _contactService = GetIt.I<ContactService>();
  final _updateCounterService = GetIt.I<UpdateCounterService>();
  final _userService = GetIt.I<UserService>();
  final _analyticsService = GetIt.I<AnalyticsService>();

  int _tabIndex;

  int _eventsCounter = 0;
  int _contactsCounter = 0;
  int _chatsCounter = 0;
  int _albumsCounter = 0;

  @override
  Future<bool> sync() async {
    var changes = await Future.wait([
      _nodeService.fetchFeeds([
        NodeType.event,
        NodeType.timeline,
        NodeType.chatGroup,
        NodeType.chatDirect,
        NodeType.thread,
        NodeType.photos,
      ]),
      _nodeService.fetchFeed(NodeType.participation),
      _permissionService.fetchUpdates(),
      _contactService.refreshContacts().then((value) => value.isNotEmpty),
    ]);

    return changes.contains(true);
  }

  @override
  Future<void> loadState() async {
    await Future.wait([
      () async {
        _eventsCounter = await _updateCounterService.countUnreadRoots([
          NodeType.activity,
          NodeType.participant,
          NodeType.event,
        ]);
      }(),
      () async {
        var contact = await _userService.getAllContactUsers();
        _contactsCounter = contact
            .where(
              (e) => e.contact.contactState == ContactState.invited,
            )
            .length;
      }(),
      () async {
        _chatsCounter = await _updateCounterService.count([
          NodeType.message,
        ]);
      }(),
      () async {
        _albumsCounter = await _updateCounterService.count([
          NodeType.photo,
        ]);
      }(),
    ]);
  }

  @override
  Future<bool> handleNotification(NotificationData notification, bool clicked) {
    refresh(true);

    return SynchronousFuture(false);
  }

  void _onTab(int tabIndex) {
    _tabIndex = tabIndex;
    _logTab();
  }

  void _logTab() {
    _analyticsService.logScreen([
      EventsPage,
      ContactsPage,
      ChatsPage,
      PhotosPage,
      AccountPage,
    ][_tabIndex]
        .toString());
  }

  @override
  void onResurfaced() {
    _logTab();
  }

  @override
  Widget build(BuildContext context) {
    return TabScaffold(
      onTab: _onTab,
      tabs: <Widget>[
        EventsPage(),
        ContactsPage(),
        ChatsPage(),
        PhotosPage(),
        AccountPage(),
      ],
      items: [
        BottomNavigationBarItem(
          icon: _BottomNavigationBadge(
            _eventsCounter,
            child: PlatformIcon(Icons.home, MoreIcons.cupertino_home_filled),
          ),
          label: "Events",
        ),
        BottomNavigationBarItem(
          icon: _BottomNavigationBadge(
            _contactsCounter,
            child: PlatformIcon(Icons.people, CupertinoIcons.group_solid),
          ),
          label: "Contacts",
        ),
        BottomNavigationBarItem(
          icon: _BottomNavigationBadge(
            _chatsCounter,
            child:
                PlatformIcon(Icons.chat_bubble, MoreIcons.cupertino_conversation_filled),
          ),
          label: "Chats",
        ),
        BottomNavigationBarItem(
          icon: _BottomNavigationBadge(
            _albumsCounter,
            child: PlatformIcon(Icons.photo, CupertinoIcons.collections_solid),
          ),
          label: "Albums",
        ),
        BottomNavigationBarItem(
          icon: PlatformIcon(Icons.account_circle, MoreIcons.cupertino_profile_filled),
          label: "Account",
        ),
      ],
    );
  }
}

class _BottomNavigationBadge extends TopRightBadge {
  _BottomNavigationBadge(int counter, {@required Widget child, Key key})
      : super(
          counter,
          top: platformLook == PlatformLook.cupertino ? 0 : -6,
          right: -21,
          child: child,
          key: key,
        );
}
