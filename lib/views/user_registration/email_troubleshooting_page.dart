import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/app_settings.dart';
import 'package:planhive/services/registration_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:url_launcher/url_launcher.dart';

class VerificationEmailTroubleshootingPage extends StatefulWidget {
  @override
  State<VerificationEmailTroubleshootingPage> createState() {
    return _VerificationEmailTroubleshootingPageState();
  }
}

class _VerificationEmailTroubleshootingPageState
    extends State<VerificationEmailTroubleshootingPage> with ViewControllerMixin {
  final _registrationService = GetIt.I<RegistrationService>();

  void _resendVerificationCode() async {
    await runAsync(() => _registrationService.resendCode(), exclusive: true);

    navigation.pop();
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Verification"),
      leading: NavBarButton(
        text: "Back",
        materialIcon: Icons.arrow_back,
        onPressed: () => navigation.pop(),
      ),
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: [
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(16),
                children: <Widget>[
                  Text(
                    "In case you have not received an email yet, please check your spam "
                    "folder. If you are using Gmail, then the verification email might "
                    "accidentally get categorised as promotional.",
                    style: theme.textTheme.subtitle1,
                  ),
                  SizedBox(height: 16),
                  Text(
                    "If you have not received an email after a few minutes, you can "
                    "request a new one to be sent to you.",
                    style: theme.textTheme.subtitle1,
                  ),
                  SizedBox(height: 32),
                  TextButton(
                    text: "Request New Verification Email",
                    onPressed: _resendVerificationCode,
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(16),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Still having trouble? ",
                  style: theme.textTheme.caption,
                  children: <TextSpan>[
                    TextSpan(
                      text: "Contact us",
                      style: const TextStyle(decoration: TextDecoration.underline),
                      recognizer: new TapGestureRecognizer()
                        ..onTap = () {
                          _launchURL(contactUsURL);
                        },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
