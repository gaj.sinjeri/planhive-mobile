import 'package:flutter/material.dart' hide TextFormField, TextButton;
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/pick_and_crop.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';
import 'package:planhive/views/user_registration/username_input_page.dart';

class CreateProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CreateProfilePageState();
  }
}

class _CreateProfilePageState extends State<CreateProfilePage> with ViewControllerMixin {
  final _userService = GetIt.I<UserService>();

  final _formKey = GlobalKey<FormState>();

  ImagePickAndCrop _imagePickAndCrop;
  TextEditingController _nameInput;

  @override
  void initState() {
    super.initState();

    _nameInput = TextEditingController();

    _imagePickAndCrop = ImagePickAndCrop(
      context,
      Size(userAvatarMaxResolution.toDouble(), userAvatarMaxResolution.toDouble()),
    );
  }

  @override
  void dispose() {
    super.dispose();

    _nameInput.dispose();

    _imagePickAndCrop.dispose();
  }

  void _pickAvatar() async {
    if (await _imagePickAndCrop.pickImage(context)) {
      setState(() {});
    }
  }

  void _onUserProfileSubmitted() async {
    var name = _nameInput.text.trim();

    if (!User.displayNameRegExp.hasMatch(name)) {
      await showAlertDialog<bool>(
        title: "Invalid Display Name",
        content: "Please make sure you enter a valid display name.",
        actions: [AlertActionData(text: "OK", value: true)],
      );
    } else {
      runAsync(() async {
        var avatar = await _imagePickAndCrop.toImage();

        GetIt.I<AnalyticsService>().logEvent(AnalyticsEvent.completedProfile);

        navigation.push(UsernameInputPage(avatar: avatar, name: name), modal: false);
      }, exclusive: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Profile"),
      trailing: NavBarButton(
        onPressed: _onUserProfileSubmitted,
        text: "Next",
      ),
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        loading: loading,
        child: Form(
          key: _formKey,
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: [
              Center(
                child: SizedBox(
                  height: 120,
                  width: 120,
                  child: ClipOval(
                    child: Stack(
                      children: [
                        if (_imagePickAndCrop.imageProvider != null)
                          Image(
                            image: _imagePickAndCrop.imageProvider,
                          )
                        else
                          Container(
                            color: Theme.of(context).primaryColor,
                          ),
                        SizedBox.expand(
                          child: FlatButton(
                            shape: CircleBorder(),
                            child: Icon(
                              Icons.add_a_photo,
                              color: _imagePickAndCrop.imageProvider == null
                                  ? Colors.white70
                                  : Colors.transparent,
                            ),
                            onPressed: _pickAvatar,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: TextButton(
                  text: _imagePickAndCrop.imageProvider == null
                      ? "Add Photo"
                      : "Edit Photo",
                  onPressed: _pickAvatar,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              TextFormField(
                controller: _nameInput,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.go,
                textCapitalization: TextCapitalization.words,
                hintText: "Display Name",
                validator: _userService.validateDisplayName,
                maxLength: User.maxDisplayNameLength,
                onFieldSubmitted: (String input) {
                  _onUserProfileSubmitted();
                },
                useIOSNativeView: false,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
