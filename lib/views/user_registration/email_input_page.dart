import 'dart:ui';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart' hide TextFormField;
import 'package:get_it/get_it.dart';
import 'package:planhive/common/constants/app_settings.dart';
import 'package:planhive/services/registration_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/theme_utility.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';
import 'package:planhive/views/user_registration/code_validation_page.dart';
import 'package:url_launcher/url_launcher.dart';

class EmailInputPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EmailInputPageState();
  }
}

class _EmailInputPageState extends State<EmailInputPage> with ViewControllerMixin {
  static const logo = const AssetImage('images/logo_1024.png');

  final _accountStore = GetIt.I<AccountStore>();
  final _registrationService = GetIt.I<RegistrationService>();

  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailInput;
  FocusNode _emailInputFocusNode;

  bool _focused = false;

  @override
  void initState() {
    _emailInput = TextEditingController(text: _accountStore.email);
    _emailInputFocusNode = FocusNode();

    _emailInputFocusNode.addListener(() {
      if (_focused != _emailInputFocusNode.hasFocus) {
        setState(() {
          _focused = _emailInputFocusNode.hasFocus;
        });
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _emailInput.dispose();

    super.dispose();
  }

  Future<String> _emailValidator(String input) async {
    return EmailValidator.validate(input) ? null : "Invalid email";
  }

  void _requestCode() async {
    var emailInput = _emailInput.text;

    if (!EmailValidator.validate(emailInput)) {
      await _registrationService.showInvalidEmailDialog();
      return;
    }

    runAsync(() async {
      if (await _registrationService.requestCode(emailInput)) {
        navigation.push(CodeValidationPage(), modal: false);
      }
    }, exclusive: true);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return PageScaffold(
      title: const Text("Welcome"),
      trailing: NavBarButton(
        text: "Next",
        onPressed: _requestCode,
      ),
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: [
            Expanded(
              child: ListView(
                padding: EdgeInsets.all(16),
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: SizedBox(
                      height: 200,
                      child: Image(
                        image: logo,
                      ),
                    ),
                  ),
                  Text(
                    appName,
                    textAlign: TextAlign.center,
                    style: theme.textTheme.headline3,
                  ),
                  SizedBox(height: 8),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Text(
                      "Enter your email to begin",
                      textAlign: TextAlign.center,
                      style: ThemeUtility.caption1(theme.textTheme),
                    ),
                  ),
                  SizedBox(height: 40),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Form(
                      key: _formKey,
                      child: TextFormField(
                        controller: _emailInput,
                        focusNode: _emailInputFocusNode,
                        keyboardType: TextInputType.emailAddress,
                        textInputAction: TextInputAction.go,
                        hintText: "Email",
                        errorMaxLines: 2,
                        validator: _emailValidator,
                        cacheValidationResult: false,
                        onFieldSubmitted: (String input) {
                          _requestCode();
                        },
                        useIOSNativeView: false,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (!_focused)
              Padding(
                padding: EdgeInsets.all(16),
                child: DefaultTextStyle(
                  style: theme.textTheme.caption,
                  child: const _TermsAndPrivacy(),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class _TermsAndPrivacy extends StatelessWidget {
  const _TermsAndPrivacy();

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: "By clicking next, you agree to the ",
        style: DefaultTextStyle.of(context).style,
        children: <TextSpan>[
          TextSpan(
            text: "Terms & Conditions",
            style: const TextStyle(decoration: TextDecoration.underline),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                _launchURL(termsAndConditionsURL);
              },
          ),
          TextSpan(
            text: " and ",
          ),
          TextSpan(
            text: "Privacy Policy",
            style: const TextStyle(decoration: TextDecoration.underline),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                _launchURL(privacyPolicyURL);
              },
          ),
          TextSpan(
            text: ".",
          ),
        ],
      ),
    );
  }
}
