import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';

class AccountRestoredPage extends StatelessWidget {
  void _onDone(BuildContext context) {
    GetIt.I<NavigationService>().startPage();
  }

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;

    return PageScaffold(
      title: Text("Account Restored"),
      trailing: NavBarButton(
        text: "Done",
        onPressed: () {
          _onDone(context);
        },
      ),
      backgroundColor: Colors.white,
      body: ListView(
        padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
        children: [
          Center(
            child: Text(
              "You have been logged into your existing account. "
              "Tap the done button to start using the app!",
              style: textTheme.subtitle1,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
