import 'package:flutter/material.dart' hide Image, TextFormField;
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:image/image.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/registration_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/views/common/format/force_lowercase_text_input_formatter.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';

class UsernameInputPage extends StatefulWidget {
  final Image avatar;
  final String name;

  UsernameInputPage({this.avatar, this.name, Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _UsernameInputPageState();
  }
}

class _UsernameInputPageState extends State<UsernameInputPage> with ViewControllerMixin {
  final _userService = GetIt.I<UserService>();
  final _registrationService = GetIt.I<RegistrationService>();

  final _formKey = GlobalKey<FormState>();

  TextEditingController _usernameInput;

  @override
  void initState() {
    super.initState();
    _usernameInput = TextEditingController();
  }

  @override
  void dispose() {
    _usernameInput.dispose();
    super.dispose();
  }

  void _submit() async {
    if (!User.usernameRegExp.hasMatch(_usernameInput.text)) {
      await showAlertDialog<bool>(
        title: "Invalid Username",
        content: "Please make sure you enter a valid username.",
        actions: [AlertActionData(text: "OK", value: true)],
      );
      return;
    }

    runAsync(() async {
      await _registrationService.registerUser(
        widget.name,
        _usernameInput.text,
        widget.avatar,
      );
    }, exclusive: true);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Username"),
      trailing: NavBarButton(
        text: "Next",
        onPressed: _submit,
      ),
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            const Text(
              "Please choose a username. You can use letters, "
              "numbers, periods (.) and underscores (_).",
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
            Divider(
              height: 16,
            ),
            Form(
              key: _formKey,
              child: TextFormField(
                controller: _usernameInput,
                textInputAction: TextInputAction.go,
                textCapitalization: TextCapitalization.none,
                keyboardType: TextInputType.text,
                hintText: "Username",
                validator: (input) => _userService.validateUsername(input),
                cacheValidationResult: true,
                inputFormatters: [
                  const ForceLowercaseTextInputFormatter(),
                  WhitelistingTextInputFormatter(User.usernameCharactersRegex),
                ],
                maxLength: User.maxUsernameLength,
                onFieldSubmitted: (String input) {
                  _submit();
                },
                useIOSNativeView: false,
              ),
            ),
            SizedBox(height: 16),
            Text(
              "Your username is your account's unique identifier. "
              "Your contacts will be able to find you using your username.",
              style: theme.textTheme.caption,
              strutStyle: StrutStyle(
                forceStrutHeight: true,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
