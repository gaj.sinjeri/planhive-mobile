import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:get_it/get_it.dart';
import 'package:planhive/services/registration_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/cross_platform/filled_button.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_button.dart';
import 'package:planhive/views/cross_platform/text_formatter.dart';
import 'package:planhive/views/user_registration/email_input_page.dart';
import 'package:planhive/views/user_registration/email_troubleshooting_page.dart';
import 'package:url_launcher/url_launcher.dart';

class CodeValidationPage extends StatefulWidget {
  @override
  State<CodeValidationPage> createState() {
    return _CodeValidationPageState();
  }
}

class _CodeValidationPageState extends State<CodeValidationPage>
    with ViewControllerMixin {
  final _registrationService = GetIt.I<RegistrationService>();
  final _accountStore = GetIt.I<AccountStore>();

  void _openEmailApp() {
    if (Platform.isAndroid) {
      AndroidIntent intent = AndroidIntent(
        action: 'android.intent.action.MAIN',
        category: 'android.intent.category.APP_EMAIL',
      );
      intent.launch();
    } else if (Platform.isIOS) {
      launch("message://");
    }
  }

  void _resendVerificationCode() async {
    // TODO(#106): This should be a separate page or data should be shown on this page
    //  instead of using alert dialogs
    var confirmed = await showAlertDialog(
      title: "Resend Verification Email",
      content: "Please allow a couple of minutes for the email to be delivered.",
      actions: [
        AlertActionData(text: "Cancel", value: false),
        AlertActionData(text: "Resend", value: true),
      ],
    );
    if (confirmed == true) {
      runAsync(() => _registrationService.resendCode(), exclusive: true);
    }
  }

  void _back() {
    if (navigation.canPop()) {
      navigation.pop();
    } else {
      navigation.replaceRoot(EmailInputPage(), modal: false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: const Text("Verification"),
      leading: NavBarButton(
        text: "Back",
        materialIcon: Icons.arrow_back,
        onPressed: _back,
      ),
      backgroundColor: Colors.white,
      body: LoadingIndicator(
        loading: loading,
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            Text.rich(
              TextSpan(
                text: "An email was sent to ",
                children: [
                  TextSpan(
                    text: _accountStore.email,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(
                    text: " and should arrive shortly.\n\n"
                        "Please click the link in the email to continue.",
                  )
                ],
              ),
              style: Theme.of(context).textTheme.subtitle1,
            ),
            SizedBox(height: 16),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 32),
              child: FilledButton(
                content: Text(
                  toPlatformStyleButtonText("Open Email App"),
                  style: theme.textTheme.subtitle1.copyWith(color: Colors.white),
                ),
                onPressed: _openEmailApp,
                backgroundColor: const Color(0xff09D669),
                outlineColor: const Color(0xff09D669),
              ),
            ),
            Divider(height: 32),
            TextButton(
              text: "I haven't received an email",
              onPressed: () =>
                  navigation.push(VerificationEmailTroubleshootingPage(), modal: false),
            ),
          ],
        ),
      ),
    );
  }
}
