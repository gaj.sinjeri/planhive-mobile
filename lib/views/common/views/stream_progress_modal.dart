import 'package:planhive/common/logger.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const _logger = Logger("StreamProgressModal");

Future<void> showStreamProgressModal({
  int operationCount,
  Stream stream,
  String operationDescription,
  String title,
}) async {
  var progressNotifier = ValueNotifier<int>(0);
  bool error = false;

  var subscription = stream.listen((event) {
    assert(progressNotifier.value + 1 <= operationCount);
    progressNotifier.value = progressNotifier.value + 1;
  }, onError: (e, s) {
    // set this to cause the modal to close
    progressNotifier.value = operationCount;
    _logger.error("An error occurred during: $operationDescription", e, s);
    error = true;
  }, onDone: () {
    assert(progressNotifier.value == operationCount);
  });

  await showAlertDialog(
    title: title,
    barrierDismissible: false,
    actions: [
      AlertActionData(text: "Cancel"),
    ],
    customContent: _StreamProgressView(
      operationCount,
      progressNotifier,
      operationDescription,
    ),
  );

  await subscription.cancel();

  if (error) {
    await showAlertDialog(
      title: title,
      actions: [AlertActionData(text: "Ok")],
      content: "$operationDescription encountered an error",
    );
  }
}

class _StreamProgressView extends StatefulWidget {
  final int operationCount;
  final ValueNotifier<int> progressNotifier;
  final String operationDescription;

  _StreamProgressView(
    this.operationCount,
    this.progressNotifier,
    this.operationDescription,
  );

  @override
  State<StatefulWidget> createState() {
    return _StreamProgressState();
  }
}

class _StreamProgressState extends State<_StreamProgressView> {
  int _uploadCounter;

  @override
  void initState() {
    _setUploadIndex();

    widget.progressNotifier.addListener(_setUploadIndex);

    super.initState();
  }

  void _setUploadIndex() {
    var value = widget.progressNotifier.value;
    if (value == widget.operationCount) {
      // delay the execution, because this can theoretically be called while the
      // widget is building, which is not supported by the framework.
      Future.microtask(
        () => NavigationService.instance.pop(),
      );
    } else {
      setState(() {
        _uploadCounter = value + 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Text(
        "${widget.operationDescription} $_uploadCounter of ${widget.operationCount}",
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }
}
