import 'package:flutter/material.dart';

class ScrollViewPlaceholder extends StatelessWidget {
  final bool hasContent;
  final Widget content;
  final Widget placeholder;

  ScrollViewPlaceholder({
    @required this.hasContent,
    @required this.content,
    @required this.placeholder,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    return hasContent
        ? content
        : CustomScrollView(
            slivers: <Widget>[
              SliverFillRemaining(
                child: Container(
                  padding: const EdgeInsets.all(16),
                  alignment: Alignment.center,
                  child: DefaultTextStyle(
                    style: textTheme.subtitle1.copyWith(color: textTheme.caption.color),
                    textAlign: TextAlign.center,
                    child: placeholder,
                  ),
                ),
              ),
            ],
          );
  }
}
