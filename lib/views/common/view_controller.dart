import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';

mixin ViewControllerMixin<T extends StatefulWidget> on State<T> {
  @protected
  final logger = Logger(T);

  bool _loading = false;

  bool get loading => _loading;

  NavigationService get navigation => GetIt.I<NavigationService>();

  ScaffoldState get scaffold => Scaffold.of(context);

  ThemeData get theme => Theme.of(context);

  MaterialLocalizations get localizations => MaterialLocalizations.of(context);

  /// Wraps the execution of [delegate], so any unhandled exception is logged and an error
  /// message is displayed to the user.
  ///
  /// If [exclusive] is set to true, [loading] will be set to true just before invoking
  /// [delegate], and back to false after [delegate] completes. In both cases the state
  /// will be rebuilt so the UI can display a loading indicator.
  /// While an exclusive operation is running, any subsequent operation will not be
  /// executed.
  ///
  /// [errorDialog] indicates whether the operation was initiated by the user, and
  /// a generic error message should be shown to the user if any error is thrown by
  /// [delegate]. The value is true by default.
  @protected
  Future<void> runAsync(
    Future<void> Function() delegate, {
    bool exclusive = false,
    bool errorDialog = true,
  }) async {
    assert(delegate != null);

    if (exclusive == true && _loading) {
      return;
    }

    if (exclusive == true) {
      setState(() {
        _loading = true;
      });
    }

    try {
      await delegate();
    } catch (e) {
      if (errorDialog == true && e is! ApiUnauthorizedException) {
        String title;
        String content;

        if (e is TimeoutException || e is SocketException) {
          title = "Connection Issue";
          content = "Please make sure you are connected to the internet.";
        } else {
          title = "Something Went Wrong";
        }

        showAlertDialog(
          context: context,
          title: title,
          content: content,
          actions: [AlertActionData(text: "Ok", value: true)],
        );
      }
      rethrow;
    } finally {
      if (mounted && exclusive == true) {
        setState(() {
          _loading = false;
        });
      }
    }
  }

  Future<void> setStateAsync(Future<void> Function() delegate) async {
    try {
      await delegate();

      if (mounted) {
        setState(() {});
      }
    } catch (e, s) {
      logger.error("Unhandled error during async operation", e, s);
    }
  }

  Future<void> setStateParallel(List<Future<void>> delegates) async {
    try {
      await Future.wait(delegates);

      if (mounted) {
        setState(() {});
      }
    } catch (e, s) {
      logger.error("Unhandled error during async operation", e, s);
    }
  }
}
