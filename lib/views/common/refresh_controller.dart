import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/async_debouncer.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/permission_service.dart';

/// Mixin that provides functionality to refresh a hierarchy of [StatefulWidget]s.
///
/// Classes using this mixin need to implement two methods: [sync] and [loadState].
/// [sync] should query the server and return [true] if any updates were fetched, while
/// [loadState] should load the state from the local DB.
///
/// [sync] and [loadState] shouldn't be called directly, instead [refresh] should be
/// called. [refresh] will also recursively trigger a refresh of any child
/// [StatefulWidget] that also uses this mixin.
mixin RefreshControllerMixin<T extends StatefulWidget> on State<T>
    implements WidgetsBindingObserver, RouteAware {
  final _logger = Logger("RefreshControllerMixin|$T");

  final _navigationService = GetIt.I<NavigationService>();

  RefreshControllerMixin _parent;
  List<RefreshControllerMixin> _children = [];
  ModalRoute _modalRoute;

  bool get isCurrentPage => _modalRoute?.isCurrent ?? true;

  RefreshControllerMixin get root => _parent == null ? this : _parent.root;

  bool _didPop = false;

  bool _initialized = false;

  /// When [sync] is [false] (default), only a local reload is performed. When [sync] is
  /// [true], a server sync is performed first, and a local reload is performed iff any new
  /// data was fetched.
  Future<void> refresh([bool sync = false]) async {
    assert(sync != null);

    if (!mounted) {
      return;
    }

    await root._refreshTree(sync);
  }

  /// An asynchronous operation that reads from the local DB and loads the required
  /// data into the [State]
  @protected
  Future<void> loadState() => Future.value();

  /// An asynchronous operation that queries the server, and returns [true] iff any new
  /// data was fetched.
  @protected
  Future<bool> sync() => Future.value(false);

  @protected
  Future<void> initAsync() => Future.value();

  @protected
  FutureOr<void> onResurfaced() => Future.value();

  @override
  void initState() {
    _parent = _findParent(context);
    _parent?._addChild(this);

    WidgetsBinding.instance.addObserver(this);

    super.initState();

    if (_parent == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) => _initAsync());
    } else if (root._initialized) {
      _initAsync();
    }
  }

  void _initAsync() async {
    await Future.wait(_initTree());

    if (!mounted) {
      return;
    }

    await Future.wait(_reloadTree());

    _initialized = true;

    if (mounted) {
      _rebuildTree();

      await _refreshTree(true);
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _modalRoute = ModalRoute.of(context);
    _navigationService.routeObserver.subscribe(this, _modalRoute);
  }

  @override
  void dispose() {
    _navigationService.routeObserver.unsubscribe(this);

    _parent?._removeChild(this);
    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  @override
  void didUpdateWidget(StatefulWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    _parent?._removeChild(this);
    _parent = _findParent(context);
    _parent?._addChild(this);
  }

  Future<void> _refreshTree(bool sync) async {
    bool reload = true;

    if (sync) {
      reload = await Future.wait(_syncTree()).then((e) => e.contains(true));

      if (!mounted) {
        return;
      }
    }

    if (reload) {
      await Future.wait(_reloadTree());

      if (mounted) {
        _rebuildTree();
      }
    }
  }

  Iterable<Future> _initTree() {
    return [initAsync()].followedBy(_children.map((e) => e._initTree()).expand((e) => e));
  }

  Iterable<Future<bool>> _syncTree() {
    return [_sync()].followedBy(_children.map((e) => e._syncTree()).expand((e) => e));
  }

  Iterable<Future> _reloadTree() {
    return [_loadState()]
        .followedBy(_children.map((e) => e._reloadTree()).expand((e) => e));
  }

  void _rebuildTree() {
    setState(() {});
    _children.forEach((e) => e._rebuildTree);
  }

  Future<void> _loadState() {
    _logger.debug("loadState");
    return loadState();
  }

  Future<bool> _sync() {
    _logger.debug("sync");
    return sync();
  }

  void _addChild(RefreshControllerMixin child) {
    _children.add(child);
  }

  void _removeChild(RefreshControllerMixin child) {
    _children.remove(child);
  }

  static RefreshControllerMixin _findParent(BuildContext context) {
    RefreshControllerMixin controller;
    context.visitAncestorElements((element) {
      if (element is StatefulElement && element.state is RefreshControllerMixin) {
        controller = element.state as RefreshControllerMixin;
        return false;
      }
      return true;
    });
    return controller;
  }

  static Future<bool> _fullSync() async {
    var results = await Future.wait([
      GetIt.I<NodeService>().fetchFeeds([
        NodeType.event,
        NodeType.chatDirect,
        NodeType.chatGroup,
        NodeType.thread,
        NodeType.participation,
        NodeType.timeline,
        NodeType.photos,
      ]),
      GetIt.I<PermissionService>().fetchUpdates(),
      GetIt.I<ContactService>().fetchUpdates(),
    ]);

    return results.contains(true);
  }

  // RouteAware interface

  @override
  void didPop() {
    _didPop = true;
  }

  @override
  void didPopNext() {
    Future.microtask(() async {
      if (!_didPop) {
        try {
          await onResurfaced();
        } finally {
          if (_parent == null) {
            await Future.wait(_reloadTree());

            if (mounted) {
              _rebuildTree();
            }
          }
        }
      }
    });
  }

  @override
  void didPush() {}

  @override
  void didPushNext() {}

  // WidgetsBindingObserver interface

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      if (await debounce(_fullSync) && isCurrentPage && _parent == null) {
        await Future.wait(_reloadTree());

        if (mounted) {
          _rebuildTree();
        }
      }
    }
  }

  @override
  void didChangeAccessibilityFeatures() {}

  @override
  void didChangeLocales(List<Locale> locale) {}

  @override
  void didChangeMetrics() {}

  @override
  void didChangePlatformBrightness() {}

  @override
  void didChangeTextScaleFactor() {}

  @override
  void didHaveMemoryPressure() {}

  @override
  Future<bool> didPopRoute() async => false;

  @override
  Future<bool> didPushRoute(String route) async => false;

  @override
  Future<bool> didPushRouteInformation(RouteInformation routeInformation) async => false;
}
