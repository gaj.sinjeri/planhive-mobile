import 'package:flutter/material.dart';

class ThemeUtility {
  static TextStyle caption1(TextTheme textTheme) {
    return textTheme.bodyText2.copyWith(color: textTheme.caption.color);
  }
}
