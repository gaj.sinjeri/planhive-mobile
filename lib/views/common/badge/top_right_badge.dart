import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/common/badge/badge.dart';
import 'package:planhive/views/common/badge/badge_animation_type.dart';
import 'package:planhive/views/common/badge/badge_position.dart';

class TopRightBadge extends StatelessWidget {
  final Widget child;
  final double top;
  final double right;
  final String _badge;
  final bool _hideCounter;
  final bool _showBadge;

  TopRightBadge(
    int counter, {
    @required this.child,
    this.top,
    this.right,
    bool empty,
    Key key,
  })  : assert(counter != null && counter >= 0),
        _badge = "${min(999, counter)}",
        _hideCounter = counter == 0 || empty == true,
        _showBadge = counter > 0,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    const backgroundColor = Colors.redAccent;
    return Badge(
      size: 20,
      position: BadgePosition.topRight(top: top ?? 12, right: right ?? 16),
      badgeColor: backgroundColor,
      animationType: BadgeAnimationType.scale,
      badgeContent: Text(
        _badge,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 12,
          color: _hideCounter ? backgroundColor : Colors.white,
        ),
      ),
      child: child,
      padding: const EdgeInsets.all(4),
      showBadge: _showBadge,
    );
  }
}
