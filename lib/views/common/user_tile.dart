import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/common/image/user_avatar.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class UserTile extends StatelessWidget {
  final User user;
  final Function onTap;
  final double avatarSize;
  final Widget trailing;
  final bool withBottomDivider;
  final bool withTopDivider;

  UserTile({
    @required this.user,
    @required this.onTap,
    this.avatarSize = 40,
    this.trailing,
    this.withBottomDivider = true,
    this.withTopDivider = false,
  });

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Column(
      children: <Widget>[
        if (withTopDivider)
          Padding(
            padding: EdgeInsets.only(
              left: 64,
            ),
            child: Divider(
              height: 2,
            ),
          ),
        TouchTarget(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.only(left: 16, bottom: 4, top: 4),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: avatarSize,
                  height: avatarSize,
                  child: UserAvatar(
                    user: user,
                    avatarSize: AvatarSize.thumbnail,
                    placeholderSize: avatarSize * 0.9,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(
                        user.id == GetIt.I<AccountService>().userId ? "Me" : user.name,
                        style: theme.textTheme.subtitle1,
                        strutStyle: StrutStyle(
                          forceStrutHeight: true,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(
                        "@${user.username}",
                        style: theme.textTheme.caption,
                        strutStyle: StrutStyle(
                          forceStrutHeight: true,
                        ),
                      ),
                    ),
                  ],
                ),
                Spacer(),
                if (trailing != null)
                  Padding(
                    padding: EdgeInsets.only(right: 16),
                    child: trailing,
                  ),
              ],
            ),
          ),
        ),
        if (withBottomDivider)
          Padding(
            padding: EdgeInsets.only(
              left: 64,
            ),
            child: Divider(
              height: 2,
            ),
          ),
      ],
    );
  }
}
