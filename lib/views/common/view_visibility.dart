import 'package:flutter/widgets.dart';

class ViewVisibility extends InheritedWidget {
  final bool Function() visibility;

  ViewVisibility({
    Key key,
    @required Widget child,
    @required this.visibility,
  })  : assert(visibility != null),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }

  static bool of(BuildContext context) {
    var element = context.getElementForInheritedWidgetOfExactType<ViewVisibility>();

    if (element == null) {
      // if no ViewVisibility ancestor is found, assume the view is always visible
      return true;
    }

    var visible = (element.widget as ViewVisibility).visibility();

    assert(visible is bool, "The visibility delegate must not return null");

    return visible;
  }
}
