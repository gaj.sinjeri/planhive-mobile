import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/views/common/refresh_controller.dart';

class TabAnalytics extends StatefulWidget {
  final TabController controller;
  final String tabPrefix;
  final List<String> tabNames;
  final Widget child;

  TabAnalytics({
    Key key,
    @required this.controller,
    @required this.child,
    @required this.tabPrefix,
    @required this.tabNames,
  })  : assert(tabNames != null && tabNames.length == controller.length),
        assert(tabPrefix != null && tabPrefix.isNotEmpty),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TabAnalyticsState();
  }
}

class _TabAnalyticsState extends State<TabAnalytics> with RefreshControllerMixin {
  final _analyticsService = GetIt.I<AnalyticsService>();

  @override
  void initState() {
    initWithWidget(widget);

    _logScreen();

    super.initState();
  }

  @override
  void didUpdateWidget(TabAnalytics oldWidget) {
    oldWidget.controller.removeListener(_logScreen);

    initWithWidget(widget);

    super.didUpdateWidget(oldWidget);
  }

  void initWithWidget(TabAnalytics widget) {
    widget.controller.addListener(_logScreen);
  }

  void _logScreen() {
    _analyticsService.logScreen(
      "${widget.tabPrefix}.${widget.tabNames[widget.controller.index]}",
    );
  }

  @override
  void onResurfaced() {
    _logScreen();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
