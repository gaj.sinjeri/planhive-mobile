import 'package:flutter/cupertino.dart';

class _CupertinoIconData extends IconData {
  const _CupertinoIconData(int codePoint)
      : super(
          codePoint,
          fontFamily: CupertinoIcons.iconFont,
          fontPackage: CupertinoIcons.iconFontPackage,
          matchTextDirection: true,
        );
}

// To add more icons, drop the font here https://fontdrop.info/, hover the
// glyph with the mouse and note down the unicode.

class MoreIcons {
  MoreIcons._();

  static const IconData cupertino_home_filled = _CupertinoIconData(0xf448);
  static const IconData cupertino_conversation_filled = _CupertinoIconData(0xf3fc);
  static const IconData cupertino_profile_filled = _CupertinoIconData(0xf41a);
  static const IconData cupertino_calendar_filled = _CupertinoIconData(0xf3f4);
  static const IconData cupertino_calendar = _CupertinoIconData(0xf3f3);
  static const IconData cupertino_arrow_back = _CupertinoIconData(0xf3d5);
  static const IconData cupertino_arrow_forward = _CupertinoIconData(0xf3d6);
  static const IconData cupertino_info_filled = _CupertinoIconData(0xf44d);
  static const IconData cupertino_sleep_filled = _CupertinoIconData(0xf468);
}
