import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  final Widget _child;
  final bool loading;

  LoadingIndicator({@required Widget child, @required this.loading, Key key})
      : _child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        _child,
        if (loading) LinearProgressIndicator(backgroundColor: Colors.transparent),
      ],
    );
  }
}
