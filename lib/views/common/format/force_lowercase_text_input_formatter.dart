import 'package:flutter/services.dart';

class ForceLowercaseTextInputFormatter implements TextInputFormatter {
  static final _upperCase = RegExp(r"\p{Lu}", unicode: true);

  const ForceLowercaseTextInputFormatter();

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    var text = newValue.text.replaceAllMapped(
      _upperCase,
      (match) => match.group(0).toLowerCase(),
    );

    return TextEditingValue(
      text: text,
      composing: TextRange.empty,
      selection: newValue.selection,
    );
  }
}
