import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/lat_lng.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/platform/location_service.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:platform_maps_flutter/platform_maps_flutter.dart' as maps;

class MapLocationPage extends StatefulWidget {
  final LatLng target;

  MapLocationPage([this.target]);

  @override
  State<StatefulWidget> createState() {
    return _MapLocationPage();
  }
}

class _MapLocationPage extends State<MapLocationPage> with ViewControllerMixin {
  LatLng _target;
  double _zoomLevel = 0;

  @override
  void initState() {
    super.initState();

    _target = widget.target;

    if (_target == null) {
      _initLocation();
    } else {
      _zoomLevel = 16;
    }
  }

  void _initLocation() async {
    setStateAsync(() async {
      _target = await GetIt.I<LocationService>().getCurrentPosition();

      if (_target != null) {
        _zoomLevel = 16;
      } else {
        _target = LatLng(0, 0);
      }
    });
  }

  void _onCameraMove(maps.CameraPosition position) {
    _target = LatLng(position.target.latitude, position.target.longitude);
  }

  void _done() {
    NavigationService.instance.pop(_target);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Pick Location"),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.arrow_back,
        onPressed: () => NavigationService.instance.pop(),
      ),
      trailing: NavBarButton(
        text: "Done",
        materialIcon: Icons.done,
        onPressed: _done,
      ),
      body: _target == null
          ? SizedBox.expand()
          : Stack(
              children: <Widget>[
                maps.PlatformMap(
                  rotateGesturesEnabled: false,
                  tiltGestureEnabled: false,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: true,
                  initialCameraPosition: maps.CameraPosition(
                    target: maps.LatLng(_target.lat, _target.lng),
                    zoom: _zoomLevel,
                  ),
                  onCameraMove: _onCameraMove,
                  markers: [
                    if (widget.target != null)
                      maps.Marker(
                        position: maps.LatLng(widget.target.lat, widget.target.lng),
                        markerId: maps.MarkerId("prev_location"),
                      ),
                  ].toSet(),
                ),
                Center(
                  child: IgnorePointer(
                    child: Icon(Icons.location_searching, color: Colors.black54),
                  ),
                ),
              ],
            ),
    );
  }
}
