import 'package:flutter/material.dart' hide TextFormField;
import 'package:get_it/get_it.dart';
import 'package:planhive/api/third_party/google_maps.dart';
import 'package:planhive/common/debouncer.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/platform/location_service.dart';
import 'package:planhive/services/platform/platform_permission_service.dart';
import 'package:planhive/views/common/loading_indicator.dart';
import 'package:planhive/views/common/view_controller.dart';
import 'package:planhive/views/cross_platform/scaffold/nav_bar_button.dart';
import 'package:planhive/views/cross_platform/scaffold/page_scaffold.dart';
import 'package:planhive/views/cross_platform/text_form_field.dart';

class PlaceSearchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PlaceSearchPageState();
  }
}

class _PlaceSearchPageState extends State<PlaceSearchPage> with ViewControllerMixin {
  final _googleMapsApi = GetIt.I<GoogleMapsApi>();
  final _locationService = GetIt.I<LocationService>();
  final _platformPermissionService = GetIt.I<PlatformPermissionService>();
  final _debouncer = Debouncer(duration: const Duration(seconds: 1));

  TextEditingController _textController;
  List<PlacePrediction> _predictions = const [];

  var _hasLocationPermission = true;

  @override
  void initState() {
    super.initState();

    _textController = TextEditingController();
    _textController.addListener(_onTextChanged);

    runAsync(_loadState);
  }

  Future<void> _loadState() async {
    setStateAsync(() async {
      _hasLocationPermission =
          await _platformPermissionService.hasLocationPermission(true);
    });
  }

  @override
  void dispose() {
    super.dispose();

    _textController.dispose();
    _debouncer.dispose();
  }

  void _onTextChanged() {
    if (loading) {
      return;
    }

    _debouncer.debounce(() {
      setStateAsync(() async {
        _predictions = await _googleMapsApi.placeAutocomplete(
          input: _textController.text,
          location: _hasLocationPermission == true
              ? await _locationService.getCurrentPosition()
              : null,
        );
        if (_predictions.isEmpty && !_hasLocationPermission) {
          _hasLocationPermission =
              await _platformPermissionService.hasLocationPermission(false);
        }
      });
    });
  }

  void _pickPlace(PlacePrediction prediction) {
    runAsync(() async {
      var details = await _googleMapsApi.placeDetails(prediction.placeId);
      navigation.pop(details);
    }, exclusive: true);
  }

  @override
  Widget build(BuildContext context) {
    return PageScaffold(
      title: Text("Find Place"),
      leading: NavBarButton(
        text: "Cancel",
        materialIcon: Icons.arrow_back,
        onPressed: () => NavigationService.instance.pop(),
      ),
      backgroundColor: theme.cardColor,
      body: LoadingIndicator(
        loading: loading,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16),
              child: TextFormField(
                controller: _textController,
                labelText: "Type a name or address",
                textInputAction: TextInputAction.search,
              ),
            ),
            if (_hasLocationPermission == false && _predictions.isEmpty)
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  "For more accurate search results, please grant location access.",
                  style: theme.textTheme.caption,
                  strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                ),
              ),
            Expanded(
              child: ListView(
                children: _predictions.map((prediction) {
                  final desc = prediction.description;
                  int splitIndex = desc.indexOf(',');

                  if (splitIndex != -1) {
                    return ListTile(
                      title: Text(desc.substring(0, splitIndex)),
                      subtitle: Text(desc.substring(splitIndex + 2)),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () => _pickPlace(prediction),
                    );
                  } else {
                    return ListTile(
                      title: Text(desc),
                      trailing: Icon(Icons.chevron_right),
                      onTap: () => _pickPlace(prediction),
                    );
                  }
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
