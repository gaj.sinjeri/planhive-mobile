import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_formatter.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class TextButton extends StatelessWidget {
  final String text;
  final Function() _onPressed;
  final EdgeInsetsGeometry padding;

  TextButton({
    @required String text,
    @required Function() onPressed,
    bool formatText = true,
    this.padding,
    Key key,
  })  : text = formatText ? toPlatformStyleButtonText(text) : text,
        _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    if (platformLook == PlatformLook.cupertino) {
      return CupertinoButton(
        child: Text(text),
        onPressed: _onPressed,
        padding: padding ?? EdgeInsets.symmetric(horizontal: 16),
      );
    } else {
      return TouchTarget(
        onTap: _onPressed,
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: kMinInteractiveDimension,
            minHeight: kMinInteractiveDimension,
          ),
          child: Padding(
            padding: padding ?? EdgeInsets.symmetric(horizontal: 16),
            child: Center(
              widthFactor: 1,
              heightFactor: 1,
              child: Text(
                text,
                style: theme.textTheme.button.copyWith(color: theme.primaryColor),
              ),
            ),
          ),
        ),
      );
    }
  }
}
