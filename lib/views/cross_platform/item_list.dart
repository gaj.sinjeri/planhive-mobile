import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class ItemListData {
  final String text;
  final PlatformIcon icon;
  final Function onTap;
  final bool showForwardArrow;

  ItemListData({
    @required this.text,
    @required this.icon,
    @required this.onTap,
    this.showForwardArrow = true,
  });
}

class ItemList extends StatelessWidget {
  final List<ItemListData> itemListData;

  ItemList({@required this.itemListData});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    List<Widget> items = [];
    for (int i = 0; i < itemListData.length; i++) {
      items.add(
        Column(
          children: <Widget>[
            TouchTarget(
              onTap: itemListData[i].onTap,
              child: Padding(
                padding: EdgeInsets.only(
                  left: 16,
                  top: 12,
                  bottom: 12,
                ),
                child: Row(
                  children: <Widget>[
                    itemListData[i].icon,
                    Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(
                        itemListData[i].text,
                        style: theme.textTheme.subtitle1,
                        strutStyle: StrutStyle(
                          forceStrutHeight: true,
                        ),
                      ),
                    ),
                    Spacer(),
                    if (itemListData[i].showForwardArrow)
                      Padding(
                        padding: EdgeInsets.only(right: 16),
                        child: PlatformIcon(
                          Icons.navigate_next,
                          CupertinoIcons.forward,
                          color: theme.primaryColor,
                        ),
                      ),
                  ],
                ),
              ),
            ),
            if (i != itemListData.length - 1)
              Padding(
                padding: EdgeInsets.only(
                  left: 56,
                ),
                child: Divider(
                  height: 0,
                  thickness: 0.3,
                ),
              ),
          ],
        ),
      );
    }

    return Align(
      alignment: Alignment.center,
      child: Card(
        elevation: 0,
        margin: EdgeInsets.all(0),
        child: Column(
          children: items,
        ),
      ),
    );
  }
}
