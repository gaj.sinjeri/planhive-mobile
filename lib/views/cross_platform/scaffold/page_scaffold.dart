import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide Scaffold;
import 'package:flutter/material.dart' as material show Scaffold;
import 'package:planhive/views/cross_platform/platform_look.dart';

class PageScaffold extends StatelessWidget {
  final Widget title;
  final Widget leading;
  final Widget trailing;
  final Widget body;
  final bool forceMaterial;
  final bool largeTitle;
  final Color backgroundColor;
  final bool safeAreaEnabled;
  final Color safeAreaColor;

  const PageScaffold({
    @required this.title,
    @required this.body,
    this.forceMaterial = true,
    this.leading,
    this.trailing,
    this.largeTitle = false,
    this.backgroundColor,
    this.safeAreaEnabled = false,
    this.safeAreaColor,
    Key key,
  })  : assert(largeTitle != null),
        assert(forceMaterial != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (platformLook == PlatformLook.cupertino) {
      var child = forceMaterial
          ? Material(
              color: backgroundColor,
              child: body,
            )
          : body;

      Widget scaffold;

      if (largeTitle) {
        final screenHeight = MediaQuery.of(context).size.height;
        const largeNavBarHeight = 120;

        scaffold = CustomScrollView(
          physics: const ClampingScrollPhysics(),
          slivers: <Widget>[
            CupertinoSliverNavigationBar(
              largeTitle: title,
              trailing: trailing,
              leading: leading,
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: screenHeight - largeNavBarHeight,
                child: child,
              ),
            ),
          ],
        );
      } else {
        scaffold = CupertinoPageScaffold(
          child: safeAreaEnabled
              ? Container(
                  color:
                      safeAreaColor ?? backgroundColor ?? Theme.of(context).canvasColor,
                  child: SafeArea(
                    child: child,
                  ),
                )
              : child,
          navigationBar: CupertinoNavigationBar(
            transitionBetweenRoutes: false,
            middle: title,
            leading: leading,
            trailing: trailing,
            backgroundColor: Theme.of(context).appBarTheme.color,
            padding: EdgeInsetsDirectional.only(
              start: 16,
              end: 8,
            ),
          ),
        );
      }
      return scaffold;
    } else {
      return material.Scaffold(
        body: body,
        backgroundColor: backgroundColor,
        appBar: AppBar(
          title: title,
          leading: leading,
          actions: trailing == null ? null : <Widget>[trailing],
        ),
      );
    }
  }
}
