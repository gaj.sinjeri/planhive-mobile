import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_formatter.dart';

class NavBarButton extends StatelessWidget {
  final String text;
  final Function() onPressed;
  final IconData materialIcon;
  final IconData cupertinoIcon;

  NavBarButton({
    String text,
    @required this.onPressed,
    this.materialIcon,
    this.cupertinoIcon,
    Key key,
  })  : assert(
          text != null || (materialIcon != null && cupertinoIcon != null),
          "The text must be set if either the cupertino or material icons are null",
        ),
        text = text == null ? null : toPlatformStyleButtonText(text),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    if (platformLook == PlatformLook.cupertino) {
      return CupertinoButton(
        padding: cupertinoIcon != null ? EdgeInsets.zero : EdgeInsets.only(right: 8),
        child: cupertinoIcon != null ? Icon(materialIcon) : Text(text),
        onPressed: onPressed,
      );
    } else {
      return materialIcon != null
          ? IconButton(
              icon: Icon(materialIcon),
              onPressed: onPressed,
            )
          : FlatButton(
              child: Text(text),
              onPressed: onPressed,
            );
    }
  }
}
