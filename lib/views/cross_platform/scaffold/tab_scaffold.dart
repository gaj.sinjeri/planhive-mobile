import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

class TabScaffold extends StatefulWidget {
  final List<Widget> tabs;
  final List<BottomNavigationBarItem> items;
  /// Called when a tab is shown, including the initial tab
  final void Function(int) onTab;

  const TabScaffold({
    @required this.tabs,
    @required this.items,
    this.onTab,
  });

  @override
  State<StatefulWidget> createState() {
    return _TabScaffoldState();
  }
}

class _TabScaffoldState extends State<TabScaffold> {
  int _tabIndex = 0;

  PageController _pageController;

  @override
  void initState() {
    _pageController = PageController();

    widget.onTab?.call(_tabIndex);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

    _pageController.dispose();
  }

  void _changePage(int index) {
    if(index != _tabIndex) {
      setState(() {
        _tabIndex = index;
        _pageController.jumpToPage(index);
      });

      widget.onTab?.call(index);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (platformLook == PlatformLook.cupertino) {
      return buildCupertino();
    } else {
      return buildMaterial();
    }
  }

  Widget buildCupertino() {
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        backgroundColor: Theme.of(context).bottomAppBarColor,
        items: widget.items,
        onTap: widget.onTab,
      ),
      tabBuilder: (context, index) => widget.tabs[index],
    );
  }

  Widget buildMaterial() {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: widget.items,
        onTap: _changePage,
        currentIndex: _tabIndex,
      ),
      body: PageView.builder(
        controller: _pageController,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: widget.tabs.length,
        itemBuilder: (context, index) => _PersistentPage(widget.tabs[index]),
      ),
    );
  }
}

class _PersistentPage extends StatefulWidget {
  final Widget child;

  _PersistentPage(this.child);

  @override
  State<StatefulWidget> createState() {
    return _PersistentPageState();
  }
}

class _PersistentPageState extends State<_PersistentPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.child;
  }
}
