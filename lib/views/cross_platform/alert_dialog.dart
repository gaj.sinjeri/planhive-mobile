import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_formatter.dart';

class AlertActionData<T> {
  final String text;
  final T value;

  AlertActionData({
    @required this.text,
    this.value,
  });
}

Future<T> showAlertDialog<T>({
  BuildContext context,
  String title,
  @required List<AlertActionData<T>> actions,
  String content,
  Widget customContent,
  bool barrierDismissible = true,
}) {
  context ??= NavigationService.instance.context;
  if (platformLook == PlatformLook.cupertino) {
    return _showIosDialog<T>(
      context,
      title == null ? null : Text(title),
      customContent ?? (content != null ? Text(content) : null),
      actions,
      barrierDismissible,
    );
  } else {
    return _showAndroidDialog<T>(
      context,
      title == null ? null : Text(title),
      customContent ?? (content != null ? Text(content) : null),
      actions,
      barrierDismissible,
    );
  }
}

Future<T> _showAndroidDialog<T>(
  BuildContext context,
  Widget title,
  Widget content,
  List<AlertActionData<T>> actions,
  bool barrierDismissible,
) async {
  var buttons = actions
      .map((action) => FlatButton(
            child: Text(toPlatformStyleButtonText(action.text)),
            onPressed: () => Navigator.of(context).pop(action.value),
          ))
      .toList();

  return await showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (ctx) {
      return AlertDialog(
        title: title,
        content: content,
        actions: buttons,
      );
    },
  );
}

Future<T> _showIosDialog<T>(
  BuildContext context,
  Widget title,
  Widget content,
  List<AlertActionData<T>> actions,
  bool barrierDismissible,
) async {
  List<Widget> buttons = [];
  for (int i = 0; i < actions.length; i++) {
    buttons.add(
      CupertinoDialogAction(
        isDefaultAction: i == actions.length - 1 ? true : false,
        child: Text(
          toPlatformStyleButtonText(actions[i].text),
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        onPressed: () => Navigator.of(context).pop(actions[i].value),
      ),
    );
  }

  return await showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (ctx) {
      return CupertinoAlertDialog(
        title: title,
        content: content,
        actions: buttons,
      );
    },
  );
}
