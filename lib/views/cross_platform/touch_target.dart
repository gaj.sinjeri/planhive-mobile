import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

class TouchTarget extends StatelessWidget {
  final Widget child;
  final Function onTap;

  TouchTarget({
    @required this.child,
    @required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    if (platformLook == PlatformLook.cupertino) {
      return InkWell(
        onTap: onTap,
        child: child,
        splashColor: Colors.transparent,
      );
    } else {
      return InkWell(
        onTap: onTap,
        child: child,
      );
    }
  }
}
