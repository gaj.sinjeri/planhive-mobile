import 'package:planhive/views/cross_platform/platform_look.dart';

// TODO(#104): Either create multiple functions for each use case or have one generic
//  function.
String toPlatformStyleButtonText(String source) {
  if (platformLook == PlatformLook.cupertino) {
    return toCapitalizedWords(source);
  } else {
    return source.toUpperCase();
  }
}

// Note: This won't work properly if first letter of word is e.g. '('
String toCapitalizedWords(String source) {
  return source
      .split(' ')
      .map((word) => word[0].toUpperCase() + word.substring(1).toLowerCase())
      .join(' ');
}
