import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide TextFormField;
import 'package:flutter/material.dart' as material show TextFormField;
import 'package:flutter/services.dart';
import 'package:flutter_native_text_input/flutter_native_text_input.dart';
import 'package:planhive/common/logger.dart';

typedef TextFieldValidator = FutureOr<String> Function(String);

class TextFormField extends StatefulWidget {
  final TextEditingController controller;
  final TextFieldValidator validator;
  final bool cacheValidationResult;
  final TextCapitalization textCapitalization;
  final TextInputType keyboardType;
  final String labelText;
  final String hintText;
  final TextInputAction textInputAction;
  final int errorMaxLines;
  final int minLines;
  final int maxLines;
  final ValueChanged<String> onFieldSubmitted;
  final List<TextInputFormatter> inputFormatters;
  final int maxLength;
  final FocusNode focusNode;
  final bool underline;
  final bool isDense;
  final Function onChanged;
  final bool useIOSNativeView;

  const TextFormField({
    Key key,
    this.controller,
    this.validator,
    this.cacheValidationResult = false,
    this.textCapitalization = TextCapitalization.none,
    this.keyboardType,
    this.labelText,
    this.hintText,
    this.textInputAction,
    this.errorMaxLines,
    this.minLines,
    this.maxLines = 1,
    this.onFieldSubmitted,
    this.inputFormatters,
    this.maxLength,
    this.focusNode,
    this.underline = true,
    this.isDense = false,
    this.onChanged,
    this.useIOSNativeView = true,
  })  : assert(cacheValidationResult != null),
        assert(textCapitalization != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _TextFormFieldState();
  }
}

class _TextFormFieldState extends State<TextFormField> {
  final _logger = Logger(_TextFormFieldState);

  final _fieldKey = GlobalKey<FormFieldState>();

  bool _autoValidate = false;
  var _validationCache = <String, String>{};
  String _prevValidation;
  bool _isRepeatValidation = false;
  int _validationCounter = 0;

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS && widget.useIOSNativeView) {
      return _getIOSTextField();
    }

    return material.TextFormField(
      key: _fieldKey,
      inputFormatters: widget.inputFormatters,
      maxLength: widget.maxLength,
      controller: widget.controller,
      textCapitalization: widget.textCapitalization,
      keyboardType: widget.keyboardType,
      textInputAction: widget.textInputAction,
      decoration: InputDecoration(
        border: widget.underline ? UnderlineInputBorder() : InputBorder.none,
        labelText: widget.labelText,
        hintText: widget.hintText,
        errorMaxLines: widget.errorMaxLines,
        isDense: widget.isDense,
      ),
      validator: _validator,
      minLines: widget.minLines,
      maxLines: widget.maxLines,
      onFieldSubmitted: widget.onFieldSubmitted,
      onChanged: _onChanged,
      focusNode: widget.focusNode,
      autocorrect: Platform.isIOS && !widget.useIOSNativeView ? false : true,
    );
  }

  Widget _getIOSTextField() {
    return NativeTextInput(
      controller: widget.controller,
      validator: _validator,
      focusNode: widget.focusNode,
      textInputAction: widget.textInputAction,
      textCapitalization: widget.textCapitalization,
      labelText: widget.labelText,
      hintText: widget.hintText,
      keyboardType: KeyboardType.defaultType,
      maxLines: widget.maxLines,
      onSubmitted: widget.onFieldSubmitted,
      onChanged: widget.onChanged,
      errorMaxLines: widget.errorMaxLines,
      underline: widget.underline,
    );
  }

  void _onChanged(String input) {
    if (input.isNotEmpty || _autoValidate) {
      _fieldKey.currentState?.validate();
      _autoValidate = true;
    }

    if (widget.onChanged != null) {
      widget.onChanged.call();
    }
  }

  String _validator(String input) {
    if (widget.validator == null) {
      return null;
    }

    if (_isRepeatValidation) {
      return _prevValidation;
    }

    if (widget.cacheValidationResult && _validationCache.containsKey(input)) {
      return _prevValidation = _validationCache[input];
    }

    var ret = widget.validator(input);

    if (ret is Future<String>) {
      _validateAsync(input, ret, ++_validationCounter);
      return _prevValidation;
    } else {
      return _prevValidation = ret;
    }
  }

  void _validateAsync(String input, Future<String> validator, int count) async {
    try {
      var error = await validator;

      if (widget.cacheValidationResult) {
        _validationCache[input] = error;
      }

      if (count == _validationCounter) {
        _prevValidation = error;
        _isRepeatValidation = true;
        _fieldKey.currentState?.validate();
        _isRepeatValidation = false;
      }
    } catch (e, s) {
      _logger.error("Error in validator delegate", e, s);
    }
  }
}
