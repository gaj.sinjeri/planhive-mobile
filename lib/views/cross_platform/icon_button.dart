import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material show IconButton;
import 'package:flutter/material.dart' hide IconButton;
import 'package:planhive/views/cross_platform/platform_look.dart';

class IconButton extends StatelessWidget {
  final IconData materialIcon;
  final IconData cupertinoIcon;
  final Function() _onPressed;
  final Color color;

  IconButton({
    @required IconData materialIcon,
    @required IconData cupertinoIcon,
    @required Function() onPressed,
    Color color,
    Key key,
  })  : materialIcon = materialIcon,
        cupertinoIcon = materialIcon,
        _onPressed = onPressed,
        color = color,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    if (platformLook == PlatformLook.cupertino) {
      return CupertinoButton(
        padding: EdgeInsets.zero,
        child: Icon(cupertinoIcon, color: color ?? theme.primaryColor),
        onPressed: _onPressed,
      );
    } else {
      return material.IconButton(
        icon: Icon(
          materialIcon,
          color: color ?? theme.primaryColor,
        ),
        onPressed: _onPressed,
      );
    }
  }
}
