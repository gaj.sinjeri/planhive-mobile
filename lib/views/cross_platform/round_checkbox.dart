import 'package:flutter/material.dart';

class RoundCheckBox extends StatelessWidget {
  final bool selected;

  RoundCheckBox(this.selected);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      height: 20,
      width: 20,
      decoration: BoxDecoration(
        color: selected ? theme.primaryColor : Colors.white,
        shape: BoxShape.circle,
        border: Border.all(
          color: theme.primaryColor,
        ),
      ),
      child: Center(
        child: selected
            ? Icon(
                Icons.check,
                size: 16.0,
                color: Colors.white,
              )
            : Container(),
      ),
    );
  }
}
