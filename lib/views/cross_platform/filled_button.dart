import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class FilledButton extends StatelessWidget {
  final Widget content;
  final Color backgroundColor;
  final Color outlineColor;
  final Function() _onPressed;

  FilledButton({
    @required Widget content,
    @required Function() onPressed,
    this.backgroundColor,
    this.outlineColor,
    Key key,
  })  : content = content,
        _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border: Border.all(
          color: outlineColor ?? theme.primaryColor,
        ),
      ),
      child: TouchTarget(
        onTap: _onPressed,
        child: Container(
          height: 32,
          width: 56,
          child: Center(
            child: content,
          ),
        ),
      ),
    );
  }
}
