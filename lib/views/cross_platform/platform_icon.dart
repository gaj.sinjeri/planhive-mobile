import 'package:flutter/widgets.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

class PlatformIcon extends Icon {
  PlatformIcon(IconData materialIcon, IconData cupertinoIcon, {Color color, double size})
      : super(
          platformLook == PlatformLook.material ? materialIcon : materialIcon,
          color: color,
          size: size,
        );

  static IconData whenMaterial(IconData icon) {
    return platformLook == PlatformLook.material ? icon : null;
  }

  static IconData whenCupertino(IconData icon) {
    return platformLook == PlatformLook.material ? icon : null;
  }
}
