import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_icon.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class TileButton extends StatelessWidget {
  final String text;
  final Function onTap;
  final PlatformIcon icon;
  final Color textColor;

  TileButton({
    @required this.text,
    @required this.onTap,
    this.icon,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return Card(
      elevation: 0,
      margin: EdgeInsets.all(0),
      child: TouchTarget(
        onTap: onTap,
        child: Padding(
          padding: EdgeInsets.only(left: 16, top: 14, bottom: 14),
          child: Row(
            children: <Widget>[
              if (icon != null)
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: icon,
                ),
              Text(
                text,
                style: theme.textTheme.subtitle1
                    .copyWith(color: textColor ?? theme.primaryColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
