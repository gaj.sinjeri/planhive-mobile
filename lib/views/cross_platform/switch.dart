import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material show Switch;
import 'package:flutter/material.dart' hide Switch;
import 'package:planhive/views/cross_platform/platform_look.dart';

class Switch extends StatelessWidget {
  final bool value;
  final Function onChanged;

  Switch({
    Key key,
    @required this.value,
    @required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    if (platformLook == PlatformLook.cupertino) {
      return CupertinoSwitch(
        value: value,
        onChanged: onChanged,
        activeColor: theme.primaryColor,
      );
    } else {
      return material.Switch(
        value: value,
        onChanged: onChanged,
        activeColor: theme.primaryColor,
      );
    }
  }
}
