import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

class ModalActionData<T> {
  final String text;
  final IconData icon;
  final T value;

  ModalActionData({
    @required this.text,
    this.icon,
    @required this.value,
  }) : assert(value != null);
}

Future<T> showModalSheet<T>({
  BuildContext context,
  String title,
  @required List<ModalActionData<T>> actions,
}) {
  context ??= NavigationService.instance.context;
  if (platformLook == PlatformLook.cupertino) {
    return _showIosSheet<T>(context, title, actions);
  } else {
    return _showAndroidSheet<T>(context, title, actions);
  }
}

Future<T> _showAndroidSheet<T>(
  BuildContext context,
  String title,
  List<ModalActionData<T>> actions,
) async {
  return await showModalBottomSheet<T>(
    context: context,
    builder: (ctx) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          if (title != null)
            ListTile(
              title: Text(
                title,
                style:
                    Theme.of(context).textTheme.subtitle1.copyWith(color: Colors.black45),
                strutStyle: StrutStyle(
                  forceStrutHeight: true,
                ),
              ),
            ),
        ]..addAll(actions.map((action) => _buildMaterialActionTile(context, action))),
      );
    },
    enableDrag: true,
  );
}

Widget _buildMaterialActionTile(BuildContext context, ModalActionData action) {
  return ListTile(
    leading: action.icon != null
        ? Icon(
            action.icon,
            color: Theme.of(context).primaryColor,
          )
        : null,
    title: Text(action.text),
    onTap: () => Navigator.pop(context, action.value),
  );
}

Future<T> _showIosSheet<T>(
  BuildContext context,
  String title,
  List<ModalActionData<T>> actions,
) async {
  var buttons = actions
      .map(
        (action) => CupertinoActionSheetAction(
          child: Text(action.text),
          onPressed: () async {
            Navigator.pop(context, action.value);
          },
        ),
      )
      .toList(growable: false);

  var sheet = CupertinoActionSheet(
    title: title != null ? Text(title) : null,
    actions: buttons,
    cancelButton: CupertinoActionSheetAction(
      child: Text("Cancel"),
      onPressed: () => {
        Navigator.pop(context, null),
      },
    ),
  );

  return await showCupertinoModalPopup<T>(
    context: context,
    builder: (ctx) {
      return sheet;
    },
  );
}
