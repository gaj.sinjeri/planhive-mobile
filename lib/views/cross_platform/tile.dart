import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class Tile extends StatelessWidget {
  final Widget content;
  final Widget leading;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final Function onTap;

  Tile({
    @required this.content,
    this.leading,
    this.padding = const EdgeInsets.only(left: 16, top: 14, bottom: 14),
    this.margin = const EdgeInsets.all(0),
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: margin,
      child: _addTouchTarget(
        Padding(
          padding: padding,
          child: Row(
            children: <Widget>[
              if (leading != null)
                Padding(
                  padding: EdgeInsets.only(right: 16),
                  child: leading,
                ),
              Expanded(
                child: content,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _addTouchTarget(Widget child) {
    return onTap != null ? TouchTarget(onTap: onTap, child: child) : child;
  }
}
