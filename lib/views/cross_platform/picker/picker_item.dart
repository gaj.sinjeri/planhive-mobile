import 'package:flutter/widgets.dart';

class PickerItem extends StatelessWidget {
  final Widget leading;
  final Widget label;
  final Widget trailing;
  final bool expandedLabel;

  const PickerItem({
    @required this.label,
    this.leading,
    this.trailing,
    this.expandedLabel,
  }) : assert(label != null);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        if (leading != null)
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: leading,
          ),
        Flexible(
          fit: expandedLabel == true ? FlexFit.tight : FlexFit.loose,
          flex: 1,
          child: label,
        ),
        if (trailing != null)
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: trailing,
          ),
      ],
    );
  }
}
