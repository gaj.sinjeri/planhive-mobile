import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

Future<DateTime> pickDate({
  @required BuildContext context,
  @required DateTime initialDate,
  @required DateTime firstDate,
  @required DateTime lastDate,
}) async {
  // Disable cupertino style picker until a better alternative is found
  final platformLook = PlatformLook.material;

  if (platformLook == PlatformLook.cupertino) {
    return await showModalBottomSheet<DateTime>(
        context: context,
        builder: (ctx) {
          return _CupertinoDatePickerModal(
            initialDate: initialDate,
            minimumDate: firstDate,
            maximumDate: lastDate,
          );
        });
  } else {
    return await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: lastDate,
      locale: Locale(ui.window.locale.languageCode, ui.window.locale.countryCode),
    );
  }
}

class _CupertinoDatePickerModal extends StatefulWidget {
  final DateTime initialDate;
  final DateTime minimumDate;
  final DateTime maximumDate;

  _CupertinoDatePickerModal({
    @required this.initialDate,
    @required this.minimumDate,
    @required this.maximumDate,
  });

  @override
  State<StatefulWidget> createState() {
    return _CupertinoDatePickerModalState();
  }
}

class _CupertinoDatePickerModalState extends State<_CupertinoDatePickerModal> {
  DateTime _newDate;

  @override
  void initState() {
    super.initState();

    _newDate = widget.initialDate;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            CupertinoButton(
              child: Text("Discard"),
              onPressed: () => Navigator.of(context).pop(),
            ),
            Spacer(),
            CupertinoButton(
              child: Text("Done"),
              onPressed: () => Navigator.of(context).pop(_newDate),
            ),
          ],
        ),
        Container(
          height: 216,
          margin: EdgeInsets.only(bottom: 40),
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            onDateTimeChanged: _onDateTimeChanged,
            initialDateTime: widget.initialDate,
            minimumDate: widget.minimumDate,
            maximumDate: widget.maximumDate,
          ),
        ),
      ],
    );
  }

  void _onDateTimeChanged(DateTime newDate) {
    setState(() {
      _newDate = newDate;
    });
  }
}
