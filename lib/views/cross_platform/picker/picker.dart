import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' hide TextButton;
import 'package:planhive/views/cross_platform/platform_look.dart';
import 'package:planhive/views/cross_platform/text_button.dart';

typedef _Builder = Widget Function(BuildContext, int);

class Picker extends StatefulWidget {
  final _Builder itemBuilder;
  final int initialIndex;
  final int childCount;

  Picker({
    @required this.itemBuilder,
    this.initialIndex,
    this.childCount,
  });

  @override
  State<Picker> createState() {
    return _PickerState();
  }

  static Future<int> show({
    @required BuildContext context,
    @required _Builder itemBuilder,
    int childCount,
    int initialIndex,
  }) async {
    return await showModalBottomSheet<int>(
      context: context,
      builder: (context) => Picker(
        itemBuilder: itemBuilder,
        childCount: childCount,
        initialIndex: initialIndex,
      ),
    );
  }
}

class _PickerState extends State<Picker> {
  FixedExtentScrollController _scrollController;

  int _selectedIndex;

  @override
  void initState() {
    super.initState();

    _selectedIndex = widget.initialIndex ?? 0;

    _scrollController = FixedExtentScrollController(initialItem: _selectedIndex);
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (platformLook == PlatformLook.cupertino) {
      return buildCupertino();
    } else {
      return buildMaterial();
    }
  }

  void _onSelectionChanged(int index) {
    _selectedIndex = index;
  }

  Widget buildCupertino() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            TextButton(
              text: "Discard",
              onPressed: () => Navigator.of(context).pop(),
            ),
            Spacer(),
            TextButton(
              text: "Done",
              onPressed: () => Navigator.of(context).pop(_selectedIndex),
            ),
          ],
        ),
        Container(
          height: 216,
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: CupertinoPicker.builder(
            scrollController: _scrollController,
            itemExtent: 40,
            childCount: widget.childCount,
            itemBuilder: (context, i) => widget.itemBuilder(context, i),
            onSelectedItemChanged: _onSelectionChanged,
          ),
        ),
      ],
    );
  }

  Widget buildMaterial() {
    // TODO(#109): material style pop-up
    return buildCupertino();
  }
}
