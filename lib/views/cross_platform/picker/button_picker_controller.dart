import 'dart:math';

import 'package:flutter/foundation.dart';

class ButtonPickerController extends ValueNotifier<int> {
  ButtonPickerController(value) : super(max(0, value));

  @override
  set value(int index) {
    index = max(0, index);
    super.value = index;
  }
}
