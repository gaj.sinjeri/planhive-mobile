import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/common/time.dart';
import 'package:planhive/views/cross_platform/platform_look.dart';

Future<TimeOfDay> pickTime({
  @required BuildContext context,
  @required DateTime initialDate,
}) async {
  if (platformLook == PlatformLook.cupertino) {
    return await showModalBottomSheet<TimeOfDay>(
        context: context,
        builder: (ctx) {
          return _CupertinoTimePickerModal(
            initialDate: initialDate,
          );
        });
  } else {
    // TODO(#101): Implement custom time picker with 5-minute interval.
    return await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(initialDate),
    );
  }
}

class _CupertinoTimePickerModal extends StatefulWidget {
  final DateTime initialDate;

  _CupertinoTimePickerModal({
    @required this.initialDate,
  });

  @override
  State<StatefulWidget> createState() {
    return _CupertinoTimePickerModalState();
  }
}

class _CupertinoTimePickerModalState extends State<_CupertinoTimePickerModal> {
  DateTime _newDate;

  @override
  void initState() {
    super.initState();

    _newDate = widget.initialDate;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          children: <Widget>[
            CupertinoButton(
              child: Text("Discard"),
              onPressed: () => Navigator.of(context).pop(),
            ),
            Spacer(),
            CupertinoButton(
              child: Text("Done"),
              onPressed: () => Navigator.of(context).pop(
                TimeOfDay(
                  hour: _newDate.hour,
                  minute: _newDate.minute,
                ),
              ),
            ),
          ],
        ),
        Container(
          height: 216,
          margin: EdgeInsets.only(bottom: 40),
          child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.time,
            onDateTimeChanged: _onDateTimeChanged,
            initialDateTime: widget.initialDate,
            use24hFormat: use24hFormat(context),
            // TODO(#101): Enable the 5 minute-interval once it's implemented for android.
            //  No update handler should be setting the minutes to values that are not
            //  multiples of 5 anymore, as otherwise this will break.
//            minuteInterval: 5,
          ),
        ),
      ],
    );
  }

  void _onDateTimeChanged(DateTime newDate) {
    setState(() {
      _newDate = newDate;
    });
  }
}
