import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/picker/button_picker_controller.dart';
import 'package:planhive/views/cross_platform/picker/picker.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

typedef _Builder = Widget Function(BuildContext, int);

class ButtonPicker extends StatefulWidget {
  final _Builder itemBuilder;
  final _Builder buttonItemBuilder;
  final int childCount;
  final ButtonPickerController controller;

  ButtonPicker({
    @required this.itemBuilder,
    @required this.childCount,
    @required this.controller,
    _Builder buttonItemBuilder,
  })  : assert(itemBuilder != null),
        assert(childCount != null),
        assert(controller != null),
        buttonItemBuilder = buttonItemBuilder ?? itemBuilder;

  @override
  State<ButtonPicker> createState() {
    return _ButtonPickerState();
  }
}

class _ButtonPickerState extends State<ButtonPicker> {
  void _showPicker() async {
    var selection = await Picker.show(
      context: context,
      itemBuilder: widget.itemBuilder,
      childCount: widget.childCount,
      initialIndex: widget.controller.value,
    );

    if (selection != null) {
      setState(() {
        widget.controller.value = selection;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return TouchTarget(
      onTap: _showPicker,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 12),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (widget.childCount > 0)
              widget.buttonItemBuilder(context, widget.controller.value),
            const Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
    );
  }
}
