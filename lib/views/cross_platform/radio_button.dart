import 'package:flutter/material.dart';
import 'package:planhive/views/cross_platform/touch_target.dart';

class RadioButton<T> extends StatelessWidget {
  final void Function(T) onChanged;
  final T value;
  final T groupValue;
  final String text;

  RadioButton({
    Key key,
    @required this.onChanged,
    @required this.value,
    @required this.groupValue,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TouchTarget(
      onTap: () => onChanged(value),
      child: _buildButtons(context),
    );
  }

  Widget _buildButtons(BuildContext context) {
    var theme = Theme.of(context);

    return Row(
      children: <Widget>[
        Radio<T>(
          onChanged: onChanged,
          value: value,
          groupValue: groupValue,
          activeColor: theme.primaryColor,
        ),
        Text(
          text,
          style: theme.textTheme.subtitle1,
          strutStyle: StrutStyle(
            forceStrutHeight: true,
          ),
        ),
        SizedBox(width: 16)
      ],
    );
  }
}
