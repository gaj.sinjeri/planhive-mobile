import 'package:flutter/foundation.dart';

enum Permissions {
  read,
  write,
  delete,
}

class Permission {
  final String userId;
  final String groupId;
  final int flags;
  final int creationTs;
  final int lastUpdateTs;
  final bool deleted;

  Permission({
    @required this.flags,
    @required this.userId,
    @required this.groupId,
    @required this.creationTs,
    @required this.lastUpdateTs,
    this.deleted,
  });

  bool can(Permissions permission) => flags & (1 << permission.index) != 0;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Permission &&
          runtimeType == other.runtimeType &&
          userId == other.userId &&
          groupId == other.groupId;

  @override
  int get hashCode => userId.hashCode ^ groupId.hashCode;

  @override
  String toString() {
    return 'Permission{userId: $userId, groupId: $groupId, flags: $flags, '
        'creationTs: $creationTs, lastUpdateTs: $lastUpdateTs, deleted: $deleted}';
  }
}
