import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/user.dart';

class UserReaction {
  final User user;
  final ActivityReactionType reaction;

  UserReaction(this.user, this.reaction);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserReaction &&
          runtimeType == other.runtimeType &&
          user == other.user &&
          reaction == other.reaction;

  @override
  int get hashCode => user.hashCode ^ reaction.hashCode;

  @override
  String toString() {
    return 'UserReaction{user: $user, reaction: $reaction}';
  }
}
