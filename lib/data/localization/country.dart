import 'package:flutter/foundation.dart';

class Country {
  final String emoji;
  final String code;
  final String dialCode;
  final String name;

  const Country({
    @required this.emoji,
    @required this.code,
    @required this.dialCode,
    @required this.name,
  })  : assert(emoji != null),
        assert(code != null),
        assert(dialCode != null),
        assert(name != null);

  @override
  String toString() {
    return 'Country{emoji: $emoji, code: $code, dialCode: $dialCode, name: $name}';
  }

  @override
  bool operator ==(Object other) => other is Country && code == other.code;

  @override
  int get hashCode => code.hashCode;
}
