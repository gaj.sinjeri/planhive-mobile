import 'package:flutter/foundation.dart';

class User {
  static final usernameRegExp = RegExp(r"(?=^.{3,25}$)^[a-z0-9]([._]?[a-z0-9])*$");
  static final displayNameRegExp =
      RegExp(r"(?=^.{3,25}$)^\p{L}([ '-]?\p{L})*$", unicode: true);
  static final usernameCharactersRegex = RegExp(r"[._a-z0-9]");
  static final displayNameCharactersRegex = RegExp(r"[-' \p{L}]", unicode: true);
  static const minUsernameLength = 3;
  static const maxUsernameLength = 25;
  static const minDisplayNameLength = 3;
  static const maxDisplayNameLength = 25;

  final String id;
  final String username;
  final String name;
  final String email;
  final int lastUpdateTs;
  final int lastAvatarUpdateTs;

  User({
    @required this.id,
    @required this.username,
    @required this.name,
    @required this.email,
    @required this.lastUpdateTs,
    @required this.lastAvatarUpdateTs,
  }) : assert(id != null);

  String get initials {
    if (name.length == 0) {
      return "";
    }
    var initials = name[0];
    final lastInitial = name.lastIndexOf(' ') + 1;
    if (lastInitial > 0 && lastInitial < name.length) {
      initials += name[lastInitial];
    }
    return initials;
  }

  bool get hasAvatar => lastAvatarUpdateTs != 0;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'User{id: $id, username: $username, name: $name, email: $email, '
        'lastUpdateTs: $lastUpdateTs, lastAvatarUpdateTs: $lastAvatarUpdateTs}';
  }
}
