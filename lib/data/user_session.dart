import 'package:planhive/data/user.dart';

class UserSession {
  final User user;
  final String sessionId;

  UserSession(this.user, this.sessionId);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserSession &&
          runtimeType == other.runtimeType &&
          user == other.user &&
          sessionId == other.sessionId;

  @override
  int get hashCode => user.hashCode ^ sessionId.hashCode;

  @override
  String toString() {
    return 'UserSession{user: $user, sessionId: $sessionId}';
  }
}
