import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/user.dart';

class UserNode<T extends NodeData> {
  final User user;
  final Node<T> node;

  UserNode(this.user, this.node);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserNode &&
          runtimeType == other.runtimeType &&
          user == other.user &&
          node == other.node;

  @override
  int get hashCode => user.hashCode ^ node.hashCode;

  @override
  String toString() {
    return 'UserNode{user: $user, node: $node}';
  }
}
