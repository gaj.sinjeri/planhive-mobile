import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/notification/notification_data.dart';

class NodeNotificationData extends NotificationData {
  final List<NodeId> nodes;

  NodeNotificationData(this.nodes);

  bool matches(List pattern, {matchAll = false}) {
    for (int i = 0; i < pattern.length; i++) {
      if (i == nodes.length) {
        return false;
      }

      if (!_match(pattern[i], nodes[i])) {
        return false;
      }
    }

    return !matchAll || pattern.length == nodes.length;
  }

  static bool _match(dynamic match, NodeId node) {
    if (match is Node) {
      return match.id == node.id;
    } else if (match is NodeType) {
      return match == node.type;
    } else if (match is String) {
      return match == node.id;
    } else if (match is List) {
      return match.any((match) => _match(match, node));
    } else if (match is Type) {
      return match == Node;
    }

    return false;
  }

  @override
  String toString() {
    return 'NodeNotificationData{nodes: $nodes}';
  }
}
