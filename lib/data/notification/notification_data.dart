import 'package:flutter/foundation.dart';
import 'package:planhive/data/node/node.dart';

@immutable
abstract class NotificationData {
  /// Takes a list of objects to match against this notification's data.
  ///
  /// The type of each element in the pattern list is dynamic, and depends on the type
  /// of notification.
  /// [matchAll] can be set to true if the pattern needs to match the all the notification
  /// data.
  ///
  /// An element of the pattern can also be a [List], in which case it will match if any
  /// element of the nested [List] matches.
  ///
  /// Other allowed element types are:
  /// * [String] to match an exact ID
  /// * [Type] to match any resource of that type (e.g. [User], [Node])
  /// * [NodeType] to match any node with that type
  /// * [User] and [Node] to match an exact user or node
  bool matches(List pattern, {bool matchAll = false});
}
