import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/user.dart';

class UserNotificationData extends NotificationData {
  final String userId;

  UserNotificationData(this.userId);

  @override
  bool matches(List pattern, {matchAll = false}) {
    if (pattern.length == 0) {
      return !matchAll;
    }

    var match = pattern[0];

    if (match is User) {
      return match.id == userId;
    } else if (match is String) {
      return match == userId;
    } else if (match is Type) {
      return match == User;
    }

    return false;
  }

  @override
  String toString() {
    return 'UserNotificationData{userId: $userId}';
  }
}
