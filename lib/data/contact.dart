import 'package:flutter/cupertino.dart';

enum ContactState {
  requested,
  invited,
  connected,
  rejected,
  deleted,
}

class Contact {
  final ContactState contactState;
  final String contactId;
  final int lastUpdateTs;

  Contact({
    @required this.contactState,
    @required this.contactId,
    this.lastUpdateTs,
  })  : assert(contactState != null),
        assert(contactId != null);

  @override
  String toString() {
    return 'Contact{contactState: $contactState, contactId: $contactId, '
        'lastUpdateTs: $lastUpdateTs}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Contact &&
          runtimeType == other.runtimeType &&
          contactId == other.contactId;

  @override
  int get hashCode => contactId.hashCode;
}
