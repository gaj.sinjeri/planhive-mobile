import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/message_data.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/data/node/photos_data.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/node/timeline_data.dart';

enum NodeType {
  event,
  participation,
  thread,
  timeline,
  photos,
  participant,
  activity,
  activityReaction,
  chatDirect,
  chatGroup,
  message,
  photo
}

class Node<T extends NodeData> {
  final NodeId nodeId;
  final String permissionGroupId;
  final String creatorId;
  final int creationTs;
  final int lastUpdateTs;
  final String jsonData;
  final bool deleted;
  final bool unread;
  final bool localOnly;

  T _data;

  T get data {
    if (_data == null && nodeId.type != null && jsonData != null) {
      _data = NodeData.parse<T>(jsonData);
    }
    return _data;
  }

  Node({
    @required this.nodeId,
    @required this.lastUpdateTs,
    @required this.creatorId,
    this.creationTs,
    this.permissionGroupId,
    this.jsonData,
    this.deleted = false,
    this.unread = false,
    this.localOnly = false,
  })  : assert(nodeId != null),
        assert(lastUpdateTs != null),
        assert(creatorId != null),
        assert(deleted != null),
        assert(unread != null),
        assert(localOnly != null);

  Node<R> as<R extends NodeData>() {
    if (this is Node<R>) {
      return this as Node<R>;
    } else {
      assert(nodeId.type == NodeData.getNodeType<R>());
      return Node<R>(
        nodeId: nodeId,
        lastUpdateTs: lastUpdateTs,
        creationTs: creationTs,
        permissionGroupId: permissionGroupId,
        creatorId: creatorId,
        jsonData: jsonData,
        unread: unread,
        localOnly: localOnly,
      );
    }
  }

  String get id => nodeId.id;

  String get parentId => nodeId.parentId;

  NodeType get type => nodeId.type;

  @override
  bool operator ==(Object other) => other is Node && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'Node{nodeId: $nodeId, permissionGroupId: $permissionGroupId, '
        'creatorId: $creatorId, creationTs: $creationTs, lastUpdateTs: $lastUpdateTs, '
        'jsonData: $jsonData, deleted: $deleted, unread: $unread, localOnly: $localOnly}';
  }
}

typedef NodeDataConstructor = NodeData Function(Map<String, dynamic>);

abstract class NodeData {
  static final _constructors = <Type, NodeDataConstructor>{
    EventData: EventData.fromMap,
    ParticipationData: (map) => const ParticipationData(),
    ParticipantData: ParticipantData.fromMap,
    ThreadData: (map) => const ThreadData(),
    TimelineData: (map) => const TimelineData(),
    ActivityData: ActivityData.fromMap,
    ActivityReactionData: ActivityReactionData.fromMap,
    MessageData: MessageData.fromMap,
    PhotosData: (map) => const PhotosData(),
    PhotoData: (map) => const PhotoData(),
    DirectChatData: (map) => const DirectChatData(),
    GroupChatData: GroupChatData.fromMap,
  };

  static const _runtimeTypeToEnum = <Type, NodeType>{
    EventData: NodeType.event,
    ParticipationData: NodeType.participation,
    ParticipantData: NodeType.participant,
    ThreadData: NodeType.thread,
    TimelineData: NodeType.timeline,
    ActivityData: NodeType.activity,
    ActivityReactionData: NodeType.activityReaction,
    MessageData: NodeType.message,
    PhotosData: NodeType.photos,
    PhotoData: NodeType.photo,
    DirectChatData: NodeType.chatDirect,
    GroupChatData: NodeType.chatGroup,
  };

  const NodeData();

  static T parse<T extends NodeData>(String json) {
    return _constructors[T](jsonDecode(json));
  }

  static NodeType getNodeType<T extends NodeData>() => _runtimeTypeToEnum[T];
}
