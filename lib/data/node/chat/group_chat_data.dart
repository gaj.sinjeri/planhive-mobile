import 'package:planhive/data/node/node.dart';

class GroupChatData extends NodeData {
  static final nameRegExp = RegExp(r"^.{1,25}$", unicode: true);

  final String name;
  final int coverTs;

  GroupChatData(this.name, this.coverTs);

  static GroupChatData fromMap(Map<String, dynamic> map) {
    return GroupChatData(map["name"], map["coverTs"]);
  }

  bool get hasCoverImage => coverTs != 0;

  @override
  String toString() {
    return 'GroupChatData{name: $name, coverTs: $coverTs}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is GroupChatData &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          coverTs == other.coverTs;

  @override
  int get hashCode => name.hashCode ^ coverTs.hashCode;
}
