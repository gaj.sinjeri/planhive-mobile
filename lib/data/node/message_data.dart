import 'package:planhive/data/node/node.dart';

class MessageData extends NodeData {
  final String text;
  final bool imagePresent;

  const MessageData({this.text, this.imagePresent});

  static MessageData fromMap(Map map) {
    return MessageData(
        text: map["textContent"], imagePresent: map["imagePresent"] == true);
  }

  Map<String, dynamic> toMap() => {
        'textContent': text,
        'imagePresent': imagePresent,
      };

  @override
  String toString() {
    return 'MessageData{text: $text, imagePresent: $imagePresent}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MessageData &&
          runtimeType == other.runtimeType &&
          text == other.text &&
          imagePresent == other.imagePresent;

  @override
  int get hashCode => text.hashCode ^ imagePresent.hashCode;
}
