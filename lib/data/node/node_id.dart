import 'package:flutter/foundation.dart';
import 'package:planhive/data/node/node.dart';

class NodeId {
  final String id;
  final String parentId;
  final NodeType type;

  NodeId({
    @required this.id,
    this.parentId,
    @required this.type,
  })  : assert(id != null),
        assert(type != null);

  bool get isRoot => parentId == null;

  @override
  String toString() {
    return 'NodeId{parentId: $parentId, id: $id, type: $type}';
  }

  @override
  bool operator ==(Object other) => other is NodeId && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
