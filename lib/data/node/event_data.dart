import 'package:flutter/cupertino.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';

enum EventState { active, canceled }

class EventData implements NodeData {
  static final nameRegExp = RegExp(r"^.{1,25}$", unicode: true);
  static final descriptionRegExp = RegExp(r"^.{0,512}$", unicode: true);

  final EventState eventState;
  final String name;
  final String description;
  final int lastCoverUpdateTs;
  final String inviteCode;

  bool get hasCoverImage => lastCoverUpdateTs != 0;

  const EventData({
    @required this.eventState,
    @required this.name,
    @required this.description,
    @required this.lastCoverUpdateTs,
    @required this.inviteCode,
  });

  static EventData fromMap(Map map) {
    return EventData(
      eventState: allCapsToEnum(map["eventState"], EventState.values),
      name: map["name"],
      description: map["description"],
      lastCoverUpdateTs: map["coverTs"],
      inviteCode: map["encryptedEventId"],
    );
  }

  @override
  String toString() {
    return 'EventData{eventState: $eventState, name: $name, description: $description, lastCoverUpdateTs: $lastCoverUpdateTs, inviteCode: $inviteCode}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is EventData &&
          runtimeType == other.runtimeType &&
          eventState == other.eventState &&
          name == other.name &&
          description == other.description &&
          lastCoverUpdateTs == other.lastCoverUpdateTs &&
          inviteCode == other.inviteCode;

  @override
  int get hashCode =>
      eventState.hashCode ^
      name.hashCode ^
      description.hashCode ^
      lastCoverUpdateTs.hashCode ^
      inviteCode.hashCode;
}
