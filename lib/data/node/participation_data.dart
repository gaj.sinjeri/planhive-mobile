import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';

class ParticipationData implements NodeData {
  const ParticipationData();
}

enum ParticipantStatus {
  accepted,
  maybe,
  declined,
}

class ParticipantData implements NodeData {
  final ParticipantStatus status;

  const ParticipantData(this.status);

  static ParticipantData fromMap(Map map) {
    return ParticipantData(
      allCapsToEnum(map["status"], ParticipantStatus.values),
    );
  }

  @override
  String toString() {
    return 'ParticipantData{status: $status}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ParticipantData &&
          runtimeType == other.runtimeType &&
          status == other.status;

  @override
  int get hashCode => status.hashCode;
}
