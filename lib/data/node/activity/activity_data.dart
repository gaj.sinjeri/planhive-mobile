import 'package:flutter/foundation.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/lat_lng.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/data/node/activity/time_range.dart';
import 'package:planhive/data/node/node.dart';

enum ActivityState {
  suggestion,
  approved,
  canceled,
}

class ActivityData implements NodeData, Comparable {
  static final nameRegExp = RegExp(r"^.{1,25}$", unicode: true);
  static final descriptionRegExp = RegExp(r"^.{0,512}$", unicode: true);
  static final notesRegExp = RegExp(r"^.{0,128}$", unicode: true);

  final ActivityState state;
  final String title;
  final String description;
  final String note;
  final TimeRange time;
  final Location location;

  bool get isSuggestion => state == ActivityState.suggestion;

  bool get isCanceled => state == ActivityState.canceled;

  const ActivityData({
    @required this.state,
    @required this.title,
    this.description,
    this.note,
    @required this.time,
    this.location,
  });

  static ActivityData fromMap(Map map) {
    var timeMap = map["time"];
    var locationMap = map["location"];

    var time = TimeRange(timeMap["startTs"], timeMap["endTs"]);
    var location = locationMap != null
        ? Location(
            title: locationMap["title"],
            address: locationMap["address"],
            coordinates: LatLng(
              double.parse(locationMap["latitude"]),
              double.parse(locationMap["longitude"]),
            ),
          )
        : null;

    return ActivityData(
      state: allCapsToEnum(map["activityState"], ActivityState.values),
      title: map["title"],
      description: map["description"],
      note: map["note"],
      time: time,
      location: location,
    );
  }

  @override
  String toString() {
    return 'ActivityData{state: $state, title: $title, description: $description, '
        'note: $note, time: $time, location: $location}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActivityData &&
          runtimeType == other.runtimeType &&
          state == other.state &&
          title == other.title &&
          description == other.description &&
          note == other.note &&
          time == other.time &&
          location == other.location;

  @override
  int get hashCode =>
      state.hashCode ^
      title.hashCode ^
      description.hashCode ^
      note.hashCode ^
      time.hashCode ^
      location.hashCode;

  int compareForPreview(other) {
    var _title = title.toLowerCase();

    if (other is ActivityData) {
      return (_compareActivityState(other) != 0
          ? _compareActivityState(other)
          : time.compareTo(other.time) != 0
              ? time.compareTo(other.time)
              : _title.compareTo(other.title.toLowerCase()));
    }
    throw ArgumentError(
      "TimeRange compared to an object of type ${other.runtimeType}",
    );
  }

  @override
  int compareTo(other) {
    var _title = title.toLowerCase();

    if (other is ActivityData) {
      return time.compareTo(other.time) != 0
          ? time.compareTo(other.time)
          : (_compareActivityState(other) != 0
              ? _compareActivityState(other)
              : _title.compareTo(other.title.toLowerCase()));
    }
    throw ArgumentError(
      "TimeRange compared to an object of type ${other.runtimeType}",
    );
  }

  int _compareActivityState(ActivityData other) {
    if (state.index == 0) {
      // Suggestion
      if (other.state.index == 0) return 0;
      return -1;
    } else if (state.index == 1) {
      // Approved
      if (other.state.index == 0) return 1;
      if (other.state.index == 1) return 0;
      return -1;
    } else {
      // Cancelled
      if (other.state.index == 2) return 0;
      return 1;
    }
  }
}
