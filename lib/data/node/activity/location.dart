import 'package:flutter/foundation.dart';
import 'package:planhive/data/lat_lng.dart';

class Location {
  final String title;
  final String address;
  final LatLng coordinates;

  const Location({
    this.title,
    this.address,
    @required this.coordinates,
  });

  @override
  String toString() {
    return 'Location{title: $title, address: $address, coordinates: $coordinates}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Location &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          address == other.address &&
          coordinates == other.coordinates;

  @override
  int get hashCode => title.hashCode ^ address.hashCode ^ coordinates.hashCode;
}
