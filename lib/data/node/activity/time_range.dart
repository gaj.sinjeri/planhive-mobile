import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planhive/common/time.dart';

class TimeRange implements Comparable {
  final int startTs;
  final int endTs;

  int get endOrEndOfDayTs {
    if (endTs != null) {
      return endTs;
    }

    var start = DateTime.fromMillisecondsSinceEpoch(startTs);
    return DateTime(
      start.year,
      start.month,
      start.day,
      23,
      59,
      59,
      999,
      999,
    ).millisecondsSinceEpoch;
  }

  String getStartDateText(BuildContext context) {
    final loc = MaterialLocalizations.of(context);

    var now = DateTime.now();
    final startDate = DateTime.fromMillisecondsSinceEpoch(startTs);

    if (dateMatches(now, startDate)) {
      return "Today";
    }

    if (now.year == startDate.year) {
      return loc.formatMediumDate(startDate);
    }

    return loc.formatFullDate(startDate);
  }

  String getStartTimeText(BuildContext context) {
    final loc = MaterialLocalizations.of(context);

    return loc.formatTimeOfDay(
      TimeOfDay.fromDateTime(
        DateTime.fromMillisecondsSinceEpoch(startTs),
      ),
    );
  }

  String getEndTimeText(BuildContext context) {
    assert(endTs != null);

    final loc = MaterialLocalizations.of(context);

    final startDate = DateTime.fromMillisecondsSinceEpoch(startTs);

    final endDate = DateTime.fromMillisecondsSinceEpoch(endTs);
    final endTime = loc.formatTimeOfDay(TimeOfDay.fromDateTime(endDate));

    if (dateMatches(startDate, endDate)) {
      return "$endTime";
    } else if (yearMatches(startDate, endDate)) {
      return "$endTime (${DateFormat.MMMd().format(endDate)})";
    } else {
      return "$endTime (${loc.formatShortDate(endDate)}";
    }
  }

  const TimeRange(int startTs, [int endTs])
      : assert(startTs != null),
        assert(endTs == null || endTs >= 0 || startTs < endTs),
        startTs = startTs,
        endTs = endTs == 0 ? null : endTs;

  TimeRange.fromDateTime(DateTime start, [DateTime end])
      : this(start?.millisecondsSinceEpoch, end?.millisecondsSinceEpoch);

  @override
  String toString() {
    return 'TimeRange{startTs: $startTs, endTs: $endTs}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TimeRange &&
          runtimeType == other.runtimeType &&
          startTs == other.startTs &&
          endTs == other.endTs;

  @override
  int get hashCode => startTs.hashCode ^ endTs.hashCode;

  @override
  int compareTo(other) {
    if (other is TimeRange) {
      var ret = startTs.compareTo(other.startTs);

      if (ret == 0) {
        if (endTs != null) {
          if (other.endTs != null) {
            ret = endTs.compareTo(other.endTs);
          } else {
            ret = -1;
          }
        } else {
          if (other.endTs != null) {
            ret = 1;
          } else {
            ret = 0;
          }
        }
      }

      return ret;
    }
    throw ArgumentError(
      "$TimeRange compared to an object of type ${other.runtimeType}",
    );
  }
}
