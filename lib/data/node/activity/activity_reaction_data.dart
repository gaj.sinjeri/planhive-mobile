import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';

enum ActivityReactionType {
  vote,
  reject,
}

class ActivityReactionData implements NodeData {
  final ActivityReactionType reaction;

  const ActivityReactionData(this.reaction);

  static ActivityReactionData fromMap(Map map) {
    return ActivityReactionData(
      allCapsToEnum(map["activityReaction"], ActivityReactionType.values),
    );
  }

  @override
  String toString() {
    return 'ActivityReactionData{reaction: $reaction}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ActivityReactionData &&
          runtimeType == other.runtimeType &&
          reaction == other.reaction;

  @override
  int get hashCode => reaction.hashCode;
}
