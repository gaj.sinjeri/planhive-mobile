import 'package:flutter/foundation.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/user.dart';

class ContactUser {
  final Contact contact;
  final User user;

  ContactUser({
    @required this.contact,
    @required this.user,
  });

  @override
  String toString() {
    return 'ContactUser{contact: $contact, user: $user}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ContactUser &&
          runtimeType == other.runtimeType &&
          contact == other.contact &&
          user == other.user;

  @override
  int get hashCode => contact.hashCode ^ user.hashCode;
}
