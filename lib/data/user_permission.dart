import 'package:planhive/data/permission.dart';
import 'package:planhive/data/user.dart';

class UserPermission {
  final User user;
  final Permission permission;

  UserPermission(this.user, this.permission);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserPermission &&
          runtimeType == other.runtimeType &&
          user == other.user &&
          permission == other.permission;

  @override
  int get hashCode => user.hashCode ^ permission.hashCode;

  @override
  String toString() {
    return 'UserPermission{user: $user, permission: $permission}';
  }
}
