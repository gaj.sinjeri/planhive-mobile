import 'dart:math';

import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';

enum EventPreviewSection {
  upcoming,
  undecided,
  past,
}

class EventPreviewData {
  static const _pastSectionOffset = 0x5FFFFFFFFFFFFFFF;

  final Node<EventData> event;
  final List<Node<ActivityData>> activities;
  final Node<ThreadData> thread;
  final Node<ActivityData> activity;

  final int _activityTimeScore;
  final int _lastChangedScore;

  EventPreviewSection get section {
    if (_activityTimeScore == null) {
      return EventPreviewSection.undecided;
    }
    if (_activityTimeScore >= _pastSectionOffset) {
      return EventPreviewSection.past;
    } else {
      return EventPreviewSection.upcoming;
    }
  }

  bool get hasTime => _activityTimeScore != null;

  EventPreviewData(this.event, this.thread, this.activities, int now)
      : activity = activities.where((e) => e.data.state != ActivityState.suggestion).fold(
              null,
              (a, b) => a != null && a.data.compareForPreview(b.data) < 0 ? a : b,
            ),
        _activityTimeScore = _computeActivityTimeScore(event, activities, now),
        _lastChangedScore = _computeLastChangedScore(thread, activities);

  static int _computeLastChangedScore(Node thread, List<Node> activities) {
    return activities.fold(
      thread.lastUpdateTs,
      (prev, node) => max(prev, node.lastUpdateTs),
    );
  }

  static int _computeActivityTimeScore(
    Node<EventData> event,
    List<Node<ActivityData>> activities,
    int now,
  ) {
    bool containsApproved = activities.any((e) => e.data.state == ActivityState.approved);
    int startTs = 0x7FFFFFFFFFFFFFFF;
    int endTs = 0;

    for (var node in activities) {
      var curr = node.data;
      if (curr.state == ActivityState.suggestion) {
        continue;
      }
      if (curr.state == ActivityState.canceled && containsApproved) {
        continue;
      }
      startTs = min(startTs, curr.time.startTs);
      endTs = max(endTs, curr.time.endOrEndOfDayTs);
    }

    if (endTs == 0) {
      return null;
    } else {
      var startDelta = startTs - now;
      var endDelta = endTs - now;
      return endDelta > 0 ? startDelta : _pastSectionOffset - startDelta;
    }
  }

  static int activityTimeComparator(EventPreviewData a, EventPreviewData b) {
    assert(
      a._activityTimeScore != null && b._activityTimeScore != null,
      "You tried to compare $EventPreviewData with no time info.",
    );
    return a._activityTimeScore.compareTo(b._activityTimeScore);
  }

  static int lastChangeComparator(EventPreviewData a, EventPreviewData b) {
    return b._lastChangedScore.compareTo(a._lastChangedScore);
  }

  @override
  String toString() {
    return 'EventPreviewData{event: $event, activity: $activity}';
  }
}
