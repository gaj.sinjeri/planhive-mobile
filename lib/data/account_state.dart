enum AccountState {
  Initial,
  PendingEmailVerification,
  PendingAccountCreation,
  Complete,
}
