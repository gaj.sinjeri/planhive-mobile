class UpdateTimestamps {
  final int oldest;
  final int latest;

  const UpdateTimestamps(this.oldest, this.latest);

  @override
  String toString() {
    return 'UpdateTimestamps{oldest: $oldest, latest: $latest}';
  }
}
