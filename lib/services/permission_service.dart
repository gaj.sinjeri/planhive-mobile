import 'dart:math';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/permission_api.dart';
import 'package:planhive/common/async_debouncer.dart';
import 'package:planhive/data/permission.dart';
import 'package:planhive/data/update_timestamps.dart';
import 'package:planhive/data/user_permission.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/update_management_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/storage/permission_store.dart';
import 'package:tuple/tuple.dart';

@singleton
class PermissionService {
  static const _updateManagementId = "permissions";

  final AccountService _accountService;
  final NodeStore _nodeStore;
  final PermissionStore _permissionStore;
  final PermissionApi _permissionApi;
  final UserService _userService;
  final UpdateManagementService _updateManagementService;

  PermissionService(
    this._accountService,
    this._permissionStore,
    this._nodeStore,
    this._permissionApi,
    this._userService,
    this._updateManagementService,
  );

  Future<bool> fetchUpdates() async {
    return await debounce(_fetchUpdates);
  }

  Future<bool> _fetchUpdates() async {
    var times = await _updateManagementService.getTimestamps(_updateManagementId);

    var startTs = times.latest ?? _updateManagementService.updateStartTs;

    var permissions = await _permissionApi.fetchUpdates(startTs);

    var missingPermissions = await _permissionStore.getMissing(
      permissions.map((e) => e.groupId),
    );
    var permissionLists = await Future.wait(
      missingPermissions.map((e) => _permissionApi.getPermissions(e, 0)),
    );

    if (permissionLists.isNotEmpty) {
      permissions = Set.of(
        permissions.followedBy(permissionLists.expand((e) => e)),
      ).toList();
    }

    await _storePermissions(permissions);

    var newTimes = UpdateTimestamps(
      null,
      permissions.fold(
        startTs,
        (prevTs, permission) => max(prevTs, permission.lastUpdateTs),
      ),
    );

    await _updateManagementService.setTimestamps(_updateManagementId, newTimes);

    return permissions.isNotEmpty;
  }

  Future<void> fetch(String groupId) async {
    await debounce(() => _fetch(groupId), debounceId: Tuple2(_fetch, groupId));
  }

  Future<void> _fetch(String groupId) async {
    var permissions = await _permissionApi.getPermissions(groupId, 0);

    await _storePermissions(permissions);
  }

  Future<void> _storePermissions(List<Permission> permissions) async {
    if (permissions.isEmpty) {
      return;
    }

    var removedPermissionGroupIds = permissions
        .where((permission) =>
            permission.deleted && permission.userId == _accountService.userId)
        .map((e) => e.groupId);
    await _nodeStore.deleteNodesForPermissionGroupIds(removedPermissionGroupIds);

    await _userService.fetchMissing(permissions.map((p) => p.userId));

    await _permissionStore.addPermissions(permissions);
  }

  Future<bool> fetchIfMissing(String groupId) async {
    if (!await _permissionStore.exists(groupId)) {
      await fetch(groupId);
      return true;
    }
    return false;
  }

  Future<void> deletePermission(String userId, String permissionId) async {
    await _permissionStore.deletePermission(userId, permissionId);
  }

  Future<List<UserPermission>> getContactPermissions(String groupId) async {
    var userPermissions = await _permissionStore.getContactPermissions(groupId);

    if (userPermissions.isEmpty && await fetchIfMissing(groupId)) {
      userPermissions = await _permissionStore.getContactPermissions(groupId);
    }

    return userPermissions;
  }

  Future<UserPermission> getUserPermission(String groupId) async {
    var userPermission = await _permissionStore.getUserPermission(groupId);

    if (userPermission == null && await fetchIfMissing(groupId)) {
      userPermission = await _permissionStore.getUserPermission(groupId);
    }

    return userPermission;
  }
}
