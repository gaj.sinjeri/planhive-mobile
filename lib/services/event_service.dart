import 'package:image/image.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/event_api.dart';
import 'package:planhive/api/participation_api.dart';
import 'package:planhive/api/timeline_api.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/event/event_preview_data.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/data/node/activity/time_range.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/node/timeline_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/analytics/params/activity_event_params.dart';
import 'package:planhive/services/analytics/params/status_event_params.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

import 'node_service.dart';

@lazySingleton
class EventService {
  static const _logger = Logger(EventService);

  final AccountService _accountService;
  final EventApi _eventApi;
  final ParticipationApi _participationApi;
  final TimelineApi _timelineApi;
  final NodeService _nodeService;
  final PermissionService _permissionService;
  final FileCacheStore _fileCacheStore;
  final AnalyticsService _analyticsService;

  EventService(
    this._accountService,
    this._eventApi,
    this._participationApi,
    this._timelineApi,
    this._nodeService,
    this._permissionService,
    this._fileCacheStore,
    this._analyticsService,
  );

  Future<Node<EventData>> createEvent(
    String name,
    String description,
    Image image,
    TimeRange time,
    Location location,
  ) async {
    assert(name.isNotEmpty);

    List<int> coverPhoto;
    if (image != null) {
      coverPhoto = encodeJpg(image, quality: imageQuality);
    }

    var nodes =
        await _eventApi.createEvent(name, description, coverPhoto, time, location);

    await _permissionService.fetchUpdates();

    _logger.debug(
      "Event created with the following "
      "returned ${nodes.length} nodes: ${nodes.join("; ")}",
    );

    await _nodeService.addNodes(nodes);

    var eventNode =
        nodes.firstWhere((node) => node.type == NodeType.event).as<EventData>();

    if (coverPhoto != null) {
      var thumbnail = encodeJpg(
          copyResize(
            image,
            width: eventCoverPhotoThumbnailWidth,
            height: eventCoverPhotoThumbnailHeight,
          ),
          quality: imageQuality);

      _fileCacheStore.put(
        eventNode.id,
        eventNode.data.lastCoverUpdateTs.toString(),
        coverPhoto,
      );
      _fileCacheStore.put(
        toThumbnailId(eventNode.id),
        eventNode.data.lastCoverUpdateTs.toString(),
        thumbnail,
      );
    }

    await _analyticsService.logEvent(AnalyticsEvent.createdEvent);

    if (time != null || location != null) {
      await _analyticsService.logEvent(
        AnalyticsEvent.createdActivity,
        ActivityEventParams(ActivityData(
          state: ActivityState.approved,
          title: "",
          time: time,
          location: location,
        )),
      );
    }

    return eventNode;
  }

  Future<Node<EventData>> updateEventData(
    Node<EventData> eventNode, {
    String name,
    String description,
    Image image,
    bool imageChanged = false,
    bool inviteLinkEnabled,
  }) async {
    assert(eventNode != null);
    assert(imageChanged != null);

    var data = eventNode.data;

    List<int> coverImage;
    if (image != null && imageChanged == true) {
      coverImage = encodeJpg(image, quality: imageQuality);
    }

    var updatedEventNode = await _eventApi.updateEventData(
      eventNode.id,
      name ?? data.name,
      description ?? data.description,
      imageChanged ? 0 : data.lastCoverUpdateTs,
      inviteLinkEnabled ?? eventNode.data.inviteCode != null,
      coverImage,
    );

    await _nodeService.addNodes([updatedEventNode]);

    if (coverImage != null && imageChanged) {
      var thumbnail = encodeJpg(
          copyResize(
            image,
            width: eventCoverPhotoThumbnailWidth,
            height: eventCoverPhotoThumbnailHeight,
          ),
          quality: imageQuality);

      _fileCacheStore.put(
        eventNode.id,
        eventNode.data.lastCoverUpdateTs.toString(),
        coverImage,
      );
      _fileCacheStore.put(
        toThumbnailId(eventNode.id),
        eventNode.data.lastCoverUpdateTs.toString(),
        thumbnail,
      );
    }

    return updatedEventNode;
  }

  Future<void> changeParticipation(
    Node<ParticipationData> node,
    ParticipantStatus currentStatus,
    ParticipantStatus newStatus,
  ) async {
    if(newStatus == currentStatus) {
      return;
    }

    await _nodeService.addNodes([
      await _participationApi.updateParticipationStatus(node, newStatus),
    ]);

    await _analyticsService.logEvent(
      AnalyticsEvent.changedStatus,
      StatusEventParams(currentStatus, newStatus),
    );
  }

  Future<void> addActivity(Node<TimelineData> timeline, ActivityData activity) async {
    assert(timeline != null);
    assert(activity != null);

    await _nodeService.addNodes([
      await _timelineApi.addActivity(timeline, activity),
    ]);

    await _analyticsService.logEvent(
      AnalyticsEvent.createdActivity,
      ActivityEventParams(activity),
    );
  }

  Future<void> updateActivityState(
    Node<TimelineData> timeline,
    String activityId,
    ActivityState activityState,
  ) async {
    assert(timeline != null);
    assert(activityId != null);
    assert(activityState != null);

    await _nodeService.addNodes([
      await _timelineApi.updateActivityState(timeline, activityId, activityState),
    ]);
  }

  Future<void> updateActivityData(
    Node<TimelineData> timeline,
    Node<ActivityData> activity,
    ActivityData data,
  ) async {
    assert(timeline != null);
    assert(activity != null);
    assert(data != null);

    await _nodeService.addNodes([
      await _timelineApi.updateActivityData(timeline, activity, data),
    ]);
  }

  /// Pass null for [reaction] to remove the reaction
  Future<void> updateActivityReaction(
    Node<TimelineData> timeline,
    Node<ActivityData> activity,
    ActivityReactionType reaction,
  ) async {
    assert(timeline != null);
    assert(activity != null);

    await _nodeService.addNodes([
      await _timelineApi.updateActivityReaction(timeline, activity, reaction),
    ]);

    await _analyticsService.logEvent(AnalyticsEvent.votedActivity);
  }

  Future<void> deleteActivity(
      Node<TimelineData> timeline, Node<ActivityData> activity) async {
    assert(timeline != null);
    assert(activity != null);

    await _nodeService.addNodes([
      await _timelineApi.deleteActivity(timeline, activity),
    ]);
  }

  Future<void> setMemberAdminStatus(
      String eventId, String memberId, bool setAdmin) async {
    assert(eventId != null);
    assert(memberId != null);

    await _eventApi.setMemberAdminStatus(eventId, memberId, setAdmin);
    await _permissionService.fetchUpdates();
  }

  Future<void> updateEventState(Node eventNode, EventState eventState) async {
    assert(eventNode != null);

    await _nodeService.addNodes([
      await _eventApi.updateEventState(eventNode.id, eventState),
    ]);
  }

  Future<void> removeFromEvent(Node eventNode, [String userId]) async {
    assert(eventNode != null);

    await _eventApi.removeFromEvent(eventNode.id, userId ?? _accountService.userId);
    if (userId == null) await _nodeService.deleteNodeTreeAndPermissions(eventNode);
    if (userId != null)
      await _permissionService.deletePermission(userId, eventNode.permissionGroupId);
  }

  Future<void> deleteEvent(Node eventNode) async {
    assert(eventNode != null);

    await _eventApi.deleteEvent(eventNode.id);
    await _nodeService.deleteNodeTreeAndPermissions(eventNode);
  }

  Future<void> invite(Node<EventData> event, Iterable<User> users) async {
    await _eventApi.invite(event, users);
    await _permissionService.fetchUpdates();

    for (var _ in users) {
      _analyticsService.logEvent(AnalyticsEvent.sentInviteToEvent);
    }
  }

  Future<List<EventPreviewData>> getMainActivities() async {
    var queries = await Future.wait([
      _nodeService.getCachedNodes<ActivityData>(),
      _nodeService.getCachedNodes<TimelineData>(),
      _nodeService.getCachedNodes<EventData>(),
      _nodeService.getCachedNodes<ThreadData>(),
    ]);

    var activities = queries[0];
    var timelines = queries[1];
    var events = queries[2];
    var threads = queries[3];

    var eventMap = Map.fromIterables(events.map((e) => e.id), events);
    var threadMap = Map.fromIterables(threads.map((e) => e.parentId), threads);

    var timelineMap = Map<String, List<Node<ActivityData>>>();
    for (var activity in activities) {
      timelineMap.putIfAbsent(activity.parentId, () => []).add(activity);
    }

    var previewData = List<EventPreviewData>(timelines.length);
    var now = DateTime.now().millisecondsSinceEpoch;

    for (int i = 0; i < timelines.length; i++) {
      var timeline = timelines[i];
      previewData[i] = EventPreviewData(
        eventMap[timeline.parentId],
        threadMap[timeline.parentId],
        timelineMap[timeline.id] ?? const [],
        now,
      );
    }

    return List.unmodifiable(previewData);
  }
}
