import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/notification/node_notification_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/notification/user_notification_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/core/chats/direct_chat_page.dart';
import 'package:planhive/views/core/chats/group_chat_page.dart';
import 'package:planhive/views/core/contacts/contact_info_page.dart';
import 'package:planhive/views/core/event/event_main_page.dart';
import 'package:planhive/views/core/main_navigation.dart';
import 'package:planhive/views/user_registration/code_validation_page.dart';
import 'package:planhive/views/user_registration/email_input_page.dart';
import 'package:planhive/views/user_registration/profile_creation_page.dart';

abstract class NavigationPage {
  /// Return true if this page is a wrapper for multiple child views (e.g. tabs)
  bool get isWrapperPage;
}

class NavigationPageData {
  final String name;
  final bool isWrapperPage;

  const NavigationPageData(this.name, this.isWrapperPage);
}

@lazySingleton
class NavigationService {
  static const _logger = Logger(NavigationService);

  var _navigatorKey;
  final routeObserver = RouteObserver<ModalRoute>();

  static get instance => GetIt.I<NavigationService>();

  BuildContext get context {
    var context = _navigatorKey.currentContext;
    context?.visitChildElements((element) => context = element);
    return context;
  }

  set navigatorKey(GlobalKey<NavigatorState> value) => _navigatorKey = value;

  final AccountStore _accountStore;

  final NodeService _nodeService;
  final UserService _userService;

  NavigatorState get _navigator => _navigatorKey.currentState;

  NavigationService(this._accountStore, this._nodeService, this._userService);

  Future<void> navigateForNotification(NotificationData notificationData) async {
    if (notificationData is NodeNotificationData) {
      var nodes = notificationData.nodes;
      await navigateToNodeId(nodes[min(1, nodes.length - 1)].id);
    } else if (notificationData is UserNotificationData) {
      await navigateToUserId(notificationData.userId);
    }
  }

  Future<void> navigateToNodeId(String nodeId) async {
    var node = await _nodeService.getNode(nodeId);

    if (node == null) {
      _logger.error("Node $nodeId was not found in store");
      return;
    }

    await navigateToNode(node);
  }

  Future<void> navigateToNode(Node node) async {
    assert(node != null);

    var parent = node.parentId == null ? node : await _nodeService.getNode(node.parentId);

    if (parent == null) {
      _logger.error(
        "Node ${node.id} of type ${node.type} has no parent with id ${node.parentId} in store",
      );
      return;
    }

    Widget page;

    switch (parent.type) {
      case NodeType.event:
        page = EventPage(parent.as<EventData>(), node);
        break;
      case NodeType.chatDirect:
        var targetThread = node.type == NodeType.thread ? node.as<ThreadData>() : null;
        page = DirectChatPage(parent.as<DirectChatData>(), targetThread);
        break;
      case NodeType.chatGroup:
        var targetThread = node.type == NodeType.thread ? node.as<ThreadData>() : null;
        page = GroupChatPage(parent.as<GroupChatData>(), targetThread);
        break;
      default:
        _logger.error("No navigation configured for node $node");
        return;
    }

    _logger.trace("Navigating to page: ${page.runtimeType}");

    pushAboveStart(page);
  }

  Future<void> navigateToUserId(String userId) async {
    assert(userId != null);

    await navigateToUser(await _userService.getUserById(userId));
  }

  Future<void> navigateToUser(User user) async {
    assert(user != null);

    pushAboveStart(ContactInfoPage(user));
  }

  Future<T> startPage<T>() {
    return replaceRoot(_startPageDispatcher());
  }

  Future<T> replaceRoot<T>(Widget page, {bool modal = true}) {
    return _navigator.pushAndRemoveUntil<T>(
      _makeRoute<T>(page, root: true, modal: modal),
      (route) => false,
    );
  }

  void popUntilStart() {
    _navigator.popUntil(
      (route) => isRoot(route.settings),
    );
  }

  Future<T> pushAboveStart<T>(Widget page, {bool modal = true}) {
    return _navigator.pushAndRemoveUntil(
      _makeRoute<T>(page, modal: modal),
      (route) => isRoot(route.settings),
    );
  }

  Future<T> push<T>(Widget page, {bool modal = true}) {
    return _navigator.push<T>(_makeRoute<T>(page, modal: modal));
  }

  void pop<T>([T value]) => _navigator.pop<T>(value);

  bool canPop() => _navigator.canPop();

  static Route<T> _makeRoute<T>(Widget page, {bool modal = true, bool root = false}) {
    return MaterialPageRoute(
      settings: RouteSettings(
        name: root ? "/" : "/${page.runtimeType}",
        arguments: _RouteArgs(page),
      ),
      builder: (context) => page,
      fullscreenDialog: modal,
    );
  }

  Widget _startPageDispatcher() {
    switch (_accountStore.accountState) {
      case AccountState.Initial:
        return EmailInputPage();
      case AccountState.PendingEmailVerification:
        return CodeValidationPage();
      case AccountState.PendingAccountCreation:
        return CreateProfilePage();
      case AccountState.Complete:
        return MainNavigation();
      default:
        throw StateError("Invalid user authentication state");
    }
  }

  static bool isRoot(RouteSettings settings) =>
      settings?.name == Navigator.defaultRouteName;

  static NavigationPageData getRouteData(RouteSettings settings) {
    var args = settings?.arguments;
    return args is _RouteArgs ? args.data : null;
  }

  static NavigationPageData getRouteDataFromContext(BuildContext context) {
    return getRouteData(ModalRoute.of(context)?.settings);
  }
}

class _RouteArgs {
  final Widget pageWidget;

  final NavigationPageData data;

  _RouteArgs(this.pageWidget)
      : data = NavigationPageData(
          pageWidget.runtimeType.toString(),
          _isWrapper(pageWidget),
        );

  static bool _isWrapper(dynamic page) =>
      page is NavigationPage ? page.isWrapperPage : false;
}
