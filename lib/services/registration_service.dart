import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:image/image.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/data/verification_result.dart';
import 'package:planhive/api/registration_api.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_session.dart';
import 'package:planhive/exception/api/code_request_limit_exception.dart';
import 'package:planhive/exception/api/invalid_state_exception.dart';
import 'package:planhive/exception/api/session_expired_exception.dart';
import 'package:planhive/exception/api/unauthorized_exception.dart';
import 'package:planhive/exception/api/username_exception.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/notification/notification_service.dart';
import 'package:planhive/services/platform/app_link_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';
import 'package:planhive/views/user_registration/account_restored_page.dart';
import 'package:planhive/views/user_registration/email_input_page.dart';
import 'package:planhive/views/user_registration/profile_creation_page.dart';
import 'package:planhive/views/user_registration/registration_complete_page.dart';

@singleton
class RegistrationService {
  final RegistrationApi _registrationApi;
  final AccountStore _accountStore;
  final UserService _userService;
  final AccountService _accountService;
  final AppLinkService _appLinkService;
  final NotificationService _notificationService;
  final NavigationService _navigationService;
  final AnalyticsService _analyticsService;

  RegistrationService(
    this._registrationApi,
    this._accountStore,
    this._userService,
    this._accountService,
    this._appLinkService,
    this._notificationService,
    this._navigationService,
    this._analyticsService,
  ) {
    _appLinkService.onData.addListener(_onAppLink);
  }

  Future<bool> requestCode(String email) async {
    assert(email != null);
    assert(EmailValidator.validate(email));

    if (_accountStore.email == email &&
        _accountStore.accountState == AccountState.PendingEmailVerification) {
      // The email hasn't changed, so we don't send a new email.
      // This happens when the user goes back to the initial page,
      // but doesn't change the email.
      return true;
    }

    var ret = await _sendCode(email);

    await _analyticsService.setUserId(email);
    await _analyticsService.logEvent(AnalyticsEvent.submittedEmail);

    return ret;
  }

  Future<void> showInvalidEmailDialog() async {
    await showAlertDialog(
      title: "Invalid Email",
      content: "Please make sure you enter a valid email.",
      actions: [AlertActionData(text: "OK")],
    );
  }

  Future<void> resendCode() async {
    var email = _accountStore.email;
    assert(email != null && email.isNotEmpty);

    await _sendCode(email);

    await _analyticsService.logEvent(AnalyticsEvent.requestedNewEmail);
  }

  Future<bool> _sendCode(String email) async {
    try {
      if (_accountStore.email != email) {
        await _accountStore.setEmail(email);
      }

      await _registrationApi.sendVerificationCode(email.trim());

      return true;
    } on CodeRequestLimitException catch (e) {
      var localizations = MaterialLocalizations.of(NavigationService.instance.context);

      await showAlertDialog(
        title: "Verification Requests Exceeded",
        content: "The maximum number of verification links have been sent for this "
            "email. The next link can be requested at"
            " ${localizations.formatTimeOfDay(TimeOfDay.fromDateTime(e.earliestTime))}. "
            "The last link that was sent can be reused for 24 hours.",
        actions: [AlertActionData(text: "OK")],
        barrierDismissible: false,
      );

      return false;
    }
  }

  void _onAppLink(Uri link) async {
    if (link?.path?.startsWith("/email/") != true) {
      return;
    }

    switch (_accountStore.accountState) {
      case AccountState.PendingAccountCreation:
      case AccountState.Complete:
        await showAlertDialog(
          title: "Already Verified",
          content: "You have already verified your email.",
          actions: [AlertActionData(text: "OK")],
        );
        break;
      case AccountState.Initial:
      case AccountState.PendingEmailVerification:
        if (_accountStore.email != null) {
          await _verifyCode(link.pathSegments.last);
        } else {
          await showAlertDialog(
            title: "Email Missing",
            content: "Please submit your email and try again.",
            actions: [AlertActionData(text: "OK")],
            barrierDismissible: false,
          );
        }
        break;
    }
  }

  Future<void> _verifyCode(String code) async {
    try {
      var res = await _registrationApi.verifyCode(_accountStore.email, code);

      if (res.sessionId == null) {
        await _accountStore.setCodeVerificationData(res);

        await _analyticsService.logEvent(AnalyticsEvent.validatedEmail);

        _navigationService.replaceRoot(CreateProfilePage());
      } else {
        await _accountService.restoreUser(res.userId, res.sessionId);

        await _analyticsService.logEvent(AnalyticsEvent.logIn);

        try {
          await _notificationService.enablePushNotificationsForUser();
        } finally {
          _navigationService.replaceRoot(AccountRestoredPage());
        }
      }
    } on UnauthorizedException {
      await showAlertDialog(
        title: "Verification Unsuccessful",
        content: "If you have requested a new verification link, "
            "please ensure you are using the latest one.",
        actions: [AlertActionData(text: "OK")],
        barrierDismissible: false,
      );
    } on SessionExpiredException {
      await showAlertDialog(
        title: "Verification Link Expired",
        content: "Please request a new verification message to receive a new link.",
        actions: [AlertActionData(text: "OK")],
        barrierDismissible: false,
      );
    }
  }

  Future<void> registerUser(String name, String username, Image image) async {
    assert(name != null && name.isNotEmpty);
    assert(User.displayNameRegExp.hasMatch(name));
    assert(username != null && username.isNotEmpty);
    assert(User.usernameRegExp.hasMatch(username));

    List<int> avatar;
    if (image != null) {
      avatar = encodeJpg(image, quality: imageQuality);
    }

    UserSession userSession;
    try {
      userSession = await _registrationApi.registerUser(
        email: _accountStore.email,
        fullName: name,
        userName: username,
        avatar: avatar,
        verificationResult: VerificationResult.mac(
          userId: _accountStore.userId,
          mac: _accountStore.mac,
          macCreationTs: _accountStore.macCreationTs,
        ),
      );
    } on UsernameException {
      await showAlertDialog(
        title: "Username Unavailable",
        content: "Username is already taken. Please choose a different username.",
        actions: [AlertActionData(text: "OK")],
        barrierDismissible: false,
      );
      return;
    } on SessionExpiredException {
      await _restartRegistration(
        "Session Expired",
        "User registration session has expired. Please verify your email again.",
      );
      return;
    } on InvalidStateException {
      await _restartRegistration("Something went wrong", "Please try registering again.");
      return;
    }

    await _accountService.setUser(userSession);

    // run as an async operation so any errors don't cause the registration to fail
    Future.microtask(() async {
      await _analyticsService.logEvent(AnalyticsEvent.signUp);

      await _notificationService.enablePushNotificationsForUser();

      if (avatar != null) {
        _userService.storeLocalAvatar(image);
      }
    });

    _navigationService.replaceRoot(RegistrationCompletePage());
  }

  Future<void> _restartRegistration(String reason, String message) async {
    await showAlertDialog(
      title: reason,
      content: message,
      actions: [AlertActionData(text: "OK")],
      barrierDismissible: false,
    );

    await _accountStore.resetRegistrationSession();

    _navigationService.replaceRoot(EmailInputPage());
  }
}
