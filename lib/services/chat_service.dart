import 'package:flutter/foundation.dart';
import 'package:image/image.dart';
import 'package:injectable/injectable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:planhive/api/chat_api.dart';
import 'package:planhive/api/thread_api.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/scale.dart';
import 'package:planhive/data/node/chat/direct_chat_data.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/analytics/params/message_event_params.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/services/user_service.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

@lazySingleton
class ChatService {
  final ThreadApi _threadApi;
  final ChatApi _chatApi;
  final AccountService _accountService;
  final NodeService _nodeService;
  final UserService _userService;
  final PermissionService _permissionService;
  final FileCacheStore _fileCacheStore;
  final AnalyticsService _analyticsService;

  ChatService(
    this._threadApi,
    this._chatApi,
    this._accountService,
    this._nodeService,
    this._userService,
    this._permissionService,
    this._fileCacheStore,
    this._analyticsService,
  );

  Stream<void> sendPhotos(
    Node parentNode,
    Node<ThreadData> thread,
    List<Asset> images,
  ) async* {
    assert(parentNode.type == NodeType.event ||
        parentNode.type == NodeType.chatDirect ||
        parentNode.type == NodeType.chatGroup);
    assert(images != null);

    for (var image in images) {
      await sendMessage(
        parentNode: parentNode,
        thread: thread,
        image: image,
      );

      yield image;
    }
  }

  Future<void> sendMessage({
    @required Node parentNode,
    @required Node<ThreadData> thread,
    String textContent,
    Asset image,
  }) async {
    assert(parentNode.type == NodeType.event ||
        parentNode.type == NodeType.chatDirect ||
        parentNode.type == NodeType.chatGroup);

    assert((image != null) ^ (textContent != null));

    List<int> scaledImage;

    if (image != null) {
      scaledImage = await scaleImage(image, messageImageMaxResolution);
    }

    var response = await _threadApi.sendMessage(
      parentNodeType: parentNode.type,
      thread: thread,
      textContent: textContent,
      image: scaledImage,
    );
    await _nodeService.addNodes(response);

    await _analyticsService.logEvent(
      AnalyticsEvent.sentMessage,
      MessageEventParams(parentNode.type, scaledImage != null),
    );

    if (scaledImage != null) {
      var thumbnail = await scaleImage(image, messageImageThumbnailResolution);

      var messageNode = response.firstWhere((e) => e.type == NodeType.message);

      await Future.wait([
        _fileCacheStore.put(
          messageNode.id,
          messageNode.lastUpdateTs.toString(),
          scaledImage,
        ),
        _fileCacheStore.put(
          toThumbnailId(messageNode.id),
          messageNode.lastUpdateTs.toString(),
          thumbnail,
        ),
      ]);
    }
  }

  Future<Node<DirectChatData>> getDirectChat(String contactId) async {
    var chatId = _getDirectChatNodeId(contactId);

    var chat = await _nodeService.getNode<DirectChatData>(chatId);

    if (chat == null) {
      var nodes = await _chatApi.getDirectChat(contactId);
      await Future.forEach(
        nodes.map((e) => e.permissionGroupId).toSet(),
        (e) => _permissionService.fetch(e),
      );
      await _nodeService.addNodes(nodes);
      chat = nodes
          .firstWhere((node) => node.type == NodeType.chatDirect)
          .as<DirectChatData>();
    }

    return chat;
  }

  Future<Node<GroupChatData>> createGroupChat(
    Iterable<String> userIds,
    String name,
    Image coverImage,
  ) async {
    List<int> coverImageJpg;
    if (coverImage != null) {
      coverImageJpg = encodeJpg(coverImage, quality: imageQuality);
    }

    var nodes = await _chatApi.createGroupChat(userIds, name, coverImageJpg);
    await _permissionService.fetchUpdates();
    await _nodeService.addNodes(nodes);

    _analyticsService.logEvent(AnalyticsEvent.createdGroupChat);

    for (var _ in userIds) {
      _analyticsService.logEvent(AnalyticsEvent.sentInviteToChat);
    }

    var chatNode =
        nodes.firstWhere((node) => node.type == NodeType.chatGroup).as<GroupChatData>();

    if (coverImageJpg != null) {
      var thumbnail = encodeJpg(
          copyResize(
            coverImage,
            width: groupChatCoverPhotoThumbnailResolution,
            height: groupChatCoverPhotoThumbnailResolution,
          ),
          quality: imageQuality);

      _fileCacheStore.put(
        chatNode.id,
        chatNode.data.coverTs.toString(),
        coverImageJpg,
      );
      _fileCacheStore.put(
        toThumbnailId(chatNode.id),
        chatNode.data.coverTs.toString(),
        thumbnail,
      );
    }

    return chatNode;
  }

  Future<Node<GroupChatData>> updateGroupChatData(
    Node<GroupChatData> groupChatNode,
    String name,
    Image image,
    bool imageChanged,
  ) async {
    assert(groupChatNode != null);
    assert(name.isNotEmpty);

    List<int> coverImage;
    int coverTs = imageChanged ? 0 : groupChatNode.data.coverTs;
    if (image != null && imageChanged == true) {
      coverImage = encodeJpg(image, quality: imageQuality);
    }

    var updatedGroupChatNode =
        await _chatApi.updateGroupChatData(groupChatNode.id, name, coverTs, coverImage);
    await _nodeService.addNodes([updatedGroupChatNode]);

    if (coverImage != null && imageChanged) {
      var thumbnail = encodeJpg(
          copyResize(
            image,
            width: groupChatCoverPhotoThumbnailResolution,
            height: groupChatCoverPhotoThumbnailResolution,
          ),
          quality: imageQuality);

      _fileCacheStore.put(
        groupChatNode.id,
        groupChatNode.data.coverTs.toString(),
        coverImage,
      );
      _fileCacheStore.put(
        toThumbnailId(groupChatNode.id),
        groupChatNode.data.coverTs.toString(),
        thumbnail,
      );
    }

    return updatedGroupChatNode;
  }

  Future<void> setGroupChatMemberAdminStatus(
      String groupChatId, String memberId, bool setAdmin) async {
    assert(groupChatId != null);
    assert(memberId != null);

    await _chatApi.setGroupChatMemberAdminStatus(groupChatId, memberId, setAdmin);
    await _permissionService.fetchUpdates();
  }

  Future<String> getChatName(Node node) {
    if (node.type == NodeType.chatGroup) {
      return SynchronousFuture(node.as<GroupChatData>().data.name);
    } else {
      return _getDirectChatName(node);
    }
  }

  Future<String> _getDirectChatName(Node node) async {
    var permissions = await _permissionService.getContactPermissions(
      node.permissionGroupId,
    );
    return permissions.first.user.name;
  }

  String _getDirectChatNodeId(String contactId) {
    var ids = [_accountService.userId, contactId];
    ids.sort();
    return ids.join("~");
  }

  Future<User> getDirectChatUser(String chatNodeId) async {
    var userId = chatNodeId.split("~").where((id) => id != _accountService.userId).first;

    return await _userService.getUserById(userId);
  }

  Future<void> inviteToGroupChat(
      Node<GroupChatData> groupChatNode, Iterable<User> users) async {
    await _chatApi.inviteToGroupChat(groupChatNode, users);
    await _permissionService.fetchUpdates();

    for (var _ in users) {
      _analyticsService.logEvent(AnalyticsEvent.sentInviteToChat);
    }
  }

  Future<void> removeFromGroupChat(Node groupChatNode, [String userId]) async {
    assert(groupChatNode != null);

    await _chatApi.removeFromGroupChat(
        groupChatNode.id, userId ?? _accountService.userId);
    if (userId == null) await _nodeService.deleteNodeTreeAndPermissions(groupChatNode);
    if (userId != null)
      await _permissionService.deletePermission(userId, groupChatNode.permissionGroupId);
  }

  Future<void> deleteGroupChat(Node groupChatNode) async {
    assert(groupChatNode != null);

    await _chatApi.deleteGroupChat(groupChatNode.id);
    await _nodeService.deleteNodeTreeAndPermissions(groupChatNode);
  }

  Future<void> clearThreadForUser(
    Node<ThreadData> threadNode,
  ) async {
    await _threadApi.clearThreadForUser(threadNode, _accountService.userId);
    await _nodeService.deleteChildNodes(threadNode.id);
  }
}
