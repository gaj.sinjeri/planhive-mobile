import 'package:image/image.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/data/contact_user.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/exceptions/users_not_found_exception.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/storage/common/photo_identifier.dart';
import 'package:planhive/storage/user_store.dart';

@lazySingleton
class UserService {
  final UserApi _userApi;
  final UserStore _userStore;
  final AccountService _accountService;
  final FileCacheStore _fileCacheStore;
  final ContactService _contactService;

  UserService(
    this._userApi,
    this._userStore,
    this._accountService,
    this._fileCacheStore,
    this._contactService,
  );

  Future<void> updateLocalUser(
    String username,
    String name,
    bool updateAvatar,
    Image image,
  ) async {
    User updatedUser;
    if (image != null && updateAvatar == true) {
      var avatar = encodeJpg(image, quality: imageQuality);

      updatedUser = await _userApi.updateUserDetails(username, name, 0, avatar);

      storeLocalAvatar(image);
    } else {
      var existingUser = await _accountService.user;

      updatedUser = await _userApi.updateUserDetails(
          username, name, updateAvatar ? 0 : existingUser.lastAvatarUpdateTs, null);
    }

    await _accountService.updateUser(
      username: updatedUser.username,
      name: updatedUser.name,
      lastUpdateTs: updatedUser.lastUpdateTs,
      lastAvatarUpdateTs: updatedUser.lastAvatarUpdateTs,
    );
  }

  void storeLocalAvatar(Image image) {
    var userId = _accountService.userId;

    var thumbnail = encodeJpg(
      copyResize(
        image,
        width: userAvatarThumbnailResolution,
        height: userAvatarThumbnailResolution,
      ),
      quality: imageQuality,
    );
    var microThumbnail = encodeJpg(
      copyResize(
        image,
        width: userAvatarMicroResolution,
        height: userAvatarMicroResolution,
      ),
      quality: imageQuality,
    );

    var now = DateTime.now();

    _fileCacheStore.put(
      userId,
      now.millisecondsSinceEpoch.toString(),
      encodeJpg(image, quality: imageQuality),
    );
    _fileCacheStore.put(
      toThumbnailId(userId),
      now.millisecondsSinceEpoch.toString(),
      thumbnail,
    );
    _fileCacheStore.put(
      toMicroThumbnailId(userId),
      now.millisecondsSinceEpoch.toString(),
      microThumbnail,
    );
  }

  Future<List<User>> getConnectedUsers() async {
    var users = List<User>.from(await _userStore.getConnectedUsers());
    users.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));

    return List<User>.unmodifiable(users);
  }

  Future<List<ContactUser>> getAllContactUsers() async {
    return await _userStore.getAllContactUsers();
  }

  Future<List<User>> getUsersById(Iterable<String> userIds) async {
    var users = <User>[];

    var userIdSet = Set<String>.from(userIds);

    var existingUsers = await _userStore.getUsers(userIdSet);
    users.addAll(existingUsers);
    users.addAll(await _fetchMissing(
      userIdSet.difference(existingUsers.map((e) => e.id).toSet()),
    ));

    assert(users.length <= userIdSet.length);

    if (users.length < userIdSet.length) {
      throw UsersNotFoundException(
        userIdSet.difference(users.map((e) => e.id).toSet()).toList(),
      );
    }

    users.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));

    return List<User>.unmodifiable(users);
  }

  Future<User> getUserById(String userId) async {
    var users = await getUsersById([userId]);
    return users.first;
  }

  Future<void> fetchMissing(Iterable<String> userIds) async {
    var missingUserIds = Set<String>.from(userIds);

    var existingUsers = await _userStore.getUsers(missingUserIds);

    await _fetchMissing(missingUserIds..removeAll(existingUsers.map((user) => user.id)));
  }

  Future<List<User>> _fetchMissing(Iterable<String> userIds) async {
    if (userIds.isEmpty) return const [];

    var missingUserIds = List<String>.from(userIds);
    var users = <User>[];

    var newContactUsers = await _contactService.fetchUpdates();

    newContactUsers.forEach((newContactUser) {
      if (userIds.contains(newContactUser.user.id)) {
        users.add(newContactUser.user);
        missingUserIds.remove(newContactUser.user.id);
      }
    });

    if (missingUserIds.isNotEmpty) {
      var userPublicViews = await _userApi.getUserDetails(missingUserIds);
      await _userStore.addUsers(userPublicViews);
      users.addAll(userPublicViews);
    }

    return List.unmodifiable(users);
  }

  bool isUsernameAutocompleteQueryValid(String query) {
    var queryTrimmed = query.trim();

    return queryTrimmed.length >= 4 ||
        (queryTrimmed.length >= 3 && queryTrimmed[0] != "@");
  }

  Future<List<User>> getUsernameAutocompleteSuggestion(String query) async {
    if (!isUsernameAutocompleteQueryValid(query)) return const [];

    var queryTrimmed = query.trim();
    if (queryTrimmed[0] == "@") {
      queryTrimmed = queryTrimmed.substring(1, queryTrimmed.length);
    }

    var suggestions = await _userApi.getUsernameAutocompleteSuggestion(queryTrimmed);

    return List.unmodifiable(
        suggestions.where((user) => user.id != _accountService.userId));
  }

  Future<String> validateUsername(String username) async {
    username = username.trim();

    if (username.length < User.minUsernameLength) {
      return "Minimum length is ${User.minUsernameLength} characters";
    }

    if (username.length > User.maxDisplayNameLength) {
      return "Maximum length is ${User.maxDisplayNameLength} characters";
    }

    if (!RegExp(r"^[a-z0-9]").hasMatch(username)) {
      return "Username must start with a letter or number";
    }

    if (!RegExp(r"[a-z0-9]$").hasMatch(username)) {
      return "Username must end with a letter or number";
    }

    if (RegExp(r"[._]{2,}").hasMatch(username)) {
      return "Username must not contain consecutive symbols";
    }

    assert(User.usernameRegExp.hasMatch(username));

    var available = await _userApi.checkUsernameAvailability(username.toLowerCase());

    return available ? null : "Username unavailable";
  }

  String validateDisplayName(String name) {
    if (name.trim().length < User.minDisplayNameLength) {
      return "Minimum length is ${User.minDisplayNameLength} characters";
    }

    return null;
  }
}
