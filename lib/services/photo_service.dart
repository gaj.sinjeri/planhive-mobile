import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:planhive/api/photo_api.dart';
import 'package:planhive/common/constants/image_settings.dart';
import 'package:planhive/common/image/scale.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/photo_data.dart';
import 'package:planhive/data/node/photos_data.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/storage/common/photo_identifier.dart';

import 'node_service.dart';

@lazySingleton
class PhotoService {
  final _fileCacheStore = GetIt.I<FileCacheStore>();
  final _nodeService = GetIt.I<NodeService>();

  final PhotoApi _photoApi;

  PhotoService(this._photoApi);

  Stream<Node<PhotoData>> addPhotos(
    Node<PhotosData> photosNode,
    List<Asset> photos,
  ) async* {
    var photoUploadCount = 0;
    try {
      for (var photo in photos) {
        var fullSizeImage = await scaleImage(photo, photoMaxResolution);
        var thumbnail = await scaleImage(photo, photoThumbnailResolution);

        var response = await _photoApi.addPhoto(
          groupNodeId: photosNode.parentId,
          photosNodeId: photosNode.id,
          photo: fullSizeImage,
        );

        await _nodeService.addNodes(response);

        GetIt.I<AnalyticsService>().logEvent(AnalyticsEvent.sharedToAlbum);

        var photoNode = response.firstWhere((e) => e.type == NodeType.photo);

        await Future.wait([
          _fileCacheStore.put(
            photoNode.id,
            photoNode.lastUpdateTs.toString(),
            fullSizeImage,
          ),
          _fileCacheStore.put(
            toThumbnailId(photoNode.id),
            photoNode.lastUpdateTs.toString(),
            thumbnail,
          ),
        ]);

        const Logger(PhotoService).trace("Uploaded photo: $photoNode");

        photoUploadCount++;

        yield photoNode.as<PhotoData>();
      }
    } finally {
      if (photoUploadCount > 0)
        await _photoApi.sendPhotoUploadNotification(
          groupNodeId: photosNode.parentId,
          photosNodeId: photosNode.id,
        );
    }
  }

  Future<void> deletePhoto(
      String groupNodeId, String photosNodeId, String photoId) async {
    await _photoApi.deletePhoto(groupNodeId, photosNodeId, photoId);

    _fileCacheStore.delete(photoId);
    await _nodeService.deleteNode(photoId);
  }
}
