import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/data/sort_order.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/common/async_debouncer.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/update_timestamps.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/services/update_management_service.dart';
import 'package:planhive/storage/node_store.dart';
import 'package:planhive/storage/permission_store.dart';
import 'package:tuple/tuple.dart';

class _FetchResult<T> {
  final int lastTimestamp;
  final List<T> results;

  _FetchResult(this.lastTimestamp, this.results);
}

@lazySingleton
class NodeService {
  final NodeStore _nodeStore;
  final PermissionStore _permissionStore;

  final PermissionService _permissionService;
  final UpdateManagementService _updateService;

  final NodeApi _nodeApi;

  NodeService(
    this._nodeStore,
    this._permissionStore,
    this._permissionService,
    this._updateService,
    this._nodeApi,
  );

  Future<void> addNodes(Iterable<Node> nodes) {
    return _nodeStore.addNodes(nodes);
  }

  Future<List<Node<T>>> getCachedNodes<T extends NodeData>() {
    return _nodeStore.getNodes<T>();
  }

  Future<List<Node<T>>> getNonEmptyNodes<T extends NodeData>() {
    return _nodeStore.getNonEmptyNodes<T>();
  }

  Future<List<Node<T>>> getNonEmptyForParentTypes<T extends NodeData>(
    List<NodeType> parentTypes,
  ) {
    return _nodeStore.getNonEmptyForParentTypes<T>(parentTypes);
  }

  Future<List<Node<T>>> getCachedChildNodes<T extends NodeData>(Node parent) {
    return _nodeStore.getChildrenOfType<T>(parent.id);
  }

  Future<Node<T>> getNode<T extends NodeData>(String nodeId) {
    return _nodeStore.getNode<T>(nodeId);
  }

  Future<bool> fetchFeeds(Iterable<NodeType> types) async {
    var results = await Future.wait(
      types.map((type) => fetchFeed(type)),
    );
    return results.contains(true);
  }

  Future<bool> fetchFeed(NodeType nodeType) async {
    assert(nodeType != null);

    final updateId = enumToString(nodeType, NodeType.values);
    final times = await _updateService.getTimestamps(updateId);

    return await Future.wait([
      if (times.oldest == null)
        debounce(
          () async {
            var startTs = _updateService.updateStartTs;
            var result = await _fetchAll<Node>(
              startTs,
              (t) => _nodeApi.getPastUpdates(nodeType, t),
              (prev, node) => min(prev, node.lastUpdateTs),
            );

            if (nodeType == NodeType.timeline) {
              // this is a special case for the timeline, because we also need the activity
              // nodes to sort the events on the home page
              await Future.forEach(
                result.results,
                (e) => fetchChildren(e),
              );
            } else {
              await Future.forEach(
                result.results
                    .map((e) => e.permissionGroupId)
                    .where((e) => e != null)
                    .toSet(),
                _permissionService.fetch,
              );
            }

            await _nodeStore.addNodes(result.results);
            await _updateService.setTimestamps(
              updateId,
              UpdateTimestamps(result.lastTimestamp, times.latest),
            );

            return result.results.isNotEmpty;
          },
          debounceId: Tuple3(NodeService, nodeType, "history"),
        ),
      debounce(
        () async {
          var startTs = times.latest ?? _updateService.updateStartTs;
          var result = await _fetchAll<Node>(
            startTs,
            (t) => _nodeApi.getUpdates(nodeType, t),
            (prev, node) => max(prev, node.lastUpdateTs),
          );

          await _nodeStore.addNodes(result.results);
          await _updateService.setTimestamps(
            updateId,
            UpdateTimestamps(times.oldest, max(startTs, result.lastTimestamp)),
          );

          return result.results.isNotEmpty;
        },
        debounceId: Tuple3(NodeService, nodeType, "updates"),
      ),
    ]).then((results) => results.contains(true));
  }

  Future<bool> fetchChildren(Node parent) async {
    assert(parent != null);

    return await debounce(() async {
      var updateId = _getNodeUpdateId(parent);
      var times = await _updateService.getTimestamps(updateId);

      if (times.oldest != null) {
        return false;
      }

      _FetchResult<Node> history;

      await Future.wait([
        () async {
          history = await _fetchChildHistory(
            parent,
            DateTime.now().millisecondsSinceEpoch,
          );
        }(),
        _permissionService.fetchIfMissing(parent.permissionGroupId),
      ]);

      await addNodes(history.results);

      await _updateService.setTimestamps(
        updateId,
        UpdateTimestamps(history.lastTimestamp, null),
      );

      return history.results.isNotEmpty;
    }, debounceId: Tuple2(NodeService, parent));
  }

  String _getNodeUpdateId(Node n) => enumToString(n.type, NodeType.values) + n.id;

  Future<_FetchResult<Node>> _fetchChildHistory(Node parent, int startTs) async {
    var history = await _fetchAll(
      startTs,
      (ts) => _nodeApi.getChildren(parent.nodeId, ts, SortOrder.DESC),
      (prevTs, node) => min(prevTs, node.lastUpdateTs),
    );

    await Future.forEach(
      history.results
          .map((e) => e.permissionGroupId)
          .where((e) => e != null && e != parent.permissionGroupId)
          .toSet(),
      _permissionService.fetch,
    );

    return history;
  }

  static Future<_FetchResult<T>> _fetchAll<T>(
    int time,
    Future<List<T>> Function(int) fetcher,
    int Function(int, T) sortTime,
  ) async {
    var history = Set<T>();
    int prevLength = -1;
    while (prevLength != history.length) {
      prevLength = history.length;
      var fetched = await fetcher(time);
      time = fetched.fold(time, (prev, e) => sortTime(prev, e));
      history.addAll(fetched);
    }

    return _FetchResult(time, List.unmodifiable(history));
  }

  Future<void> deleteNode(String nodeId) {
    return _nodeStore.deleteNode(nodeId);
  }

  // TODO(#111): Traverse entire node tree when deleting
  Future<void> deleteNodeTreeAndPermissions(Node parentNode) async {
    assert(parentNode != null);

    var nodesToDelete = <Node>[parentNode];
    var permissionGroupIdsToDelete = HashSet<String>();

    var childNodes = await _nodeStore.getChildren(parentNode);
    nodesToDelete.addAll(childNodes);
    permissionGroupIdsToDelete.addAll(childNodes
        .where((e) => e.permissionGroupId != null)
        .map((e) => e.permissionGroupId));

    var updateIdsToDelete = nodesToDelete.map(_getNodeUpdateId).toList();

    // Retrieve all leaf nodes and add to list for deletion
    for (var childNode in childNodes) {
      var leafNodes = await _nodeStore.getChildren(childNode);
      nodesToDelete.addAll(leafNodes);
      permissionGroupIdsToDelete.addAll(leafNodes
          .where((e) => e.permissionGroupId != null)
          .map((e) => e.permissionGroupId));
    }

    await _updateService.deleteTimestamps(updateIdsToDelete);
    await _nodeStore.deleteNodes(nodesToDelete);
    await _permissionStore.deletePermissions(permissionGroupIdsToDelete);
  }

  Future<void> fetchMissing(List<NodeId> ids) async {
    var missing = await _nodeStore.getMissing(ids);

    var nodes = await Future.wait(missing.map((nodeId) {
      return _nodeApi.getNode(nodeId);
    }));

    await Future.forEach(
      nodes.map((e) => e.permissionGroupId).toSet(),
      (e) => _permissionService.fetch(e),
    );

    await addNodes(nodes);
  }

  Future<void> deleteChildNodes(String parentNodeId) async {
    await _nodeStore.deleteChildNodes(parentNodeId);
  }
}
