// IMPORTANT: do not rename these enums, because it will break the continuity of the
// analytics data.

enum AnalyticsEvent {
  submittedEmail,
  requestedNewEmail,
  validatedEmail,
  completedProfile,
  logIn,
  signUp,

  requestedContact,
  acceptedContact,
  sentInviteToEvent,
  sentInviteToChat,

  changedStatus,
  createdEvent,
  createdGroupChat,
  createdActivity,
  votedActivity,
  sharedToAlbum,
  dynamicLink,

  sentMessage,
}