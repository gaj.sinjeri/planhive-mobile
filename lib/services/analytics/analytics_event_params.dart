import 'package:planhive/services/analytics/analytics_event.dart';

abstract class AnalyticsEventParams {
  final AnalyticsEvent event;

  const AnalyticsEventParams(this.event);

  Map<String, dynamic> toMap();

  @override
  String toString() => toMap().toString();
}