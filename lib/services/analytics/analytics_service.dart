import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_event_params.dart';
import 'package:planhive/services/navigation_service.dart';

@singleton
@preResolve
class AnalyticsService {
  static const _logger = Logger(AnalyticsService);

  final FirebaseAnalytics _analytics;
  final NavigatorObserver navigatorObserver;

  AnalyticsService._(this._analytics)
      : navigatorObserver = FirebaseAnalyticsObserver(
          analytics: _analytics,
          nameExtractor: _nameExtractor,
        );

  @factoryMethod
  static Future<AnalyticsService> factory() async {
    var analytics = FirebaseAnalytics();

    await analytics.setAnalyticsCollectionEnabled(kReleaseMode);
    await analytics.setUserProperty(
      name: "app_type",
      value: kReleaseMode ? "prod" : "dev",
    );

    return AnalyticsService._(analytics);
  }

  static String _nameExtractor(RouteSettings settings) {
    var routeData = NavigationService.getRouteData(settings);
    // Exclude wrapper pages from analytics, they need to manually log the sub-page
    // currently in view.
    return routeData.isWrapperPage ? null : routeData.name;
  }

  Future<void> setUserId(String userId) {
    const salt = "4F#4R9*Q8Zhb\$!je3GYm%";
    var hash = base64Url.encode(sha256.convert(utf8.encode("$userId$salt")).bytes);
    _logger.debug("Setting analytics user id: $hash");
    return _analytics.setUserId(hash);
  }

  Future<void> logScreen(String screenName) {
    return _analytics.setCurrentScreen(screenName: screenName);
  }

  Future<void> logEvent(AnalyticsEvent event, [AnalyticsEventParams params]) async {
    assert(params == null || params.event == event);

    if (params != null && params.event != event) {
      return;
    }

    _logger.debug("Logging $event: $params");

    return _analytics.logEvent(
      name: enumToString(
        event,
        AnalyticsEvent.values,
        EnumCaseStyle.snake,
      ),
      parameters: params?.toMap(),
    );
  }
}
