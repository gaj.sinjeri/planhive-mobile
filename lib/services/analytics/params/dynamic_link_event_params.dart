import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_event_params.dart';

class DynamicLinkEventParams extends AnalyticsEventParams {
  final String action;

  const DynamicLinkEventParams(this.action) : super(AnalyticsEvent.dynamicLink);

  @override
  Map<String, dynamic> toMap() {
    return {
      "link_action" : action,
    };
  }
}
