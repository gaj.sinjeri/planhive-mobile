import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_event_params.dart';

class MessageEventParams extends AnalyticsEventParams {
  final NodeType rootNodeType;
  final bool isImage;

  MessageEventParams(this.rootNodeType, this.isImage) : super(AnalyticsEvent.sentMessage);

  @override
  Map<String, dynamic> toMap() {
    return {
      "container": enumToString(rootNodeType, NodeType.values),
      "image": isImage ? 1 : 0,
    };
  }

}