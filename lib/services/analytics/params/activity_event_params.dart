import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_event_params.dart';

class ActivityEventParams extends AnalyticsEventParams {
  final ActivityData activityData;

  const ActivityEventParams(this.activityData) : super(AnalyticsEvent.createdActivity);

  @override
  Map<String, dynamic> toMap() {
    return {
      "suggestion": activityData.isSuggestion ? 1 : 0,
      "end_date": activityData.time.endTs != null ? 1 : 0,
      "location": activityData.location != null ? 1 : 0,
      "description": activityData.description?.isNotEmpty == true ? 1 : 0,
      "notes": activityData.note?.isNotEmpty == true ? 1 : 0
    };
  }
}
