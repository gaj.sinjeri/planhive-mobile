import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/participation_data.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_event_params.dart';

class StatusEventParams extends AnalyticsEventParams {
  final ParticipantStatus currentStatus;
  final ParticipantStatus newStatus;

  const StatusEventParams(
    this.currentStatus,
    this.newStatus,
  ) : super(AnalyticsEvent.changedStatus);

  @override
  Map<String, dynamic> toMap() {
    return {
      "prev_status": currentStatus == null
          ? "none"
          : enumToString(currentStatus, ParticipantStatus.values, EnumCaseStyle.snake),
      "new_status": enumToString(newStatus, ParticipantStatus.values),
    };
  }
}
