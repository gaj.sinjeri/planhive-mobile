import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/api/event_api.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/analytics/params/dynamic_link_event_params.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/platform/app_link_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';

@singleton
class InviteLinkService {
  static const _logger = Logger(InviteLinkService);

  final AccountStore _accountStore;
  final AnalyticsService _analyticsService;
  final AppLinkService _appLinkService;
  final EventApi _eventApi;
  final NodeService _nodeService;

  InviteLinkService(
    this._accountStore,
    this._analyticsService,
    this._appLinkService,
    this._eventApi,
    this._nodeService,
  ) {
    _appLinkService.onData.addListener(_handleLink);

    _accountStore.onAccountStateChanged.addListener(_onAccountStateChanged);

    _handleLink(_appLinkService.initialLink, true);
  }

  Future<Uri> shareInviteLink(Node<EventData> event) async {
    assert(event.data.inviteCode?.isNotEmpty == true);

    var user = await GetIt.I<AccountService>().user;

    return _appLinkService.generateShortLink(
      link: Uri.https(
          "go.planhive.app", "event-invite/${event.id}/${event.data.inviteCode}"),
      title: "${user.name} invited you to join ${event.data.name}",
      description: "Hassle free planning. Organize a range of events from catch-ups, "
          "birthdays, anniversaries to outdoor activities, trips.",
    );
  }

  void _handleLink(Uri link, [bool initial = false]) async {
    if (link?.path?.startsWith("/event-invite/") != true) {
      return;
    }

    String eventId = link.pathSegments[1];
    String inviteCode = link.pathSegments[2];

    if (_accountStore.accountState == AccountState.Complete) {
      var event = await _nodeService.getNode<EventData>(eventId);

      if (event != null && initial) {
        _logger.debug("Stop processing invite on start due to existing event");
        return;
      }

      if (event == null) {
        event = await _join(inviteCode);
        await _nodeService.addNodes([event]);
        await _nodeService.fetchChildren(event);

        _analyticsService.logEvent(
          AnalyticsEvent.dynamicLink,
          DynamicLinkEventParams("join_event"),
        );
      }

      GetIt.I<NavigationService>().navigateToNode(event);
    } else {
      _accountStore.setInviteCode(inviteCode);
    }
  }

  Future<Node<EventData>> _join(String inviteCode) async {
    try {
      return await _eventApi.acceptEventInvite(inviteCode);
    } on ApiException {
      await showAlertDialog(
        title: "Could not join event",
        content: "Please ask for a new invitation link.",
        barrierDismissible: false,
        actions: [
          AlertActionData(text: "OK"),
        ],
      );

      rethrow;
    }
  }

  Future<void> _onAccountStateChanged(AccountState accountState) async {
    if (accountState != AccountState.Complete) {
      return;
    }

    if (_accountStore.inviteCode != null) {
      try {
        await _join(_accountStore.inviteCode);

        _analyticsService.logEvent(
          AnalyticsEvent.dynamicLink,
          DynamicLinkEventParams("delayed_join_event"),
        );
      } on dynamic {
        // swallow exceptions as we don't want this to cause other failures
      }
    }
  }
}
