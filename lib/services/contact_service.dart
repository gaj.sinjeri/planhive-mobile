import 'package:injectable/injectable.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/contact_user.dart';
import 'package:planhive/services/analytics/analytics_event.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/update_management_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/storage/common/update_management_ids.dart';
import 'package:planhive/storage/contact_store.dart';
import 'package:planhive/storage/user_store.dart';

@singleton
class ContactService {
  static const _logger = Logger(ContactService);

  final AccountStore _accountStore;
  final ContactStore _contactStore;
  final UpdateManagementService _updateManagementService;
  final UserApi _userApi;
  final UserStore _userStore;
  final AnalyticsService _analyticsService;

  ContactService(
    this._accountStore,
    this._contactStore,
    this._updateManagementService,
    this._userApi,
    this._userStore,
    this._analyticsService,
  ) {
    if (_accountStore.accountState == AccountState.Complete) {
      refreshContacts();
    }
  }

  // TODO: this should run periodically (e.g. every 24 hours)
  Future<List<ContactUser>> refreshContacts() async {
    return await fetchUpdates();
  }

  Future<List<ContactUser>> fetchUpdates() async {
    var latestTs = await _updateManagementService.getLatestTs(contactUpdateManagementId);

    var contactUsers = await _userApi.getContactUpdates(latestTs);
    _logger.debug("Server returned ${contactUsers.length} new contacts");

    if (contactUsers.length > 0) {
      await _userStore.addUsers(contactUsers.map((e) => e.user));
      await _contactStore.addContacts(contactUsers.map((e) => e.contact));

      var newLatestTs = contactUsers
          .reduce((a, b) => a.contact.lastUpdateTs > b.contact.lastUpdateTs ? a : b)
          .contact
          .lastUpdateTs;
      await _updateManagementService.updateLatestTs(
          contactUpdateManagementId, newLatestTs);
    }

    return contactUsers;
  }

  Future<Contact> getContact(String contactId) async {
    return await _contactStore.getContact(contactId);
  }

  Future<void> createContactRequest(String contactId) async {
    var updatedContact = await _userApi.createContactRequest(contactId);
    await _contactStore.addContacts([updatedContact]);

    _analyticsService.logEvent(AnalyticsEvent.requestedContact);
  }

  Future<void> cancelContactRequest(String contactId) async {
    var updatedContact = await _userApi.cancelContactRequest(contactId);
    await _contactStore.addContacts([updatedContact]);
  }

  Future<void> respondToContactRequest(String contactId, bool accepted) async {
    var updatedContact = await _userApi.respondToContactRequest(contactId, accepted);
    await _contactStore.addContacts([updatedContact]);

    _analyticsService.logEvent(AnalyticsEvent.acceptedContact);
  }

  Future<void> deleteContact(String contactId) async {
    var updatedContact = await _userApi.deleteContact(contactId);
    await _contactStore.addContacts([updatedContact]);
  }
}
