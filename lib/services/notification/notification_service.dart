import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';
import 'package:planhive/data/notification/node_notification_data.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/data/notification/user_notification_data.dart';
import 'package:planhive/services/contact_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/services/node_service.dart';
import 'package:planhive/services/notification/notification_handler.dart';
import 'package:planhive/services/permission_service.dart';
import 'package:planhive/storage/account_store.dart';

enum _LocalNotificationType {
  forwardedPush,
}

@singleton
class NotificationService {
  static const _logger = Logger(NotificationService);

  final _firebase = FirebaseMessaging();
  final _localNotifications = FlutterLocalNotificationsPlugin();
  final AccountStore _accountStore;
  final UserApi _userApi;
  final NavigationService _navigationService;
  final NodeService _nodeService;
  final ContactService _contactService;
  final PermissionService _permissionService;

  final _notificationListeners = <NotificationHandler>[];

  Future<bool> _localNotificationsInitialized;

  int _localNotificationIdCounter = 0;

  NotificationService(
    this._accountStore,
    this._userApi,
    this._navigationService,
    this._nodeService,
    this._contactService,
    this._permissionService,
  ) {
    _localNotificationsInitialized = _localNotifications
        .initialize(
          const InitializationSettings(
            android: const AndroidInitializationSettings("notification_icon"),
            iOS: const IOSInitializationSettings(
              requestAlertPermission: false,
              requestBadgePermission: false,
              requestSoundPermission: false,
            ),
          ),
          onSelectNotification: _onLocalNotificationClick,
        )
        .catchError(
          (e, s) => _logger.error("Error initializing local notifications", e, s),
        );

    _firebase.configure(
      onMessage: _onMessage,
      onResume: _onResume,
      onLaunch: _onLaunch,
    );

    _firebase.onTokenRefresh.listen((token) {
      _logger.info("Firebase notified client about new token: '$token'");
      _refreshTokenIfChanged(token);
    });

    if (_accountStore.accountState == AccountState.Complete) {
      enablePushNotificationsForUser();
    }
  }

  void addListener(NotificationHandler listener) {
    _notificationListeners.add(listener);
  }

  void removeListener(NotificationHandler listener) {
    _notificationListeners.remove(listener);
  }

  Future<void> enablePushNotificationsForUser() async {
    await _firebase.requestNotificationPermissions();
    await _refreshTokenIfChanged(await _firebase.getToken());
  }

  Future<void> _onMessage(Map<String, dynamic> msg) async {
    _logger.trace("onMessage: $msg");

    Map data = _getMessageData(msg);

    var notification = _makeNotification(data);

    await _fetchChanges(notification);

    if (!await _tryHandleNotification(notification, false)) {
      await _forwardAsLocalNotification(msg);
    }
  }

  Future<void> _onResume(Map<String, dynamic> msg) async {
    _logger.trace("onResume: $msg");

    await _handleNotificationClick(msg, true);
  }

  Future<void> _onLaunch(Map<String, dynamic> msg) async {
    _logger.trace("onLaunch: $msg");

    var notification = _makeNotification(_getMessageData(msg));
    await _fetchChanges(notification);
    await _updateNavigation(notification);
  }

  Future<void> _onLocalNotificationClick(String payloadString) async {
    var payload = jsonDecode(payloadString);
    switch (_LocalNotificationType.values[payload["type"]]) {
      case _LocalNotificationType.forwardedPush:
        await _handleNotificationClick(payload["msg"], false);
        break;
    }
  }

  Future<void> _handleNotificationClick(Map msg, bool sync) async {
    var notification = _makeNotification(_getMessageData(msg));

    if (sync) {
      await _fetchChanges(notification);
    }

    if (!await _tryHandleNotification(notification, true)) {
      await _updateNavigation(notification);
    }
  }

  Future<void> _updateNavigation(NotificationData notification) async {
    await _navigationService.navigateForNotification(notification);
  }

  Future<void> _fetchChanges(NotificationData notification) async {
    try {
      if (notification is NodeNotificationData) {
        await Future.wait([
          _permissionService.fetchUpdates(),
          _nodeService.fetchFeeds(notification.nodes.take(2).map((e) => e.type)),
        ]);
      } else if (notification is UserNotificationData) {
        await _contactService.fetchUpdates();
      }
    } catch (e, s) {
      _logger.error("Error while fetching updates for notification", e, s);
    }
  }

  NotificationData _makeNotification(Map data) {
    switch (data["typ"]) {
      case "node":
        return _makeNodeNotification(data["nodeIds"]);
      case "user":
        return UserNotificationData(data["userId"]);
      default:
        _logger.error("Unsupported notification type ${data["typ"]}");
        return null;
    }
  }

  NodeNotificationData _makeNodeNotification(String nodeIdsJson) {
    List nodeIds = jsonDecode(nodeIdsJson);
    var notification = NodeNotificationData(
      List.unmodifiable(nodeIds.map((e) => NodeId(
            id: e["id"],
            parentId: e["parentId"],
            type: allCapsToEnum(e["type"], NodeType.values),
          ))),
    );
    return notification;
  }

  Future<bool> _tryHandleNotification(NotificationData notification, bool clicked) async {
    if (notification == null) {
      return false;
    }

    var res = await Future.wait<bool>(_notificationListeners.map(
      (l) async {
        try {
          return await l.handleNotification(notification, clicked);
        } catch (e, s) {
          _logger.error("Error in notification handler '${l.runtimeType}'", e, s);
          return false;
        }
      },
    ));

    return res.contains(true);
  }

  Future<void> _forwardAsLocalNotification(Map msg) async {
    Map notification = _getNotification(msg);

    _sendLocalNotification(
      _LocalNotificationType.forwardedPush,
      notification["title"],
      notification["body"],
      msg,
    );
  }

  Future<void> _sendLocalNotification(
    _LocalNotificationType type,
    String title,
    String body,
    dynamic data,
  ) async {
    await _localNotificationsInitialized;

    await _localNotifications.show(
      _localNotificationIdCounter++,
      title,
      body,
      NotificationDetails(
        android: const AndroidNotificationDetails(
          "in_app",
          "In-app notifications",
          "Notifications received while you are using the app",
          icon: "notification_icon",
          priority: Priority.defaultPriority,
          importance: Importance.high,
        ),
        iOS: const IOSNotificationDetails(
          presentAlert: true,
          presentSound: true,
        ),
      ),
      payload: jsonEncode({
        "type": type.index,
        "msg": data,
      }),
    );
  }

  // On Android, the message contains an additional field data containing the data.
  // On iOS, the data is directly appended to the message and the additional data-field is omitted.
  Map _getMessageData(Map msg) => msg["data"] ?? msg;

  Map _getNotification(Map msg) => msg["notification"] ?? msg["aps"]["alert"];

  Future<void> _refreshTokenIfChanged(String token) async {
    if (_accountStore.accountState != AccountState.Complete) {
      return;
    }

    if (token == _accountStore.notificationToken) {
      _logger.info("Notification token is already up-to-date");
      return;
    }

    try {
      // TODO: implement re-try with exponential backoff for API call
      await _userApi.updateNotificationToken(token);
      await _accountStore.setNotificationToken(token);

      _logger.info("Notification token updated successfully");
    } catch (e, s) {
      _logger.error("Could not refresh notification token", e, s);
    }
  }

  @visibleForTesting
  void sendDummyNotification(
    NotificationData data, {
    String title = "Dummy Title",
    String body = "Dummy body",
  }) {
    _onMessage({
      "notification": {
        "title": title,
        "body": body,
      },
      "data": {
        "typ": data is NodeNotificationData ? "node" : "user",
        if (data is UserNotificationData) "userId": data.userId,
        if (data is NodeNotificationData) "nodeIds": data.nodes.map((e) => e.id).toList(),
      },
    });
  }
}
