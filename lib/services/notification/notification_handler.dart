import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:planhive/data/notification/notification_data.dart';
import 'package:planhive/services/notification/notification_service.dart';

abstract class NotificationHandler {
  /// Called when a new notification is received or clicked by the user.
  /// Implementations should return true if they can handle the notification
  /// and false otherwise.
  /// The [notification] object contains the details of the current notification,
  /// while [clicked] indicates whether this method is being invoked as a consequence
  /// of a user click.
  Future<bool> handleNotification(NotificationData notification, bool clicked);
}

mixin NotificationHandlerMixin<T extends StatefulWidget> on State<T>
    implements NotificationHandler {
  final _notificationService = GetIt.I<NotificationService>();

  @override
  void initState() {
    _notificationService.addListener(this);
    super.initState();
  }

  @override
  void dispose() {
    _notificationService.removeListener(this);
    super.dispose();
  }
}
