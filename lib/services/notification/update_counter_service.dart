import 'package:flutter/widgets.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/storage/node_store.dart';

@singleton
class UpdateCounterService with WidgetsBindingObserver {
  final UserApi _userApi;
  final NodeStore _nodeStore;
  final AccountStore _accountStore;

  UpdateCounterService(this._userApi, this._nodeStore, this._accountStore) {
    _setNotificationCounter(0);
    WidgetsBinding.instance.addObserver(this);
  }

  Future<void> clear(Iterable<String> nodeIds) async {
    await _nodeStore.markRead(nodeIds);
  }

  Future<void> clearWithChildren(String parentId) async {
    await _nodeStore.markReadWithChildren(parentId);
  }

  Future<int> count(Iterable<NodeType> nodeTypes) async {
    return await _nodeStore.countUnread(nodeTypes);
  }

  Future<int> countChildren(String parentId, Iterable<NodeType> nodeTypes) async {
    return await _nodeStore.countUnreadChildren(parentId, nodeTypes);
  }

  Future<int> countUnreadRoots(Iterable<NodeType> nodeTypes) async {
    return (await _nodeStore.getUnreadRoots(nodeTypes)).length;
  }

  Future<Set<String>> getUnreadRoots(Iterable<NodeType> nodeTypes) async {
    return await _nodeStore.getUnreadRoots(nodeTypes);
  }

  @protected
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _setNotificationCounter(0);
    }
  }

  void _setNotificationCounter(int counter) {
    if (_accountStore.accountState == AccountState.Complete) {
      _userApi.setNotificationCounter(0);
    }
  }
}
