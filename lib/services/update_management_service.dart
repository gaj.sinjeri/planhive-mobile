import 'package:injectable/injectable.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/update_timestamps.dart';
import 'package:planhive/storage/update_management_store.dart';

@lazySingleton
class UpdateManagementService {
  static const _logger = Logger(UpdateManagementService);

  final UpdateManagementStore _updateManagementStore;

  UpdateManagementService(
    this._updateManagementStore,
  );

  int get updateStartTs => _updateManagementStore.updateStartTs;

  Future<UpdateTimestamps> getTimestamps(String updateManagementId) {
    return _updateManagementStore.getTimestamps(updateManagementId);
  }

  Future<void> setTimestamps(String updateManagementId, UpdateTimestamps timestamps) {
    return _updateManagementStore.setTimestamps(updateManagementId, timestamps);
  }

  Future<void> deleteTimestamps(List<String> updateManagementIds) {
    return _updateManagementStore.deleteTimestamps(updateManagementIds);
  }

  Future<int> getLatestTs(String updateManagementId) async {
    var latestTs = await _updateManagementStore.getLatestTs(updateManagementId);

    if (latestTs == null) {
      _logger.debug("No updateManagement item exists for $updateManagementId. "
          "Inserting new item with latestTs 0");
      await _updateManagementStore.updateLatestTs(updateManagementId, 0);
      return 0;
    }

    _logger.debug(
        "Retrieved latestTs {$latestTs} for updateManagementId {$updateManagementId}");
    return latestTs;
  }

  Future<void> updateLatestTs(String updateManagementId, int latestTs) async {
    _logger.debug(
        "Updating latestTs to {$latestTs} for updateManagementId $updateManagementId");

    await _updateManagementStore.updateLatestTs(updateManagementId, latestTs);
  }
}
