import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/data/user.dart';
import 'package:planhive/data/user_session.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/storage/caching/file_cache.dart';
import 'package:planhive/storage/common/sqlite.dart';
import 'package:planhive/storage/user_store.dart';

@singleton
class AccountService {
  static const _logger = Logger(AccountService);

  final AccountStore _accountStore;
  final FileCacheStore _fileCacheStore;
  final UserApi _userApi;
  final UserStore _userStore;

  bool _loggingOut = false;

  User _user;

  AccountService(
    this._accountStore,
    this._fileCacheStore,
    this._userApi,
    this._userStore,
  );

  Future<User> get user async {
    _user ??= await _userStore.localUser;

    if (_user == null) {
      await _fetchUser();
    }

    return _user;
  }

  String get userId => _accountStore.userId;

  Future<void> updateUser({
    String username,
    String name,
    String email,
    int lastUpdateTs,
    int lastAvatarUpdateTs,
  }) async {
    _user = await _userStore.updateLocalUser(
      username: username,
      name: name,
      email: email,
      lastUpdateTs: lastUpdateTs,
      lastAvatarUpdateTs: lastAvatarUpdateTs,
    );
  }

  Future<void> setUser(UserSession userSession) async {
    assert(_accountStore.accountState == AccountState.PendingAccountCreation);
    await _accountStore.setUserSession(userSession.user.id, userSession.sessionId);
    await _userStore.addUsers([userSession.user]);
    _user = userSession.user;
  }

  Future<void> restoreUser(String userId, String sessionId) async {
    assert(_accountStore.accountState == AccountState.PendingEmailVerification);
    await _accountStore.setUserSession(userId, sessionId);

    try {
      await _fetchUser();
    } catch (e, s) {
      _logger.error("Could not fetch user", e, s);
    }
  }

  Future<void> _fetchUser() async {
    _user = await _userApi.getUser();
    await _userStore.addUsers([_user]);
  }

  Future<bool> logout([wipeNotificationToken = true]) async {
    if (_loggingOut) {
      return false;
    }

    if (_accountStore.accountState != AccountState.Complete) {
      return false;
    }

    _loggingOut = true;

    try {
      if (wipeNotificationToken) await _userApi.updateNotificationToken(null);

      await Future.wait([
        GetIt.I<Sqlite>().deleteAllData(),
        _fileCacheStore.deleteAllFiles(),
      ]);

      _user = null;

      GetIt.I<NavigationService>().startPage();

      return true;
    } finally {
      _loggingOut = false;
    }
  }
}
