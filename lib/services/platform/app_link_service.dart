import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/common/event.dart';
import 'package:planhive/common/logger.dart';
import 'package:uni_links/uni_links.dart';

class ShortLinkGenerationException implements Exception {
  final String message;

  ShortLinkGenerationException(this.message);

  @override
  String toString() => message;
}

@preResolve
@singleton
class AppLinkService {
  static const _logger = Logger(AppLinkService);

  final Uri initialLink;

  final _onData = EventSource<Uri>();

  Event<Uri> get onData => _onData.event;

  AppLinkService(this.initialLink) {
    _logger.debug("Initial deep link: $initialLink");

    getLinksStream().listen(_onDeepLink);

    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData dynamicLink) async {
        final Uri deepLink = dynamicLink?.link;

        if (deepLink != null) {
          _onData(deepLink);
          _logger.debug("Dynamic link: $deepLink");
        }
      },
    );
  }

  @factoryMethod
  static Future<AppLinkService> factory() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();

    return AppLinkService(data?.link);
  }

  Future<Uri> generateShortLink({
    @required Uri link,
    String title,
    String description,
  }) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://go.planhive.app/l',
      link: link,
      androidParameters: AndroidParameters(
        packageName: 'app.planhive',
        minimumVersion: 44,
      ),
      iosParameters: IosParameters(
        bundleId: 'app.planhive',
        appStoreId: '1526799886',
        minimumVersion: '1.0.7',
      ),
      socialMetaTagParameters: SocialMetaTagParameters(
        title: title,
        description: description,
        imageUrl: Uri.parse(
          "https://static.wixstatic.com/media/141efc_cf69b1bcca1748d191cd0c7daf9223ba~mv2.png/v1/fill/w_2048,h_1024,al_c/141efc_cf69b1bcca1748d191cd0c7daf9223ba~mv2.png",
        ),
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.unguessable,
      ),
    );

    var dynamicLink = await parameters.buildShortLink();
    return dynamicLink.shortUrl;
  }

  static Uri _getUri(String link) {
    try {
      return link == null ? null : Uri.parse(link);
    } on FormatException {
      return null;
    }
  }

  void _onDeepLink(String link) {
    var uri = _getUri(link);
    if (uri != null) {
      _logger.debug("New link: $link");
      _onData(uri);
    } else {
      _logger.error("Could not parse link: $link");
    }
  }
}
