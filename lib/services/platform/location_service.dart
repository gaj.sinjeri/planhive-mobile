import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:location/location.dart';
import 'package:planhive/data/lat_lng.dart';

@singleton
class LocationService {
  final _location = Location();

  LocationService() {
    _location.changeSettings(accuracy: LocationAccuracy.low);
  }

  Future<LatLng> getCurrentPosition() async {
    try {
      var loc = await _location.getLocation();
      return LatLng(loc.latitude, loc.longitude);
    } on PlatformException catch (e) {
      switch (e.code) {
        case "PERMISSION_DENIED_NEVER_ASK":
        case "PERMISSION_DENIED":
          return null;
        default:
          rethrow;
      }
    }
  }
}
