import 'dart:io';

import 'package:app_settings/app_settings.dart';
import 'package:injectable/injectable.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';

@lazySingleton
class PlatformPermissionService {
  Future<bool> canAccessPhotos(bool requestAccess) async {
    var permissionType = Platform.isAndroid ? Permission.storage : Permission.photos;

    return await _hasPermissionType(permissionType, "Photos", requestAccess);
  }

  Future<bool> _hasPermissionType(
      Permission permissionType, String permissionName, bool requestAccess) async {
    var status = await permissionType.status;

    if (status == PermissionStatus.granted) {
      return true;
    } else if (requestAccess == true) {
      if ((Platform.isAndroid && status == PermissionStatus.permanentlyDenied) ||
          (Platform.isIOS && status == PermissionStatus.denied)) {
        await _showPermissionPreviouslyDeniedMessage(permissionName);
      } else {
        var canAccess = await permissionType.request() == PermissionStatus.granted;
        if (canAccess == true) {
          return true;
        }
      }
    }
    return false;
  }

  Future<void> _showPermissionPreviouslyDeniedMessage(String permissionGroup) async {
    var res = await showAlertDialog<bool>(
      title: "$permissionGroup Access",
      content:
          "PlanHive does not have permission to access your ${permissionGroup.toLowerCase()}. "
          "Make sure you enable it in your device's privacy settings.",
      actions: [
        AlertActionData<bool>(text: "Settings", value: true),
        AlertActionData<bool>(text: "Ok", value: false),
      ],
    );
    if (res == true) {
      await AppSettings.openAppSettings();
    }
  }

  Future<bool> hasLocationPermission(bool requestAccess) async {
    if (requestAccess) {
      return await Permission.location.request() == PermissionStatus.granted;
    } else {
      return (await Permission.location.status) == PermissionStatus.granted ||
          (await Permission.locationAlways.status) == PermissionStatus.granted;
    }
  }

  Future<bool> canAccessStorage(bool requestAccess) async {
    PermissionStatus status;
    if (requestAccess) {
      status = await Permission.storage.request();
    } else {
      status = await Permission.storage.status;
    }
    return status == PermissionStatus.granted;
  }

  Future<bool> hasNotificationsPermission() async {
    return (await Permission.notification.status) == PermissionStatus.granted;
  }
}
