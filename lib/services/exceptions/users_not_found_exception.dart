class UsersNotFoundException implements Exception {
  final List<String> userId;

  const UsersNotFoundException(this.userId);

  @override
  String toString() {
    return "The following users were not found: ${userId.join(", ")}";
  }
}
