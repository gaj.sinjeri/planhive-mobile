import 'package:flutter/foundation.dart';

class VerificationResult {
  final String userId;
  final String sessionId;
  final String mac;
  final int macCreationTs;

  VerificationResult.mac({
    @required this.userId,
    @required this.mac,
    @required this.macCreationTs,
  })  : assert(userId != null),
        assert(mac != null),
        assert(macCreationTs != null),
        sessionId = null;

  VerificationResult.session({
    @required this.userId,
    @required this.sessionId,
  })  : assert(userId != null),
        assert(sessionId != null),
        mac = null,
        macCreationTs = null;
}
