import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/activity/activity_data.dart';
import 'package:planhive/data/node/activity/activity_reaction_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/timeline_data.dart';

@lazySingleton
class TimelineApi {
  final Api _api;

  TimelineApi(this._api);

  Future<Node> addActivity(Node<TimelineData> timeline, ActivityData activity) async {
    assert(timeline != null);
    assert(activity?.time != null);

    var res = await _api
        .jsonRequest(
          Method.post,
          "events/${timeline.parentId}/timelines/${timeline.id}/activities",
          {
            "activityState": enumToAllCaps(
              activity.state,
              ActivityState.values,
            ),
          }..addAll(_commonActivityData(activity)),
        )
        .asMap();

    return NodeApi.decodeNode(res);
  }

  Future<Node> updateActivityState(
    Node<TimelineData> timeline,
    String activityId,
    ActivityState activityState,
  ) async {
    var res = await _api.jsonRequest(
        Method.put,
        "events/${timeline.parentId}/timelines/${timeline.id}/activities/$activityId/state",
        {
          "activityState": enumToAllCaps(
            activityState,
            ActivityState.values,
          ),
        }).asMap();

    return NodeApi.decodeNode(res);
  }

  Future<Node> updateActivityData(
    Node<TimelineData> timeline,
    Node<ActivityData> activity,
    ActivityData data,
  ) async {
    assert(timeline != null);
    assert(activity != null);
    assert(data != null);

    var res = await _api
        .jsonRequest(
          Method.patch,
          "events/${timeline.parentId}/timelines/${timeline.id}/activities/${activity.id}/data",
          _commonActivityData(data),
        )
        .asMap();

    return NodeApi.decodeNode(res);
  }

  /// Pass null for [reaction] to remove the reaction
  Future<Node> updateActivityReaction(
    Node<TimelineData> timeline,
    Node<ActivityData> activity,
    ActivityReactionType reaction,
  ) async {
    assert(activity != null);

    var res = await _api.jsonRequest(
      Method.put,
      "events/${timeline.parentId}/timelines/${timeline.id}/activities/${activity.id}/reaction",
      {
        "reset": reaction == null,
        if (reaction != null)
          "activityReaction": enumToAllCaps(reaction, ActivityReactionType.values),
      },
    ).asMap();

    return NodeApi.decodeNode(res);
  }

  Future<Node> deleteActivity(
      Node<TimelineData> timeline, Node<ActivityData> activity) async {
    var res = await _api
        .emptyRequest(
          Method.delete,
          "events/${timeline.parentId}/timelines/${timeline.id}/activities/${activity.id}",
        )
        .asMap();

    return NodeApi.decodeNode(res);
  }

  Map<String, dynamic> _commonActivityData(ActivityData activity) {
    var location = activity.location;
    var latLng = location?.coordinates;

    return {
      "activityTitle": activity.title,
      "description": activity.description,
      "note": activity.note,
      "startTs": activity.time.startTs,
      "endTs": activity.time.endTs ?? 0,
      "locationTitle": location?.title,
      "address": location?.address,
      "latitude": latLng?.lat?.toString(),
      "longitude": latLng?.lng?.toString(),
    };
  }
}
