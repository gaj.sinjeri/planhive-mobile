import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/lat_lng.dart';
import 'package:uuid/uuid.dart';

class GoogleMapsApiException implements Exception {
  final String status;
  final String message;

  GoogleMapsApiException(this.status, this.message);

  @override
  String toString() {
    return 'GoogleMapsApiException{status: $status, message: $message}';
  }
}

class PlacePrediction {
  final String placeId;
  final String description;

  PlacePrediction({this.placeId, this.description});

  @override
  String toString() {
    return 'PlacePrediction{placeId: $placeId, description: $description}';
  }

  @override
  bool operator ==(Object other) => other is PlacePrediction && placeId == other.placeId;

  @override
  int get hashCode => placeId.hashCode;
}

class PlaceDetails {
  final String placeId;
  final double lat;
  final double lng;
  final String name;
  final String address;

  PlaceDetails({this.placeId, this.lat, this.lng, this.name, this.address});

  @override
  String toString() {
    return 'PlaceDetails{placeId: $placeId, name: $name}';
  }

  @override
  bool operator ==(Object other) => other is PlaceDetails && placeId == other.placeId;

  @override
  int get hashCode => placeId.hashCode;
}

@singleton
class GoogleMapsApi {
  static const _logger = Logger(GoogleMapsApi);

  static const _host = "maps.googleapis.com";
  static const _placeAutocompletePath = "/maps/api/place/autocomplete/json";
  static const _placeDetailsPath = "/maps/api/place/details/json";
  static const _apiKey = "AIzaSyCl9E2HmQ5TC_JFtVFQ8En9Wa2S_P6DT-M";

  final _client = Client();
  final _uuid = Uuid();

  String _placeAutocompleteSessionToken;

  Future<List<PlacePrediction>> placeAutocomplete({
    @required String input,
    LatLng location,
    double radius = 20000,
  }) async {
    assert(radius != null && radius > 0);

    if (_placeAutocompleteSessionToken == null) {
      _placeAutocompleteSessionToken = _uuid.v4();
    }

    try {
      var res = await _callApi(_placeAutocompletePath, {
        "input": input,
        "sessiontoken": _placeAutocompleteSessionToken,
        if (location != null) "location": "${location.lat},${location.lng}",
        if (location != null) "radius": radius.toString(),
      });

      return List.unmodifiable(
        (res["predictions"] as List).map(
          (e) => PlacePrediction(
            placeId: e["place_id"],
            description: e["description"],
          ),
        ),
      );
    } on GoogleMapsApiException catch (e) {
      _logger.error(e.toString(), e);
      return const [];
    }
  }

  Future<PlaceDetails> placeDetails(String placeId) async {
    var token = _placeAutocompleteSessionToken;
    _placeAutocompleteSessionToken = null;

    var res = await _callApi(_placeDetailsPath, {
      if (token != null) "sessiontoken": token,
      "place_id": placeId,
      "fields": "name,place_id,formatted_address,geometry/location",
    });

    var result = res["result"];
    var location = result["geometry"]["location"];

    return PlaceDetails(
      name: result["name"],
      placeId: result["place_id"],
      address: result["formatted_address"],
      lat: location["lat"].toDouble(),
      lng: location["lng"].toDouble(),
    );
  }

  Future<Map<String, dynamic>> _callApi(String path, Map<String, String> data) async {
    var res = await _client.send(
      Request(
        "get",
        Uri.https(
          _host,
          path,
          data
            ..addAll({
              "key": _apiKey,
            }),
        ),
      )..persistentConnection = true,
    );

    var bodyString = await res.stream.bytesToString();

    _logger.trace("$path: $bodyString");

    var body = jsonDecode(bodyString);

    String status = body["status"];

    switch (status) {
      case "OK":
        return body;
      default:
        throw GoogleMapsApiException(status, body["error_message"]);
    }
  }
}
