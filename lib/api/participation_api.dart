import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/participation_data.dart';

@lazySingleton
class ParticipationApi {
  final Api _api;

  ParticipationApi(this._api);

  Future<Node> updateParticipationStatus(
    Node<ParticipationData> participation,
    ParticipantStatus status,
  ) async {
    var res = await _api.jsonRequest(
      Method.put,
      "events/${participation.parentId}/participation/${participation.id}/status",
      {
        "participationStatus": enumToAllCaps(status, ParticipantStatus.values),
      },
    ).asMap();

    return NodeApi.decodeNode(res);
  }
}
