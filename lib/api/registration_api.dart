import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/api_error_code.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/binary_multipart_data.dart';
import 'package:planhive/api/common/multipart/json_multipart_data.dart';
import 'package:planhive/api/data/verification_result.dart';
import 'package:planhive/api/user_api.dart';
import 'package:planhive/data/user_session.dart';
import 'package:planhive/exception/api/code_request_limit_exception.dart';
import 'package:planhive/exception/api/invalid_state_exception.dart';
import 'package:planhive/exception/api/session_expired_exception.dart';
import 'package:planhive/exception/api/unauthorized_exception.dart';
import 'package:planhive/exception/api/username_exception.dart';

@lazySingleton
class RegistrationApi {
  final Api _api;

  RegistrationApi(this._api);

  Future<void> sendVerificationCode(String email) async {
    try {
      await _api
          .emptyRequest(
            Method.post,
            "user-registration/$email/codes",
            authenticated: false,
          )
          .asEmpty();
    } on ApiForbiddenException catch (e) {
      if (e.code == ApiErrorCode.MAX_VERIFICATION_CODES_REACHED) {
        throw CodeRequestLimitException(
          DateTime.now().add(Duration(milliseconds: e.data["timeRemaining"])),
        );
      }
      rethrow;
    }
  }

  Future<VerificationResult> verifyCode(String email, String code) async {
    try {
      var res = await _api
          .emptyRequest(
            Method.get,
            "user-registration/$email/codes/$code",
            authenticated: false,
          )
          .asMap();

      var userId = res["userId"];
      var session = res["sessionId"];

      if (session != null) {
        return VerificationResult.session(
          userId: userId,
          sessionId: session,
        );
      } else {
        return VerificationResult.mac(
          userId: userId,
          mac: res["mac"],
          macCreationTs: res["macCreationTs"],
        );
      }
    } on ApiForbiddenException {
      throw UnauthorizedException("Invalid verification code");
    } on ApiNotFoundException {
      throw SessionExpiredException("Code verification session has expired");
    }
  }

  Future<UserSession> registerUser({
    @required String email,
    @required String fullName,
    @required String userName,
    List<int> avatar,
    @required VerificationResult verificationResult,
  }) async {
    assert(verificationResult.mac != null);
    assert(verificationResult.macCreationTs != null);

    try {
      var res = await _api
          .multipartRequest(
            Method.put,
            "user-registration/$email/user",
            [
              JsonMultipartData(
                "request",
                {
                  "userId": verificationResult.userId,
                  "mac": verificationResult.mac,
                  "macCreationTs": verificationResult.macCreationTs,
                  "name": fullName,
                  "username": userName,
                },
              ),
              if (avatar != null) BinaryMultipartData("avatar", avatar, "image/jpeg"),
            ],
            authenticated: false,
          )
          .asMap();

      var user = res["user"];

      return UserSession(UserApi.makeUser(user), res["sessionId"]);
    } on ApiForbiddenException catch (e) {
      if (e.code == ApiErrorCode.MAC_EXPIRED) {
        throw SessionExpiredException("User registration session has expired");
      } else if (e.code == ApiErrorCode.USERNAME_TAKEN) {
        throw UsernameException("Username taken");
      } else if (e.code == ApiErrorCode.EMAIL_TAKEN) {
        throw InvalidStateException("Email already exists");
      } else if (e.code == ApiErrorCode.USER_ALREADY_EXISTS) {
        throw InvalidStateException("User already exists");
      } else if (e.code == ApiErrorCode.MAC_INVALID) {
        throw InvalidStateException("Invalid mac code");
      }
      rethrow;
    }
  }
}
