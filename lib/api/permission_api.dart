import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/data/permission.dart';

@lazySingleton
class PermissionApi {
  final Api _api;

  PermissionApi(this._api);

  Future<List<Permission>> getPermissions(
    String groupId,
    int lastUpdateTs,
  ) async {
    final res = await _api.jsonRequest(
      Method.get,
      "permissions/$groupId",
      {
        "lastUpdateTs": lastUpdateTs,
      },
    ).asList();

    return makePermissions(res);
  }

  Future<List<Permission>> fetchUpdates(int startTs) async {
    var res = await _api.jsonRequest(Method.get, "permissions/updates", {
      "lastUpdateTs": startTs,
    }).asList();

    return makePermissions(res);
  }

  static Permission makePermission(Map<String, dynamic> permission) {
    return Permission(
      userId: permission["userId"],
      groupId: permission["permissionGroupId"],
      // this assumes there are no more than 8 flags
      flags: base64Decode(permission["permissionFlags"])[0],
      creationTs: permission["creationTs"],
      lastUpdateTs: permission["lastUpdateTs"],
      deleted: permission["deleted"],
    );
  }

  static List<Permission> makePermissions(List permissions) {
    return List.unmodifiable(permissions.map((map) => makePermission(map)));
  }
}
