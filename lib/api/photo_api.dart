import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/binary_multipart_data.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/data/node/node.dart';

@lazySingleton
class PhotoApi {
  final Api _api;

  PhotoApi(this._api);

  Future<List<Node>> addPhoto({
    @required String groupNodeId,
    @required String photosNodeId,
    @required List<int> photo,
  }) async {
    assert(photo.length != 0);

    var endpoint = "events/$groupNodeId/photos/$photosNodeId/files";

    var result = await _api
        .multipartRequest(
          Method.post,
          endpoint,
          [
            if (photo != null) BinaryMultipartData("photo", photo, "image/jpeg"),
          ],
          timeout: const Duration(seconds: 20),
        )
        .asList();

    return List.unmodifiable(NodeApi.decodeNodes(result));
  }

  Future<void> sendPhotoUploadNotification({
    @required String groupNodeId,
    @required String photosNodeId,
  }) async {
    var endpoint = "events/$groupNodeId/photos/$photosNodeId/notification";

    await _api
        .emptyRequest(
          Method.post,
          endpoint,
        )
        .asEmpty();
  }

  Future<Uint8List> getPhoto(
      String groupNodeId, String photosNodeId, String photoId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "events/$groupNodeId/photos/$photosNodeId/files/$photoId/original",
        )
        .asBytes();
  }

  Future<Uint8List> getThumbnail(
      String groupNodeId, String photosNodeId, String photoId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "events/$groupNodeId/photos/$photosNodeId/files/$photoId/thumbnail",
        )
        .asBytes();
  }

  Future<void> deletePhoto(
      String groupNodeId, String photosNodeId, String photoId) async {
    return await _api
        .emptyRequest(
          Method.delete,
          "events/$groupNodeId/photos/$photosNodeId/files/$photoId",
        )
        .asEmpty();
  }
}
