import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/thread_data.dart';

import 'common/multipart/binary_multipart_data.dart';
import 'common/multipart/json_multipart_data.dart';

@lazySingleton
class ThreadApi {
  final Api _api;

  ThreadApi(this._api);

  Future<List<Node>> sendMessage({
    @required NodeType parentNodeType,
    @required Node<ThreadData> thread,
    String textContent,
    List<int> image,
  }) async {
    var result = await _api.multipartRequest(
      Method.post,
      "threads/${thread.id}/messages",
      [
        JsonMultipartData(
          "request",
          {
            "parentNodeType": enumToAllCaps(parentNodeType, NodeType.values),
            "parentNodeId": thread.parentId,
            "textContent": textContent,
          },
        ),
        if (image != null) BinaryMultipartData("image", image, "image/jpeg"),
      ],
    ).asList();

    return List.unmodifiable(NodeApi.decodeNodes(result));
  }

  Future<Uint8List> getImage(String threadNodeId, String messageNodeId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "threads/$threadNodeId/messages/$messageNodeId/image",
        )
        .asBytes();
  }

  Future<Uint8List> getImageThumbnail(String threadNodeId, String messageNodeId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "threads/$threadNodeId/messages/$messageNodeId/image-thumbnail",
        )
        .asBytes();
  }

  Future<void> clearThreadForUser(
    Node<ThreadData> threadNode,
    String userId,
  ) async {
    await _api.jsonRequest(
      Method.patch,
      "threads/${threadNode.id}/users/$userId",
      {
        "parentNodeId": threadNode.parentId,
      },
    ).asList();
  }
}
