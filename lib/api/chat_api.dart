import 'dart:typed_data';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/binary_multipart_data.dart';
import 'package:planhive/api/common/multipart/json_multipart_data.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/data/node/chat/group_chat_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/user.dart';

@lazySingleton
class ChatApi {
  final Api _api;

  ChatApi(this._api);

  /// Returns the direct chat node along with any thread nodes
  Future<List<Node>> getDirectChat(String contactId) async {
    var res = await _api.emptyRequest(Method.get, "direct-chats/$contactId").asList();

    return List.unmodifiable(NodeApi.decodeNodes(res));
  }

  /// Returns the newly created chat node along with a thread node
  Future<List<Node>> createGroupChat(
    Iterable<String> userIds,
    String chatName,
    List<int> coverImage,
  ) async {
    var res = await _api.multipartRequest(Method.post, "chats", [
      JsonMultipartData("request", {
        "userIds": userIds.toList(),
        "chatName": chatName,
      }),
      if (coverImage != null)
        BinaryMultipartData(
          "coverPhoto",
          coverImage,
          "image/jpeg",
        ),
    ]).asList();

    return List.unmodifiable(NodeApi.decodeNodes(res));
  }

  Future<Uint8List> getGroupChatCoverImage(String chatId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "chats/$chatId/cover-photo",
        )
        .asBytes();
  }

  Future<Uint8List> getGroupChatCoverImageThumbnail(String chatId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "chats/$chatId/cover-photo-thumbnail",
        )
        .asBytes();
  }

  Future<Node<GroupChatData>> updateGroupChatData(
    String groupChatId,
    String name,
    int coverTs,
    List<int> coverImage,
  ) async {
    var result = await _api.multipartRequest(Method.patch, 'chats/$groupChatId', [
      JsonMultipartData("request", {
        "name": name,
        "coverTs": coverTs,
      }),
      if (coverImage != null) BinaryMultipartData("coverPhoto", coverImage, "image/jpeg"),
    ]).asMap();

    return NodeApi.decodeNode(result).as<GroupChatData>();
  }

  Future<void> inviteToGroupChat(
      Node<GroupChatData> event, Iterable<User> contacts) async {
    await _api.jsonRequest(
      Method.put,
      "chats/${event.id}/members",
      {
        "userIds": contacts.map((contact) => contact.id).toList(),
      },
    ).asEmpty();
  }

  Future<void> setGroupChatMemberAdminStatus(
      String groupChatId, String memberId, bool setAdmin) async {
    assert(groupChatId != null);
    assert(memberId != null);

    await _api.jsonRequest(
      Method.put,
      "chats/$groupChatId/members/$memberId/admin-state",
      {
        "setAdmin": setAdmin,
      },
    ).asEmpty();
  }

  Future<void> removeFromGroupChat(String groupChatId, String userId) async {
    assert(groupChatId != null);

    await _api
        .emptyRequest(
          Method.delete,
          "chats/$groupChatId/members/$userId",
        )
        .asEmpty();
  }

  Future<void> deleteGroupChat(String groupChatId) async {
    assert(groupChatId != null);

    await _api
        .emptyRequest(
          Method.delete,
          "chats/$groupChatId",
        )
        .asEmpty();
  }
}
