import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/data/sort_order.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/node/node_id.dart';

@lazySingleton
class NodeApi {
  final Api _api;

  NodeApi(this._api);

  Future<List<Node>> getUpdates(NodeType type, int lastUpdateTs) async {
    var res = await _api.jsonRequest(
      Method.get,
      "nodes/${enumToAllCaps(type, NodeType.values)}",
      {
        "lastUpdateTs": lastUpdateTs,
      },
    ).asList();

    return List.unmodifiable(decodeNodes(res));
  }

  Future<List<Node>> getPastUpdates(NodeType type, int beforeTs) async {
    var res = await _api.jsonRequest(
      Method.get,
      "nodes/${enumToAllCaps(type, NodeType.values)}/historical-data",
      {
        "lastUpdateTs": beforeTs,
      },
    ).asList();

    return List.unmodifiable(decodeNodes(res));
  }

  Future<List<Node>> getChildren(
    NodeId nodeId,
    int endTs,
    SortOrder order,
  ) async {
    if (order == SortOrder.ASC) {
      throw UnimplementedError("Children lookup in ascending order isn't supported yet");
    }

    var res = await _api.jsonRequest(
      Method.get,
      nodeId.isRoot
          ? "nodes/${enumToAllCaps(nodeId.type, NodeType.values)}/${nodeId.id}/children"
          : "nodes/${nodeId.parentId}/${enumToAllCaps(nodeId.type, NodeType.values)}/${nodeId.id}/children",
      {
        "toTs": endTs,
      },
    ).asList();

    return List.unmodifiable(decodeNodes(res));
  }

  Future<Node> getNode(NodeId node) async {
    var nodeType = enumToAllCaps(node.type, NodeType.values);

    var res = await _api
        .emptyRequest(
          Method.get,
          node.isRoot
              ? "nodes/$nodeType/${node.id}"
              : "nodes/${node.parentId}/$nodeType/${node.id}",
        )
        .asMap();

    return decodeNode(res);
  }

  static Iterable<Node<T>> decodeNodes<T extends NodeData>(Iterable encodedNodes) {
    return encodedNodes.map((node) => decodeNode<T>(node));
  }

  static Node<T> decodeNode<T extends NodeData>(Map node) {
    return Node<T>(
      nodeId: NodeId(
        id: node["id"],
        parentId: node["parentId"],
        type: allCapsToEnum(node["type"], NodeType.values),
      ),
      permissionGroupId: node["permissionGroupId"],
      creatorId: node["creatorId"],
      creationTs: node["creationTs"],
      lastUpdateTs: node["lastUpdateTs"],
      jsonData: node["data"],
      deleted: node["deleted"] == true,
    );
  }
}
