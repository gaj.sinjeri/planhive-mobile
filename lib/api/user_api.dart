import 'dart:typed_data';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/binary_multipart_data.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/contact.dart';
import 'package:planhive/data/contact_user.dart';
import 'package:planhive/data/user.dart';

import 'common/multipart/json_multipart_data.dart';

@lazySingleton
class UserApi {
  final Api _api;

  UserApi(this._api);

  Future<User> updateUserDetails(
    String username,
    String name,
    int lastAvatarUpdateTs,
    List<int> avatar,
  ) async {
    var res = await _api
        .multipartRequest(
          Method.patch,
          "user",
          [
            JsonMultipartData(
              "request",
              {
                "username": username,
                "name": name,
                "lastAvatarUpdateTs": lastAvatarUpdateTs,
              },
            ),
            if (avatar != null) BinaryMultipartData("avatar", avatar, "image/jpeg"),
          ],
          timeout: Duration(seconds: 20),
        )
        .asMap();

    return makeUser(res);
  }

  Future<User> getUser() async {
    var res = await _api
        .emptyRequest(
          Method.get,
          "user",
        )
        .asMap();

    return makeUser(res);
  }

  Future<List<ContactUser>> getContactUpdates(int lastUpdateTs) async {
    var res = await _api.jsonRequest(
      Method.get,
      "user/contact-updates",
      {
        "lastUpdateTs": lastUpdateTs,
      },
    ).asList();

    return List.unmodifiable(res.map((contactUser) => makeContactUser(contactUser)));
  }

  Future<Iterable<User>> getUserDetails(Iterable<String> userIds) async {
    var res = await _api.jsonRequest(
      Method.get,
      "users",
      {
        "userIds": userIds,
      },
    ).asList();

    return res.map((user) => makeUser(user));
  }

  Future<Uint8List> getAvatar(String userId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "users/$userId/avatar",
        )
        .asBytes();
  }

  Future<Uint8List> getAvatarThumbnail(String userId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "users/$userId/avatar-thumbnail",
        )
        .asBytes();
  }

  Future<Uint8List> getAvatarMicro(String userId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "users/$userId/avatar-micro",
        )
        .asBytes();
  }

  Future<void> updateNotificationToken(String token) async {
    await _api.jsonRequest(
      Method.put,
      "user/notification-token",
      {
        "notificationToken": token,
      },
    ).asEmpty();
  }

  Future<Contact> createContactRequest(String contactId) async {
    var res = await _api
        .emptyRequest(
          Method.post,
          "user/contact-requests/$contactId",
        )
        .asMap();

    return makeContact(res);
  }

  Future<Contact> cancelContactRequest(String contactId) async {
    var res = await _api
        .emptyRequest(
          Method.patch,
          "user/contact-request-cancellations/$contactId",
        )
        .asMap();

    return makeContact(res);
  }

  Future<Contact> respondToContactRequest(String contactId, bool accepted) async {
    var res = await _api.jsonRequest(
      Method.patch,
      "user/contact-requests/$contactId",
      {
        "accepted": accepted,
      },
    ).asMap();

    return makeContact(res);
  }

  Future<Contact> deleteContact(String contactId) async {
    var res = await _api
        .emptyRequest(
          Method.delete,
          "user/contacts/$contactId",
        )
        .asMap();

    return makeContact(res);
  }

  Future<bool> checkUsernameAvailability(String username) async {
    var res = await _api
        .emptyRequest(
          Method.get,
          "usernames/$username/availability",
          authenticated: false,
        )
        .asMap();

    return res["available"];
  }

  Future<List<User>> getUsernameAutocompleteSuggestion(String query) async {
    var res = await _api
        .emptyRequest(
          Method.get,
          "usernames/$query/autocomplete",
          authenticated: false,
        )
        .asList();

    return List.unmodifiable(res.map((user) => makeUser(user)));
  }

  Future<void> setNotificationCounter(int counter) async {
    await _api.jsonRequest(Method.put, "user/notification-count", {
      "notificationCount": counter,
    }).asEmpty();
  }

  static Contact makeContact(Map map) {
    return Contact(
      contactState: allCapsToEnum(map["contactState"], ContactState.values),
      contactId: map["contactId"],
      lastUpdateTs: map["lastUpdateTs"],
    );
  }

  static User makeUser(Map map) {
    return User(
      id: map["id"],
      username: map["username"],
      name: map["name"],
      email: map["email"],
      lastUpdateTs: map["lastUpdateTs"],
      lastAvatarUpdateTs: map["lastAvatarUpdateTs"],
    );
  }

  static ContactUser makeContactUser(Map map) {
    return ContactUser(
      contact: makeContact(map["contact"]),
      user: makeUser(map["userView"]),
    );
  }
}
