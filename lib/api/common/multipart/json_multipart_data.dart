import 'dart:convert';

import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:planhive/api/common/multipart/multipart_data.dart';

class JsonMultipartData implements MultipartData {
  final String field;
  final Map data;

  const JsonMultipartData(this.field, this.data);

  @override
  MultipartFile getFile() {
    return MultipartFile.fromString(
      field,
      jsonEncode(data),
      contentType: MediaType.parse("application/json"),
    );
  }
}
