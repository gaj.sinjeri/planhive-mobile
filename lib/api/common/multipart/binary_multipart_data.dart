import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:planhive/api/common/multipart/multipart_data.dart';

class BinaryMultipartData implements MultipartData {
  final String field;
  final String contentType;
  final List<int> data;

  const BinaryMultipartData(this.field, this.data, this.contentType);

  @override
  MultipartFile getFile() {
    return MultipartFile.fromBytes(
      field,
      data,
      filename: field,
      contentType: MediaType.parse(contentType),
    );
  }
}
