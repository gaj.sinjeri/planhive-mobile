import 'package:http/http.dart';

abstract class MultipartData {
  MultipartFile getFile();
}
