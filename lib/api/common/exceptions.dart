import 'package:planhive/api/common/api_error_code.dart';

class ApiException implements Exception {
  final int statusCode;
  final String path;
  final ApiErrorCode code;
  final dynamic data;

  ApiException(this.statusCode, this.path, this.code, this.data);

  @override
  String toString() {
    return "Resource '$path' returned error $statusCode";
  }
}

class ApiNotModifiedException extends ApiException {
  ApiNotModifiedException(String path, ApiErrorCode code, data)
      : super(304, path, code, data);
}

class ApiUnauthorizedException extends ApiException {
  ApiUnauthorizedException(String path, ApiErrorCode code, data)
      : super(401, path, code, data);
}

class ApiBadRequestException extends ApiException {
  ApiBadRequestException(String path, ApiErrorCode code, data)
      : super(400, path, code, data);
}

class ApiForbiddenException extends ApiException {
  ApiForbiddenException(String path, ApiErrorCode code, data)
      : super(403, path, code, data);
}

class ApiNotFoundException extends ApiException {
  ApiNotFoundException(String path, ApiErrorCode code, data)
      : super(404, path, code, data);
}

class ApiInternalServerErrorException extends ApiException {
  ApiInternalServerErrorException(String path, ApiErrorCode code, data)
      : super(500, path, code, data);
}
