import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import "package:http/http.dart" hide Response;
import 'package:planhive/api/common/api_error_code.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/common/logger.dart';

class ResponseParser {
  static const _logger = Logger(ResponseParser);

  final Duration timeout;
  final BaseRequest _request;

  ResponseParser(this._request, this.timeout);

  Future<dynamic> _asJson() async {
    var res = await _getResponse();
    var jsonString = await res.stream.bytesToString();

    _logResponse(res, jsonString);

    return jsonDecode(jsonString)["result"];
  }

  Future<Map<String, dynamic>> asMap() async {
    return (await _asJson()) as Map<String, dynamic>;
  }

  Future<List> asList() async {
    return (await _asJson()) as List;
  }

  Future<Uint8List> asBytes() async {
    var res = await _getResponse();
    var bytes = await res.stream.toBytes();

    _logResponse(res, "byte[${bytes.length}]");

    return bytes;
  }

  Future<void> asEmpty() async {
    var res = await _getResponse();

    _logResponse(res, "");
  }

  Future<StreamedResponse> _getResponse() async {
    dynamic res;

    try {
      res = await _request.send().timeout(timeout);
    } on TimeoutException {
      _logger.error(
          "API request to ${_request.url} timed out after ${timeout.inSeconds} seconds");
      rethrow;
    }

    if (res.statusCode == 200) {
      return res;
    }

    ApiErrorCode code;
    dynamic data;

    try {
      var errorResponse = await res.stream.bytesToString();
      _logResponse(res, errorResponse);
      var error = jsonDecode(errorResponse)["error"];
      if (error is Map<String, dynamic>) {
        code = _getApiErrorCodeSafe(error["code"]);
        data = error["data"];
      }
    } on FormatException catch (e) {
      _logger.error("Could not decode error response", e);
    }

    switch (res.statusCode) {
      case 304:
        throw ApiNotModifiedException(_request.url.path, code, data);
      case 400:
        throw ApiBadRequestException(_request.url.path, code, data);
      case 401:
        throw ApiUnauthorizedException(_request.url.path, code, data);
      case 403:
        throw ApiForbiddenException(_request.url.path, code, data);
      case 404:
        throw ApiNotFoundException(_request.url.path, code, data);
      case 500:
        throw ApiInternalServerErrorException(_request.url.path, code, data);
      default:
        throw ApiException(res.statusCode, _request.url.path, code, data);
    }
  }

  ApiErrorCode _getApiErrorCodeSafe(dynamic code) {
    if (code is int && code >= 0 && code < ApiErrorCode.values.length) {
      return ApiErrorCode.values[code];
    }
    _logger.warn("Returned error code $code cannot be mapped to an $ApiErrorCode");
    return null;
  }

  static void _logResponse(StreamedResponse res, String responseString) {
    var req = res.request;

    _logger.trace([
      "${req.method} ${req.url.path}",
      if (req is Request) "- Request: ${req.body}",
      "- Status: ${res.statusCode} ${res.reasonPhrase}",
      "- Response: $responseString",
    ].join("\n"));
  }
}
