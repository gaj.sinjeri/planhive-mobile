import 'dart:convert';
import 'dart:io';

import "package:http/http.dart";
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/multipart_data.dart';
import 'package:planhive/api/common/response_parser.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/storage/account_store.dart';

@lazySingleton
class Api {
  static const defaultTimeout = Duration(seconds: 10);

  final AccountStore _accountStore;

  Api(this._accountStore);

  static const _host = "api.planhive.app";

  Uri _getUriFromPath(String path) => Uri.https(_host, path);

  void _addAuthenticationHeaders(BaseRequest req) {
    req
      ..headers["User-Id"] = _accountStore.userId
      ..headers["Session-Id"] = _accountStore.sessionId;
  }

  ResponseParser jsonRequest(
    Method method,
    String endpoint,
    Map body, {
    bool authenticated = true,
    Duration timeout = defaultTimeout,
  }) {
    if (authenticated == true) endpoint = "authenticated/$endpoint";

    var req = Request(enumToAllCaps(method, Method.values), _getUriFromPath(endpoint))
      ..headers[HttpHeaders.contentTypeHeader] = "application/json"
      ..body = jsonEncode(body);

    if (authenticated == true) {
      _addAuthenticationHeaders(req);
    }

    return ResponseParser(req, timeout);
  }

  ResponseParser emptyRequest(
    Method method,
    String endpoint, {
    bool authenticated = true,
    Duration timeout = defaultTimeout,
  }) {
    if (authenticated == true) endpoint = "authenticated/$endpoint";

    var req = Request(enumToAllCaps(method, Method.values), _getUriFromPath(endpoint));

    if (authenticated == true) {
      _addAuthenticationHeaders(req);
    }

    return ResponseParser(req, timeout);
  }

  ResponseParser multipartRequest(
    Method method,
    String endpoint,
    List<MultipartData> data, {
    bool authenticated = true,
    Duration timeout = defaultTimeout,
  }) {
    if (authenticated == true) endpoint = "authenticated/$endpoint";

    var req = MultipartRequest(
      enumToAllCaps(method, Method.values),
      _getUriFromPath(endpoint),
    )..files.addAll(data.map((data) => data.getFile()));

    if (authenticated == true) {
      _addAuthenticationHeaders(req);
    }

    return ResponseParser(req, timeout);
  }
}
