import 'dart:typed_data';

import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/api.dart';
import 'package:planhive/api/common/method.dart';
import 'package:planhive/api/common/multipart/binary_multipart_data.dart';
import 'package:planhive/api/common/multipart/json_multipart_data.dart';
import 'package:planhive/api/node_api.dart';
import 'package:planhive/common/enums.dart';
import 'package:planhive/data/node/activity/location.dart';
import 'package:planhive/data/node/activity/time_range.dart';
import 'package:planhive/data/node/event_data.dart';
import 'package:planhive/data/node/node.dart';
import 'package:planhive/data/user.dart';

@lazySingleton
class EventApi {
  final Api _api;

  EventApi(this._api);

  Future<Iterable<Node>> createEvent(
    String name,
    String description,
    List<int> coverImage,
    TimeRange time,
    Location location,
  ) async {
    var res = await _api.multipartRequest(Method.post, 'events', [
      JsonMultipartData("request", {
        "name": name,
        "description": description,
        "startTs": time?.startTs ?? 0,
        "endTs": time?.endTs ?? 0,
        "locationTitle": location?.title,
        "address": location?.address,
        "latitude": location?.coordinates?.lat,
        "longitude": location?.coordinates?.lng,
      }),
      if (coverImage != null) BinaryMultipartData("coverPhoto", coverImage, "image/jpeg"),
    ]).asList();

    return NodeApi.decodeNodes(res);
  }

  Future<Node> updateEventState(String eventId, EventState eventState) async {
    assert(eventId != null);
    assert(eventState != null);

    var res = await _api.jsonRequest(
      Method.put,
      "events/$eventId/state",
      {
        "eventState": enumToAllCaps(
          eventState,
          EventState.values,
        ),
      },
    ).asMap();

    return NodeApi.decodeNode(res);
  }

  Future<Node<EventData>> updateEventData(
    String eventId,
    String name,
    String description,
    int coverTs,
    bool inviteLinkEnabled,
    List<int> coverImage,
  ) async {
    assert(eventId?.isNotEmpty == true);
    assert(name?.isNotEmpty == true);
    assert(coverTs != null);
    assert(inviteLinkEnabled != null);

    var res = await _api.multipartRequest(Method.patch, 'events/$eventId', [
      JsonMultipartData("request", {
        "name": name,
        "description": description,
        "coverTs": coverTs,
        "invitationLinkEnabled": inviteLinkEnabled,
      }),
      if (coverImage != null) BinaryMultipartData("coverImage", coverImage, "image/jpeg"),
    ]).asMap();

    return NodeApi.decodeNode(res).as<EventData>();
  }

  Future<Uint8List> getEventCoverImage(String eventId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "events/$eventId/cover-photo",
        )
        .asBytes();
  }

  Future<Uint8List> getEventCoverImageThumbnail(String eventId) async {
    return await _api
        .emptyRequest(
          Method.get,
          "events/$eventId/cover-photo-thumbnail",
        )
        .asBytes();
  }

  Future<Node<EventData>> acceptEventInvite(String invitationCode) async {
    assert(invitationCode != null && invitationCode.isNotEmpty);

    return NodeApi.decodeNode(
      await _api
          .emptyRequest(Method.put, "events/encrypted/$invitationCode/members")
          .asMap(),
    );
  }

  Future<void> invite(Node<EventData> event, Iterable<User> contacts) async {
    await _api.jsonRequest(
      Method.put,
      "events/${event.id}/members",
      {
        "contactIds": contacts.map((contact) => contact.id).toList(),
      },
    ).asEmpty();
  }

  Future<void> setMemberAdminStatus(
      String eventId, String memberId, bool setAdmin) async {
    assert(eventId != null);
    assert(memberId != null);

    await _api.jsonRequest(
      Method.put,
      "events/$eventId/members/$memberId/admin-state",
      {
        "setAdmin": setAdmin,
      },
    ).asEmpty();
  }

  Future<void> removeFromEvent(String eventId, String userId) async {
    assert(eventId != null);

    await _api
        .emptyRequest(
          Method.delete,
          "events/$eventId/users/$userId",
        )
        .asEmpty();
  }

  Future<void> deleteEvent(String eventId) async {
    assert(eventId != null);

    await _api
        .emptyRequest(
          Method.delete,
          "events/$eventId",
        )
        .asEmpty();
  }
}
