import 'dart:async';
import 'dart:developer';
import 'dart:isolate';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:planhive/api/common/exceptions.dart';
import 'package:planhive/base_theme.dart';
import 'package:planhive/common/logger.dart';
import 'package:planhive/data/account_state.dart';
import 'package:planhive/main.config.dart';
import 'package:planhive/services/account_service.dart';
import 'package:planhive/services/analytics/analytics_service.dart';
import 'package:planhive/services/navigation_service.dart';
import 'package:planhive/storage/account_store.dart';
import 'package:planhive/views/cross_platform/alert_dialog.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();
  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(!kDebugMode);

  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  Isolate.current.addErrorListener(RawReceivePort((pair) async {
    final List<dynamic> errorAndStacktrace = pair;
    await FirebaseCrashlytics.instance.recordError(
      errorAndStacktrace.first,
      errorAndStacktrace.last,
    );
  }).sendPort);

  runZonedGuarded(() => runApp(PlanHive()), (e, s) async {
    FirebaseCrashlytics.instance.recordError(e, s);

    if (e is ApiUnauthorizedException) {
      if (await GetIt.I<AccountService>().logout(false)) {
        await showAlertDialog(
          title: "Session Expired",
          content: "You have been logged out.",
          barrierDismissible: false,
          actions: [
            AlertActionData(text: "OK"),
          ],
        );
      }
    } else {
      const Logger("Main").error("Uncaught exception", e, s);
    }
  });
}

class PlanHive extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PlanHiveState();
  }
}

class _PlanHiveState extends State<PlanHive> {
  var _navigatorObservers = const <NavigatorObserver>[];
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();

    _init();
  }

  void _init() async {
    await _initializeServiceLocator();

    if (kDebugMode && GetIt.I<AccountStore>().accountState == AccountState.Complete) {
      log("UserId: " + GetIt.I<AccountStore>().userId);
      log("SessionId: " + GetIt.I<AccountStore>().sessionId);
    }

    var navigationService = GetIt.I<NavigationService>();

    navigationService.navigatorKey = _navigatorKey;

    navigationService.startPage();

    setState(() {
      _navigatorObservers = List<NavigatorObserver>.unmodifiable([
        navigationService.routeObserver,
        GetIt.I<AnalyticsService>().navigatorObserver,
      ]);
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "PlanHive",
        navigatorObservers: _navigatorObservers,
        navigatorKey: _navigatorKey,
        theme: baseTheme,
        home: Container(
          alignment: Alignment.topLeft,
          color: Theme.of(context).scaffoldBackgroundColor,
          child: SafeArea(
            child: LinearProgressIndicator(),
          ),
        ),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'AU'),
          const Locale('en', 'BZ'),
          const Locale('en', 'CA'),
          const Locale('en', 'IE'),
          const Locale('en', 'JM'),
          const Locale('en', 'NZ'),
          const Locale('en', 'ZA'),
          const Locale('en', 'TT'),
          const Locale('en', 'GB'),
          const Locale('en', 'US'),
        ],
      ),
    );
  }
}

@injectableInit
Future<void> _initializeServiceLocator() async {
  await $initGetIt(GetIt.instance);
}
