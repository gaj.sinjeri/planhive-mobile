package app.planhive

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(
                    makeChannel(
                            "event_updates",
                            "Event updates",
                            "Notifications for event specific updates",
                            NotificationManager.IMPORTANCE_DEFAULT
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "albums",
                            "Albums",
                            "Notifications for new photos added to an album",
                            NotificationManager.IMPORTANCE_DEFAULT
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "invites",
                            "Invites",
                            "Notifications for invites to events or chats",
                            NotificationManager.IMPORTANCE_HIGH
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "contacts",
                            "Contacts",
                            "Notifications for contact requests",
                            NotificationManager.IMPORTANCE_HIGH
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "direct_chats",
                            "Direct chats",
                            "Notifications for private conversations",
                            NotificationManager.IMPORTANCE_HIGH
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "group_chats",
                            "Group chats",
                            "Notifications for group chats",
                            NotificationManager.IMPORTANCE_HIGH
                    )
            )
            notificationManager.createNotificationChannel(
                    makeChannel(
                            "event_chats",
                            "Event chats",
                            "Notifications for event chats",
                            NotificationManager.IMPORTANCE_HIGH
                    )
            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun makeChannel(id: String, name: String, description: String, importance: Int): NotificationChannel {
        val mChannel = NotificationChannel(id, name, importance)
        mChannel.description = description
        mChannel.setShowBadge(true)
        return mChannel
    }
}
